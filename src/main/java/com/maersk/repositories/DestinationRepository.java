package com.maersk.repositories;

import com.maersk.model.ConfigDestination;
import com.maersk.model.Location;

import java.util.Collection;
import java.util.List;

public interface DestinationRepository {

    void add(List<ConfigDestination> destinations);

    Collection<ConfigDestination> getDestination();

    void remove(List<Integer> destinationIds);
}
