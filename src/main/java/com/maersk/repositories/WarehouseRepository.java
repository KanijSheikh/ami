package com.maersk.repositories;

import com.maersk.model.Warehouse;

import java.util.Collection;
import java.util.List;

public interface WarehouseRepository {

    void add(List<Warehouse> warehouses);

    Collection<Warehouse> getWarehouses();

    void remove(List<Integer> warehouseIds);

    Warehouse getWareHouse(Integer id);
}
