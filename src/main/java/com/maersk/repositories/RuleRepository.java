package com.maersk.repositories;

import com.maersk.model.MatchingCriteria;
import com.maersk.model.Rule;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/3/2017.
 */
public interface RuleRepository  {

    Integer add(Rule rule);

    void copy(Rule rule);

    MatchingCriteria getMatchingCriteria(Rule rule);

    Rule getRule(Integer ruleId);

    Collection<Rule> getAllRules();

    Collection<Rule> getRulesBy(String criteria);

    Collection<Rule> getRulesBy(Integer ruleId);

    void remove(Integer ruleId);

    List getOnlyRules();

    Integer getMaxTreeOrder();

    void decreaseTreeOrderFrom(Integer treeOrder);

    void changeTreeOrder(Integer oldOrder, Integer newOrder);

    void updateTreeOrder(Integer ruleId, Integer newOrder);
}
