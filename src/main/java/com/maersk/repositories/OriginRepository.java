package com.maersk.repositories;

import com.maersk.model.ConfigDestination;
import com.maersk.model.ConfigOrigin;

import java.util.Collection;
import java.util.List;

public interface OriginRepository {

    void add(List<ConfigOrigin> origins);

    Collection<ConfigOrigin> getOrigin();

    void remove(List<Integer> originIds);

    List<String> getPlaceOfReceiptByOrigin(String origin);
}
