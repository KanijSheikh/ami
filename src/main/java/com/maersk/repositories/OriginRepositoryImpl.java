package com.maersk.repositories;

import com.maersk.model.ConfigOrigin;
import com.maersk.model.PurchaseOrderOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;

@Repository
public class OriginRepositoryImpl implements OriginRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<ConfigOrigin> origins) {
        for (ConfigOrigin origin : origins) {
            entityManager.persist(origin);
        }
    }

    @Override
    public Collection<ConfigOrigin> getOrigin() {
        String query = "FROM ConfigOrigin AS o";
        return entityManager.createQuery(query, ConfigOrigin.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> originIds) {
        for (Integer originId : originIds) {
            ConfigOrigin origin = entityManager.find(ConfigOrigin.class, originId);
            if (origin == null) {
                throw new EntityNotFoundException();
            }
            entityManager.remove(origin);
        }

    }

    @Override
    public List<String> getPlaceOfReceiptByOrigin(String origin) {
        String query = "select place_of_receipt from config_origin where origin_name in ( "+origin+" );";
        Query q = entityManager.createNativeQuery(query);
        return q.getResultList();
    }
}

