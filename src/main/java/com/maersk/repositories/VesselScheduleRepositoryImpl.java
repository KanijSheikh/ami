package com.maersk.repositories;

import com.maersk.model.VesselSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;



@Repository
public class VesselScheduleRepositoryImpl implements VesselScheduleRepository {

    @Autowired
    EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<VesselSchedule> vesselSchedules) {
        for (VesselSchedule vesselSchedule : vesselSchedules) {
            entityManager.persist(vesselSchedule);
        }
    }

    @Override
    public Collection<VesselSchedule> getVesselSchedule() {
        String query = "FROM VesselSchedule AS vs";
        return entityManager.createQuery(query, VesselSchedule.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> vesselIds) {
        for (Integer vesselId : vesselIds) {
            VesselSchedule vesselSchedule = entityManager.find(VesselSchedule.class, vesselId);
            if (vesselSchedule == null) {
                throw new EntityNotFoundException();
            }
            entityManager.remove(vesselSchedule);
        }

    }
}
