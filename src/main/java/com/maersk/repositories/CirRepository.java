package com.maersk.repositories;

import com.maersk.model.PurchaseOrderOp;

import java.util.Collection;

public interface CirRepository {

    void cirUpload(Collection<PurchaseOrderOp> purchaseOrderOps);
}
