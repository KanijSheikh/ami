package com.maersk.repositories;

import com.maersk.model.ConfigDestination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Repository
public class DestinationRepositoryImpl implements DestinationRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<ConfigDestination> destinations) {
        for (ConfigDestination destination : destinations) {
            entityManager.persist(destination);
        }
    }

    @Override
    public Collection<ConfigDestination> getDestination() {
        String query = "FROM ConfigDestination AS r";
        return entityManager.createQuery(query, ConfigDestination.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> destinationIds) {
        for (Integer destinationId : destinationIds) {
            ConfigDestination destination = entityManager.find(ConfigDestination.class, destinationId);
            if (destination == null) {
                throw new EntityNotFoundException();
            }
            entityManager.remove(destination);
        }

    }
}
