package com.maersk.repositories;


import com.maersk.enums.AppDataType;
import com.maersk.model.ApplicationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository("appDataRepository")
public class AppDataRepositoryImpl implements AppDataRepository{

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<ApplicationData> applicationData) {

        for (ApplicationData  appData:  applicationData) {
            entityManager.persist(appData);
        }
    }

    @Override
    public List<ApplicationData> getAppDataByType(AppDataType appDataType) {
        String query = "FROM ApplicationData AS ad where ad.type=:type";
        return entityManager.createQuery(query, ApplicationData.class).setParameter("type", appDataType).getResultList();
    }

    @Transactional
    @Override
    public void deleteAppDataByType(AppDataType appDataType) {
        String query = "DELETE FROM ApplicationData ad where ad.type=:type";
        entityManager.createQuery(query).setParameter("type", appDataType).executeUpdate();
    }
}
