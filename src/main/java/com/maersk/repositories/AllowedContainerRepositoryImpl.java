package com.maersk.repositories;

import com.maersk.model.AllowedContainer;
import com.maersk.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository
public class AllowedContainerRepositoryImpl implements AllowedContainerRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<AllowedContainer> allowedContainers) {
        for (AllowedContainer container : allowedContainers) {
            entityManager.persist(container);
        }
    }

    @Override
    public Collection<AllowedContainer> getAllowedContainers() {
        String query = "FROM AllowedContainer AS w";
        return entityManager.createQuery(query, AllowedContainer.class).getResultList();
    }


    @Transactional
    @Override
    public void remove(List<Integer> containerIds) {
        for (Integer id : containerIds) {
            AllowedContainer container = entityManager.find(AllowedContainer.class, id);
            if (container != null) {
                entityManager.remove(container);
            }
        }
    }


    @Override
    public AllowedContainer geAllowedContainer(Integer id) {
        return entityManager.find(AllowedContainer.class, id);
    }
}
