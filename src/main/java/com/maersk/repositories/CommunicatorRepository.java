package com.maersk.repositories;

import com.maersk.model.Communicator;
import java.util.Collection;

public interface CommunicatorRepository {

    void add(Communicator communicator);

    Collection<Communicator> getRulesBy(String loadplanid);
    Collection<Communicator> getAllRules();
}
