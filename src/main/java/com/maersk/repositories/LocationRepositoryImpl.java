package com.maersk.repositories;

import com.maersk.model.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Repository
public class LocationRepositoryImpl implements LocationRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<Location> locations) {
        for (Location location : locations) {
            entityManager.persist(location);
        }
    }

    @Override
    public Collection<Location> getLocation() {
        String query = "FROM Location AS r";
        return entityManager.createQuery(query, Location.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> locationIds) {
        for (Integer locationId : locationIds) {
            Location location = entityManager.find(Location.class, locationId);
            if (location == null) {
                throw new EntityNotFoundException();
            }
            entityManager.remove(location);
        }

    }
}
