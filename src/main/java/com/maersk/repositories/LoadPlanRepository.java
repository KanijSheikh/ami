package com.maersk.repositories;

import com.maersk.domain.ContainerDto;
import com.maersk.model.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.List;

public interface LoadPlanRepository {

    String saveLoadPlan(LoadPlan loadPlan, String containerPoMapping, String origin, String destination, Boolean confirm, Integer loadPlanId,List poList);

    LoadPlan getLoadPlan(Integer id);

    void saveSimulateData(SimulateLoadPlan simulateLoadPlan);

    SimulateLoadPlan getSimulateData();

    void removeSimulateLoadPlan(Integer id);

    void AddContainer(List<Container> containerList, Integer loadPlanId);

    void deleteContainer(Collection<Container> containers);

    LoadPlan getLoadPlanById(Integer id);
}
