package com.maersk.repositories;

import com.maersk.model.Communicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collection;

@Repository
public class CommunicatorRepositoryImpl implements CommunicatorRepository{

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(Communicator communicator) {
        entityManager.persist(communicator);
    }

    @Override
    public Collection<Communicator> getAllRules() {
        String query = "FROM Communicator AS r";
        return entityManager.createQuery(query, Communicator.class).getResultList();
    }

    @Override
    public Collection<Communicator> getRulesBy(String loadplanid) {
        String query = "FROM Communicator where loadplan_id =:loadplanid";
        return entityManager.createQuery(query, Communicator.class).setParameter("loadplanid", loadplanid).getResultList();
    }
}
