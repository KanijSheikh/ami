package com.maersk.repositories;

import com.maersk.enums.AppDataType;
import com.maersk.model.ApplicationData;

import java.util.List;

public interface AppDataRepository {

    void add(List<ApplicationData> applicationData);

    List<ApplicationData> getAppDataByType(AppDataType appDataType);

    void deleteAppDataByType(AppDataType appDataType);
}
