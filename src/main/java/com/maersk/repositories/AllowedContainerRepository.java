package com.maersk.repositories;

import com.maersk.model.AllowedContainer;
import com.maersk.model.Warehouse;

import java.util.Collection;
import java.util.List;

public interface AllowedContainerRepository {

    void add(List<AllowedContainer> allowedContainers);

    Collection<AllowedContainer> getAllowedContainers();

    void remove(List<Integer> containerIds);

    AllowedContainer geAllowedContainer(Integer id);

}
