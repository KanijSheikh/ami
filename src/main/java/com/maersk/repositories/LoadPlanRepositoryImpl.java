package com.maersk.repositories;

import com.maersk.common.Constants;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.model.SimulateLoadPlan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import java.util.*;

import static java.util.Objects.nonNull;


@Repository
public class LoadPlanRepositoryImpl implements LoadPlanRepository, Constants {

    @Autowired
    private EntityManager entityManager;

    private static final Logger LOG = LoggerFactory.getLogger(LoadPlanRepository.class);

    private void updateRule(String poNumber, String fieldName, String fieldValue) {
        String action = new String("");
        String poStatus = "";
        if ((fieldName.equals("shipmentStatus") || fieldName.equals("status"))) {
            if (fieldName.equals("status")) {
                fieldName = "shipmentStatus";
            }
            if (fieldValue.equalsIgnoreCase("release")) {
                poStatus = "optimized";
            } else if (fieldValue.equalsIgnoreCase("on hold")) {
                poStatus = "not planned";
            } else if (fieldValue.equalsIgnoreCase("exception")) {
                poStatus = "exception";
            } else {
                poStatus = fieldValue;
            }
            updatePurchaseOrderField(action, poNumber, fieldName, fieldValue, poStatus);
        }
        updatePurchaseOrderField(action, poNumber, fieldName, fieldValue);
    }

    private void updatePurchaseOrderField(String action, String poNumber, String fieldName, String fieldValue) {
        String query = "update PurchaseOrderOp set " + fieldName + " = :fieldValue where poNumber= :poNumber";
        Query q = entityManager.createQuery(query);
        q.setParameter("fieldValue", fieldValue);
        q.setParameter("poNumber", poNumber);
        q.executeUpdate();
    }

    private void updatePurchaseOrderField(String action, String poNumber, String fieldName, String fieldValue, String poStatus) {
        String query = "update PurchaseOrderOp set " + fieldName + " = :fieldValue,poStatus =:poStatus where poNumber= :poNumber";
        Query q = entityManager.createQuery(query);
        q.setParameter("fieldValue", fieldValue);
        q.setParameter("poNumber", poNumber);
        q.setParameter("poStatus", poStatus);
        q.executeUpdate();
    }

    public void updatePurchaseOrderOnHold(PurchaseOrderOp purchaseOrderOp) {
        String query = "update po_orders_op set shipment_status='on hold',po_status='not planned',container_id=NULL,modified_by= '" + purchaseOrderOp.getModifiedBy() +
                "' ,modified_date= '" + dt.format(purchaseOrderOp.getModifiedDate()).
                toString() + "' where po_id= " + purchaseOrderOp.getId() + ";";
        Query q = entityManager.createNativeQuery(query);
        q.executeUpdate();
    }


    public void updatePurchaseOrderRelease(PurchaseOrderOp purchaseOrderOp) {
        String query = "update PurchaseOrderOp set shipmentStatus=:shipmentStatus,poStatus=:poStatus,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("shipmentStatus", "release");
        q.setParameter("poStatus", "optimized");
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
    }

    public void updateOriginService(PurchaseOrderOp purchaseOrderOp) {
//        LoadPlan loadPlan1 = entityManager.find(LoadPlan.class, loadPlan.getId());
//        loadPlan1.setServiceType(loadPlan.getServiceType());
//        loadPlan1.setModifiedDate(loadPlan.getModifiedDate());
//        loadPlan1.setModifiedBy(loadPlan.getModifiedBy());
        String query = "update PurchaseOrderOp set originServiceType=:serviceType,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("serviceType", purchaseOrderOp.getUpdateAction());
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
        // entityManager.merge(purchaseOrderOp);
    }

    public void updateModeOfTransport(PurchaseOrderOp purchaseOrderOp) {
        String query = "update PurchaseOrderOp set modeOfTransport=:modeOfTransport,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("modeOfTransport", purchaseOrderOp.getUpdateAction());
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
        // entityManager.merge(purchaseOrderOp);
    }


    public void updateDestinationCity(PurchaseOrderOp purchaseOrderOp) {
        String query = "update PurchaseOrderOp set podCity=:podCity,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("podCity", purchaseOrderOp.getUpdateAction());
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
        //entityManager.merge(purchaseOrderOp);
    }


    public void updateDestinationCountry(PurchaseOrderOp purchaseOrderOp) {
        String query = "update PurchaseOrderOp set podCountry=:podCountry,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("podCountry", purchaseOrderOp.getUpdateAction());
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
        // entityManager.merge(purchaseOrderOp);
    }


    public void updateDestinationRegion(PurchaseOrderOp purchaseOrderOp) {
        String query = "update PurchaseOrderOp set podRegion=:podRegion,modifiedBy= :modifiedBy,modifiedDate= :modifiedDate " +
                "where id= :id";
        Query q = entityManager.createQuery(query);
        q.setParameter("podRegion", purchaseOrderOp.getUpdateAction());
        q.setParameter("modifiedBy", purchaseOrderOp.getModifiedBy());
        q.setParameter("modifiedDate", purchaseOrderOp.getModifiedDate());
        q.setParameter("id", purchaseOrderOp.getId());
        q.executeUpdate();
        // entityManager.merge(purchaseOrderOp);
    }

    public void updatePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps) {
        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {
            if (nonNull(purchaseOrderOp) && nonNull(purchaseOrderOp.getId()) && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction().equals("Hold cargo")) {
                updatePurchaseOrderOnHold(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp) && nonNull(purchaseOrderOp.getId()) && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction().equals("Release cargo")) {
                updatePurchaseOrderRelease(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp.getUpdateActionValue()) && purchaseOrderOp.getUpdateActionValue().equals("Change origin Service") && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction() != "") {
                updateOriginService(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp.getUpdateActionValue()) && purchaseOrderOp.getUpdateActionValue().equals("Change mode of transportation") && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction() != "") {
                updateModeOfTransport(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp.getUpdateActionValue()) && purchaseOrderOp.getUpdateActionValue().equals("Change Destination") && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction() != "" && purchaseOrderOp.getDestinationSelect().equalsIgnoreCase("city")) {
                updateDestinationCity(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp.getUpdateActionValue()) && purchaseOrderOp.getUpdateActionValue().equals("Change Destination") && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction() != "" && purchaseOrderOp.getDestinationSelect().equalsIgnoreCase("country")) {
                updateDestinationCountry(purchaseOrderOp);
            }
            if (nonNull(purchaseOrderOp.getUpdateActionValue()) && purchaseOrderOp.getUpdateActionValue().equals("Change Destination") && nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction() != "" && purchaseOrderOp.getDestinationSelect().equalsIgnoreCase("region")) {
                updateDestinationRegion(purchaseOrderOp);
            }
        }

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String saveLoadPlan(LoadPlan loadPlan, String containerPoMapping, String origin, String destination, Boolean confirm, Integer loadPlanId, List poList) {
        String status = "not created";
        Boolean poStatus = false;

        entityManager.flush();


        for (int i = 0; i < poList.size(); i++) {
            if (confirm.equals(true)) {
                updatePurchaseOrder((HashSet<PurchaseOrderOp>) poList.get(i));
            }
            if (poStatus.equals(false)) {
                List<PurchaseOrderOp> purchaseOrderOps1 = new ArrayList<>((HashSet<PurchaseOrderOp>) poList.get(i));
                poStatus = purchaseOrderOps1.stream().anyMatch(po -> po.getPoStatus().equalsIgnoreCase("exception"));
            }
        }

        if (poStatus.equals(false)) {
            loadPlan.setStatus("optimized");
        } else {
            loadPlan.setStatus("exception");
        }

        LOG.info("saveLoadPlan Procedure Start");

        if (nonNull(containerPoMapping) && !containerPoMapping.isEmpty()) {
            StoredProcedureQuery query = entityManager.createStoredProcedureQuery("dbo.save_load_plan");
            query.registerStoredProcedureParameter("load_plan_name", String.class, ParameterMode.IN).setParameter("load_plan_name", loadPlan.getLoadPlanName());
            query.registerStoredProcedureParameter("load_plan_number", String.class, ParameterMode.IN).setParameter("load_plan_number", loadPlan.getLoadPlanNumber());
            query.registerStoredProcedureParameter("container_po_mapping_var", String.class, ParameterMode.IN).setParameter("container_po_mapping_var", containerPoMapping);
            query.registerStoredProcedureParameter("origin", String.class, ParameterMode.IN).setParameter("origin", origin);
            query.registerStoredProcedureParameter("destination", String.class, ParameterMode.IN).setParameter("destination", destination);
            query.registerStoredProcedureParameter("created_by", String.class, ParameterMode.IN).setParameter("created_by", loadPlan.getCreatedBy());
            query.registerStoredProcedureParameter("created_date", Date.class, ParameterMode.IN).setParameter("created_date", loadPlan.getCreatedDate());
            query.registerStoredProcedureParameter("modified_by", String.class, ParameterMode.IN).setParameter("modified_by", userId);
            query.registerStoredProcedureParameter("modified_date", Date.class, ParameterMode.IN).setParameter("modified_date", new Date());
            query.registerStoredProcedureParameter("mode_of_transport", String.class, ParameterMode.IN).setParameter("mode_of_transport", loadPlan.getModeOfTransport());
            query.registerStoredProcedureParameter("service_type", String.class, ParameterMode.IN).setParameter("service_type", loadPlan.getServiceType());
            query.registerStoredProcedureParameter("cut_off", Date.class, ParameterMode.IN).setParameter("cut_off", loadPlan.getCutOff());
            query.registerStoredProcedureParameter("load_plan_id_pr", Integer.class, ParameterMode.IN).setParameter("load_plan_id_pr", loadPlanId);
            query.registerStoredProcedureParameter("load_plan_status", String.class, ParameterMode.INOUT).setParameter("load_plan_status", loadPlan.getStatus());
            query.execute();
            status = query.getOutputParameterValue("load_plan_status").toString();
        }
        return status;
    }

    @Override
    public LoadPlan getLoadPlan(Integer id) {
        return entityManager.find(LoadPlan.class, id);
    }

    @Transactional
    @Override
    public void saveSimulateData(SimulateLoadPlan simulateLoadPlan) {
        entityManager.persist(simulateLoadPlan);
    }

    @Override
    public SimulateLoadPlan getSimulateData() {
        String query = "FROM  SimulateLoadPlan as slp";
        return entityManager.createQuery(query, SimulateLoadPlan.class).getSingleResult();
    }

    @Transactional
    @Override
    public void removeSimulateLoadPlan(Integer id) {
//        Query q = entityManager.createQuery("delete SimulateLoadPlan where id = :id");
//        q.setParameter("id", simulateLoadPlan.getId());
//        q.executeUpdate();
        SimulateLoadPlan plan = entityManager.find(SimulateLoadPlan.class, id);
        entityManager.remove(entityManager.merge(plan));
        entityManager.flush();
    }

    @Transactional
    @Override
    public void AddContainer(List<Container> containerList, Integer loadPlanId) {
        LoadPlan loadPlan = entityManager.find(LoadPlan.class, loadPlanId);
        loadPlan.setContainers(new ArrayList<>());
        for (Container container : containerList) {
            loadPlan.addContainersToLoadPlan(container);
        }
        entityManager.merge(loadPlan);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteContainer(Collection<Container> containers) {
        for (Container container : containers) {
            Query query = entityManager.createNativeQuery("update po_orders_op set container_id=NULL,modified_by=:modifiedBy,modified_date=:modifiedDate,po_status='not planned' where container_id=:id");
            query.setParameter("modifiedDate", new Date());
            query.setParameter("modifiedBy", container.getModifiedBy());
            query.setParameter("id", container.getId());
            query.executeUpdate();

            Query query1 = entityManager.createNativeQuery("delete from container where container_id=:id");
            query1.setParameter("id", container.getId());
            query1.executeUpdate();
        }
    }

    @Override
    public LoadPlan getLoadPlanById(Integer id) {
        return (LoadPlan) entityManager.createQuery("from LoadPlan as lp where id=:id").setParameter("id", id).getSingleResult();
    }

}

