package com.maersk.repositories;

import com.maersk.model.VesselSchedule;

import java.util.Collection;
import java.util.List;

public interface VesselScheduleRepository {

    void add(List<VesselSchedule> vesselSchedules);

    Collection<VesselSchedule> getVesselSchedule();

    void remove(List<Integer> vesselIds);
}
