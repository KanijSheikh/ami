package com.maersk.repositories;

import com.maersk.common.Constants;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.service.PurchaseOrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;

import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/17/2017.
 */
@Repository
public class PurchaseOrderRepositoryImpl extends JdbcDaoSupport implements PurchaseOrderRepository, Constants {

    @Autowired
    private EntityManager entityManager;

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderRepositoryImpl.class);

    @Override
    public Collection<PurchaseOrderOp> getAllPurchaseOrders(LoadPlan loadPlan, String placeOfReceipts, String destination, String originType, String destinationType) {
        // String query = "FROM PurchaseOrderOp AS pr where pr.poStatus!='optimized'";
        //return entityManager.createQuery(query, PurchaseOrderOp.class).getResultList();
        String query = "select * from po_orders_op" +
                " where " +
                "po_status='not planned' and " +
                "expected_cargo_receipt_date<= '" + dt.format(loadPlan.getCutOff()).toString() + "' and ";

        if (!loadPlan.getModeOfTransport().toLowerCase().equals("all")) {
            query = query + " mode_of_transport = '" + loadPlan.getModeOfTransport() + "' and ";
        }
        if (!loadPlan.getServiceType().toLowerCase().equals("all")) {
            query = query + " origin_service_type = '" + loadPlan.getServiceType() + "' and ";
        }
        if (destinationType.trim().equalsIgnoreCase("City")) {
            query = query + "pod_city in (" + destination + ") and ";
        }
        if (destinationType.trim().equalsIgnoreCase("Country")) {
            query = query + "pod_country in (" + destination + ") and ";
        }
        if (destinationType.trim().equalsIgnoreCase("Region")) {
            query = query + "pod_region in (" + destination + ") and ";
        }
        if (originType.trim().equalsIgnoreCase("City")) {
            query = query + "load_port_city in (" + placeOfReceipts + ");";
        }
//        if (originType.trim().equalsIgnoreCase("Country")) {
//            query = query + "load_port_country in (" + origin + ");";
//        }
//        if (originType.trim().equalsIgnoreCase("Region")) {
//            query = query + "load_port_region in (" + origin + ");";
//        }
        Query q = entityManager.createNativeQuery(query, PurchaseOrderOp.class);
        return q.getResultList();
    }

    @Transactional
    public void updateShipmentStatus(String action, String poNumber) {
        String query = "update PurchaseOrder set shipmentStatus= :action where poNumber= :poNumber";
        Query q = entityManager.createQuery(query);
        q.setParameter("action", action);
        q.setParameter("poNumber", poNumber);
        q.executeUpdate();
    }

    @Override
    public Collection<LoadPlan> getLoadPlansByStatus(String status) {
        String query = "select lp FROM  LoadPlan as lp where lp.status= :status ";
        return entityManager.createQuery(query, LoadPlan.class).setParameter("status", status.trim().toLowerCase()).getResultList();
    }

    @Override
    public Collection<LoadPlan> getAllLoadPlans() {
        String query = "select lp FROM  LoadPlan as lp";
        return entityManager.createQuery(query, LoadPlan.class).getResultList();
    }

    @Override
    public List getAllPo() {
        Query query = entityManager.createNativeQuery("select po.*," +
                "ISNULL(lp.status,po.po_status) as load_plan_status," +
                "'' as dest_type," +
                "'' as origin_type, " +
                " c.container_number as container_number, " +
                " lp.load_plan_number as load_plan_number " +
                "from po_orders_op po left join container c on po.container_id = c.container_id left join load_plan lp  on lp.load_plan_id=c.lp_id;", "poNativeMapping");
        return query.getResultList();
    }

    @Override
    public List getAllPo(Integer id, String poStatus) {
        String query = "SELECT po.*,'' AS load_plan_status,a.dest_type,b.origin_type,c.container_number as container_number,lp.load_plan_number as load_plan_number " +
                "FROM po_orders_op po " +
                "JOIN container c ON po.container_id = c.container_id " +
                "JOIN load_plan lp " +
                "ON c.lp_id = lp.load_plan_id " +
                "JOIN (select distinct type as dest_type,lp_id from load_plan_destination) as a on  lp.load_plan_id = a.lp_id " +
                "JOIN (select distinct type as origin_type,lp_id from load_plan_origin) as b on  lp.load_plan_id = b.lp_id " +
                "WHERE lp.load_plan_id =" + id + " and po.po_status = '" + poStatus + "';";
        return entityManager.createNativeQuery(query, "poNativeMapping").getResultList();
    }

    @Override
    public Collection<PurchaseOrderOp> getAllPoWithStatus(String poStatus) {
        String query = "select po FROM PurchaseOrderOp po where po.poStatus =:poStatus  ";
        return entityManager.createQuery(query, PurchaseOrderOp.class).setParameter("poStatus", poStatus).getResultList();
    }


    @Override
    public List getAllPoExException(Integer id, String poStatus) {
        String query = "SELECT po.*,'' AS load_plan_status,a.dest_type,b.origin_type,c.container_number as container_number,lp.load_plan_number as load_plan_number " +
                "FROM po_orders_op po " +
                "JOIN container c ON po.container_id = c.container_id " +
                "JOIN load_plan lp " +
                "ON c.lp_id = lp.load_plan_id " +
                "JOIN (select distinct type as dest_type,lp_id from load_plan_destination) as a on  lp.load_plan_id = a.lp_id " +
                "JOIN (select distinct type as origin_type,lp_id from load_plan_origin) as b on  lp.load_plan_id = b.lp_id " +
                "WHERE lp.load_plan_id =" + id + " and po.po_status != '" + poStatus + "';";
        return entityManager.createNativeQuery(query, "poNativeMapping").getResultList();
    }

    @Override
    public Collection<Container> getContainersByStatus(String status, Integer id) {
        String queryParam = "select c FROM Container AS c inner join fetch c.purchaseOrderOp as po  where  c.loadPlan.id= :id and  po.poStatus= :status ";
        Query query = entityManager.createQuery(queryParam, Container.class);
        // need to check
        Set setItems = new LinkedHashSet(query.setParameter("status", status.trim().toLowerCase()).setParameter("id", id).getResultList());
        Collection<Container> uniqueContainers = new ArrayList<>();
        uniqueContainers.addAll(setItems);
        return uniqueContainers;
        //return query.setParameter("status", status.trim().toLowerCase()).setParameter("id", id).getResultList();
    }

    @Override
    public Collection<Container> getContainersByLpId(Integer id) {
        String query = "FROM Container AS c where c.loadPlan.id= :id";
        return entityManager.createQuery(query, Container.class).setParameter("id", id).getResultList();
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void persistPurchaseOrders(List<PurchaseOrderOp> purchaseOrderOps) {

        String sql = "insert into po_orders_op (actual_receipt_date, assigned_to, book_date, booked_cartons, booked_cbm, booked_destination_service, " +
                "booked_quantity,booked_weight, booking_number, carrier, consignee, created_by, created_date, destination, destination_shipment_type, " +
                "discharge_port_city, discharge_port_country, discharge_port_province, eta, edt, expected_cargo_receipt_date, expected_cargo_receipt_week, " +
                "expected_delivery_date, latest_cargo_receipt_date, latest_delivery_date, load_port_city, load_port_country, load_port_province, load_port_region, " +
                "mode_of_transport, modified_by, modified_date, order_type, origin_service_type, origin_shipment_type, plant, po_line, po_line_reference1, " +
                "po_line_reference10, po_line_reference11, po_line_reference12, po_line_reference13, po_line_reference14, po_line_reference15, " +
                "po_line_reference16, po_line_reference17, po_line_reference18, po_line_reference19, po_line_reference2, po_line_reference20, " +
                "po_line_reference3, po_line_reference4, po_line_reference5, po_line_reference6, po_line_reference7, po_line_reference8, po_line_reference9, " +
                "po_number, po_status, pod_city, pod_country, pod_province, pod_region, por_city, por_country, por_province, Product_Type, product_type_code, " +
                "pslv, received_catons,received_cbm, received_quantity,received_weight, region, shipment_status, shipper, shipto, sku_number, so_number, " +
                "so_reference1, so_reference10, so_reference11, so_reference12, so_reference13, so_reference14, so_reference15, so_reference16, so_reference17, " +
                "so_reference18, so_reference19, so_reference2, so_reference20, so_reference3, so_reference4, so_reference5, so_reference6, so_reference7, " +
                "so_reference8, so_reference9, vendor) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?);";

        List<Object[]> parameters = new ArrayList<>();

        LOG.info("persistPurchaseOrders start");

        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {

            LOG.debug("persistPurchaseOrders :" + purchaseOrderOp.getPoNumber());

            Double bookedCbm = 0.00;
            Double bookedWt = 0.00;
            Double receivedCbm = 0.00;
            Double receivedWt = 0.00;

            if (nonNull(purchaseOrderOp.getBookedCbm())) {
                bookedCbm = purchaseOrderOp.getBookedCbm();
            }

            if (nonNull(purchaseOrderOp.getBookedWeight())) {
                bookedWt = purchaseOrderOp.getBookedWeight();
            }

            if (nonNull(purchaseOrderOp.getReceivedCbm())) {
                receivedCbm = purchaseOrderOp.getReceivedCbm();
            }

            if (nonNull(purchaseOrderOp.getReceivedWeight())) {
                receivedWt = purchaseOrderOp.getReceivedWeight();
            }

            parameters.add(new Object[]{
                            purchaseOrderOp.getActualRecieptDate(),
                            purchaseOrderOp.getAssignedTo(),
                            purchaseOrderOp.getBookDate(),
                            purchaseOrderOp.getBookedCartons(),
                            bookedCbm,
                            purchaseOrderOp.getBookedDestinationService(),
                            purchaseOrderOp.getBookedQuantity(),
                            bookedWt,
                            purchaseOrderOp.getBookingNumber(),
                            purchaseOrderOp.getCarrier(),
                            purchaseOrderOp.getConsignee(),
                            purchaseOrderOp.getCreatedBy(),
                            purchaseOrderOp.getCreatedDate(),
                            purchaseOrderOp.getDestination(),
                            purchaseOrderOp.getDestinationShipmentType(),
                            purchaseOrderOp.getDischargePortCity(),
                            purchaseOrderOp.getDischargePortCountry(),
                            purchaseOrderOp.getDischargePortProvince(),
                            purchaseOrderOp.getEta(),
                            purchaseOrderOp.getEtd(),
                            purchaseOrderOp.getExpectedCargoReceiptDate(),
                            purchaseOrderOp.getExpectedCargoReceiptWeek(),
                            purchaseOrderOp.getExpectedDeliveryDate(),
                            purchaseOrderOp.getLatestCargoReceiptDate(),
                            purchaseOrderOp.getLatestDeliveryDate(),
                            purchaseOrderOp.getLoadPortCity(),
                            purchaseOrderOp.getLoadPortCountry(),
                            purchaseOrderOp.getLoadPortProvince(),
                            purchaseOrderOp.getLoadPortRegion(),
                            purchaseOrderOp.getModeOfTransport(),
                            purchaseOrderOp.getModifiedBy(),
                            purchaseOrderOp.getModifiedDate(),
                            purchaseOrderOp.getOrderType(),
                            purchaseOrderOp.getOriginServiceType(),
                            purchaseOrderOp.getOriginShipmentType(),
                            purchaseOrderOp.getPlant(),
                            purchaseOrderOp.getPoLine(),
                            purchaseOrderOp.getPoLineReference1(),
                            purchaseOrderOp.getPoLineReference10(),
                            purchaseOrderOp.getPoLineReference11(),
                            purchaseOrderOp.getPoLineReference12(),
                            purchaseOrderOp.getPoLineReference13(),
                            purchaseOrderOp.getPoLineReference14(),
                            purchaseOrderOp.getPoLineReference15(),
                            purchaseOrderOp.getPoLineReference16(),
                            purchaseOrderOp.getPoLineReference17(),
                            purchaseOrderOp.getPoLineReference18(),
                            purchaseOrderOp.getPoLineReference19(),
                            purchaseOrderOp.getPoLineReference2(),
                            purchaseOrderOp.getPoLineReference20(),
                            purchaseOrderOp.getPoLineReference3(),
                            purchaseOrderOp.getPoLineReference4(),
                            purchaseOrderOp.getPoLineReference5(),
                            purchaseOrderOp.getPoLineReference6(),
                            purchaseOrderOp.getPoLineReference7(),
                            purchaseOrderOp.getPoLineReference8(),
                            purchaseOrderOp.getPoLineReference9(),
                            purchaseOrderOp.getPoNumber(),
                            purchaseOrderOp.getPoStatus(),
                            purchaseOrderOp.getPodCity(),
                            purchaseOrderOp.getPodCountry(),
                            purchaseOrderOp.getPodProvince(),
                            purchaseOrderOp.getPodRegion(),
                            purchaseOrderOp.getPorCity(),
                            purchaseOrderOp.getPorCountry(),
                            purchaseOrderOp.getPorProvince(),
                            purchaseOrderOp.getProductType(),
                            purchaseOrderOp.getProductTypeCode(),
                            purchaseOrderOp.getPslv(),
                            purchaseOrderOp.getReceivedCatons(),
                            receivedCbm,
                            purchaseOrderOp.getReceivedQuantity(),
                            receivedWt,
                            purchaseOrderOp.getRegion(),
                            purchaseOrderOp.getShipmentStatus(),
                            purchaseOrderOp.getShipper(),
                            purchaseOrderOp.getShipto(),
                            purchaseOrderOp.getSkuNumber(),
                            purchaseOrderOp.getSoNumber(),
                            purchaseOrderOp.getSoReference1(),
                            purchaseOrderOp.getSoReference10(),
                            purchaseOrderOp.getSoReference11(),
                            purchaseOrderOp.getSoReference12(),
                            purchaseOrderOp.getSoReference13(),
                            purchaseOrderOp.getSoReference14(),
                            purchaseOrderOp.getSoReference15(),
                            purchaseOrderOp.getSoReference16(),
                            purchaseOrderOp.getSoReference17(),
                            purchaseOrderOp.getSoReference18(),
                            purchaseOrderOp.getSoReference19(),
                            purchaseOrderOp.getSoReference2(),
                            purchaseOrderOp.getSoReference20(),
                            purchaseOrderOp.getSoReference3(),
                            purchaseOrderOp.getSoReference4(),
                            purchaseOrderOp.getSoReference5(),
                            purchaseOrderOp.getSoReference6(),
                            purchaseOrderOp.getSoReference7(),
                            purchaseOrderOp.getSoReference8(),
                            purchaseOrderOp.getSoReference9(),
                            purchaseOrderOp.getVendoe()
                    }
            );
        }
        getSimpleJdbcTemplate().batchUpdate(sql, parameters);

        LOG.info("persistPurchaseOrders end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addPurchaseOrder(Container container) {
        entityManager.merge(container);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deletePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps) {
        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {
            Container container = findContainerByPoId(purchaseOrderOp);
            ArrayList<PurchaseOrderOp> purchaseOrderOps1 = new ArrayList<>();
            Double volume = 0.0;
            if (nonNull(container.getVolume()) && nonNull(purchaseOrderOp.getBookedCbm())) {
                volume = container.getVolume() - purchaseOrderOp.getBookedCbm();
            }
            container.setVolume(Double.parseDouble(formatter.format(volume)));
            purchaseOrderOp.setModifiedDate(new Date());
            purchaseOrderOp.setModifiedBy(userId);
            purchaseOrderOp.setPoStatus("not planned");
            purchaseOrderOp.setContainer(null);
            purchaseOrderOps1.add(purchaseOrderOp);
            container.setPurchaseOrderOp(purchaseOrderOps1);
            entityManager.merge(container);
        }
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void splitPurchaseOrder(Container container) {
        entityManager.merge(container);
    }

    @Override
    public PurchaseOrderOp findPoById(PurchaseOrderOp purchaseOrderOp) {
        return entityManager.find(PurchaseOrderOp.class, purchaseOrderOp.getId());
    }

    @Override
    public Container findContainerById(Container container) {
        return entityManager.find(Container.class, container.getId());
    }

    @Override
    public Container findContainerByPoId(PurchaseOrderOp purchaseOrderOp) {
        return entityManager.createQuery("select c FROM Container AS c inner join c.purchaseOrderOp as po where po.id=:id", Container.class).setParameter("id", purchaseOrderOp.getId()).getSingleResult();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void removePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps) {
        String sql = "delete from po_orders_op  where po_number= ? and so_number= ? and sku_number= ?";

        List<Object[]> parameters = new ArrayList<>();

        LOG.info("removePurchaseOrder start");

        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {
            parameters.add(new Object[]{
                            purchaseOrderOp.getPoNumber(), purchaseOrderOp.getSoNumber(), purchaseOrderOp.getSkuNumber()
                    }
            );
        }

        getSimpleJdbcTemplate().batchUpdate(sql, parameters);

        LOG.info("removePurchaseOrder end");
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void cancelPurchaseOrders(String soNumber) {
        String query = "insert into po_orders_op_history(actual_receipt_date, assigned_to, book_date , booked_cartons, booked_cbm, booked_quantity, booked_weight, booking_number, carrier, consignee, created_by," +
                "created_date, destination, destination_shipment_type, discharge_port_city, discharge_port_country, discharge_port_province, eta, edt," +
                "expected_cargo_receipt_date, expected_cargo_receipt_week, expected_delivery_date , latest_cargo_receipt_date , latest_delivery_date ," +
                "load_port_city, load_port_country, load_port_province, load_port_region, mode_of_transport, modified_by, modified_date , order_type," +
                "origin_service_type, origin_shipment_type, plant, po_line, po_line_reference1, po_line_reference10, po_line_reference11, po_line_reference12," +
                "po_line_reference13, po_line_reference14, po_line_reference15, po_line_reference16, po_line_reference17, po_line_reference18, po_line_reference19," +
                "po_line_reference2, po_line_reference20, po_line_reference3, po_line_reference4, po_line_reference5, po_line_reference6, po_line_reference7," +
                "po_line_reference8, po_line_reference9, po_number, po_status, pod_city, pod_country, pod_province, pod_region, por_city, por_country, por_province," +
                "Product_Type, product_type_code, pslv, received_catons, received_cbm, received_quantity, received_weight, region, shipment_status, shipper, shipto," +
                "sku_number, so_number, so_reference1, so_reference10, so_reference11, so_reference12, so_reference13, so_reference14, so_reference15, so_reference16," +
                "so_reference17, so_reference18, so_reference19, so_reference2, so_reference20, so_reference3, so_reference4, so_reference5, so_reference6, so_reference7," +
                "so_reference8,so_reference9, vendor, container_id, booked_destination_service)  select " +
                "actual_receipt_date, assigned_to, book_date , booked_cartons, booked_cbm, booked_quantity, booked_weight, booking_number, carrier, consignee, created_by," +
                "created_date, destination, destination_shipment_type, discharge_port_city, discharge_port_country, discharge_port_province, eta, edt," +
                "expected_cargo_receipt_date, expected_cargo_receipt_week, expected_delivery_date , latest_cargo_receipt_date , latest_delivery_date ," +
                "load_port_city, load_port_country, load_port_province, load_port_region, mode_of_transport, modified_by, modified_date , order_type," +
                "origin_service_type, origin_shipment_type, plant, po_line, po_line_reference1, po_line_reference10, po_line_reference11, po_line_reference12," +
                "po_line_reference13, po_line_reference14, po_line_reference15, po_line_reference16, po_line_reference17, po_line_reference18, po_line_reference19," +
                "po_line_reference2, po_line_reference20, po_line_reference3, po_line_reference4, po_line_reference5, po_line_reference6, po_line_reference7," +
                "po_line_reference8, po_line_reference9, po_number, po_status, pod_city, pod_country, pod_province, pod_region, por_city, por_country, por_province," +
                "Product_Type, product_type_code, pslv, received_catons, received_cbm, received_quantity, received_weight, region, shipment_status, shipper, shipto," +
                "sku_number, so_number, so_reference1, so_reference10, so_reference11, so_reference12, so_reference13, so_reference14, so_reference15, so_reference16," +
                "so_reference17, so_reference18, so_reference19, so_reference2, so_reference20, so_reference3, so_reference4, so_reference5, so_reference6, so_reference7," +
                "so_reference8,so_reference9, vendor, container_id, booked_destination_service from po_orders_op where so_number in (" + soNumber + ")";
        Query q = entityManager.createNativeQuery(query);
        q.executeUpdate();

        String deleteQuery = "delete from po_orders_op where so_number in (" + soNumber + ");";
        Query query1 = entityManager.createNativeQuery(deleteQuery);
        query1.executeUpdate();
    }

}
