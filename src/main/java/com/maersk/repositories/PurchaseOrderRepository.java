package com.maersk.repositories;

import com.maersk.domain.PurchaseOrderDto;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.PurchaseOrder;
import com.maersk.model.PurchaseOrderOp;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/17/2017.
 */
public interface PurchaseOrderRepository {

    Collection<PurchaseOrderOp> getAllPurchaseOrders(LoadPlan loadPlan, String origin, String destination, String originType, String destinationType);

    Collection<LoadPlan> getLoadPlansByStatus(String Status);

    Collection<LoadPlan> getAllLoadPlans();

    Collection<Container> getContainersByStatus(String status, Integer id);

    List getAllPo();

    List getAllPo(Integer id, String poStatus);

    Collection<PurchaseOrderOp> getAllPoWithStatus(String poStatus);

    List getAllPoExException(Integer id, String poStatus);

    Collection<Container> getContainersByLpId(Integer id);

    void persistPurchaseOrders(List<PurchaseOrderOp> purchaseOrderOp);

    void addPurchaseOrder(Container container);

    void deletePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps);

    void splitPurchaseOrder(Container container);

    PurchaseOrderOp findPoById(PurchaseOrderOp purchaseOrderOp);

    Container findContainerById(Container container);

    Container findContainerByPoId(PurchaseOrderOp purchaseOrderOp);

    void removePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps);

    void cancelPurchaseOrders(String soNumber);


}


