package com.maersk.repositories;

import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.MatchingCriteria;
import com.maersk.model.Rule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/3/2017.
 */
@Repository
public class RuleRepositoryImpl implements RuleRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public Integer add(Rule rule) {
        entityManager.persist(rule);
        entityManager.flush();
        Integer lastId = rule.getId();
        return lastId;
    }

    @Override
    @Transactional
    public void copy(Rule rule) {
        entityManager.persist(rule);
    }

    @Override
    public MatchingCriteria getMatchingCriteria(Rule rule) {
//        MatchingCriteria matchingCriteria = rule.getMatchingCriteriaList();
//        String query = "FROM MatchingCriteria m where m.client =:client and m.destination =:destination and m.productType =:productType";
//        TypedQuery<MatchingCriteria> q = entityManager.createQuery(query, MatchingCriteria.class);
//        q.setParameter("client", matchingCriteria.getCriteriaName());
//        q.setParameter("destination", matchingCriteria.getCriteriaValue());
//        q.setParameter("productType", matchingCriteria.getProductType());
        return null;
    }

    @Override
    public Rule getRule(Integer ruleId) {
        return entityManager.find(Rule.class, ruleId);
    }

    @Override
    public Collection<Rule> getAllRules() {
        String query = "FROM Rule AS r order by treeOrder asc";
        return entityManager.createQuery(query, Rule.class).getResultList();
    }

    @Override
    public Collection<Rule> getRulesBy(String criteria) {
        String query = "Select rule FROM Rule as rule inner join rule.matchingCriteriaList as c where c.criteriaValue =:criteria";
        return entityManager.createQuery(query, Rule.class).setParameter("criteria", criteria).getResultList();
    }

    @Override
    public Collection<Rule> getRulesBy(Integer ruleId) {
        String query = "Select rule FROM Rule as rule where rule.id=:id";
        return entityManager.createQuery(query, Rule.class).setParameter("id", ruleId).getResultList();
    }

    @Transactional
    @Override
    public void remove(Integer ruleId) {
        Rule rule = entityManager.find(Rule.class, ruleId);
        if (rule == null) {
            throw new EntityNotFoundException("The requested rule not found");
        }
        entityManager.remove(rule);
    }

    @Transactional
    @Override
    public List getOnlyRules() {
        String query = "select ar.* from ami_rule ar order by ar.tree_order asc ";
        return entityManager.createNativeQuery(query).getResultList();
    }

    @Override
    public Integer getMaxTreeOrder() {
        String query = "Select Max(r.treeOrder) FROM Rule AS r";
        Object o = entityManager.createQuery(query).getSingleResult();
        return (Integer) o;
    }

    @Transactional
    @Override
    public void decreaseTreeOrderFrom(Integer treeOrder) {
        String query = "update Rule r  set r.treeOrder = (r.treeOrder -1)  where r.treeOrder > "+ treeOrder;
        Query q = entityManager.createQuery(query);
        q.executeUpdate();
    }

    @Override
    public void changeTreeOrder(Integer oldOrder, Integer newOrder) {
        String query;
        if(oldOrder < newOrder){
            query = "update Rule r  set r.treeOrder = (r.treeOrder -1)  where r.treeOrder <= "+ newOrder + " AND r.treeOrder > "+ oldOrder;
        }else {
            query = "update Rule r  set r.treeOrder = (r.treeOrder +1)  where r.treeOrder >= "+ newOrder + " AND r.treeOrder < "+ oldOrder;
        }
        Query q = entityManager.createQuery(query);
        q.executeUpdate();

    }

    @Override
    public void updateTreeOrder(Integer ruleId, Integer newOrder) {
        String query = "update Rule r  set r.treeOrder = "+ newOrder+"  where r.id = "+ ruleId;
        Query q = entityManager.createQuery(query);
        q.executeUpdate();
    }


}

