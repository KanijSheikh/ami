package com.maersk.repositories;

import com.maersk.model.PurchaseOrderOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;

@Repository
public class CirRepositoryImpl implements CirRepository {

    @Autowired
    private EntityManager entityManager;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void cirUpload(Collection<PurchaseOrderOp> purchaseOrderOps) {
        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {
            entityManager.merge(purchaseOrderOp);
        }
    }
}
