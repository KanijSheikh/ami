package com.maersk.repositories;

import com.maersk.model.ContainerFillRate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Repository
public class ContainerFillRateRepositoryImpl implements ContainerFillRateRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<ContainerFillRate> containers) {
        for (ContainerFillRate container : containers) {
            entityManager.persist(container);
        }
    }

    @Override
    public Collection<ContainerFillRate> getContainerFillRate() {
        String query = "FROM ContainerFillRate AS r";
        return entityManager.createQuery(query, ContainerFillRate.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> containerIds) {
        for (Integer containerId : containerIds) {
            ContainerFillRate container = entityManager.find(ContainerFillRate.class, containerId);
            if (container == null) {
                throw new EntityNotFoundException();
            }
            entityManager.remove(container);
        }

    }
}
