package com.maersk.repositories;

import com.maersk.model.Warehouse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository
public class WarehouseRepositoryImpl implements WarehouseRepository {

    @Autowired
    private EntityManager entityManager;

    @Transactional
    @Override
    public void add(List<Warehouse> warehouses) {
        for (Warehouse warehouse : warehouses) {
            entityManager.persist(warehouse);
        }
    }

    @Override
    public Collection<Warehouse> getWarehouses() {
        String query = "FROM Warehouse AS w";
        return entityManager.createQuery(query, Warehouse.class).getResultList();
    }

    @Transactional
    @Override
    public void remove(List<Integer> warehouseIds) {
        for (Integer id : warehouseIds) {
            Warehouse warehouse = entityManager.find(Warehouse.class, id);
            if (warehouse != null) {
                entityManager.remove(warehouse);
            }

        }

    }

    @Override
    public Warehouse getWareHouse(Integer id) {
        return entityManager.find(Warehouse.class, id);
    }
}
