package com.maersk.repositories;

import com.maersk.model.ContainerFillRate;
import com.maersk.model.Location;

import java.util.Collection;
import java.util.List;

public interface ContainerFillRateRepository {

    void add(List<ContainerFillRate> containers);

    Collection<ContainerFillRate> getContainerFillRate();

    void remove(List<Integer> containerIds);
}
