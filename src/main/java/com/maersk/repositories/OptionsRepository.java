package com.maersk.repositories;

import com.maersk.model.Options;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public interface OptionsRepository {

    Collection<Options> getAllOptions(String type1,String type2);
}
