package com.maersk.repositories;

import com.maersk.model.Options;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Collection;

@Repository
public class OptionsRespositoryImpl implements OptionsRepository {

    @Autowired
    EntityManager entityManager;

    @Override
    public Collection<Options> getAllOptions(String type1, String type2) {
        String query = "From Options as op where op.type1=:type1 and op.type2=:type2";
        Query query1 = entityManager.createQuery(query);
        query1.setParameter("type1", type1);
        query1.setParameter("type2", type2);
        return query1.getResultList();
    }

}