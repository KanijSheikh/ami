package com.maersk.repositories;

import com.maersk.model.Communicator;
import com.maersk.model.Location;

import java.util.Collection;
import java.util.List;

public interface LocationRepository {

    void add(List<Location> locations);

    Collection<Location> getLocation();

    void remove(List<Integer> locationIds);
}
