package com.maersk.controller;

import com.maersk.domain.AllowedContainerDto;
import com.maersk.domain.WarehouseDto;
import com.maersk.service.AllowedContainerService;
import com.maersk.service.WarehouseService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class AllowedContainerControllerRest {

    @Resource
    private AllowedContainerService allowedContainerService;


    private static final String ALLOWED_CONTAINER_URL = "/allowedContainer";
    private static final String ALLOWED_CONTAINER_UPLOAD_URL = "/allowedContainer/upload";
    private static final String ALLOWED_CONTAINER_DELETE_URL = "/allowedContainer/delete";

    @RequestMapping(value = ALLOWED_CONTAINER_UPLOAD_URL, method = POST)
    public ResponseEntity<String> uploadAllowedContainer(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();
        allowedContainerService.importAllowedContainer(mySheet, formatter);
        return ResponseEntity.ok("Data uploaded successfully");
    }


    @RequestMapping(value = ALLOWED_CONTAINER_URL, method = GET, produces = {"application/json"})
    public Collection<AllowedContainerDto> getAllowedContainer() {
        return allowedContainerService.getAllowedContainers();
    }


    @RequestMapping(value = ALLOWED_CONTAINER_DELETE_URL, method = POST, produces = {"application/json"})
    public ResponseEntity deleteAllowedContainer(@RequestBody List<Integer> containersIds) {
        allowedContainerService.remove(containersIds);
        return ResponseEntity.ok("Data deleted successfully");
    }

    @RequestMapping(value = ALLOWED_CONTAINER_URL, method = POST,produces = {"application/json"},consumes = {"application/json"})
    public ResponseEntity addAllowedContainer(@RequestBody AllowedContainerDto allowedContainerDto) {
        allowedContainerService.add(allowedContainerDto);
        return new ResponseEntity(CREATED);
    }
}
