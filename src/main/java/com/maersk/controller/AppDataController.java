package com.maersk.controller;

import com.maersk.domain.ContainerTypeDto;
import com.maersk.enums.AppDataType;
import com.maersk.exception.BadRequestException;
import com.maersk.expengine.Response;
import com.maersk.service.AppDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class AppDataController {

    @Autowired
    private AppDataService appDataService;


    public static final String APP_DATA_URL = "/applicationData";
    public static final String APP_DATA_CNTR_TYPE_URL = "/applicationData/containerType";


    @RequestMapping(value = APP_DATA_URL, method = GET,  produces = {"application/json"})
    public List<String> getAppDataByType(@RequestParam(name = "type") String type) {
        AppDataType appDataType = AppDataType.fromValue(type);
        validateType(appDataType);
        return appDataService.getAppDataByType(appDataType);
    }

    @RequestMapping(value = APP_DATA_URL, method = POST,  produces = {"application/json"})
    public ResponseEntity<String> saveAppDataByType(@RequestParam(name = "type") String type,
                                                    @RequestBody List<String> values) {
        AppDataType appDataType = AppDataType.fromValue(type);
        validateType(appDataType);
        appDataService.add(appDataType, values);
        return ResponseEntity.ok("Application Data saved successfully");
    }

    @RequestMapping(value = APP_DATA_CNTR_TYPE_URL, method = GET,  produces = {"application/json"})
    public List<ContainerTypeDto> getContainerType() throws IOException {
        return appDataService.getContainerTypes();
    }

    @RequestMapping(value = APP_DATA_CNTR_TYPE_URL, method = POST,  produces = {"application/json"})
    public ResponseEntity<String> saveAppDataByType(@RequestBody List<ContainerTypeDto> types) throws IOException {
        appDataService.addContainerTypes(types);
        return ResponseEntity.ok("Container type saved successfully");
    }

    private void validateType(AppDataType appDataType) {
        if (appDataType == null) {
            throw new BadRequestException("Type is not provided");
        }

    }

}
