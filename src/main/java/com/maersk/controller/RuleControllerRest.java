package com.maersk.controller;


import com.maersk.domain.RuleDto;
import com.maersk.domain.RuleEvaluationDto;
//import com.maersk.expengine.EvaluationEngine;
import com.maersk.expengine.Response;
import com.maersk.expengine.eval.infixEvalExp;
import com.maersk.mapper.RuleMapper;
import com.maersk.model.Rule;
import com.maersk.service.RuleService;
import com.maersk.validator.RuleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;

import com.maersk.expengine.eval.Variable;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class RuleControllerRest {

    @Autowired
    private RuleService ruleService;
    @Autowired
    private RuleMapper ruleMapper;
    @Autowired
    RuleValidator ruleValidator;

    public static final String RULES = "/rules";
    public static final String CREATE_RULE = "/createRule";
    public static final String DELETE_RULE = "/deleteRule/{ruleId}";
    public static final String RULE_ENGINE = "/EvaluateRule";
    public static final String ALL_RULES = "/getAllRules";
    public static final String COPY_RULE = "/copyRule";
    public static final String REORDER_RULE = "/rules/reorder";


    @RequestMapping(value = CREATE_RULE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addRules(@RequestBody RuleDto ruleDto) {
        ruleValidator.validate(ruleDto);
        Rule rule = ruleMapper.mapToDomain(ruleDto);
        Integer lastId = ruleService.add(rule);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("lastid", String.valueOf(lastId));
        return new ResponseEntity(responseHeaders, CREATED);
    }


    @RequestMapping(value = RULES, method = GET,
            produces = {"application/json"})
    public Collection<RuleDto> getProducts(@RequestParam(value = "criteria", required = false) String criteria, @RequestParam(value = "ruleId", required = false) Integer ruleId) {
        Collection<Rule> rules = ruleService.getRules(criteria, ruleId);
        ruleValidator.checkForEntityNotFound(rules);
        Collection<RuleDto> ruleDtos = ruleMapper.mapToDto(rules);
        return ruleDtos;
    }

    @RequestMapping(value = DELETE_RULE, method = DELETE,
            produces = {"application/json"})
    public ResponseEntity deleteRule(@PathVariable("ruleId") Integer ruleId) {
        ruleService.remove(ruleId);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = RULE_ENGINE, method = POST,
            produces = {"application/json"})
    public Response evaluateRule(@RequestBody RuleEvaluationDto ruleEvaluation) {
        HashMap hashMap = new HashMap();
        hashMap.put("bookedQuantity", new Variable("bookedQuantity", "3.5", "float"));
        hashMap.put("bookedWeight", new Variable("bookedWeight", "3.1", "float"));
        hashMap.put("destCity", new Variable("destCity", "CPH", "varchar"));
        hashMap.put("bookedCbm", new Variable("bookedCbm", "60", "numeric"));
        infixEvalExp iee = new infixEvalExp();
        Response resObj = iee.evaluateExpression(ruleEvaluation.getRuleCondition(), hashMap);
        return resObj;
//        evaluationEngine.evaluate(ruleEvaluation.getRuleCondition(), hashMap);
    }

    @RequestMapping(value = ALL_RULES, method = GET,
            produces = {"application/json"})
    public Collection<RuleDto> getAllRules() {
        Collection<Rule> rules = ruleService.getOnlyRules();
        ruleValidator.checkForEntityNotFound(rules);
        Collection<RuleDto> ruleDtos = ruleMapper.mapToDto(rules);
        return ruleDtos;
    }


    @RequestMapping(value = COPY_RULE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity copyRule(@RequestParam Integer ruleId) {
        ruleService.copy(ruleId);
        return new ResponseEntity(CREATED);
    }
    @RequestMapping(value = REORDER_RULE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<String> reorder(@RequestParam Integer ruleId,
                                  @RequestParam Integer oldOrder,
                                  @RequestParam Integer newOrder) {

        ruleValidator.validateReorderRequest(ruleId,oldOrder,newOrder);
        ruleService.reOrderRule(ruleId,oldOrder,newOrder);
        return ResponseEntity.ok("Tree order updated successfully");
    }

}