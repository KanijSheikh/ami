package com.maersk.controller;

import com.maersk.domain.LocationDto;
import com.maersk.mapper.LocationMapper;
import com.maersk.model.Location;
import com.maersk.service.LocationService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class LocationControllerRest {

    @Autowired
    private LocationService locationService;

    @Autowired
    private LocationMapper locationMapper;

    public static final String UPLOAD_LOCATION = "/uploadLocation";
    public static final String GET_LOCATION = "/getLocation";
    public static final String DELETE_LOCATION = "/deleteLocation";
    public static final String ADD_LOCATION = "/addLocation";

    @RequestMapping(value = UPLOAD_LOCATION, method = POST)
    public ResponseEntity uploadLocation(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        locationService.importLocations(mySheet, formatter);

        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = GET_LOCATION, method = GET,
            produces = {"application/json"}
    )
    public Collection<LocationDto> getLocation() {
        Collection<Location> location = locationService.getLocation();
        Collection<LocationDto> locationDtos = locationMapper.mapToDto(location);
        return locationDtos;
    }


    @RequestMapping(value = DELETE_LOCATION, method = POST,
            produces = {"application/json"})
    public ResponseEntity deleteLocation(@RequestBody List<Integer> locationIds) {
        locationService.remove(locationIds);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = ADD_LOCATION, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addLocation(@RequestBody LocationDto locationDto) {
        Location location = locationMapper.mapToDomain(locationDto);
        locationService.add(location);
        return new ResponseEntity(CREATED);
    }
}
