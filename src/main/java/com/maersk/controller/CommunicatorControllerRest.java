package com.maersk.controller;


import com.maersk.domain.CommunicatorDto;
import com.maersk.domain.RuleDto;
import com.maersk.mapper.CommunicatorMapper;
import com.maersk.model.Communicator;
import com.maersk.model.Rule;
import com.maersk.service.CommunicatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class CommunicatorControllerRest {
    @Autowired
    private CommunicatorService communicatorService;

    @Autowired
    private CommunicatorMapper communicatorMapper;

    public static final String CREATE_MESSAGE = "/createMessage";
    public static final String GET_MESSAGES = "/getMessages";


    @RequestMapping(value = CREATE_MESSAGE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity createMessage(@RequestBody CommunicatorDto communicatorDto) {
        Communicator communicator = communicatorMapper.mapToDomain(communicatorDto);
        communicatorService.add(communicator);
        return new ResponseEntity(CREATED);
    }


    @RequestMapping(value = GET_MESSAGES, method = GET,
            produces = {"application/json"})
    public Collection<CommunicatorDto> getMessages(@RequestParam(value = "loadplanId", required = false) String loadplanId) {
        Collection<Communicator> messages = communicatorService.getMessages(loadplanId);
        Collection<CommunicatorDto> messageDtos = communicatorMapper.mapToDto(messages);
        return messageDtos;
    }

}
