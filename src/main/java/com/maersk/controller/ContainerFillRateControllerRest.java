package com.maersk.controller;


import com.maersk.domain.ContainerFillRateDto;
import com.maersk.mapper.ContainerFillRateMapper;
import com.maersk.model.ContainerFillRate;
import com.maersk.service.ContainerFillRateService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ContainerFillRateControllerRest {

    @Autowired
    private ContainerFillRateService containerFillRateService;

    @Autowired
    private ContainerFillRateMapper containerFillRateMapper;

    public static final String UPLOAD_CONTAINER_FILL_RATE = "/uploadContainerFillRate";
    public static final String GET_CONTAINER_FILL_RATE = "/getContainerFillRate";
    public static final String DELETE_CONTAINER_FILL_RATE = "/deleteContainerFillRate";
    public static final String ADD_CONTAINER_FILL_RATE = "/addContainerFillRate";

    @RequestMapping(value = UPLOAD_CONTAINER_FILL_RATE, method = POST)
    public ResponseEntity uploadContainerFillRate(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        containerFillRateService.importContainerFillRate(mySheet, formatter);

        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = GET_CONTAINER_FILL_RATE, method = GET,
            produces = {"application/json"}
    )
    public Collection<ContainerFillRateDto> getContainerFillRate() {
        Collection<ContainerFillRate> container = containerFillRateService.getContainerFillRate();
        return containerFillRateMapper.mapToDto(container);
    }


    @RequestMapping(value = DELETE_CONTAINER_FILL_RATE, method = POST,
            produces = {"application/json"})
    public ResponseEntity deleteContainerFillRate(@RequestBody List<Integer> containerIds) {
        containerFillRateService.remove(containerIds);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = ADD_CONTAINER_FILL_RATE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addContainerFillRate(@RequestBody ContainerFillRateDto containerFillRateDto) {
        ContainerFillRate containerFillRate = containerFillRateMapper.mapToDomain(containerFillRateDto);
        containerFillRateService.add(containerFillRate);
        return new ResponseEntity(CREATED);
    }
}
