package com.maersk.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maersk.domain.ContainerDto;
import com.maersk.domain.LoadPlanDto;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.mapper.ContainerMapper;
import com.maersk.mapper.LoadPlanMapper;
import com.maersk.mapper.PoMapper;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.model.SimulateLoadPlan;
import com.maersk.service.LoadPlanService;
import com.maersk.service.PurchaseOrderService;
import com.maersk.service.PurchaseOrderServiceImpl;
import com.maersk.service.RuleService;
import com.maersk.validator.ContainerValidator;
import com.maersk.validator.LoadPlanValidator;
import com.maersk.validator.PoValidator;
import com.maersk.validator.RuleValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Created by kanij on 10/17/2017.
 */
@RestController
public class LoadPlanControllerRest {

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private LoadPlanService loadPlanService;

    @Autowired
    private RuleService ruleService;

    @Autowired
    private LoadPlanMapper loadPlanMapper;

    @Autowired
    private ContainerMapper containerMapper;

    @Autowired
    private PoMapper poMapper;

    @Autowired
    private LoadPlanValidator loadPlanValidator;

    @Autowired
    private RuleValidator ruleValidator;

    @Autowired
    private PoValidator poValidator;

    @Autowired
    private ContainerValidator containerValidator;


    public static final String CREATE_LOAD_PLAN = "/createLoadPlan";
    public static final String CONFIRM_LOAD_PLAN = "/confirmLoadPlan";
    public static final String GET_LOAD_PLAN = "/getLoadPlan";
    public static final String GET_ALL_PO = "/getAllPo";
    public static final String GET_ALL_PO_LOAD_PLAN = "/getAllPoForLoadPlan";
    public static final String GET_CONTAINER_BY_STATUS = "/getContainerByStatus";
    public static final String SAVE_SIMULATED_LOAD_PLAN = "/saveSimulatedLoadPlan";
    public static final String GET_SIMULATED_LOAD_PLAN = "/getSimulatedLoadPlan";
    public static final String ADD_CONTAINER = "/addContainer";
    public static final String DELETE_CONTAINER = "/deleteContainer";

    private ObjectMapper mapper = new ObjectMapper();

    private static final Logger LOG = LoggerFactory.getLogger(LoadPlanControllerRest.class);

    @RequestMapping(value = CREATE_LOAD_PLAN, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity createLoadPlan(@RequestBody LoadPlanDto loadPlanDto) {
        LoadPlan loadPlan = loadPlanMapper.mapToDomain(loadPlanDto);
        loadPlanValidator.validate(loadPlan);
        String status = "";
        Boolean confirm = false;
        try {
            status = purchaseOrderService.processPurchaseOrderAndExecuteRules(loadPlan, confirm, 0, new ArrayList<>(), false);
        } catch (Exception e) {
            loadPlanValidator.loadPlanCreate(e.getMessage());
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("status", status);
        return new ResponseEntity(responseHeaders, CREATED);
    }


    @RequestMapping(value = CONFIRM_LOAD_PLAN, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity confirmLoadPlan(@RequestBody Collection<PurchaseOrderDto> purchaseOrderDtos, @RequestParam Integer loadPlanId) {
        LoadPlan loadPlan = loadPlanService.getLoadPlan(loadPlanId);
        loadPlanValidator.validate(loadPlan);
        Collection<PurchaseOrderOp> purchaseOrderOps = poMapper.mapToPo(purchaseOrderDtos);
        String status = "";
        Boolean confirm = true;
        try {
            status = purchaseOrderService.processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOps, false);
        } catch (Exception e) {
            loadPlanValidator.loadPlanCreate(e.getMessage());
        }
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("status", status);
        return new ResponseEntity(responseHeaders, CREATED);
    }

    @RequestMapping(value = SAVE_SIMULATED_LOAD_PLAN, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity saveSimulatedLoadPlan(@RequestBody Collection<PurchaseOrderDto> purchaseOrderDtos, @RequestParam Integer loadPlanId) {
        Collection<PurchaseOrderOp> purchaseOrderOps = purchaseOrderService.getAllPoExException(loadPlanId, "exception");
        Collection<PurchaseOrderDto> purchaseOrderDtos1 = poMapper.mapToDto(purchaseOrderOps);
        purchaseOrderDtos.addAll(purchaseOrderDtos1);
        Collection<PurchaseOrderOp> purchaseOrders = poMapper.mapToPo(purchaseOrderDtos);
        poValidator.checkForEntityNotFound(purchaseOrders);
        try {
            String jsonInString = mapper.writeValueAsString(purchaseOrders);
            loadPlanService.saveSimulateData(new SimulateLoadPlan(jsonInString, loadPlanId));
        } catch (JsonProcessingException e) {
            LOG.info("saveSimulatedLoadPlan");
            loadPlanValidator.loadPlanCreate(e.getMessage());
        }
        return new ResponseEntity(CREATED);
    }

    @RequestMapping(value = GET_SIMULATED_LOAD_PLAN, method = GET,
            produces = {"application/json"}
    )
    public Collection<ContainerDto> getSimulatedLoadPlan() {
        LoadPlan loadPlan1;
        LoadPlanDto loadPlanDto = new LoadPlanDto();
        try {
            loadPlan1 = purchaseOrderService.simulateLoadPlan(false, new ArrayList<>());
            if (loadPlan1 != null && loadPlan1.getId() > 0) {
                loadPlanDto = loadPlanMapper.createloadPlanDto(loadPlan1);
            } else {
                loadPlanValidator.loadPlanCreate("Could not create load plan");
            }
        } catch (Exception e) {
            LOG.info("getSimulatedLoadPlan");
            loadPlanValidator.loadPlanCreate(e.getMessage());
        }
        return loadPlanDto.getContainerDtos();

    }

    @RequestMapping(value = GET_LOAD_PLAN, method = GET,
            produces = {"application/json"}
    )
    public Collection<LoadPlanDto> getLoadPlans(@RequestParam(value = "status", required = false) String status) {
        Collection<LoadPlan> loadPlans = purchaseOrderService.getLoadPlansByStatus(status);
        loadPlanValidator.checkForEntityNotFound(loadPlans);
        Collection<LoadPlanDto> planDtos = loadPlanMapper.mapToDto(loadPlans);
        return planDtos;
    }

    @RequestMapping(value = GET_ALL_PO, method = GET,
            produces = {"application/json"}
    )
    public Collection<PurchaseOrderDto> getAllPo(@RequestParam(value = "id", required = false) Integer id, @RequestParam(value = "status", required = false) String poStatus) {
        Collection<PurchaseOrderOp> purchaseOrderOps = purchaseOrderService.getAllPo(id, poStatus);
        poValidator.checkForEntityNotFound(purchaseOrderOps);
        Collection<PurchaseOrderDto> purchaseOrderDtos = poMapper.mapToDto(purchaseOrderOps);
        return purchaseOrderDtos;
    }

    @RequestMapping(value = GET_CONTAINER_BY_STATUS, method = GET,
            produces = {"application/json"}
    )
    public Collection<ContainerDto> getContainersByStatus(@RequestParam(value = "status", required = false) String status, @RequestParam(value = "id", required = false) Integer id) {
        Collection<Container> containers = purchaseOrderService.getContainersByStatus(status, id);
        containerValidator.checkForEntityNotFound(containers);
        Collection<ContainerDto> containerDtos = containerMapper.mapToDto(containers);
        return containerDtos;
    }


    @RequestMapping(value = ADD_CONTAINER, method = POST,
            consumes = {"application/json"}
    )
    public ResponseEntity addContainer(@RequestParam Integer loadPlanId, @RequestBody ContainerDto containerDto) {
        Container container = containerMapper.mapToDomain(containerDto);
        loadPlanService.AddContainer(loadPlanId, container);
        return new ResponseEntity(CREATED);
    }


    @RequestMapping(value = DELETE_CONTAINER, method = POST,
            consumes = {"application/json"}
    )
    public ResponseEntity deleteContainer(@RequestBody Collection<ContainerDto> containerDtos) {
        Collection<Container> containers = containerMapper.mapFromDto(containerDtos);
        loadPlanService.deleteContainer(containers);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = GET_ALL_PO_LOAD_PLAN, method = GET,
            produces = {"application/json"}
    )
    public Collection<PurchaseOrderDto> getAllPoForLoadPlan(@RequestParam(value = "id", required = false) Integer id) {
        Collection<PurchaseOrderOp> purchaseOrderOps = purchaseOrderService.getAllPoForLoadPlan(id);
        poValidator.checkForEntityNotFound(purchaseOrderOps);
        Collection<PurchaseOrderDto> purchaseOrderDtos = poMapper.mapToDto(purchaseOrderOps);
        return purchaseOrderDtos;
    }


}
