package com.maersk.controller;

import com.maersk.domain.VesselScheduleDto;
import com.maersk.exception.EntityNotFoundException;
import com.maersk.mapper.VesselScheduleMapper;
import com.maersk.model.VesselSchedule;
import com.maersk.service.VesselScheduleService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class VesselScheduleControllerRest {

    @Autowired
    private VesselScheduleService vesselScheduleService;

    @Autowired
    private VesselScheduleMapper vesselScheduleMapper;

    public static final String UPLOAD_VESSEL_SCHEDULE = "/uploadVesselSchedule";
    public static final String GET_VESSEL_SCHEDULE = "/getVesselSchedule";
    public static final String DELETE_VESSEL_SCHEDULE = "/deleteVesselSchedule";
    public static final String ADD_VESSEL_SCHEDULE = "/addVesselSchedule";

    @RequestMapping(value = UPLOAD_VESSEL_SCHEDULE, method = POST)
    public ResponseEntity uploadVesselSchedule(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        vesselScheduleService.importVesselSchedule(mySheet, formatter);

        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = GET_VESSEL_SCHEDULE, method = GET,
            produces = {"application/json"}
    )
    public Collection<VesselScheduleDto> getVesselSchedule() {
        Collection<VesselSchedule> vesselSchedules = vesselScheduleService.getVesselSchedule();
        if(vesselSchedules.isEmpty()) {
            throw new EntityNotFoundException("Vessel schedule not found");
        }
        return vesselScheduleMapper.mapToDto(vesselSchedules);
    }


    @RequestMapping(value = DELETE_VESSEL_SCHEDULE, method = POST,
            produces = {"application/json"})
    public ResponseEntity deleteVesselSchedule(@RequestBody List<Integer> vesselIds) {
        vesselScheduleService.remove(vesselIds);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = ADD_VESSEL_SCHEDULE, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addVesselSchedule(@RequestBody VesselScheduleDto vesselScheduleDto) {
        VesselSchedule vesselSchedule = vesselScheduleMapper.mapToDomain(vesselScheduleDto);
        vesselScheduleService.add(vesselSchedule);
        return new ResponseEntity(CREATED);
    }
}
