package com.maersk.controller;

import com.maersk.domain.WarehouseDto;
import com.maersk.service.WarehouseService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
public class WarehouseControllerRest {

    @Resource
    private WarehouseService warehouseService;


    private static final String WAREHOUSE_URL = "/warehouse";
    private static final String WAREHOUSE_UPLOAD_URL = "/warehouse/upload";
    private static final String WAREHOUSE_DELETE_URL = "/warehouse/delete";

    @RequestMapping(value = WAREHOUSE_UPLOAD_URL, method = POST)
    public ResponseEntity<String> uploadWarehouse(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        warehouseService.importWarehouse(mySheet, formatter);

        return ResponseEntity.ok("Data uploaded successfully");
    }


    @RequestMapping(value = WAREHOUSE_URL, method = GET, produces = {"application/json"})
    public Collection<WarehouseDto> getWareHouse() {
        return warehouseService.getWarehouses();
    }


    @RequestMapping(value = WAREHOUSE_DELETE_URL, method = POST, produces = {"application/json"})
    public ResponseEntity deleteWareHouse(@RequestBody List<Integer> warehouseIds) {
        warehouseService.remove(warehouseIds);
        return ResponseEntity.ok("Data deleted successfully");
    }

    @RequestMapping(value = WAREHOUSE_URL, method = POST,produces = {"application/json"},consumes = {"application/json"})
    public ResponseEntity addWarehouse(@RequestBody WarehouseDto warehouseDto) {
        warehouseService.add(warehouseDto);
        return new ResponseEntity(CREATED);
    }
}
