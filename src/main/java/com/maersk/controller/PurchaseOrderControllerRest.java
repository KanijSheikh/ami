package com.maersk.controller;


import com.maersk.domain.ContainerDto;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.exception.BadRequestException;
import com.maersk.mapper.ContainerMapper;
import com.maersk.mapper.PoMapper;
import com.maersk.model.Container;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.service.CirService;
import com.maersk.service.PurchaseOrderService;
import com.maersk.service.PurchaseOrderServiceImpl;
import com.maersk.validator.PoValidator;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

//import com.maersk.expengine.EvaluationEngine;

@RestController
public class PurchaseOrderControllerRest {

    @Autowired
    private PurchaseOrderService purchaseOrderService;
    @Autowired
    private PoMapper poMapper;
    @Autowired
    PoValidator poValidator;

    @Autowired
    ContainerMapper containerMapper;

    @Autowired
    private CirService cirService;

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderControllerRest.class);

    public static final String UPDATE_PO = "/updatePurchaseOrder";
    public static final String UPLOAD_PO = "/uploadPurchaseOrder";
    public static final String ADD_PO = "/addPurchaseOrder";
    public static final String DELETE_PO = "/deletePurchaseOrder";
    public static final String SPLIT_PO = "/splitPurchaseOrder";
    public static final String MOVE_PO = "/movePurchaseOrder";

    @RequestMapping(value = UPDATE_PO, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity updatePurchaseOrder(@RequestBody Collection<PurchaseOrderDto> purchaseOrderDtos) {
        //loadPlanValidator.validate(loadPlanDto);
        Collection<PurchaseOrderOp> purchaseOrderOpList = poMapper.mapToPo(purchaseOrderDtos);
        //purchaseOrderService.updatePurchaseOrder(purchaseOrderOpList);
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = UPLOAD_PO, method = POST

    )
    public ResponseEntity uploadPurchaseOrder(@RequestParam("file") MultipartFile file, @RequestParam String fileType) {
        InputStream fis = null;
        XSSFWorkbook myWorkBook;
        XSSFSheet mySheet = null;
        DataFormatter formatter = null;
        Reader fileReader;
        BufferedReader br = null;

        try {
            fis = file.getInputStream();
        } catch (IOException e) {
            LOG.error("uploadPurchaseOrder fis");
            throw new BadRequestException("uploadPurchaseOrder fis");
        }

        if (!fileType.equalsIgnoreCase("SO-Cancel-Report")) {
            try {
                myWorkBook = new XSSFWorkbook(fis);
                mySheet = myWorkBook.getSheetAt(0);
                formatter = new DataFormatter();
            } catch (IOException e) {
                LOG.error("uploadPurchaseOrder cir/po report");
                throw new BadRequestException("uploadPurchaseOrder cir/po report");
            }
        } else {

            fileReader = new InputStreamReader(fis);
            br = new BufferedReader(fileReader);
        }

        if (fileType.toLowerCase().contains("cir")) {
            cirService.uploadCirReport(mySheet, formatter, fileType);
        }
        if (fileType.equalsIgnoreCase("SO-Cancel-Report")) {
            try {
                purchaseOrderService.cancelPurchaseOrders(br);
            } catch (IOException e) {
                LOG.error("uploadPurchaseOrder can report");
                throw new BadRequestException("uploadPurchaseOrder can report");
            }
        }
        if (fileType.equalsIgnoreCase("SO-PO-Report")) {
            String status = purchaseOrderService.importPurchaseOrders(mySheet, formatter);
            if (status.equalsIgnoreCase("failed")) {
                throw new BadRequestException("file upload failed");
            }
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = ADD_PO, method = POST,
            consumes = {"application/json"}
    )
    public ResponseEntity addPurchaseOrder(@RequestBody ContainerDto containerDto) {
        Container container = containerMapper.mapToDomain(containerDto);
        purchaseOrderService.addPurchaseOrder(container);
        return new ResponseEntity(CREATED);
    }

    @RequestMapping(value = DELETE_PO, method = POST,
            consumes = {"application/json"}
    )
    public ResponseEntity deletePo(@RequestBody Collection<PurchaseOrderDto> purchaseOrderDtos) {
        Collection<PurchaseOrderOp> purchaseOrderOps = poMapper.mapToPo(purchaseOrderDtos);
        purchaseOrderService.deletePurchaseOrder(purchaseOrderOps);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = SPLIT_PO, method = POST,
            consumes = {"application/json"}
    )
    public ResponseEntity splitPurchaseOrder(@RequestBody Collection<PurchaseOrderDto> purchaseOrderDto) {
        List<PurchaseOrderDto> purchaseOrderDtoList = (List) purchaseOrderDto;
        if (nonNull(purchaseOrderDtoList) && !purchaseOrderDtoList.isEmpty()) {
            PurchaseOrderOp purchaseOrderOp = poMapper.mapToDomain(purchaseOrderDtoList.get(0));
            purchaseOrderService.splitPurchaseOrder(purchaseOrderOp);
        }
        return new ResponseEntity(CREATED);
    }

    @RequestMapping(value = MOVE_PO, method = POST, consumes = {"application/json"})
    public ResponseEntity<String> movePurchaseOrder(@RequestBody ContainerDto containerDto) {
        Container container = containerMapper.mapToDomain(containerDto);
        purchaseOrderService.deletePurchaseOrder(container.getPurchaseOrderOp());
        purchaseOrderService.addPurchaseOrder(container);
        return ResponseEntity.ok("Purchase orders moved successfully");
    }

}