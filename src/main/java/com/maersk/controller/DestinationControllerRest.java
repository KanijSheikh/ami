package com.maersk.controller;

import com.maersk.domain.ConfigDestinationDto;
import com.maersk.mapper.DestinationMapper;
import com.maersk.model.ConfigDestination;
import com.maersk.service.DestinationService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class DestinationControllerRest {

    @Autowired
    private DestinationService destinationService;

    @Autowired
    private DestinationMapper destinationMapper;

    public static final String UPLOAD_DESTINATION = "/uploadDestination";
    public static final String GET_DESTINATION = "/getDestination";
    public static final String DELETE_DESTINATION = "/deleteDestination";
    public static final String ADD_DESTINATION = "/addDestination";

    @RequestMapping(value = UPLOAD_DESTINATION, method = POST)
    public ResponseEntity uploadDestination(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        destinationService.importDestination(mySheet, formatter);

        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = GET_DESTINATION, method = GET,
            produces = {"application/json"}
    )
    public Collection<ConfigDestinationDto> getDestination() {
        Collection<ConfigDestination> destination = destinationService.getDestination();
        return destinationMapper.mapToDto(destination);
    }


    @RequestMapping(value = DELETE_DESTINATION, method = POST,
            produces = {"application/json"})
    public ResponseEntity deleteDestination(@RequestBody List<Integer> destinationIds) {
        destinationService.remove(destinationIds);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = ADD_DESTINATION, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addDestination(@RequestBody ConfigDestinationDto configDestinationDto) {
        ConfigDestination configDestination = destinationMapper.mapToDomain(configDestinationDto);
        destinationService.add(configDestination);
        return new ResponseEntity(CREATED);
    }
}
