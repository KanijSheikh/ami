package com.maersk.controller;

import com.maersk.domain.OptionsDto;
import com.maersk.domain.RuleDto;
import com.maersk.mapper.OptionsMapper;
import com.maersk.model.Options;
import com.maersk.model.Rule;
import com.maersk.service.OptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class OptionsControllerRest {

    @Autowired
    OptionsMapper optionsMapper;

    @Autowired
    OptionsService optionsService;

    public static final String GET_ALL_OPTIONS = "/getAllOptions";

    @RequestMapping(value = GET_ALL_OPTIONS, method = GET,
            produces = {"application/json"})
    public Collection<OptionsDto> getAllOptions(@RequestParam String type1, @RequestParam String type2) {
        Collection<Options> options = optionsService.getAllOptions(type1, type2);
        Collection<OptionsDto> optionsDtos = optionsMapper.mapToDto(options);
        return optionsDtos;
    }


}
