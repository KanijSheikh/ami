package com.maersk.controller;

import com.maersk.domain.ConfigOriginDto;
import com.maersk.mapper.OriginMapper;
import com.maersk.model.ConfigOrigin;
import com.maersk.service.OriginService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class OriginControllerRest {

    @Autowired
    private OriginService originService;

    @Autowired
    private OriginMapper originMapper;

    public static final String UPLOAD_ORIGIN = "/uploadOrigin";
    public static final String GET_ORIGIN = "/getOrigin";
    public static final String DELETE_ORIGIN = "/deleteOrigin";
    public static final String ADD_ORIGIN = "/addOrigin";

    @RequestMapping(value = UPLOAD_ORIGIN, method = POST)
    public ResponseEntity uploadOrigin(@RequestParam("file") MultipartFile file) throws IOException {
        InputStream fis = file.getInputStream();
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        originService.importOrigin(mySheet, formatter);

        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = GET_ORIGIN, method = GET,
            produces = {"application/json"}
    )
    public Collection<ConfigOriginDto> getOrigin() {
        Collection<ConfigOrigin> origin = originService.getOrigin();
        return originMapper.mapToDto(origin);
    }


    @RequestMapping(value = DELETE_ORIGIN, method = POST,
            produces = {"application/json"})
    public ResponseEntity deleteOrigin(@RequestBody List<Integer> originIds) {
        originService.remove(originIds);
        return new ResponseEntity(NO_CONTENT);
    }

    @RequestMapping(value = ADD_ORIGIN, method = POST,
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity addOrigin(@RequestBody ConfigOriginDto configOriginDto) {
        ConfigOrigin configOrigin = originMapper.mapToDomain(configOriginDto);
        originService.add(configOrigin);
        return new ResponseEntity(CREATED);
    }
}
