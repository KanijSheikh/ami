package com.maersk.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JobService {

    private static final Logger LOG = LoggerFactory.getLogger(JobService.class);

    public void createLoadPlan() {
        LOG.info("Load Plan Created!");
    }
}
