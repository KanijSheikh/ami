//package com.maersk.job;
//
//import com.maersk.service.LoadPlanService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.Trigger;
//import org.springframework.scheduling.TriggerContext;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//import org.springframework.scheduling.support.CronTrigger;
//
//import javax.annotation.PreDestroy;
//import javax.annotation.Resource;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//@Configuration
//@EnableScheduling
//public class LoadPlanScheduler implements SchedulingConfigurer {
//    private static final Logger log = LoggerFactory.getLogger(LoadPlanScheduler.class);
//
//    @Resource
//    LoadPlanService loadPlanService;
//
//    int count = 0;
//    String cron = "0/5 * * * * ?";
//    private ScheduledTaskRegistrar taskRegistrar;
//
//
//    @Scheduled(fixedRate=1000)
//    public void doSomething() {
//
//    }
//
//    @Override
//    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//        this.taskRegistrar = taskRegistrar;
//        taskRegistrar.addTriggerTask(new Runnable() {
//            @Override
//            public void run() {
//                loadPlanService.removeSimulateLoadPlan(1);
//            }
//        }, triggerContext -> {
//            List<String> cronList = Arrays.asList("0/1 * * * * ?", "0/2 * * * * ?", "0/3 * * * * ?","0/4 * * * * ?" );
//            CronTrigger trigger = null;
//            Date nextExecDate = null;
//            for (String cronExpression : cronList) {
//                trigger =  new CronTrigger(cronExpression);
//            }
//            nextExecDate = trigger.nextExecutionTime(triggerContext);
//
//
//            // log.info(cron);
//            return nextExecDate;
//        });
//    }
//
//    @PreDestroy
//    public void destroy() {
//        System.out.println("Destroying after count:   " + count);
//        taskRegistrar.destroy();
//    }
//
//}
