package com.maersk.common;

import com.maersk.job.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public interface Constants {

    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    SimpleDateFormat dts = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
    NumberFormat formatter = new DecimalFormat("#0.00");
    DecimalFormat format_2Places = new DecimalFormat("0.00");
    NumberFormat formatterPercent3 = new DecimalFormat("#000");
    String userId = "Damco";
}
