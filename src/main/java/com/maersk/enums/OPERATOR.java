package com.maersk.enums;

/**
 * Created by kanij on 10/4/2017.
 */
public enum OPERATOR {
    EQUAL_TO("="),
    NOT_EQUAL_TO("!="),
    GREATER_THAN(">"),
    LESS_THAN("<"),
    GREATER_THAN_EQUAL_TO(">="),
    LESS_THAN_EQUAL_TO("<="),
    AND("&"),
    OR("|"),
    SUM("+"),
    MINUS("-"),
    MULTIPLY("*"),
    DIVISION("/"),
    LEFT_BRACKET("("),
    RIGHT_BRACKET(")");

    private String operator;

    OPERATOR(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public static boolean contains(String name) {
        for (OPERATOR operator : OPERATOR.values()) {
            if (operator.getOperator().equalsIgnoreCase(name.trim())) {
                return true;
            }
        }
        return false;
    }
}
