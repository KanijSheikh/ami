package com.maersk.enums;

/**
 * Created by kanij on 10/4/2017.
 */
public enum BUCKET {
    RELEASED("Released"),
    EXCEPTION("Exception");

    private String name;

    BUCKET(String name) {
        this.name =name;
    }
}
