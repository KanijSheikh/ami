package com.maersk.enums;

import com.maersk.expengine.eval.Variable;

import java.util.Arrays;
import java.util.List;

public enum POFIELDS {


    aaFlag("aaFlag", new Variable("aaFlag", "varchar")),
    demandKey("demandKey", new Variable("demandKey", "varchar")),
    poLineReference3("poLineReference3", new Variable("poLineReference3", "varchar")),
    poLineReference4("poLineReference4", new Variable("poLineReference4", "varchar")),
    poLineReference5("poLineReference5", new Variable("poLineReference5", "varchar")),
    poLineReference6("poLineReference6", new Variable("poLineReference6", "varchar")),
    poLineReference7("poLineReference7", new Variable("poLineReference7", "varchar")),
    poLineReference8("poLineReference8", new Variable("poLineReference8", "varchar")),
    poLineReference9("poLineReference9", new Variable("poLineReference9", "varchar")),
    poLineReference10("poLineReference10", new Variable("poLineReference10", "varchar")),
    poLineReference11("poLineReference11", new Variable("poLineReference11", "varchar")),
    poLineReference12("poLineReference12", new Variable("poLineReference12", "varchar")),
    poLineReference13("poLineReference13", new Variable("poLineReference13", "varchar")),
    poLineReference14("poLineReference14", new Variable("poLineReference14", "varchar")),
    poLineReference15("poLineReference15", new Variable("poLineReference15", "varchar")),
    poLineReference16("poLineReference16", new Variable("poLineReference16", "varchar")),
    poLineReference17("poLineReference17", new Variable("poLineReference17", "varchar")),
    poLineReference18("poLineReference18", new Variable("poLineReference18", "varchar")),
    poLineReference19("poLineReference19", new Variable("poLineReference19", "varchar")),
    poLineReference20("poLineReference20", new Variable("poLineReference20", "varchar")),
    orderType("orderType", new Variable("orderType", "varchar")),
    podCountry("podCountry", new Variable("podCountry", "varchar")),
    podProvince("podProvince", new Variable("podProvince", "varchar")),
    podCity("podCity", new Variable("podCity", "varchar")),
    porCountry("porCountry", new Variable("porCountry", "varchar")),
    porProvince("porProvince", new Variable("porProvince", "varchar")),
    porCity("porCity", new Variable("porCity", "varchar")),
    plant("plant", new Variable("plant", "varchar")),
    poNumber("poNumber", new Variable("poNumber", "varchar")),
    poLine("poLine", new Variable("poLine", "varchar")),
    productTypeCode("productTypeCode", new Variable("productTypeCode", "varchar")),
    productType("productType", new Variable("productType", "varchar")),
    pslv("pslv", new Variable("pslv", "varchar")),
    shipto("shipto", new Variable("shipto", "varchar")),
    shipper("shipper", new Variable("shipper", "varchar")),
    skuNumber("skuNumber", new Variable("skuNumber", "varchar")),
    vendor("vendor", new Variable("vendor", "varchar")),
    carrier("carrier", new Variable("carrier", "varchar")),
    bookedWeight("bookedWeight", new Variable("bookedWeight", "float")),
    expectedCargoReceiptDate("expectedCargoReceiptDate", new Variable("expectedCargoReceiptDate", "date")),
    expectedDeliveryDate("expectedDeliveryDate", new Variable("expectedDeliveryDate", "date")),
    actualRecieptDate("actualRecieptDate", new Variable("actualRecieptDate", "date")),
    bookedCartons("bookedCartons", new Variable("bookedCartons", "numeric")),
    bookedCbm("bookedCbm", new Variable("bookedCbm", "float")),
    bookedQuantity("bookedQuantity", new Variable("bookedQuantity", "numeric")),
    consignee("consignee", new Variable("consignee", "varchar")),
    expectedCargoReceiptWeek("expectedCargoReceiptWeek", new Variable("expectedCargoReceiptWeek", "numeric")),
    latestDeliveryDate("latestDeliveryDate", new Variable("latestDeliveryDate", "date")),
    latestCargoReceiptDate("latestCargoReceiptDate", new Variable("latestCargoReceiptDate", "date")),
    eta("eta", new Variable("eta", "date")),
    etd("etd", new Variable("etd", "date")),
    receivedCatons("receivedCatons", new Variable("receivedCatons", "numeric")),
    receivedCbm("receivedCbm", new Variable("receivedCbm", "float")),
    receivedQuantity("receivedQuantity", new Variable("receivedQuantity", "numeric")),
    receivedWeight("receivedWeight", new Variable("receivedWeight", "float")),

    soReference1("soReference1", new Variable("soReference1", "varchar")),
    soReference2("soReference2", new Variable("soReference2", "varchar")),
    soReference3("soReference3", new Variable("soReference3", "varchar")),
    soReference4("soReference4", new Variable("soReference4", "varchar")),
    soReference5("soReference5", new Variable("soReference5", "varchar")),
    soReference6("soReference6", new Variable("soReference6", "varchar")),
    soReference7("soReference7", new Variable("soReference7", "varchar")),
    soReference8("soReference8", new Variable("soReference8", "varchar")),
    soReference9("soReference9", new Variable("soReference9", "varchar")),
    soReference10("soReference10", new Variable("soReference10", "varchar")),
    soReference11("soReference11", new Variable("soReference11", "varchar")),
    soReference12("soReference12", new Variable("soReference12", "varchar")),
    soReference13("soReference13", new Variable("soReference13", "varchar")),
    soReference14("soReference14", new Variable("soReference14", "varchar")),
    soReference15("soReference15", new Variable("soReference15", "varchar")),
    soReference16("soReference16", new Variable("soReference16", "varchar")),
    soReference17("soReference17", new Variable("soReference17", "varchar")),
    soReference18("soReference18", new Variable("soReference18", "varchar")),
    soReference19("soReference19", new Variable("soReference19", "varchar")),
    soReference20("soReference20", new Variable("soReference20", "varchar")),
    bookingNumber("bookingNumber", new Variable("bookingNumber", "varchar")),
    currentDate("currentDate", new Variable("currentDate", "date")),

    destinationShipmentType("destinationShipmentType", new Variable("destinationShipmentType", "varchar")),
    dischargePortCity("dischargePortCity", new Variable("dischargePortCity", "varchar")),
    dischargePortCountry("dischargePortCountry", new Variable("dischargePortCountry", "varchar")),
    dischargePortProvince("dischargePortProvince", new Variable("dischargePortProvince", "varchar")),

    loadPortCity("loadPortCity", new Variable("loadPortCity", "varchar")),
    loadPortCountry("loadPortCountry", new Variable("loadPortCountry", "varchar")),
    loadPortRegion("loadPortRegion", new Variable("loadPortRegion", "varchar")),
    loadPortProvince("loadPortProvince", new Variable("loadPortProvince", "varchar")),

    originShipmentType("originShipmentType", new Variable("originShipmentType", "varchar")),
    shipmentStatus("shipmentStatus", new Variable("shipmentStatus", "varchar")),
    modeOfTransport("modeOfTransport", new Variable("modeOfTransport", "varchar")),
    originServiceType("originServiceType", new Variable("originServiceType", "varchar")),


    vol("vol", new Variable("vol", "double")),
    wt("wt", new Variable("wt", "double")),
    qty("qty", new Variable("qty", "numeric"));


    private static List<String> collectionLevelFields = Arrays.asList("vol", "wt", "qty");

    private String name;

    private Variable var;

    POFIELDS(String name, Variable var) {

        this.name = name;
        this.var = var;
    }

    public static Variable getVariable(String name) {
        for (POFIELDS key :
                values()) {
            if (key.name.equalsIgnoreCase(name)) {
                return key.var;
            }
        }
        return null;
    }

    public static boolean isCollectionLevelField(String name) {
        return collectionLevelFields.contains(name);
    }

}
