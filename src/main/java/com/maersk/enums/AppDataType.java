package com.maersk.enums;

import java.util.Arrays;

/**
 * Created by Pankaj on 8/2/2017.
 */
public enum AppDataType {

    MOT("MOT"),
    DAMCO_ORIGIN("DamcoOrigin"),
    CARRIER_ORIGIN("CarrierOrigin"),
    CARRIER_DESTINATION("CarrierDestination"),
    ORDER_STATUS("OrderStatus"),
    LOAD_PLAN_STATUS("LPStatus"),
    CONTAINER_TYPE("ContainerType"),
    FREIGHT_TERM("FreightTerm");

    private String name;

    AppDataType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static AppDataType fromValue(String value) {
        for (AppDataType type : values()) {
            if (type.name.equalsIgnoreCase(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException(
                "Unknown enum type " + value + ", Allowed values are " + Arrays.toString(values()));
    }
}
