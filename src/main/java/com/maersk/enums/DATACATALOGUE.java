package com.maersk.enums;

/**
 * Created by kanij on 10/4/2017.
 */
public enum DATACATALOGUE {
    BOOKING_NUMBER("bookingNumber"),
    VENDOR("vendor"),
    EXPECTED_CARGO_RECEIPT_DATE("expectedCargoReceiptDate"),
    EXPECTED_CARGO_RECEIPT_WEEK("expectedCargoReceiptWeek"),
    POD_CITY("podCity"),
    POD_PROVINCE("podProvince"),
    POD_COUNTRY("podCountry"),
    DEST_CITY_CODE("destCityCode"),
    DEST_CITY("destCity"),
    DEST_COUNTRY("destCountry"),
    PRODUCT_TYPE_CODE("productTypeCode"),
    PRODUCT_TYPE("productType"),
    ORDER_TYPE("orderType"),
    FILTER("filler"),
    PO_NUMBER("poNumber"),
    PO_LINE("poLine"),
    SKU_NUMBER("skuNumber"),
    BOOKED_QUANTITY("bookedQuantity"),
    BOOKED_CARTOONS("bookedCartons"),
    BOOKED_WEIGHT("bookedWeight"),
    BOOKED_CBM("bookedCbm"),
    BOOKED_VOLUME("bookedVolume"),
    SHIP_TO("shipTo"),
    DEMAND_KEY("demandKey");



    private String name;

    DATACATALOGUE(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static boolean contains(String name) {
        for (DATACATALOGUE datacatalogue : DATACATALOGUE.values()) {
            if (datacatalogue.getName().equalsIgnoreCase(name.trim())) {
                return true;
            }
        }
        return false;
    }


}
