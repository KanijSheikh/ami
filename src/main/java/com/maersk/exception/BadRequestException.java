package com.maersk.exception;

/**
 * Created by kanij on 10/3/2017.
 */
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public BadRequestException() {
        super("The requested URL could not be processed because of bad request");
    }

    public BadRequestException(String message) {
        super(message);
    }
}