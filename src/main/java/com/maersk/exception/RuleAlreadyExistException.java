package com.maersk.exception;

/**
 * Created by kanij on 10/4/2017.
 */
public class RuleAlreadyExistException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public RuleAlreadyExistException() {
        super("The requested rule already exist");
    }
}
