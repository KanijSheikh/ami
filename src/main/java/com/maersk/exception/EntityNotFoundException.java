package com.maersk.exception;

/**
 * Created by kanij on 10/3/2017.
 */
public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public EntityNotFoundException(String message) {
        super(message);
    }
}