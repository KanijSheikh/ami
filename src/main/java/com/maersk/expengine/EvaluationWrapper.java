package com.maersk.expengine;

import java.util.Map;

public class EvaluationWrapper {
    Response response;
    Values values;
    Map var;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }

    public Map getVar() {
        return var;
    }

    public void setVar(Map var) {
        this.var = var;
    }
}
