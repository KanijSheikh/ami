package com.maersk.expengine.eval;

import com.maersk.enums.POFIELDS;
import com.maersk.expengine.Response;
import com.maersk.expengine.eval.operation.*;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.resolver.POFieldValueResolver;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;
/*
Class for evaluating infix expression
 */

public class InfixExpEvaluator {

    HashSet<PurchaseOrderOp> purchaseOrders;

    infixEvalExp infixEvalExp = new infixEvalExp();

    public InfixExpEvaluator(HashSet<PurchaseOrderOp> purchaseOrders) {
        this.purchaseOrders = purchaseOrders;
    }


    public Variable performOperation(Variable lvar, Variable rvar, String operator) throws ParseException {

        boolean isLeftOperandCollectionLevel = isCollectionLevelField(lvar.getName());
        boolean isRightOperandCollectionLevel = isCollectionLevelField(rvar.getName());

        if (isLeftOperandCollectionLevel) {
            lvar.setValue(POFieldValueResolver.resolve(lvar, purchaseOrders));
        }

        if (isRightOperandCollectionLevel) {
            rvar.setValue(POFieldValueResolver.resolve(rvar, purchaseOrders));
        }

        if (!isLeftOperandCollectionLevel || !isRightOperandCollectionLevel) {
            Variable response = new Variable("unknown", "unknown", "unknown");
            for (PurchaseOrderOp po : purchaseOrders) {
                resolveOperand(isLeftOperandCollectionLevel, lvar, po);
                resolveOperand(isRightOperandCollectionLevel, rvar, po);
                response = infixEvalExp.performOperation(lvar, rvar, operator);
                if (!(response.getName().equalsIgnoreCase("result") && response.getValue().equalsIgnoreCase("true"))) {
                    return response;
                }
            }
            return response;
        }
        return infixEvalExp.performOperation(lvar, rvar, operator);

    }

    private void resolveOperand(boolean isOperandCollectionLevel, Variable var, PurchaseOrderOp po) {
        if (!isOperandCollectionLevel) {
            var.setValue(POFieldValueResolver.resolve(var, po));
        }
    }


    private boolean isCollectionLevelField(String name) {
        return name.equalsIgnoreCase("result") || name.equalsIgnoreCase("constant") || POFIELDS.isCollectionLevelField(name);

    }

    /*evaluate expression pass
    expression is the mathematical function
    value is a map having all the variables passed in the expression
    */
    public boolean evaluateLogicalExpression(String expression) throws ParseException {

        String tokens[] = expression.split(" ");
        Stack<Variable> operands = new Stack<Variable>();
        Stack<String> operators = new Stack<String>();

        for (String token : tokens) {
            boolean consumeToken = false;
            if (token.equals("")) {
                continue;
            }
            while (consumeToken == false) {
                if (infixEvalExp.tokenIsOperator(token) == true) {
                    if (token.equalsIgnoreCase("")) {
                        return false;
                    } else if (token.equalsIgnoreCase("(")) {
                        operators.push(token);
                        consumeToken = true;
                    } else if (token.equalsIgnoreCase(")")) {
                        String opr = operators.pop();
                        if (opr.equalsIgnoreCase("(")) {
                            consumeToken = true;
                        } else {
                            Variable rvar = operands.pop();
                            Variable lvar = operands.pop();
                            operators.pop();
                            Variable result = performOperation(lvar, rvar, opr);
                            if (result.getType().equalsIgnoreCase("unknown")) {
                                return false;
                            } else {
                                consumeToken = true;
                                operands.push(result);
                            }
                        }
                    } else {
                        if (operators.empty() == true) {
                            operators.push(token);
                            consumeToken = true;
                        } else {
                            String opr = operators.peek();
                            if (opr.equalsIgnoreCase("(") == false) {
                                Variable rvar = operands.pop();
                                Variable lvar = operands.pop();
                                String operator = operators.pop();
                                Variable result = performOperation(lvar, rvar, operator);
                                if (result.getType().equalsIgnoreCase("unknown")) {
                                    return false;
                                } else {
                                    operands.push(result);
                                }

                            } else {
                                consumeToken = true;
                                operators.push(token);
                            }
                        }
                    }

                } else {
                    //not an operator...can be variable or constant
                    consumeToken = true;
                    if (!token.equals("")) {
                        Variable var = POFIELDS.getVariable(token);
                        if (var == null) {
                            // it is a constant
                            if (token.matches("\\d+") == true) {
                                operands.push(new Variable("constant", token, "numeric"));
                            } else if (token.matches("\\d+.\\d+") == true) {
                                operands.push(new Variable("constant", token, "float"));
                            } else if (token.matches(".\\d+") == true) {
                                operands.push(new Variable("constant", token, "float"));
                            } else if (token.matches("\\d{2}-\\d{2}-\\d{4}")) {
                                operands.push(new Variable("constant", token, "date"));
                            } else {
                                operands.push(new Variable("constant",
                                        token,
                                        "varchar"));
                            }
                        } else {
                            //it is a variable
                            operands.push(var);
                        }
                    }
                }
            }
        }
        if (operands.size() == 2 && operators.size() == 1) {
            Variable rvar = operands.pop();
            Variable lvar = operands.pop();
            Variable result = performOperation(lvar, rvar, operators.pop());
            if (result.getValue().equalsIgnoreCase("true")) {
                return true;
            }
        } else if (operands.size() == 1 && operators.size() == 0) {
            Variable result = operands.pop();
            if (result.getValue().equalsIgnoreCase("true")) {
                return true;
            }
        }

        return false;
    }

    public Response evaluateExpression(String expression) {
        Response resObj = new Response();

        try {

            Object obj = this.evaluateLogicalExpression(expression);
            //System.out.println();

            if (obj.toString().equals("true")) {
                resObj.setResult(obj.toString());
                resObj.setErrorCode(0);
                resObj.setErrorMessage("Success");
            } else {
                resObj.setResult("false");
                resObj.setErrorCode(1);
                resObj.setErrorMessage("Failure");
            }
        } catch (Exception e) {
            resObj.setResult("false");
            resObj.setErrorCode(1);
            resObj.setErrorMessage(e.getMessage());
        }
        return resObj;
    }
}


