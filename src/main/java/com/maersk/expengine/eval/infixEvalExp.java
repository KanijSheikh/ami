package com.maersk.expengine.eval;

import com.maersk.expengine.Response;
import com.maersk.expengine.eval.operation.*;
import com.maersk.expengine.util.DateUtil2;
import com.maersk.expengine.eval.ParseException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
/*
Class for evaluating infix expression
 */

public class infixEvalExp {


    public Variable performOperation(Variable lvar, Variable rvar, String operator) throws ParseException {
        //checking if left end side operator is same as right hand side operator
        /*if (lvar.getType().equalsIgnoreCase(rvar.getType()) == false) {
            return new variable("unknown", "unknown", "unknown");
        }*/
        //addition operation
        if (operator.equalsIgnoreCase("+")) {
            //todo Addition additionObj=new Addition();
            Operator opr = new Addition();
            return opr.operate(lvar, rvar);
            //return additionOperator(lvar, rvar);
        }
        //subtraction operation
        else if (operator.equalsIgnoreCase("-")) {

            Operator opr = new Subtract();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("*")) {

            Operator opr = new Multiply();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("/")) {

            Operator opr = new Division();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase(">")) {
            Operator opr = new Grater();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase(">=")) {
            Operator opr = new GraterEq();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("<")) {
            Operator opr = new Less();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("<=")) {
            Operator opr = new LessEq();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("=")) {
            //left hand side is numeric
            Operator opr = new Equals();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("!=")) {
            Operator opr = new NotEquals();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("&")) {
            Operator opr = new And();
            return opr.operate(lvar, rvar);

        } else if (operator.equalsIgnoreCase("|")) {
            Operator opr = new Or();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("Contains")) {
            Operator opr = new Contains();
            return opr.operate(lvar, rvar);
        } else if (operator.equalsIgnoreCase("Between")) {
            Operator opr = new Between();
            return opr.operate(lvar, rvar);
        }
        else {
            throw new ParseException("Wrong data type");
        }
    }

    //function to check if the given token is a operator
    public boolean tokenIsOperator(String token) {
        if (token.equalsIgnoreCase("+") ||
                token.equalsIgnoreCase("-") ||
                token.equalsIgnoreCase("*") ||
                token.equalsIgnoreCase("/")) {
            return true;
        }
        if (token.equalsIgnoreCase("=") ||
                token.equalsIgnoreCase(">") ||
                token.equalsIgnoreCase("<") ||
                token.equalsIgnoreCase("!=")) {
            return true;
        }
        if (token.equalsIgnoreCase("&") ||
                token.equalsIgnoreCase("|")) {
            return true;
        }
        if (token.equalsIgnoreCase("(") || token.equalsIgnoreCase(")")) {

            return true;
        }
        /*not yet implemented*/
        if (token.equalsIgnoreCase("<=") || token.equalsIgnoreCase(">=")) {

            return true;
        }
        if(token.equalsIgnoreCase("contains")){
            return true;
        }

        if(token.equalsIgnoreCase("between")){
            return true;
        }
        return false;
    }

    /*evaluate expression pass
    expression is the mathematical function
    value is a map having all the variables passed in the expression
    */
    public boolean evaluateLogicalExpression(String expression, Map<String, Variable> values) throws ParseException {
        String tokens[] = expression.split(" ");
        Stack<Variable> operands = new Stack<Variable>();
        Stack<String> operators = new Stack<String>();

        for (String token : tokens) {
            boolean consumeToken = false;
            if (token.equals("")) {
                continue;
            }
            while (consumeToken == false) {
                if (tokenIsOperator(token) == true) {
                    if (token.equalsIgnoreCase("")) {
                        return false;
                    } else if (token.equalsIgnoreCase("(")) {
                        operators.push(token);
                        consumeToken = true;
                    } else if (token.equalsIgnoreCase(")")) {
                        String opr = operators.pop();
                        if (opr.equalsIgnoreCase("(")) {
                            consumeToken = true;
                        } else {
                            Variable rvar = operands.pop();
                            Variable lvar = operands.pop();
                            operators.pop();
                            Variable result = performOperation(lvar, rvar, opr);
                            if (result.getType().equalsIgnoreCase("unknown")) {
                                return false;
                            } else {
                                consumeToken = true;
                                operands.push(result);
                            }
                        }
                    } else {
                        if (operators.empty() == true) {
                            operators.push(token);
                            consumeToken = true;
                        } else {
                            String opr = operators.peek();
                            if (opr.equalsIgnoreCase("(") == false) {
                                Variable rvar = operands.pop();
                                Variable lvar = operands.pop();
                                String operator = operators.pop();
                                Variable result = performOperation(lvar, rvar, operator);
                                if (result.getType().equalsIgnoreCase("unknown")) {
                                    return false;
                                } else {
                                    operands.push(result);
                                }

                            } else {
                                consumeToken = true;
                                operators.push(token);
                            }
                        }
                    }

                } else {
                    //not an operator...can be variable or constant
                    consumeToken = true;
                    if (!token.equals("")) {
                        Variable var = values.get(token);
                        if (var == null) {
                            // it is a constant
                            if (token.matches("\\d+") == true) {
                                operands.push(new Variable("constant", token, "numeric"));
                            } else if (token.matches("\\d+.\\d+") == true) {
                                operands.push(new Variable("constant", token, "float"));
                            } else if (token.matches(".\\d+") == true) {
                                operands.push(new Variable("constant", token, "float"));
                            } else if (token.matches("\\d{2}-\\d{2}-\\d{4}")) {
                                operands.push(new Variable("constant", token, "date"));
                            } else {
                                operands.push(new Variable("constant",
                                        token,
                                        "varchar"));
                            }
                        } else {
                            //it is a variable
                            operands.push(var);
                        }
                    }
                }
            }
        }
        if (operands.size() == 2 && operators.size() == 1) {
            Variable rvar = operands.pop();
            Variable lvar = operands.pop();
            Variable result = performOperation(lvar, rvar, operators.pop());
            if (result.getValue().equalsIgnoreCase("true")) {
                return true;
            }
        } else if (operands.size() == 1 && operators.size() == 0) {
            Variable result = operands.pop();
            if (result.getValue().equalsIgnoreCase("true")) {
                return true;
            }
        }

        return false;
    }

    public Response evaluateExpression(String expression, Map mp) {
        Response resObj = new Response();

        try {
            infixEvalExp inf = new infixEvalExp();
            Object obj = inf.evaluateLogicalExpression(expression, mp);
            //System.out.println();

            if (obj.toString().equals("true")) {
                resObj.setResult(obj.toString());
                resObj.setErrorCode(0);
                resObj.setErrorMessage("Success");
            } else {
                resObj.setResult("false");
                resObj.setErrorCode(1);
                resObj.setErrorMessage("Failure");
            }
        } catch (Exception e) {
            resObj.setResult("false");
            resObj.setErrorCode(1);
            resObj.setErrorMessage(e.getMessage());
        }
        return resObj;
    }
}


