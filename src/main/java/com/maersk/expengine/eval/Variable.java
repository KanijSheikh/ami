package com.maersk.expengine.eval;

public class Variable {
    private String name;
    private String value;
    private String type;

    public Variable(String name, String value, String type) {
        this.name = name;
        this.value = value;
        this.type = type;
        //System.out.println(this.toString());
    }

    public Variable(String name,String type) {
        this.name = name;
        this.type = type;

    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Variable{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public void setValue(String value) {
        this.value = value;
    }
}