package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;
import com.maersk.expengine.util.DateUtil2;

public class Less implements Operator{
    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        if (lvar.getType().equalsIgnoreCase("numeric")) {
            if (rvar.getType().equalsIgnoreCase("numeric") || rvar.getType().equalsIgnoreCase("float")|| rvar.getType().equalsIgnoreCase("double")) {
                if (Double.parseDouble(lvar.getValue()) < Double.parseDouble(rvar.getValue())) {
                    return ( new Variable("result", "true", "boolean") );
                } else {
                    return ( new Variable("result", "false", "boolean") );
                }
            } else {
                throw new ParseException("Invalid operands for less operation");
            }
        }
        if (lvar.getType().equalsIgnoreCase("float")||lvar.getType().equalsIgnoreCase("double")) {
            if (rvar.getType().equalsIgnoreCase("numeric") || rvar.getType().equalsIgnoreCase("float")|| rvar.getType().equalsIgnoreCase("double")) {
                if (Double.parseDouble(lvar.getValue()) < Double.parseDouble(rvar.getValue())) {
                    return ( new Variable("result", "true", "boolean") );
                } else {
                    return ( new Variable("result", "false", "boolean") );
                }
            } else {
                throw new ParseException("Invalid operands for less operation float with uncompatible operand");
            }
        }
        if (lvar.getType().equalsIgnoreCase("date")) {
            if (rvar.getType().equalsIgnoreCase("date")) {
                DateUtil2 du2 = new DateUtil2();
                Object res = du2.dateCompare(lvar.getValue(), rvar.getValue(), "<");
                return ( new Variable("result", res.toString(), "boolean") );
            } else {
                throw new ParseException("date is compared with non date type");
            }
        } else {
            throw new ParseException("Invalid operands for less opetarion");
        }
    }
}
