package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;
import com.maersk.expengine.util.DateUtil2;

public class NotEquals implements Operator{
    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        if (lvar.getType().equalsIgnoreCase("numeric")) {
            if (Integer.parseInt(lvar.getValue()) != Integer.parseInt(rvar.getValue())) {
                return ( new Variable("result", "true", "boolean") );
            } else {
                return ( new Variable("result", "false", "boolean") );
            }
        }
        if (lvar.getType().equalsIgnoreCase("varchar")) {
            if (!String.valueOf(lvar.getValue()).equals(String.valueOf(rvar.getValue()))) {
                return ( new Variable("result", "true", "boolean") );
            } else {
                return ( new Variable("result", "false", "boolean") );
            }
        }
        if (lvar.getType().equalsIgnoreCase("date")) {
            if(rvar.getType().equalsIgnoreCase("date")){
                DateUtil2 du2=new DateUtil2();
                Object res=du2.dateCompare(lvar.getValue(),rvar.getValue(),"!=");
                return ( new Variable("result", res.toString(), "boolean") );
            }
            else{
                throw new ParseException("date is compared with non date type");
            }
        }
        return new Variable("unknown", "unknown", "unknown");
    }
}
