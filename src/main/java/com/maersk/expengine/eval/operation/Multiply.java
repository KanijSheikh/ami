package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;

public class Multiply implements Operator{
    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        //incase left hand side operator is numeric
        if (lvar.getType().equalsIgnoreCase("numeric")) {
            if (rvar.getType().equalsIgnoreCase("numeric")) {
                return new Variable("result", String.valueOf(Integer.parseInt(lvar.getValue()) * Integer.parseInt(rvar.getValue())), "numeric");
            } else if (rvar.getType().equalsIgnoreCase("float")||rvar.getType().equalsIgnoreCase("double")) {
                return new Variable("result", String.valueOf(Double.parseDouble(lvar.getValue()) * Double.parseDouble(rvar.getValue())), "double");
            } else if (rvar.getType().equalsIgnoreCase("varchar")) {
                throw new ParseException("Uncompatible operation (varchar and integer addition operation)");
            } else if (rvar.getType().equalsIgnoreCase("date")) {
                throw new ParseException("Uncompatible operation (date and integer addition operation)");
            }
        }

        //incase left hand side Variable is float
        if (lvar.getType().equalsIgnoreCase("float")||lvar.getType().equalsIgnoreCase("double")) {
            if (rvar.getType().equalsIgnoreCase("varchar")) {
                throw new ParseException("uncompetible operation (varchar and float multiplication operation)");
            } else if (rvar.getType().equalsIgnoreCase("numeric") || rvar.getType().equalsIgnoreCase("float")|| rvar.getType().equalsIgnoreCase("double")) {
                return new Variable("result", String.valueOf(Double.parseDouble(rvar.getValue()) * Double.parseDouble(lvar.getValue())), "double");
            } else if (rvar.getType().equalsIgnoreCase("date")) {
                throw new ParseException("uncompetible operation (date and float addition operation)");
            }
        }
        //incase left hand side Variable is string
        else {
            throw new ParseException("Invalid operands for multiplication opetarion");
        }
        return new Variable("unknown", "unknown", "unknown");
    }
}
