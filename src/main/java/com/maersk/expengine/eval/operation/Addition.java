package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;
import com.maersk.expengine.util.DateUtil2;

import java.util.Date;

public class Addition implements Operator {


    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        if (lvar.getType().equalsIgnoreCase("numeric")) {
            if (rvar.getType().equalsIgnoreCase("numeric")) {
                return new Variable("result", String.valueOf(Integer.parseInt(lvar.getValue()) + Integer.parseInt(rvar.getValue())), "numeric");
            } else if (rvar.getType().equalsIgnoreCase("double")) {
                return new Variable("result", String.valueOf(Float.parseFloat(lvar.getValue()) + Float.parseFloat(rvar.getValue())), "float");
            } else if (rvar.getType().equalsIgnoreCase("varchar")) {
                throw new ParseException("incompatible operation (varchar and integer addition operation)");
            } else if (rvar.getType().equalsIgnoreCase("date")) {
                throw new ParseException("number is added with date data type");
            }
        }

        //incase left hand side Variable is float
        if (lvar.getType().equalsIgnoreCase("float")||lvar.getType().equalsIgnoreCase("double")) {
            if (rvar.getType().equalsIgnoreCase("varchar")) {
                throw new ParseException("incompatible operation (varchar and float addition operation)");
            } else if (rvar.getType().equalsIgnoreCase("numeric") || rvar.getType().equalsIgnoreCase("float")|| rvar.getType().equalsIgnoreCase("double")) {
                return new Variable("result", String.valueOf(Double.parseDouble(rvar.getValue()) + Double.parseDouble(lvar.getValue())), "double");
            } else if (rvar.getType().equalsIgnoreCase("date")) {
                //todo
                throw new ParseException("incompatible operation (date and float addition operation)");
            }
        }

        //incase left hand side Variable is string
        if (lvar.getType().equalsIgnoreCase("varchar")) {
            if (rvar.getType().equalsIgnoreCase("varchar")) {

                return new Variable("result", rvar.getValue() + lvar.getValue(), "varchar");
            }
            if (rvar.getType().equalsIgnoreCase("date")) {
                throw new ParseException("incompatible operation (string and date addition operation)");
            } else {
                throw new ParseException("incompatible operation (varchar and numeric or date addition operation)");
            }
        }
        if (lvar.getType().equalsIgnoreCase("date")) {
            if (rvar.getType().equalsIgnoreCase("varchar")) {
                if (rvar.getValue().endsWith("d")) {
                    DateUtil2 du2 = new DateUtil2();
                    String noOfDay = rvar.getValue().replace("d", "");
                    Integer day = Integer.parseInt(noOfDay);
                    Date res = du2.dayAdd(lvar.getValue(), day);
                    return new Variable("result", du2.date2String(res), "date");
                }
                if (rvar.getValue().endsWith("m")) {
                    DateUtil2 du2 = new DateUtil2();
                    String noOfDay = rvar.getValue().replace("m", "");
                    Integer day = Integer.parseInt(noOfDay);
                    Date res = du2.monthAdd(lvar.getValue(), day);
                    return new Variable("result", du2.date2String(res), "date");
                }
            }
            if (rvar.getType().equalsIgnoreCase("date")) {
                throw new ParseException("date addition with date");
            }
        }
        return new Variable("unknown", "unknown", "unknown");
    }
}
