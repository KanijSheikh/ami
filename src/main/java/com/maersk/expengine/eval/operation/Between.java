package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;
import com.maersk.expengine.util.DateUtil2;
import org.apache.commons.lang.ArrayUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Between implements Operator {
    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        String cityString = String.valueOf(rvar.getValue());

        String decodedDates = null;
        try {
            decodedDates = URLDecoder.decode(cityString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Object res1 = null;
        Object res2 = null;

        Object tokens[] = decodedDates.split(" ");
        for (Object dateToken : tokens) {
            DateUtil2 du1 = new DateUtil2();
            res1 = du1.dateCompare(lvar.getValue(), dateToken.toString(), ">=");
            tokens =  ArrayUtils.removeElement(tokens, dateToken);
            break;
        }


        for (Object dateToken : tokens) {
            DateUtil2 du2 = new DateUtil2();
            res2 = du2.dateCompare(lvar.getValue(), dateToken.toString(), "<=");
            break;
        }


        if (res1.toString() == "true" && res2.toString() == "true") {
            return (new Variable("result", "true", "boolean"));
        } else {
            return (new Variable("result", "false", "boolean"));
        }

    }
}
