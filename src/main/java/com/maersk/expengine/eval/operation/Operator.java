package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;

public interface Operator {

    public Variable operate(Variable lvar, Variable rvar) throws ParseException;
}
