package com.maersk.expengine.eval.operation;

import com.maersk.expengine.eval.ParseException;
import com.maersk.expengine.eval.Variable;

public class Or implements Operator{

    @Override
    public Variable operate(Variable lvar, Variable rvar) throws ParseException {
        if (lvar.getType().equalsIgnoreCase("boolean")) {
            if (rvar.getType().equalsIgnoreCase("boolean")) {
                if (Boolean.valueOf(lvar.getValue()) || Boolean.valueOf(rvar.getValue())) {
                    return ( new Variable("result", "true", "boolean") );
                } else {
                    return ( new Variable("result", "false", "boolean") );
                }
            } else {
                throw new ParseException("Ill formatted expression | operator failed with boolean");
            }
        } else {
            throw new ParseException("Ill formatted expression & operator failed with boolean");
        }
    }
}
