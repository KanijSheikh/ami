package com.maersk.expengine.eval.operation;

import com.google.common.base.Splitter;
import com.maersk.expengine.eval.Variable;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class Contains implements Operator {
    @Override
    public Variable operate(Variable lvar, Variable rvar) {
        String cityString = String.valueOf(rvar.getValue());
        Splitter niceCommaSplitter = Splitter.on(',').omitEmptyStrings().trimResults();

        String decodedCities = null;
        try {
            decodedCities = URLDecoder.decode(cityString, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        List<String> listOfCities = new ArrayList<>();
        Iterable<String> tokens = niceCommaSplitter.split(decodedCities);
        for (String token : tokens) {
            listOfCities.add(token);
        }

        if (listOfCities.contains(String.valueOf(lvar.getValue()))) {
            return (new Variable("result", "true", "boolean"));
        } else {
            return (new Variable("result", "false", "boolean"));
        }


    }
}
