package com.maersk.expengine.util;

import com.maersk.expengine.eval.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil2 {

    public static Boolean dateCompare(String op1_s, String op2_s, String operator) throws ParseException {
        Date op1=dateFormatter(op1_s);
        Date op2=dateFormatter(op2_s);
        if (operator.equals("=")) {
            System.out.println(operator);
            return op1.equals(op2);
        } if (operator.equals("!=")) {
            System.out.println(operator);
            return !op1.equals(op2);
        } else if (operator.equals("<")) {
            System.out.println(operator);
            return op1.before(op2);
        } else if (operator.equals(">")) {
            System.out.println(operator);
            return op1.after(op2);
        } else if (operator.equals("<=")) {
            System.out.println(operator);
            return op1.before(op2) || op1.equals(op2);
        } else if (operator.equals(">=")) {
            System.out.println(operator);
            return op1.after(op2) || op1.equals(op2);
        } else {
            System.out.println("Operator not supported");
            return false;
        }
    }


    public static Boolean dateBetween(String op1_s, String op2_s, Date op3, String operator) throws ParseException {
        Date op1=dateFormatter(op1_s);
        Date op2=dateFormatter(op2_s);
        if ((op1.after(op2) || op1.equals(op2)) && (op1.before(op3) || op1.equals(op3))) {
            return true;
        } else
            return false;
    }

    public static Date dateFormatter(String dateString) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return dt.parse(dateString);
        } catch (java.text.ParseException e) {
            throw new ParseException("Date is not in parsable format");
        }
    }

    public static String date2String(Date date) throws ParseException{
        SimpleDateFormat formater=new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate=formater.format(date);
        return formattedDate;
    }

    public static Date monthAdd(String date_s, int num) throws ParseException {
        Date date=dateFormatter(date_s);
        System.out.println(date+":"+num);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.MILLISECOND);
        System.out.println(cal.getTime());
        cal.add(Calendar.MONTH, num);
        System.out.println(cal.getTime());
        return cal.getTime();
    }

    public static Date monthSub(String date_s, int num) throws ParseException {
        Date date=dateFormatter(date_s);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.MILLISECOND);
        cal.add(Calendar.MONTH, -num);
        return cal.getTime();
    }

    public static Date dayAdd(String date_s, int num) throws ParseException {
        Date date=dateFormatter(date_s);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.MILLISECOND);
        cal.add(Calendar.DAY_OF_MONTH, num);
        return cal.getTime();
    }

    public static Date daySub(String date_s, int num) throws ParseException {
        Date date=dateFormatter(date_s);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.clear(Calendar.MILLISECOND);
        cal.add(Calendar.DAY_OF_MONTH, -num);
        return cal.getTime();
    }


//    public static void main(String args[]) {
//      //  String op1 = "2017-10-25 00:00:00:000";
//       // String op2 = "25-10-2017";
//
//       try {
////            if (op2.contains("+") && op2.contains("m")) {
////                String numOfMonths = op2.substring(op2.indexOf("+")+1).replace("m","");
////                op2 = op2.substring(0, 10);
////                System.out.println(op2);
////                Date dop2 = monthAdd(op2, Integer.parseInt(numOfMonths));
////            }
////            else if (op2.contains("-") && op2.contains("m")) {
////                String numOfMonths = op2.substring(op2.indexOf("-")+1).replace("m","");ßßßß
////                op2 = op2.substring(0,10);
////                Date op2_2 = monthSub(op2, Integer.parseInt(numOfMonths));
////            }
////            else if (op2.contains("+") && op2.contains("d")) {
////                String numOfMonths = op2.substring(op2.indexOf("+")+1).replace("d","");
////                op2 = op2.substring(0, 10);
////                Date op2_2 = monthAdd(op2, Integer.parseInt(numOfMonths));
////            }
////            else if (op2.contains("-") && op2.contains("d")) {
////                String numOfMonths = op2.substring(op2.indexOf("-")+1).replace("d","");
////                op2 = op2.substring(0, 10);
////                Date op2_3 = monthSub(op2, Integer.parseInt(numOfMonths));
////            } else {
////                Date op2_4 = dateFormatter(op2);
////            }
//      //   String operator = "=";
////            System.out.println(op1);
////            System.out.println("Date:"+op2);
//
//         //  Calendar cal = Calendar.getInstance();
//           //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); // format
//           //cal.setTime(sdf.parse(op1));
//
//          // System.out.println("update code:"+cal.getTime());
//
//           //Boolean result = dateCompare(op1, op2, operator);
//           //System.out.println(result.booleanValue());
//
//
//           //Date test =new Date();
//            //System.out.println("test:"+test);
//           //SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
//            //String strDate = formatter.format(test);
//           //System.out.println("Date Format with dd-M-yyyy hh:mm:ss : "+strDate);
//
//
//       } catch (Exception e) {
//            System.out.println(e.getMessage());
//       }
//
//        Date dt=new Date();
//        Calendar cal=Calendar.getInstance();
//        cal.set(Calendar.DAY_OF_MONTH,5);
//        cal.set(Calendar.MONTH,2);
//        cal.set(Calendar.YEAR,2017);
//        dt=cal.getTime();
//        try {
//            System.out.println(date2String(dt));
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

}
