package com.maersk.expengine.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DateUtil {

    public static boolean dateCompare(Date op1, Date op2, String operator) {

        if (operator.equals("=")) {
            //System.out.println(operator);
            return op1.equals(op2);
        } else if (operator.equals("<")) {
            //System.out.println(operator);
            return op1.before(op2);
        } else if (operator.equals(">")) {
            //System.out.println(operator);
            return op1.after(op2);
        } else {
            //System.out.println("Operator not supported");
            return false;
        }
    }

    public static Boolean dateBetween(Date op1, Date op2, Date op3, String operator) {
        if ((op1.after(op2) || op1.equals(op2)) && (op1.before(op3) || op1.equals(op3))) {
            return true;
        } else
            return false;
    }

//    public static void main(String args[]) throws ParseException {
//
//        Calendar op1 = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        op1.setTime(sdf.parse("1997-07-25"));
//
//        Calendar op2 = Calendar.getInstance();
//        op1.setTime(sdf.parse("1997-07-25"));
//
//        Calendar op3 = Calendar.getInstance();
//        op1.setTime(sdf.parse("1997-05-29"));
//
//        String operator = "=";
//
//        Boolean result = dateCompare(op1.getTime(), op2.getTime(), operator);
//        System.out.println(result.booleanValue());
//        Boolean result1 = dateBetween(op1.getTime(), op2.getTime(),op3.getTime(),"between");
//        System.out.println(result1.booleanValue());
//    }

}