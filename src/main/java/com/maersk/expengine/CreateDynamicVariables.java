package com.maersk.expengine;

import java.util.ArrayList;
import java.util.List;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Map;

public class CreateDynamicVariables {
    public static String genStrVariableName(int i) {
        return ("Str" + Character.toString((char) (i + 65)));
    }

    public static String genFloatVariableName(int i) {
        return ("Float" + Character.toString((char) (i + 65)));
    }

    public static String genIntVariableName(int i) {
        return ("Int" + Character.toString((char) (i + 65)));
    }

    public static Values createStrVariables(Values valueObj) {
        List ls = CreateDynamicVariables.allDoubleCoats(valueObj.getSourceStr());
        //System.out.println(ls.size());
        //System.out.println(lst.get(0));
        for (int i = 0; i < ls.size() / 2; i++) {
            List lst = CreateDynamicVariables.allDoubleCoats(valueObj.getSourceStr());
            String rep = valueObj.getSourceStr().substring(Integer.parseInt(lst.get(0).toString()) + 1, Integer.parseInt(lst.get(1).toString()));

            String varName = genStrVariableName(i);

            Map varMap = valueObj.getVariables();
            varMap.put(varName, rep);
            valueObj.setVariables(varMap);

            valueObj.setSourceStr(valueObj.getSourceStr().replace("\"" + rep + "\"", varName));
        }
        //System.out.println(valueObj.getSourceStr());
        return valueObj;
    }


    public static List<Integer> allDoubleCoats(String str) {
        List<Integer> loc = new ArrayList();
        int index = str.indexOf("\"");
        loc.add(index);
        while (index >= 0) {
            index = str.indexOf("\"", index + 1);
            loc.add(index);
        }
        return loc;
    }


    public static List<String> allNumeric(String str) {
        //String str="sdf<123&abc>123.4|sdf=256";
        List<String> lst = new ArrayList<String>();
        Pattern regex = Pattern.compile("[.0-9]+");//-?\\d+(,\\d+)*?\\.?\\d+?");
        Matcher matcher = regex.matcher(str);
        while (matcher.find()) {
            lst.add(matcher.group(0).toString());
        }
        return lst;
    }

    public static Values createNumericVariables(Values valueObj) {
        List ls = CreateDynamicVariables.allNumeric(valueObj.getSourceStr());
        //System.out.println(ls.size());
        //System.out.println(lst.get(0));
        //float need to be processed before integer as it contains all int values
        List floatLst = new ArrayList();
        List intLst = new ArrayList();

        if (ls != null) {


            for (int i = 0; i < ls.size(); i++) {
                String tmp = ls.get(i).toString();
                if (tmp.indexOf('.') == -1) {
                    intLst.add(tmp);
                } else {
                    floatLst.add(tmp);
                }
            }
            if (floatLst != null) {

                for (int i = 0; i < floatLst.size(); i++) {
                    String tmp = floatLst.get(i).toString();
                    String varName = genFloatVariableName(i);
                    Map varMap = valueObj.getVariables();
                    String rep = String.valueOf(floatLst.get(i));
                    varMap.put(varName, rep);
                    valueObj.setVariables(varMap);
                    valueObj.setSourceStr(valueObj.getSourceStr().replace(rep, varName));
                }
            }
            if (intLst != null) {
                for (int i = 0; i < intLst.size(); i++) {
                    String tmp = intLst.get(i).toString();
                    String varName = genIntVariableName(i);
                    Map varMap = valueObj.getVariables();
                    String rep = String.valueOf(intLst.get(i));
                    varMap.put(varName, rep);
                    valueObj.setVariables(varMap);
                    valueObj.setSourceStr(valueObj.getSourceStr().replace(rep, varName));
                }
            }
            //System.out.println(valueObj.getSourceStr());
        }
        return valueObj;
    }
}
//    public static void main(String arg[])
//    {
//        Values v=new Values();
//        v.setSourceStr("sdfds 345 sdfg #4.5");
//
//        System.out.println(createNumericVariables(v).getSourceStr());
//    }
//}
