package com.maersk.expengine;


import java.util.HashMap;
import java.util.Map;

public class Values{

    public String sourceStr;
    public Map variables;

    public Values() {
        this.variables=new HashMap();
    }

    public Values(Map variables) {
        this.variables = new HashMap();
    }

    public String getSourceStr() {
        return sourceStr;
    }

    public void setSourceStr(String source_str) {
        this.sourceStr = source_str;
    }

    public Map getVariables() {
        return variables;
    }

    public void setVariables(Map variables) {
        this.variables = variables;
    }


}
