//package com.maersk.expengine;
//
//import com.maersk.parsexp.*;
//import com.maersk.parsexp.expressions.Expression;
//import org.mvel2.*;
//import org.springframework.stereotype.Component;
//
//import java.io.Serializable;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.Map;
//
///*
//Author : Pranav Waila
//Date :   10/10/2017
//
//
// input : expression string & variable map
// output : error code, error message, result
// */
//@Component
//public class EvaluationEngine {
//
//    public static void main1(String args[]) {
//        //test("aaa<bbb&bbb>ccc");
//        //String source_expression="ccc<53.2";
//        //String source_expression="ccc<53.2&aaa=\"abd\"";
//        String source_expression="ccc+bbb>6.1&aaa=\"abd\"&ccc>2.3";
//        //String source_expression="ccc+bbb<6.9&aaa=\"abd\"";
//        //String source_expression = "aaa=\"abd\"";
//        //String source_expression = "aaa=\"abd\"";
//
//
//        EvaluationEngine evEng=new EvaluationEngine();
//
//        //System.out.println(source_expression);
//        Map mp = new HashMap();
//        //ValueObject vo1 = new ValueObject("aaa", "abd");
//        //ValueObject vo2 = new ValueObject("bbb", "3.1");
//        //ValueObject vo3 = new ValueObject("ccc", "3.3");
//        //mp.put("aaa", vo1);
//        //mp.put("bbb", vo2);
//        //mp.put("ccc", vo3);
//        mp.put("aaa", "abd");
//        mp.put("bbb", 3.1);
//        mp.put("ccc", 3.3);
//
//        Response res= evEng.evaluate(source_expression, mp);
//        int errorCode=res.getErrorCode();
//
//        if(errorCode==0){
//            System.out.println("Success");
//            System.out.println("ErrorCode :"+errorCode);
//            System.out.println("Result :"+res.getResult());
//        }
//        else {
//            System.out.println("Failure");
//            System.out.println("ErrorCode :"+errorCode);
//            System.out.println("Result :"+res.getErrorMessage());
//        }
//
//        /*
//        Object obj=MVEL.evalToBoolean(source_expression,mp);
//        System.out.println(obj);
//        */
//    }
//
//    public Response evaluate(String source_expression, Map mp)
//    {
//        Response exe_result = new Response();
//        try {
//            EvaluationWrapper ob = isValid(source_expression, mp);
//            EvaluationWrapper obj = evaluateExpression(ob.getValues(), mp);
//            exe_result = obj.getResponse();
//        }
//        catch (Exception e){
//            exe_result.setErrorCode(1);
//            exe_result.setErrorMessage("Expression is not in correct format");
//            exe_result.setResult("");
//        }
//        return exe_result;
//    }
//
//
//    public static EvaluationWrapper isValid(String source_expression, Map mp){
//        EvaluationWrapper ew=new EvaluationWrapper();
//        try {
//            DynamicVariableCreter dvc = new DynamicVariableCreter();
//            Values valueObj = dvc.generateDimVar(source_expression);
//            ew = check_valid_expression(valueObj,mp);
//        } catch (Exception e) {
//            //e.printStackTrace();
//            //ob="Expression is not in correct format";
//            Response res = new Response();
//            res.setErrorCode(1);
//            res.setErrorMessage("Expression is not in correct format");
//            res.setResult("");
//            ew.setResponse(res);
//        }
//        return ew;
//    }
//
//
//    public static EvaluationWrapper check_valid_expression(Values valueObj, Map mp) {
//        Lexer lexer = new Lexer(valueObj.getSourceStr());
//        Parser parser = new BantamParser(lexer);
//        EvaluationWrapper resp=new EvaluationWrapper();
//        Iterator it = valueObj.getVariables().entrySet().iterator();
//
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//            if (pair.getKey().toString().startsWith("Int")) {
//
//                mp.put(pair.getKey(), pair.getValue().toString());
//            } else if (pair.getKey().toString().startsWith("Float")) {
//                mp.put(pair.getKey(), pair.getValue().toString());
//            } else if (pair.getKey().toString().startsWith("Str")) {
//                mp.put(pair.getKey(),  pair.getValue().toString());
//            }
//        }
//
//
//        try {
//            Expression result = parser.parseExpression();
//            StringBuilder builder = new StringBuilder();
//            result.print(builder);
//
//
//            //added from this line to further
//            Map vars = new HashMap();
//            String str = builder.toString();
//            //System.out.println(str);
//            String[] lst = str.split("[(),=+*-/^~!?:<>&| ]");
//            for (String l : lst) {
//                if ((!l.equals("")) && (!l.equals(" "))) {
//                    vars.put(l, mp.get(l));
//                }
//            }
//            //replacing and or and equality operators with the appropriate one which evaluation supports
//            String sst = builder.toString();
//            //System.out.println(sst);
//            sst = sst.replace("&", "&&");
//            sst = sst.replace("|", "||");
//            sst = sst.replace("=", "==");
//            valueObj.setSourceStr(sst);
//            valueObj.setVariables(valueObj.getVariables());
//            resp.setValues(valueObj);
//
//        } catch (Exception ex) {
//            Response err_resp=new Response();
//            err_resp.setErrorMessage("Parse Exception");
//            err_resp.setErrorCode(1);
//            err_resp.setResult("");
//            resp.setResponse(err_resp);
//        }
//        return resp;
//    }
//
//
//    public static EvaluationWrapper evaluateExpression(Values valueObj, Map vars) {
//
//        EvaluationWrapper obj = new EvaluationWrapper();
//
//        Iterator it = valueObj.getVariables().entrySet().iterator();
//        String str = valueObj.getSourceStr();
//
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//            String key = pair.getKey().toString();
//            if (str.contains("Str")) {
//                str = str.replace(key, "'" + pair.getValue().toString()+ "'") ;
//
//            } else {
//                str = str.replace(key, pair.getValue().toString());
//
//            }
//        }
//        System.out.println("String to be evaluated :");
//        System.out.println(str);
//        //executing equation dynamicly
//        try {
//            //Object re=MVEL.eval(str, vars);
//            Serializable compiled = MVEL.compileExpression(str);
//            Object re = MVEL.executeExpression(compiled, vars);
//
//            //response object
//            Response respo=new Response();
//            if(re.equals(true) || re.equals(false)){
//                respo.setErrorCode(0);
//                respo.setErrorMessage("");
//                respo.setResult(re.toString());
//                obj.setResponse(respo);
//            }
//            else{
//                respo.setErrorCode(1);
//                respo.setResult("");
//                respo.setErrorMessage("execution result is not boolean type");
//                respo.setResult("");
//                obj.setResponse(respo);
//            }
//        }
//        catch (Exception e)
//        {
//            Response respo=new Response();
//            respo.setErrorCode(1);
//            respo.setResult(str);
//            respo.setErrorMessage("execution result is not boolean type");
//            respo.setResult("");
//            obj.setResponse(respo);
//        }
//
//
//
//        return obj;
//    }
//
//}
