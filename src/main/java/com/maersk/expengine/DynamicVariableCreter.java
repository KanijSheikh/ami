package com.maersk.expengine;

import java.util.HashMap;
import java.util.Map;


public class DynamicVariableCreter {
    public static Map gen_variables=new HashMap();
//    public static void main(String args[]){
//        String source="a>2.4&bad=\"dfd\"&gfd<7|jdh=\"hello\"";
//        DynamicVariableCreter dvc=new DynamicVariableCreter();
//        Values valueObj=dvc.generateDimVar(source);
//        System.out.println(valueObj.getSourceStr());
//    }

    public Values generateDimVar(String src){
        String source=src;
        Values valueObj=new Values();
        valueObj.setSourceStr(source);
        CreateDynamicVariables cdv=new CreateDynamicVariables();

        valueObj= cdv.createStrVariables(valueObj);
        valueObj=cdv.createNumericVariables(valueObj);
        return valueObj;
    }

}
