package com.maersk.optimizer;

import com.maersk.model.PurchaseOrderOp;
import com.maersk.service.PurchaseOrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collections;


public class KnapsackDP {

    static int cnt = 1;

    static final Logger LOG = LoggerFactory.getLogger(KnapsackDP.class);

    public static void Knapsack(int[][] opt, boolean[][] sol, int[] weight, int[] profit, int N, int W, boolean f) {
        for (int n = 1; n <= N; n++) {
            for (int w = 1; w <= W; w++) {

                // don't take item n
                int option1 = opt[n - 1][w];
                //if (option1 == 0 ) option1 = Integer.MIN_VALUE+1;
                // take item n
                int option2 = Integer.MIN_VALUE;
                if (weight[n] <= w)
                    option2 = profit[n] + opt[n - 1][w - weight[n]];

                if (option1 == option2) {

                    int cnt1 = 0;
                    for (int z = n - 1, v = w; z > 0; z--) {
                        if (sol[z][v]) {
                            cnt1++;
                            v = v - weight[z];
                        }
                    }
                    int cnt2 = 0;
                    for (int z = n - 1, v = w - weight[n]; z > 0; z--) {
                        if (sol[z][v]) {
                            cnt2++;
                            v = v - weight[z];
                        }
                    }
                    if (cnt1 < cnt2) {
                        sol[n][w] = false;
                    } else {
                        sol[n][w] = true;

                    }

                } else {
                    sol[n][w] = (option2 > option1);
                }
                // select better of two options
                opt[n][w] = Math.max(option1, option2);
            }
        }
    }


    public static void update(HashMap<Integer, List<Integer>> clone, Integer key, Integer newKey, List<Integer> completeConList) {

        //replacing the key with the new key in the final_set's clone.
        List<Integer> temp = new ArrayList<Integer>();
        temp = clone.get(key);
        clone.remove(key);
        clone.put(newKey, temp);
        completeConList.remove(key);
        completeConList.add(newKey);
    }

    public static HashMap<Integer, List<Integer>> reShuffle(Map<Integer, Double> poVol, Map<Integer, Double> poVal,
                                                            HashMap<Integer, List<Integer>> finalSet, Map<Integer, Integer> containerVolume,
                                                            Map<Integer, Integer> containerProfit, int conMaxFilRate, List<Integer> completeConList) {
        HashMap<Integer, List<Integer>> fsClone = new HashMap<Integer, List<Integer>>(finalSet);
        //resuffling the finalSet
        for (Integer key : finalSet.keySet()) {
            double sumVol = 0.0;
            double sumWgt = 0;
            for (int i : finalSet.get(key)) {
                sumVol = sumVol + poVol.get(i);
                sumWgt = sumWgt + poVal.get(i);
            }
            int newKey = 0;
            //int total_con = final_set.size();
            //int avl_con_index = container_vol.length-total_con;
            int previousContainerVol = containerVolume.get(key);
            //again looping through the available containers to find a better fit for the set of pos.
            for (Integer o : containerVolume.keySet()) {

                if (!completeConList.contains(o)) {

                    if (containerVolume.get(o) < previousContainerVol) {
                        if ((sumWgt <= (containerVolume.get(o)))) { //&& sum >= (container_vol[o]*min_vol_percent)/100) {
                            LOG.info("Total PO volume: " + sumWgt);
                            LOG.info("REPLACE:" + key + " having volume: " + previousContainerVol + "  BY:" + o + " having volume:" + containerVolume.get(o));

                            newKey = o;
                            previousContainerVol = containerVolume.get(o);
                        }
                    }
                }
            }
            //code for minvol.
            /*
            if(sum_vol < (container_vvol.get(new_key)*min_vol_percent)/100) {
				//remove pos from done
				//remove container from done_con
			}
			*/
            if (newKey != 0) update(fsClone, key, newKey, completeConList);
        }
        //printHM(fs_clone);
        //com.optimizer.UpdateDB.updateTable(fs_clone);
        return fsClone;
    }


    public static void printFS(Map<String, List<Integer>> finalSet, Map<Integer, Double> poVol) {
        for (String conKey : finalSet.keySet()) {
            for (Integer poKey : finalSet.get(conKey)) {
                double containerVol = Double.parseDouble(conKey.split("-")[0]);
                double poVolume = poVol.get(poKey);
                LOG.info("container id: " + conKey + " container max volume: " + containerVol + " po_key: "
                        + poKey + " po_vol: " + poVolume);
            }
        }
    }

    public static boolean CheckForOne(List<Integer> avlContainers, Map<Integer, Double> poVol, Map<Integer, Integer> containerVolume,
                                      List<Integer> completePoList, Map<Integer, List<Integer>> finalSet, List<Integer> completeConList) {
        List<Integer> remPos = new ArrayList<Integer>();
        List<Integer> avlCon = new ArrayList<>();

        for (Integer conKey : containerVolume.keySet()) {
            if (!finalSet.containsKey(conKey)) {
                avlCon.add(conKey);
            }
        }

        int sumRemPo = 0;
        for (Integer poKey : poVol.keySet()) {
            if (!completePoList.contains(poKey)) {
                remPos.add(poKey);
                sumRemPo = sumRemPo + ((int) ((Math.ceil((poVol.get(poKey) * 100) / 100))));
            }
        }
        LOG.info("Entered CheckForOne with PO Sum:" + sumRemPo);
        int preContainer = 0;
        int finalContainer = 0;
        for (Integer conKey : avlCon) {
            if (sumRemPo <= containerVolume.get(conKey)
                    && (containerVolume.get(conKey) < preContainer || preContainer == 0)) {
                finalContainer = conKey;
                preContainer = containerVolume.get(conKey);
                //LOG.info("container volume:"+container_volume.get(con_key)+" container key: "+con_key);
            }

            //LOG.info("else container volume:"+container_volume.get(con_key)+" else container key: "+con_key);
        }
        //if any container is assigned to final_container that means sum of pos are small enough to fit
        // in a single available container.
        if (finalContainer > 0) {
            finalSet.put(finalContainer, remPos);
            completeConList.add(finalContainer);
            LOG.info(finalContainer + " : " + containerVolume.get(finalContainer) + " : container is added to final_set ");
            //printFS(final_set, po_vol);
            return true;
        } else {
            LOG.info("Won't fit in a single container. ");
            return false;
        }
    }


    public static LinkedHashMap<Integer, Integer> sortHashMapByValues(
            HashMap<Integer, Integer> passedMap) {
        List<Integer> mapKeys = new ArrayList<>(passedMap.keySet());
        List<Integer> mapValues = new ArrayList<>(passedMap.values());
        Collections.sort(mapValues, Collections.reverseOrder());
        Collections.sort(mapKeys, Collections.reverseOrder());

        LinkedHashMap<Integer, Integer> sortedMap =
                new LinkedHashMap<>();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<Integer> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                Integer key = keyIt.next();
                Integer comp1 = passedMap.get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }


    public static HashMap<Integer, List<Integer>> Optimizer(HashSet<PurchaseOrderOp> purchaseOrders1,
                                                            HashMap<Integer, Integer> containerVolume,
                                                            HashMap<Integer, Integer> containerProfit, int conMaxFilRate) throws IOException {


//		Map<Integer,Integer> container_volume = new LinkedHashMap<Integer,Integer>();
// 		Map<Integer,Integer> container_profit = new LinkedHashMap<Integer,Integer>();

		 /*
        container_volume.put(10001, 100);
		*/
        List<Integer> avlContainers = new ArrayList<Integer>();
        int[] tempContainers = new int[containerVolume.size()];
        int[] profitContainers = new int[containerVolume.size()];
        int l = 0;
        for (Integer conKey : containerVolume.keySet()) {
            tempContainers[l] = containerVolume.get(conKey);
            profitContainers[l] = containerProfit.get(conKey);
            avlContainers.add(conKey);
            l++;
        }

        Map<Integer, Double> poVol = new LinkedHashMap<Integer, Double>();
        Map<Integer, Double> poVal = new LinkedHashMap<Integer, Double>();


        for (PurchaseOrderOp purchaseOrders : purchaseOrders1) {
            if (purchaseOrders.getConfirmRemove().equals("n")) {
                poVol.put(purchaseOrders.getId(), purchaseOrders.getBookedCbm() * 100);
                poVal.put(purchaseOrders.getId(), purchaseOrders.getBookedCbm() * 100);
            }

            //po_vol.put(purchaseOrders.getId(), ((Math.ceil(purchaseOrders.getBookedCbm() * 100))));
        }
        /*
        po_vol.put(101, 91.0);

		po_val.put(101, 91.0);

		*/
        int N = tempContainers.length; // number of items
        int W = 0; // maximum weight of knapsack
        int ww = 0;
        for (Integer poKey : poVol.keySet()) {
            ww = ww + ((int) ((Math.ceil((poVal.get(poKey) * 100) / 100))));
        }

        W = 3300 * ((ww / 3300) + 1);
        LOG.info("Started loop for PO volume: " + W);
        int[] profit = new int[N + 1];
        int[] weight = new int[N + 1];

        // generate random instance, items 1..N
        for (int n = 1; n <= N; n++) {
            profit[n] = profitContainers[n - 1];
            weight[n] = tempContainers[n - 1];
        }

        // final_set = con_key, po_keys
        HashMap<Integer, List<Integer>> finalSet = new HashMap<Integer, List<Integer>>();

        // mapping with index with po_keys.
        List<Integer> mappedPos = new ArrayList<Integer>();

        List<Integer> pos = new ArrayList<Integer>();

        //List<Integer> avl_containers = new ArrayList<Integer>();
        List<Integer> completePoList = new ArrayList<Integer>();
        List<Integer> completeConList = new ArrayList<Integer>();

        // checking if sum of all the pos can fit inside single available container or not.
        boolean flag = CheckForOne(avlContainers, poVol, containerVolume, completePoList, finalSet, completeConList);
        //LOG.info("flag: "+flag);
        if (flag == true) {
            return finalSet;
        }

        avlContainers = new ArrayList<Integer>();
        int poN = poVol.size();

        int[] poProfit = new int[poN + 1];
        int[] poWeight = new int[poN + 1];

        // creating mapping between the index and po_keys.

        for (Integer poKey : poVol.keySet()) {
            mappedPos.add(poKey);
        }
        List<Integer> mappedCon = new ArrayList<Integer>();
        for (Integer conKey : containerVolume.keySet()) {
            mappedCon.add(conKey);
        }

        for (int m = 1; m <= poN; m++) {
            poProfit[m] = ((int) ((Math.ceil((poVal.get(mappedPos.get(m - 1)) * 100) / 100))));
            poWeight[m] = ((int) ((Math.ceil((poVol.get(mappedPos.get(m - 1)) * 100) / 100))));
            //LOG.info("profit for every po:" + po_profit[m]);

        }

        // sum of pos are more than any available container's max vol then run knapsack
        // again.
        while (completePoList.size() != poVol.size()) {

            List<Integer> containers = new ArrayList<Integer>();
            avlContainers = new ArrayList<Integer>();
            // opt[n][w] = max profit of packing items 1..n with weight limit w
            // sol[n][w] = does opt solution to pack items 1..n with weight limit w include
            // item n?
            int[][] opt = new int[N + 1][W + 1];
            boolean[][] sol = new boolean[N + 1][W + 1];
            boolean f = true;
            // to find the containers required to fill almost all the pos.
            Knapsack(opt, sol, weight, profit, N, W, f);

            // determine which items to take
            boolean[] take = new boolean[N + 1];
            for (int n = N, w = W; n > 0; n--) {
                if (sol[n][w]) {
                    take[n] = true;
                    // adding container index
                    //containers.add(n);
                    containers.add(mappedCon.get(n - 1));
                    //completeConList.add(mappedCon.get(n-1));
                    w = w - weight[n];
                } else {
                    //avl_containers.add(n);
                    avlContainers.add(mappedCon.get(n - 1));
                    //avl_containers_vol.add(temp_containers[n - 1]);
                    take[n] = false;
                }
            }
            //sorting the containers...
            LinkedHashMap<Integer, Integer> sortedCon = new LinkedHashMap<>();
            for (Integer conKey : containers) {
                sortedCon.put(conKey, containerVolume.get(conKey));
            }
            sortedCon = sortHashMapByValues(sortedCon);
            containers = new ArrayList<>();
            for (Integer conKey : sortedCon.keySet()) {
                containers.add(conKey);
                LOG.info("Container key: " + conKey + " volume: " + sortedCon.get(conKey));
            }
            mappedCon = new ArrayList<Integer>();
            for (Integer conKey : avlContainers) {
                mappedCon.add(conKey);
            }


            // print results
            LOG.info("Containers(with take:True) picked from the available container list.");
            LOG.info("Sno" + "\t" + "Profit" + "\t" + "Volume" + "\t" + "Picked");
            for (int n = 1; n <= N; n++) {
                LOG.info(n + "\t" + profit[n] + "\t" + weight[n] + "\t" + take[n]);
            }

            N = avlContainers.size();
            profit = new int[N + 1];
            weight = new int[N + 1];

            for (int m = 1; m <= N; m++) {
                profit[m] = containerProfit.get(mappedCon.get(m - 1));
                weight[m] = containerVolume.get(mappedCon.get(m - 1));

            }
            //this loop is to fill all the available pos inside selected containers.
            for (Integer conKey : containers) {
                LOG.info("Started PO assignment into container: " + conKey);
                List<Integer> donePos = new ArrayList<Integer>();
                pos = new ArrayList<Integer>();
                int keyConVolume = containerVolume.get(conKey);

                int poW = containerVolume.get(conKey);

                // here goes the code for per container for all the pos.
                int[][] optPo = new int[poN + 1][poW + 1];
                boolean[][] solPo = new boolean[poN + 1][poW + 1];

                f = false;
                // knapsack to find best fit for pos per container.
                Knapsack(optPo, solPo, poWeight, poProfit, poN, poW, f);

                boolean poTemp[] = new boolean[poN + 1];
                for (int n = poN, w = poW; n > 0; n--) {
                    if (solPo[n][w]) {
                        poTemp[n] = true;
                        LOG.info("index" + (n - 1) + " po key getting added also in complete po list :" + mappedPos.get(n - 1));
                        donePos.add(mappedPos.get(n - 1));
                        completePoList.add(mappedPos.get(n - 1));
                        w = w - poWeight[n];
                    } else {
                        LOG.info("remaining pos: " + mappedPos.get(n - 1));
                        pos.add(mappedPos.get(n - 1));
                        poTemp[n] = false;
                    }
                }

                if (!donePos.isEmpty()) {
                    finalSet.put(conKey, donePos);
                    completeConList.add(conKey);
                    LOG.info(conKey + " : " + keyConVolume + " : container is added to final_set ");
                }

                cnt++;
                mappedPos = new ArrayList<Integer>();
                for (Integer po_key : pos) {
                    mappedPos.add(po_key);
                }

                poN = pos.size();
                poProfit = new int[poN + 1];
                poWeight = new int[poN + 1];

                for (int m = 1; m <= poN; m++) {
                    poProfit[m] = ((int) ((Math.ceil((poVal.get(mappedPos.get(m - 1)) * 100) / 100))));
                    poWeight[m] = ((int) ((Math.ceil((poVol.get(mappedPos.get(m - 1)) * 100) / 100))));

                }
                boolean flag1 = CheckForOne(avlContainers, poVol, containerVolume, completePoList, finalSet, completeConList);
                if (flag1 == true) {
                    //LOG.info("flag2: "+flag1);
                    return reShuffle(poVol, poVal, finalSet, containerVolume, containerProfit, conMaxFilRate, completeConList);
                    //return final_set;
                }

            }
            W = 0;
            for (Integer poKey : poVol.keySet()) {
                if (!completePoList.contains(poKey))
                    W = W + ((int) ((Math.ceil((poVal.get(poKey) * 100) / 100))));
            }
            if (completePoList.size() == poVol.size()) {
                //printFS(final_set,po_vol);
                break;
            }

            boolean flag2 = CheckForOne(avlContainers, poVol, containerVolume, completePoList, finalSet, completeConList);
            if (flag2 == true) {
                //LOG.info("flag2: "+flag2);
                return reShuffle(poVol, poVal, finalSet, containerVolume, containerProfit, conMaxFilRate, completeConList);
                //return final_set;
            }
            if (avlContainers.size() == 0) {
                LOG.info("Not Enough Containers to fill.");
                //printFS(final_set,po_vol);
                break;
            }
        }
        //return final_set;
        return reShuffle(poVol, poVal, finalSet, containerVolume, containerProfit, conMaxFilRate, completeConList);
    }
}
