package com.maersk.optimizer;

import com.maersk.model.MatchingCriteria;
import com.maersk.model.PurchaseOrder;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.service.PurchaseOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@Component
public class LoadPlanner {

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    public void update(HashMap<Integer, List<Integer>> clone, Integer key, Integer new_key, List<Integer> done_con) {
        //System.out.println("enter update");
        //replacing the key with the new key in the final_set's clone.
        List<Integer> temp = new ArrayList<Integer>();
        temp = clone.get(key);
        clone.remove(key);
        clone.put(new_key, temp);
        done_con.remove(key);
        done_con.add(new_key);
    }

    public HashMap<Integer, List<Integer>> reShuffle(Map<Integer, Double> vvol, Map<Integer, Double> wwgt, HashMap<Integer, List<Integer>> final_set, Map<Integer, Integer> container_vvol, Map<Integer, Integer> container_wwgt, int max_vol_percent, int min_vol_percent, List<Integer> done_con) {
        HashMap<Integer, List<Integer>> fs_clone = new HashMap<Integer, List<Integer>>(final_set);
        //resuffling the final_set
        for (Integer key : final_set.keySet()) {
            double sum_vol = 0.0;
            double sum_wgt = 0;
            for (int i : final_set.get(key)) {
                sum_vol = sum_vol + vvol.get(i);
                sum_wgt = sum_wgt + wwgt.get(i);
            }
            int new_key = 0;
            //int total_con = final_set.size();
            //int avl_con_index = container_vol.length-total_con;

            //again looping through the available containers to find a better fit for the set of pos.
            for (Integer o : container_vvol.keySet()) {

                if (!done_con.contains(o)) {

                    if (container_vvol.get(o) < container_vvol.get(key)) {
                        if ((sum_vol <= (container_vvol.get(o) * max_vol_percent) / 100) && (sum_wgt <= (container_wwgt.get(o)))) { //&& sum >= (container_vol[o]*min_vol_percent)/100) {

                            new_key = o;
                        }
                    }
                }
            }
            //code for minvol.
            /*
            if(sum_vol < (container_vvol.get(new_key)*min_vol_percent)/100) {
				//remove pos from done
				//remove container from done_con
			}
			*/
            if (new_key != 0) update(fs_clone, key, new_key, done_con);
        }
        //printHM(fs_clone);
        //com.optimizer.UpdateDB.updateTable(fs_clone);
        return fs_clone;
    }

//    public void printHM(HashMap<Integer, List<Integer>> final_set) {
//        //System.out.println("enter print");
//        for (Integer con_key : final_set.keySet()) {
//            for (Integer po_key : final_set.get(con_key)) {
//                System.out.println(po_key + "_" + con_key);
//            }
//        }
//    }

//    public void printRemainingPo(List<Integer> done, int po_cnt) {
//        List<Integer> remaining = new ArrayList<Integer>();
//        for (int i = 0; i < po_cnt; i++) {
//            if (!done.contains(i)) {
//                remaining.add(i);
//                System.out.println(i);
//            }
//        }
//    }

    public void readFile(Map<Integer, Double> vvol, Map<Integer, Double> wwgt, HashSet<PurchaseOrderOp> purchaseOrders1) throws IOException {
        List<com.maersk.optimizer.PurchaseOrder> lst = new ArrayList<com.maersk.optimizer.PurchaseOrder>();

        for (PurchaseOrderOp purchaseOrder : purchaseOrders1) {
            if (purchaseOrder.getConfirmRemove().equals("n")) {
                Integer frsCol = purchaseOrder.getId();
                Double secCol = purchaseOrder.getBookedCbm();
                Double thdCol = purchaseOrder.getBookedWeight();
                com.maersk.optimizer.PurchaseOrder tmpRow = new com.maersk.optimizer.PurchaseOrder(frsCol, secCol, thdCol);
                lst.add(tmpRow);
            }
        }

        Collections.sort(lst);

        for (int i = lst.size() - 1; i >= 0; i--) {
            vvol.put((Integer) lst.get(i).getId(), lst.get(i).getVol());
            wwgt.put((Integer) lst.get(i).getId(), lst.get(i).getWgt());
        }
    }

    public HashMap<Integer, List<Integer>> Optimizer(HashSet<PurchaseOrderOp> purchaseOrders1) throws IOException {
        //get vol and wgt from the po table
        //Collection<PurchaseOrder> purchaseOrders = purchaseOrderService.getPurchaseOrders();
        Map<Integer, Double> vvol = new LinkedHashMap<Integer, Double>();
        Map<Integer, Double> wwgt = new LinkedHashMap<Integer, Double>();
        readFile(vvol, wwgt, purchaseOrders1);

        int max_vol_percent = 90;
        int min_vol_percent = 10;

        //get available container details.
        //int[] container_vol = {50, 50, 50, 40, 40, 35};
        int[] container_vol = {
                87, 87, 87, 65, 65, 37, 37};
        Map<Integer, Integer> container_vvol = new LinkedHashMap<Integer, Integer>();
        for (int i = 0; i < container_vol.length; i++) {
            container_vvol.put(i + 201, container_vol[i]);
        }

        int[] container_wgt = {11000, 11000, 11000, 10000, 10000, 8000, 8000};
        //int[] container_wgt = {100, 100, 100, 110, 110, 110};
        Map<Integer, Integer> container_wwgt = new LinkedHashMap<Integer, Integer>();
        for (int i = 0; i < container_wgt.length; i++) {
            container_wwgt.put(i + 201, container_wgt[i]);
        }

        //int [] done = new int[vol.length];
        List<Integer> done = new ArrayList<Integer>();
        List<Integer> done_con = new ArrayList<Integer>();
        HashMap<Integer, List<Integer>> final_set = new HashMap<Integer, List<Integer>>();

        //Arrays.sort(container_vol);
        //Arrays.sort(container_wgt);
        //looping through the available containers.
        for (Integer con_key : container_vvol.keySet()) {

            int cvol = container_vvol.get(con_key);
            double cmax_vol = (cvol * max_vol_percent) / 100;
            //double cmin_vol = (cvol*min_vol_percent)/100;
            int cwgt = container_wwgt.get(con_key);
            List<Integer> po_keys = new ArrayList<Integer>();
            double sum_vol = 0.0;
            double sum_wgt = 0.0;
            //looping through the pos volume.
            for (Integer po_key : vvol.keySet()) {
                //if po is already been added to lp then skip it.
                if (done.contains(po_key)) {
                    continue;
                }
                double po_vol = vvol.get(po_key);
                double po_wgt = wwgt.get(po_key);
                sum_vol = sum_vol + po_vol;
                sum_wgt = sum_wgt + po_wgt;
                if (sum_vol <= cmax_vol && sum_wgt <= cwgt) {
                    //put key : size and value : all the indexes, into hashmap
                    //done will have all the po keys that are added to a load plan
                    //indexs will have all the po keys per container.
                    po_keys.add(po_key);
                    done.add(po_key);
                } else {
                    sum_wgt = sum_wgt - po_wgt;
                    sum_vol = sum_vol - po_vol;
                }

            }

            final_set.put(con_key, po_keys);
            done_con.add(con_key);
            //if all the po are assigned then exit.
            if (done.size() == vvol.size()) {
                break;
            }
        }
        //final_set will contain key : sno_vol_wgt and value : row no for pos.
        return reShuffle(vvol, wwgt, final_set, container_vvol, container_wwgt, max_vol_percent, min_vol_percent, done_con);

    }

}
