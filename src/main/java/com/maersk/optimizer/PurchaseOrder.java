package com.maersk.optimizer;

public class PurchaseOrder implements Comparable<PurchaseOrder> {
    public final Integer id;
    public final Double vol;
    public final Double wgt;


    public Integer getId() {
        return id;
    }

    public Double getVol() {
        return vol;
    }

    public Double getWgt() {
        return wgt;
    }


    public PurchaseOrder(Integer id, Double vol, Double wgt) {
        this.id = id;
        this.vol = vol;
        this.wgt = wgt;
    }

    public int compareTo(PurchaseOrder other) {
        if (this.vol == other.vol) {
            return new Double(this.wgt).compareTo(other.wgt);
        }

        return new Double(this.vol).compareTo(other.vol);
    }

    @Override
    public String toString() {
        return "Row [id=" + id + ", vol=" + vol + ", wgt=" + wgt + "]";
    }

}