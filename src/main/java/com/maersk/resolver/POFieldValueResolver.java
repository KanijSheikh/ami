package com.maersk.resolver;

import com.maersk.expengine.eval.Variable;
import com.maersk.model.PurchaseOrder;
import com.maersk.model.PurchaseOrderOp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class POFieldValueResolver {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    public static String resolve(Variable field, HashSet<PurchaseOrderOp> purchaseOrders) {

        switch (field.getName()) {
            case "vol":
                    Double vol = 0.0;
                    for (PurchaseOrderOp p : purchaseOrders) {
                        vol += p.getBookedCbm();
                    }
                    return vol.toString();

            case "wt":
                    Double wt = 0.0;
                    for (PurchaseOrderOp p : purchaseOrders) {
                        wt += p.getBookedWeight();
                    }
                    return wt.toString();
            case "qty":
                    Integer qty = 0;
                    for (PurchaseOrderOp p : purchaseOrders) {
                        qty += p.getBookedQuantity();
                    }
                    return qty.toString();
        }
        return field.getValue();
    }

    public static String resolve(Variable field,PurchaseOrderOp purchaseOrder) {

        switch (field.getName()) {

            case "aaFlag":
                return purchaseOrder.getPoLineReference1();
            case "demandKey":
                return purchaseOrder.getPoLineReference2();
            case "poLineReference3":
                return purchaseOrder.getPoLineReference3();
            case "poLineReference4":
                return purchaseOrder.getPoLineReference4();
            case "poLineReference5":
                return purchaseOrder.getPoLineReference5();
            case "poLineReference6":
                return purchaseOrder.getPoLineReference6();
            case "poLineReference7":
                return purchaseOrder.getPoLineReference7();
            case "poLineReference8":
                return purchaseOrder.getPoLineReference8();
            case "poLineReference9":
                return purchaseOrder.getPoLineReference9();
            case "poLineReference10":
                return purchaseOrder.getPoLineReference10();
            case "poLineReference11":
                return purchaseOrder.getPoLineReference11();
            case "poLineReference12":
                return purchaseOrder.getPoLineReference12();
            case "poLineReference13":
                return purchaseOrder.getPoLineReference13();
            case "poLineReference14":
                return purchaseOrder.getPoLineReference14();
            case "poLineReference15":
                return purchaseOrder.getPoLineReference15();
            case "poLineReference16":
                return purchaseOrder.getPoLineReference16();
            case "poLineReference17":
                return purchaseOrder.getPoLineReference17();
            case "poLineReference18":
                return purchaseOrder.getPoLineReference18();
            case "poLineReference19":
                return purchaseOrder.getPoLineReference19();
            case "poLineReference20":
                return purchaseOrder.getPoLineReference20();
            case "orderType":
                return purchaseOrder.getOrderType();
            case "podCountry":
                return purchaseOrder.getPodCountry();
            case "podProvince":
                return purchaseOrder.getPodProvince();
            case "podCity":
                return purchaseOrder.getPodCity();
            case "porCountry":
                return purchaseOrder.getPorCountry();
            case "porProvince":
                return purchaseOrder.getPorProvince();
            case "porCity":
                return purchaseOrder.getPorCity();
            case "plant":
                return purchaseOrder.getPlant();
            case "poNumber":
                return purchaseOrder.getPoNumber();
            case "poLine":
                return purchaseOrder.getPoLine();
            case "productTypeCode":
                return purchaseOrder.getProductTypeCode();
            case "productType":
                return purchaseOrder.getProductType();
            case "pslv":
                return purchaseOrder.getPslv();
            case "shipto":
                return purchaseOrder.getShipto();
            case "shipper":
                return purchaseOrder.getShipper();
            case "skuNumber":
                return purchaseOrder.getSkuNumber();
            case "vendor":
                return purchaseOrder.getVendoe();
            case "carrier":
                return purchaseOrder.getCarrier();
            case "bookedWeight":
                return getString(purchaseOrder.getBookedWeight());
            case "expectedCargoReceiptDate":
                return  formatDate(purchaseOrder.getExpectedCargoReceiptDate());
            case "expectedDeliveryDate":
                return  formatDate(purchaseOrder.getExpectedDeliveryDate());
            case "actualRecieptDate":
                return  formatDate(purchaseOrder.getActualRecieptDate());
            case "bookedCartons":
                return getString(purchaseOrder.getBookedCartons());
            case "bookedCbm":
                return getString(purchaseOrder.getBookedCbm());
            case "bookedQuantity":
                return getString(purchaseOrder.getBookedQuantity());
            case "consignee":
                return purchaseOrder.getConsignee();
            case "expectedCargoReceiptWeek":
                return getString(purchaseOrder.getExpectedCargoReceiptWeek());
            case "latestDeliveryDate":
                return  formatDate(purchaseOrder.getLatestDeliveryDate());
            case "latestCargoReceiptDate":
                return  formatDate(purchaseOrder.getLatestCargoReceiptDate());
            case "eta":
                return  formatDate(purchaseOrder.getEta());
            case "etd":
                return  formatDate(purchaseOrder.getEtd());
            case "receivedCatons":
                return getString(purchaseOrder.getReceivedCatons());
            case "receivedCbm":
                return getString(purchaseOrder.getReceivedCbm());
            case "receivedQuantity":
                return getString(purchaseOrder.getReceivedQuantity());
            case "receivedWeight":
                return getString(purchaseOrder.getReceivedWeight());
            case "soReference1":
                return purchaseOrder.getSoReference1();
            case "soReference2":
                return purchaseOrder.getSoReference2();
            case "soReference3":
                return purchaseOrder.getSoReference3();
            case "soReference4":
                return purchaseOrder.getSoReference4();
            case "soReference5":
                return purchaseOrder.getSoReference5();
            case "soReference6":
                return purchaseOrder.getSoReference6();
            case "soReference7":
                return purchaseOrder.getSoReference7();
            case "soReference8":
                return purchaseOrder.getSoReference8();
            case "soReference9":
                return purchaseOrder.getSoReference9();
            case "soReference10":
                return purchaseOrder.getSoReference10();
            case "soReference11":
                return purchaseOrder.getSoReference11();
            case "soReference12":
                return purchaseOrder.getSoReference12();
            case "soReference13":
                return purchaseOrder.getSoReference13();
            case "soReference14":
                return purchaseOrder.getSoReference14();
            case "soReference15":
                return purchaseOrder.getSoReference15();
            case "soReference16":
                return purchaseOrder.getSoReference16();
            case "soReference17":
                return purchaseOrder.getSoReference17();
            case "soReference18":
                return purchaseOrder.getSoReference18();
            case "soReference19":
                return purchaseOrder.getSoReference19();
            case "soReference20":
                return purchaseOrder.getSoReference20();
            case "currentDate":
                return formatDate(new Date());
            case "destinationShipmentType":
                return purchaseOrder.getDestinationShipmentType();
            case "dischargePortCity":
                return purchaseOrder.getLoadPortCity();
            case "dischargePortCountry":
                return purchaseOrder.getDischargePortCountry();
            case "dischargePortProvince":
                return purchaseOrder.getDischargePortProvince();
            case "loadPortCity":
                return purchaseOrder.getLoadPortCity();
            case "loadPortCountry":
                return purchaseOrder.getLoadPortCountry();
            case "loadPortRegion":
                return purchaseOrder.getLoadPortRegion();
            case "loadPortProvince":
                return purchaseOrder.getLoadPortProvince();
            case "originShipmentType":
                return purchaseOrder.getOriginShipmentType();
            case "shipmentStatus":
                return purchaseOrder.getShipmentStatus();
            case "modeOfTransport":
                return purchaseOrder.getModeOfTransport();
            case "originServiceType":
                return purchaseOrder.getOriginServiceType();
            case "bookingNumber":
                return purchaseOrder.getBookingNumber();


        }
        return field.getValue();
    }

    private static String formatDate(Date date){
        return  date != null ? DATE_FORMAT.format(date) : null;
    }

    private static String getString(Double d){
        return  d != null ? d.toString() : null;
    }

    private static String getString(Integer d){
        return  d != null ? d.toString() : null;
    }
}
