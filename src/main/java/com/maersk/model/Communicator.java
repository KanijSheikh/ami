package com.maersk.model;


import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "communicator")
@DynamicUpdate(true)
public class Communicator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "communication_id")
    private Integer id;

    @Column(name = "message")
    private String message;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_time")
    private Date createdTime;


    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "loadplan_id")
    private String loadplan_id;



    public Communicator() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLoadplan_id() {
        return loadplan_id;
    }

    public void setLoadplan_id(String loadplan_id) {
        this.loadplan_id = loadplan_id;
    }
}
