package com.maersk.model;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by kanij on 10/17/2017.
 */
@Entity
@Table(name = "PO_ORDERS")
public class PurchaseOrder {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "PO_ID")
    private Integer id;

    @Column(name = "PO_Line_Reference1")
    private String poLineReference1;

    @Column(name = "PO_Line_Reference2")
    private String poLineReference2;

    @Column(name = "PO_Line_Reference3")
    private String poLineReference3;

    @Column(name = "PO_Line_Reference4")
    private String poLineReference4;

    @Column(name = "PO_Line_Reference5")
    private String poLineReference5;

    @Column(name = "PO_Line_Reference6")
    private String poLineReference6;

    @Column(name = "PO_Line_Reference7")
    private String poLineReference7;

    @Column(name = "PO_Line_Reference8")
    private String poLineReference8;

    @Column(name = "PO_Line_Reference9")
    private String poLineReference9;

    @Column(name = "PO_Line_Reference10")
    private String poLineReference10;

    @Column(name = "PO_Line_Reference11")
    private String poLineReference11;

    @Column(name = "PO_Line_Reference12")
    private String poLineReference12;

    @Column(name = "PO_Line_Reference13")
    private String poLineReference13;

    @Column(name = "PO_Line_Reference14")
    private String poLineReference14;

    @Column(name = "PO_Line_Reference15")
    private String poLineReference15;

    @Column(name = "PO_Line_Reference16")
    private String poLineReference16;

    @Column(name = "PO_Line_Reference17")
    private String poLineReference17;

    @Column(name = "PO_Line_Reference18")
    private String poLineReference18;

    @Column(name = "PO_Line_Reference19")
    private String poLineReference19;

    @Column(name = "PO_Line_Reference20")
    private String poLineReference20;

    @Column(name = "Actual_Receipt_Date")
    private Date actualRecieptDate;

    @Column(name = "Booked_Cartons")
    private Integer bookedCartons;

    @Column(name = "Booked_CBM")
    private Double bookedCbm;

    @Column(name = "Booked_Quantity")
    private Integer bookedQuantity;

    @Column(name = "Booked_Weight")
    private Double bookedWeight;

    @Column(name = "Consignee")
    private String consignee;

    @Column(name = "Expected_Cargo_Receipt_Date")
    private Date expectedCargoReceiptDate;

    @Column(name = "Expected_Cargo_Receipt_Week")
    private Integer expectedCargoReceiptWeek;

    @Column(name = "Expected_Delivery_Date")
    private Date expectedDeliveryDate;

    @Column(name = "Latest_Delivery_Date")
    private Date latestDeliveryDate;

    @Column(name = "Latest_Cargo_Receipt_Date")
    private Date latestCargoReceiptDate;

    @Column(name = "Order_Type")
    private String orderType;

    @Column(name = "POD_Country")
    private String podCountry;

    @Column(name = "POD_Province")
    private String podProvince;

    @Column(name = "POD_City")
    private String podCity;

    @Column(name = "POR_Country")
    private String porCountry;

    @Column(name = "POR_Province")
    private String porProvince;

    @Column(name = "POR_City")
    private String porCity;

    @Column(name = "Plant")
    private String plant;

    @Column(name = "PO_Number")
    private String poNumber;

    @Column(name = "PO_Line")
    private String poLine;

    @Column(name = "Product_Type_Code")
    private String productTypeCode;

    @Column(name = "Product_Type")
    private String productType;

    @Column(name = "PSLV")
    private String pslv;

    @Column(name = "Received_Catons")
    private Integer receivedCatons;

    @Column(name = "Received_CBM")
    private Double receivedCbm;

    @Column(name = "Received_Quantity")
    private Integer receivedQuantity;

    @Column(name = "Received_Weight")
    private Double receivedWeight;

    @Column(name = "Shipto")
    private String shipto;

    @Column(name = "Shipper")
    private String shipper;

    @Column(name = "SKU_Number")
    private String skuNumber;

    @Column(name = "Vendor")
    private String vendoe;

    @Column(name = "SO_Reference1")
    private String soReference1;

    @Column(name = "SO_Reference2")
    private String soReference2;

    @Column(name = "SO_Reference3")
    private String soReference3;

    @Column(name = "SO_Reference4")
    private String soReference4;

    @Column(name = "SO_Reference5")
    private String soReference5;

    @Column(name = "SO_Reference6")
    private String soReference6;

    @Column(name = "SO_Reference7")
    private String soReference7;

    @Column(name = "SO_Reference8")
    private String soReference8;

    @Column(name = "SO_Reference9")
    private String soReference9;

    @Column(name = "SO_Reference10")
    private String soReference10;

    @Column(name = "SO_Reference11")
    private String soReference11;

    @Column(name = "SO_Reference12")
    private String soReference12;

    @Column(name = "SO_Reference13")
    private String soReference13;

    @Column(name = "SO_Reference14")
    private String soReference14;

    @Column(name = "SO_Reference15")
    private String soReference15;

    @Column(name = "SO_Reference16")
    private String soReference16;

    @Column(name = "SO_Reference17")
    private String soReference17;

    @Column(name = "SO_Reference18")
    private String soReference18;

    @Column(name = "SO_Reference19")
    private String soReference19;

    @Column(name = "SO_Reference20")
    private String soReference20;

    @Column(name = "Booking_Number")
    private String bookingNumber;

    @Column(name = "Carrier")
    private String carrier;

    @Column(name = "Destination_Shipment_Type")
    private String destinationShipmentType;

    @Column(name = "Discharge_Port_City")
    private String dischargePortCity;

    @Column(name = "Discharge_Port_Country")
    private String dischargePortCountry;

    @Column(name = "Discharge_Port_Province")
    private String dischargePortProvince;

    @Column(name = "ETA")
    private Date eta;

    @Column(name = "EDT")
    private Date etd;

    @Column(name = "Load_Plan_City")
    private String loadPlanCity ;

    @Column(name = "Load_Plan_Country")
    private String loadPlanCountry;

    @Column(name = "Load_Plan_Province")
    private String loadPlanProvince;

    @Column(name = "Origin_Shipment_Type")
    private String originShipmentType;

    @Column(name = "Shipment_Status")
    private String shipmentStatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPoLineReference1() {
        return poLineReference1;
    }

    public void setPoLineReference1(String poLineReference1) {
        this.poLineReference1 = poLineReference1;
    }

    public String getPoLineReference2() {
        return poLineReference2;
    }

    public void setPoLineReference2(String poLineReference2) {
        this.poLineReference2 = poLineReference2;
    }

    public String getPoLineReference3() {
        return poLineReference3;
    }

    public void setPoLineReference3(String poLineReference3) {
        this.poLineReference3 = poLineReference3;
    }

    public String getPoLineReference4() {
        return poLineReference4;
    }

    public void setPoLineReference4(String poLineReference4) {
        this.poLineReference4 = poLineReference4;
    }

    public String getPoLineReference5() {
        return poLineReference5;
    }

    public void setPoLineReference5(String poLineReference5) {
        this.poLineReference5 = poLineReference5;
    }

    public String getPoLineReference6() {
        return poLineReference6;
    }

    public void setPoLineReference6(String poLineReference6) {
        this.poLineReference6 = poLineReference6;
    }

    public String getPoLineReference7() {
        return poLineReference7;
    }

    public void setPoLineReference7(String poLineReference7) {
        this.poLineReference7 = poLineReference7;
    }

    public String getPoLineReference8() {
        return poLineReference8;
    }

    public void setPoLineReference8(String poLineReference8) {
        this.poLineReference8 = poLineReference8;
    }

    public String getPoLineReference9() {
        return poLineReference9;
    }

    public void setPoLineReference9(String poLineReference9) {
        this.poLineReference9 = poLineReference9;
    }

    public String getPoLineReference10() {
        return poLineReference10;
    }

    public void setPoLineReference10(String poLineReference10) {
        this.poLineReference10 = poLineReference10;
    }

    public String getPoLineReference11() {
        return poLineReference11;
    }

    public void setPoLineReference11(String poLineReference11) {
        this.poLineReference11 = poLineReference11;
    }

    public String getPoLineReference12() {
        return poLineReference12;
    }

    public void setPoLineReference12(String poLineReference12) {
        this.poLineReference12 = poLineReference12;
    }

    public String getPoLineReference13() {
        return poLineReference13;
    }

    public void setPoLineReference13(String poLineReference13) {
        this.poLineReference13 = poLineReference13;
    }

    public String getPoLineReference14() {
        return poLineReference14;
    }

    public void setPoLineReference14(String poLineReference14) {
        this.poLineReference14 = poLineReference14;
    }

    public String getPoLineReference15() {
        return poLineReference15;
    }

    public void setPoLineReference15(String poLineReference15) {
        this.poLineReference15 = poLineReference15;
    }

    public String getPoLineReference16() {
        return poLineReference16;
    }

    public void setPoLineReference16(String poLineReference16) {
        this.poLineReference16 = poLineReference16;
    }

    public String getPoLineReference17() {
        return poLineReference17;
    }

    public void setPoLineReference17(String poLineReference17) {
        this.poLineReference17 = poLineReference17;
    }

    public String getPoLineReference18() {
        return poLineReference18;
    }

    public void setPoLineReference18(String poLineReference18) {
        this.poLineReference18 = poLineReference18;
    }

    public String getPoLineReference19() {
        return poLineReference19;
    }

    public void setPoLineReference19(String poLineReference19) {
        this.poLineReference19 = poLineReference19;
    }

    public String getPoLineReference20() {
        return poLineReference20;
    }

    public void setPoLineReference20(String poLineReference20) {
        this.poLineReference20 = poLineReference20;
    }

    public Integer getExpectedCargoReceiptWeek() {

        if(expectedCargoReceiptWeek==null)
        {
            expectedCargoReceiptWeek=new Integer(0);
        }

        return expectedCargoReceiptWeek;
    }

    public void setExpectedCargoReceiptWeek(Integer expectedCargoReceiptWeek) {
        this.expectedCargoReceiptWeek = expectedCargoReceiptWeek;
    }

    public Date getActualRecieptDate() {
        if(actualRecieptDate==null)
        {
            actualRecieptDate=new Date();
        }
        return actualRecieptDate;
    }

    public void setActualRecieptDate(Date actualRecieptDate) {
        this.actualRecieptDate = actualRecieptDate;
    }

    public Integer getBookedCartons() {

        if(bookedCartons==null)
        {
            bookedCartons=new Integer(0);
        }
        return bookedCartons;
    }

    public void setBookedCartons(Integer bookedCartons) {
        this.bookedCartons = bookedCartons;
    }

    public Double getBookedCbm() {
        if(bookedCbm==null)
        {
            bookedCbm=new Double(0);
        }
        return bookedCbm;
    }

    public void setBookedCbm(Double bookedCbm) {
        this.bookedCbm = bookedCbm;
    }

    public Integer getBookedQuantity() {
        if(bookedQuantity==null)
        {
            bookedQuantity=new Integer(0);
        }

        return bookedQuantity;
    }

    public void setBookedQuantity(Integer bookedQuantity) {
        this.bookedQuantity = bookedQuantity;
    }

    public Double getBookedWeight() {
        if(bookedWeight==null)
        {
            bookedWeight=new Double(0);
        }

        return bookedWeight;
    }

    public void setBookedWeight(Double bookedWeight) {
        this.bookedWeight = bookedWeight;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public Date getExpectedCargoReceiptDate() {
        if(expectedCargoReceiptDate==null)
        {
            expectedCargoReceiptDate=new Date();
        }return expectedCargoReceiptDate;
    }

    public void setExpectedCargoReceiptDate(Date expectedCargoReceiptDate) {
        expectedCargoReceiptDate = expectedCargoReceiptDate;
    }



    public Date getExpectedDeliveryDate() {
        if(expectedDeliveryDate==null)
        {
            expectedDeliveryDate=new Date();
        }
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Date getLatestDeliveryDate() {
        if(latestDeliveryDate==null)
        {
            latestDeliveryDate=new Date();
        }
        return latestDeliveryDate;
    }

    public void setLatestDeliveryDate(Date latestDeliveryDate) {
        this.latestDeliveryDate = latestDeliveryDate;
    }

    public Date getLatestCargoReceiptDate() {

        if(latestCargoReceiptDate==null)
        {
            latestCargoReceiptDate=new Date();
        }
        return latestCargoReceiptDate;
    }

    public void setLatestCargoReceiptDate(Date latestCargoReceiptDate) {
        this.latestCargoReceiptDate = latestCargoReceiptDate;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPodCountry() {
        return podCountry;
    }

    public void setPodCountry(String podCountry) {
        this.podCountry = podCountry;
    }

    public String getPodProvince() {
        return podProvince;
    }

    public void setPodProvince(String podProvince) {
        this.podProvince = podProvince;
    }

    public String getPodCity() {
        return podCity;
    }

    public void setPodCity(String podCity) {
        this.podCity = podCity;
    }

    public String getPorCountry() {
        return porCountry;
    }

    public void setPorCountry(String porCountry) {
        this.porCountry = porCountry;
    }

    public String getPorProvince() {
        return porProvince;
    }

    public void setPorProvince(String porProvince) {
        this.porProvince = porProvince;
    }

    public String getPorCity() {
        return porCity;
    }

    public void setPorCity(String porCity) {
        this.porCity = porCity;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoLine() {
        return poLine;
    }

    public void setPoLine(String poLine) {
        this.poLine = poLine;
    }

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPslv() {
        return pslv;
    }

    public void setPslv(String pslv) {
        this.pslv = pslv;
    }

    public Integer getReceivedCatons() {
        if(receivedCatons==null)
        {
            receivedCatons=new Integer(0);
        }
        return receivedCatons;
    }

    public void setReceivedCatons(Integer receivedCatons) {
        this.receivedCatons = receivedCatons;
    }

    public Double getReceivedCbm() {

        if(receivedCbm==null)
        {
            receivedCbm=new Double(0);
        }
        return receivedCbm;
    }

    public void setReceivedCbm(Double receivedCbm) {
        this.receivedCbm = receivedCbm;
    }

    public Integer getReceivedQuantity() {
        if(receivedQuantity==null)
        {
            receivedQuantity=new Integer(0);
        }
        return receivedQuantity;
    }

    public void setReceivedQuantity(Integer receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public Double getReceivedWeight() {
        if(receivedWeight==null)
        {
            receivedWeight=new Double(0);
        }
        return receivedWeight;
    }

    public void setReceivedWeight(Double receivedWeight) {
        this.receivedWeight = receivedWeight;
    }

    public String getShipto() {
        return shipto;
    }

    public void setShipto(String shipto) {
        this.shipto = shipto;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public String getVendoe() {
        return vendoe;
    }

    public void setVendoe(String vendoe) {
        this.vendoe = vendoe;
    }

    public String getSoReference1() {
        return soReference1;
    }

    public void setSoReference1(String soReference1) {
        this.soReference1 = soReference1;
    }

    public String getSoReference2() {
        return soReference2;
    }

    public void setSoReference2(String soReference2) {
        this.soReference2 = soReference2;
    }

    public String getSoReference3() {
        return soReference3;
    }

    public void setSoReference3(String soReference3) {
        this.soReference3 = soReference3;
    }

    public String getSoReference4() {
        return soReference4;
    }

    public void setSoReference4(String soReference4) {
        this.soReference4 = soReference4;
    }

    public String getSoReference5() {
        return soReference5;
    }

    public void setSoReference5(String soReference5) {
        this.soReference5 = soReference5;
    }

    public String getSoReference6() {
        return soReference6;
    }

    public void setSoReference6(String soReference6) {
        this.soReference6 = soReference6;
    }

    public String getSoReference7() {
        return soReference7;
    }

    public void setSoReference7(String soReference7) {
        this.soReference7 = soReference7;
    }

    public String getSoReference8() {
        return soReference8;
    }

    public void setSoReference8(String soReference8) {
        this.soReference8 = soReference8;
    }

    public String getSoReference9() {
        return soReference9;
    }

    public void setSoReference9(String soReference9) {
        this.soReference9 = soReference9;
    }

    public String getSoReference10() {
        return soReference10;
    }

    public void setSoReference10(String soReference10) {
        this.soReference10 = soReference10;
    }

    public String getSoReference11() {
        return soReference11;
    }

    public void setSoReference11(String soReference11) {
        this.soReference11 = soReference11;
    }

    public String getSoReference12() {
        return soReference12;
    }

    public void setSoReference12(String soReference12) {
        this.soReference12 = soReference12;
    }

    public String getSoReference13() {
        return soReference13;
    }

    public void setSoReference13(String soReference13) {
        this.soReference13 = soReference13;
    }

    public String getSoReference14() {
        return soReference14;
    }

    public void setSoReference14(String soReference14) {
        this.soReference14 = soReference14;
    }

    public String getSoReference15() {
        return soReference15;
    }

    public void setSoReference15(String soReference15) {
        this.soReference15 = soReference15;
    }

    public String getSoReference16() {
        return soReference16;
    }

    public void setSoReference16(String soReference16) {
        this.soReference16 = soReference16;
    }

    public String getSoReference17() {
        return soReference17;
    }

    public void setSoReference17(String soReference17) {
        this.soReference17 = soReference17;
    }

    public String getSoReference18() {
        return soReference18;
    }

    public void setSoReference18(String soReference18) {
        this.soReference18 = soReference18;
    }

    public String getSoReference19() {
        return soReference19;
    }

    public void setSoReference19(String soReference19) {
        this.soReference19 = soReference19;
    }

    public String getSoReference20() {
        return soReference20;
    }

    public void setSoReference20(String soReference20) {
        this.soReference20 = soReference20;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getDestinationShipmentType() {
        return destinationShipmentType;
    }

    public void setDestinationShipmentType(String destinationShipmentType) {
        this.destinationShipmentType = destinationShipmentType;
    }

    public String getDischargePortCity() {
        return dischargePortCity;
    }

    public void setDischargePortCity(String dischargePortCity) {
        this.dischargePortCity = dischargePortCity;
    }

    public String getDischargePortCountry() {
        return dischargePortCountry;
    }

    public void setDischargePortCountry(String dischargePortCountry) {
        this.dischargePortCountry = dischargePortCountry;
    }

    public String getDischargePortProvince() {
        return dischargePortProvince;
    }

    public void setDischargePortProvince(String dischargePortProvince) {
        this.dischargePortProvince = dischargePortProvince;
    }

    public Date getEta() {
        if(eta==null)
        {
            eta=new Date();
        }
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getEtd() {
        if(etd==null)
        {
            etd=new Date();
        }
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public String getLoadPlanCity() {
        return loadPlanCity;
    }

    public void setLoadPlanCity(String loadPlanCity) {
        this.loadPlanCity = loadPlanCity;
    }

    public String getLoadPlanCountry() {
        return loadPlanCountry;
    }

    public void setLoadPlanCountry(String loadPlanCountry) {
        this.loadPlanCountry = loadPlanCountry;
    }

    public String getLoadPlanProvince() {
        return loadPlanProvince;
    }

    public void setLoadPlanProvince(String loadPlanProvince) {
        this.loadPlanProvince = loadPlanProvince;
    }

    public String getOriginShipmentType() {
        return originShipmentType;
    }

    public void setOriginShipmentType(String originShipmentType) {
        this.originShipmentType = originShipmentType;
    }

    public String getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PurchaseOrder that = (PurchaseOrder) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (poLineReference1 != null ? !poLineReference1.equals(that.poLineReference1) : that.poLineReference1 != null)
            return false;
        if (poLineReference2 != null ? !poLineReference2.equals(that.poLineReference2) : that.poLineReference2 != null)
            return false;
        if (poLineReference3 != null ? !poLineReference3.equals(that.poLineReference3) : that.poLineReference3 != null)
            return false;
        if (poLineReference4 != null ? !poLineReference4.equals(that.poLineReference4) : that.poLineReference4 != null)
            return false;
        if (poLineReference5 != null ? !poLineReference5.equals(that.poLineReference5) : that.poLineReference5 != null)
            return false;
        if (poLineReference6 != null ? !poLineReference6.equals(that.poLineReference6) : that.poLineReference6 != null)
            return false;
        if (poLineReference7 != null ? !poLineReference7.equals(that.poLineReference7) : that.poLineReference7 != null)
            return false;
        if (poLineReference8 != null ? !poLineReference8.equals(that.poLineReference8) : that.poLineReference8 != null)
            return false;
        if (poLineReference9 != null ? !poLineReference9.equals(that.poLineReference9) : that.poLineReference9 != null)
            return false;
        if (poLineReference10 != null ? !poLineReference10.equals(that.poLineReference10) : that.poLineReference10 != null)
            return false;
        if (poLineReference11 != null ? !poLineReference11.equals(that.poLineReference11) : that.poLineReference11 != null)
            return false;
        if (poLineReference12 != null ? !poLineReference12.equals(that.poLineReference12) : that.poLineReference12 != null)
            return false;
        if (poLineReference13 != null ? !poLineReference13.equals(that.poLineReference13) : that.poLineReference13 != null)
            return false;
        if (poLineReference14 != null ? !poLineReference14.equals(that.poLineReference14) : that.poLineReference14 != null)
            return false;
        if (poLineReference15 != null ? !poLineReference15.equals(that.poLineReference15) : that.poLineReference15 != null)
            return false;
        if (poLineReference16 != null ? !poLineReference16.equals(that.poLineReference16) : that.poLineReference16 != null)
            return false;
        if (poLineReference17 != null ? !poLineReference17.equals(that.poLineReference17) : that.poLineReference17 != null)
            return false;
        if (poLineReference18 != null ? !poLineReference18.equals(that.poLineReference18) : that.poLineReference18 != null)
            return false;
        if (poLineReference19 != null ? !poLineReference19.equals(that.poLineReference19) : that.poLineReference19 != null)
            return false;
        if (poLineReference20 != null ? !poLineReference20.equals(that.poLineReference20) : that.poLineReference20 != null)
            return false;
        if (actualRecieptDate != null ? !actualRecieptDate.equals(that.actualRecieptDate) : that.actualRecieptDate != null)
            return false;
        if (bookedCartons != null ? !bookedCartons.equals(that.bookedCartons) : that.bookedCartons != null)
            return false;
        if (bookedCbm != null ? !bookedCbm.equals(that.bookedCbm) : that.bookedCbm != null) return false;
        if (bookedQuantity != null ? !bookedQuantity.equals(that.bookedQuantity) : that.bookedQuantity != null)
            return false;
        if (bookedWeight != null ? !bookedWeight.equals(that.bookedWeight) : that.bookedWeight != null) return false;
        if (consignee != null ? !consignee.equals(that.consignee) : that.consignee != null) return false;
        if (expectedCargoReceiptDate != null ? !expectedCargoReceiptDate.equals(that.expectedCargoReceiptDate) : that.expectedCargoReceiptDate != null)
            return false;
        if (expectedCargoReceiptWeek != null ? !expectedCargoReceiptWeek.equals(that.expectedCargoReceiptWeek) : that.expectedCargoReceiptWeek != null)
            return false;
        if (expectedDeliveryDate != null ? !expectedDeliveryDate.equals(that.expectedDeliveryDate) : that.expectedDeliveryDate != null)
            return false;
        if (latestDeliveryDate != null ? !latestDeliveryDate.equals(that.latestDeliveryDate) : that.latestDeliveryDate != null)
            return false;
        if (latestCargoReceiptDate != null ? !latestCargoReceiptDate.equals(that.latestCargoReceiptDate) : that.latestCargoReceiptDate != null)
            return false;
        if (orderType != null ? !orderType.equals(that.orderType) : that.orderType != null) return false;
        if (podCountry != null ? !podCountry.equals(that.podCountry) : that.podCountry != null) return false;
        if (podProvince != null ? !podProvince.equals(that.podProvince) : that.podProvince != null) return false;
        if (podCity != null ? !podCity.equals(that.podCity) : that.podCity != null) return false;
        if (porCountry != null ? !porCountry.equals(that.porCountry) : that.porCountry != null) return false;
        if (porProvince != null ? !porProvince.equals(that.porProvince) : that.porProvince != null) return false;
        if (porCity != null ? !porCity.equals(that.porCity) : that.porCity != null) return false;
        if (plant != null ? !plant.equals(that.plant) : that.plant != null) return false;
        if (poNumber != null ? !poNumber.equals(that.poNumber) : that.poNumber != null) return false;
        if (poLine != null ? !poLine.equals(that.poLine) : that.poLine != null) return false;
        if (productTypeCode != null ? !productTypeCode.equals(that.productTypeCode) : that.productTypeCode != null)
            return false;
        if (productType != null ? !productType.equals(that.productType) : that.productType != null) return false;
        if (pslv != null ? !pslv.equals(that.pslv) : that.pslv != null) return false;
        if (receivedCatons != null ? !receivedCatons.equals(that.receivedCatons) : that.receivedCatons != null)
            return false;
        if (receivedCbm != null ? !receivedCbm.equals(that.receivedCbm) : that.receivedCbm != null) return false;
        if (receivedQuantity != null ? !receivedQuantity.equals(that.receivedQuantity) : that.receivedQuantity != null)
            return false;
        if (receivedWeight != null ? !receivedWeight.equals(that.receivedWeight) : that.receivedWeight != null)
            return false;
        if (shipto != null ? !shipto.equals(that.shipto) : that.shipto != null) return false;
        if (shipper != null ? !shipper.equals(that.shipper) : that.shipper != null) return false;
        if (skuNumber != null ? !skuNumber.equals(that.skuNumber) : that.skuNumber != null) return false;
        if (vendoe != null ? !vendoe.equals(that.vendoe) : that.vendoe != null) return false;
        if (soReference1 != null ? !soReference1.equals(that.soReference1) : that.soReference1 != null) return false;
        if (soReference2 != null ? !soReference2.equals(that.soReference2) : that.soReference2 != null) return false;
        if (soReference3 != null ? !soReference3.equals(that.soReference3) : that.soReference3 != null) return false;
        if (soReference4 != null ? !soReference4.equals(that.soReference4) : that.soReference4 != null) return false;
        if (soReference5 != null ? !soReference5.equals(that.soReference5) : that.soReference5 != null) return false;
        if (soReference6 != null ? !soReference6.equals(that.soReference6) : that.soReference6 != null) return false;
        if (soReference7 != null ? !soReference7.equals(that.soReference7) : that.soReference7 != null) return false;
        if (soReference8 != null ? !soReference8.equals(that.soReference8) : that.soReference8 != null) return false;
        if (soReference9 != null ? !soReference9.equals(that.soReference9) : that.soReference9 != null) return false;
        if (soReference10 != null ? !soReference10.equals(that.soReference10) : that.soReference10 != null)
            return false;
        if (soReference11 != null ? !soReference11.equals(that.soReference11) : that.soReference11 != null)
            return false;
        if (soReference12 != null ? !soReference12.equals(that.soReference12) : that.soReference12 != null)
            return false;
        if (soReference13 != null ? !soReference13.equals(that.soReference13) : that.soReference13 != null)
            return false;
        if (soReference14 != null ? !soReference14.equals(that.soReference14) : that.soReference14 != null)
            return false;
        if (soReference15 != null ? !soReference15.equals(that.soReference15) : that.soReference15 != null)
            return false;
        if (soReference16 != null ? !soReference16.equals(that.soReference16) : that.soReference16 != null)
            return false;
        if (soReference17 != null ? !soReference17.equals(that.soReference17) : that.soReference17 != null)
            return false;
        if (soReference18 != null ? !soReference18.equals(that.soReference18) : that.soReference18 != null)
            return false;
        if (soReference19 != null ? !soReference19.equals(that.soReference19) : that.soReference19 != null)
            return false;
        if (soReference20 != null ? !soReference20.equals(that.soReference20) : that.soReference20 != null)
            return false;
        if (bookingNumber != null ? !bookingNumber.equals(that.bookingNumber) : that.bookingNumber != null)
            return false;
        if (carrier != null ? !carrier.equals(that.carrier) : that.carrier != null) return false;
        if (destinationShipmentType != null ? !destinationShipmentType.equals(that.destinationShipmentType) : that.destinationShipmentType != null)
            return false;
        if (dischargePortCity != null ? !dischargePortCity.equals(that.dischargePortCity) : that.dischargePortCity != null)
            return false;
        if (dischargePortCountry != null ? !dischargePortCountry.equals(that.dischargePortCountry) : that.dischargePortCountry != null)
            return false;
        if (dischargePortProvince != null ? !dischargePortProvince.equals(that.dischargePortProvince) : that.dischargePortProvince != null)
            return false;
        if (eta != null ? !eta.equals(that.eta) : that.eta != null) return false;
        if (etd != null ? !etd.equals(that.etd) : that.etd != null) return false;
        if (loadPlanCity != null ? !loadPlanCity.equals(that.loadPlanCity) : that.loadPlanCity != null) return false;
        if (loadPlanCountry != null ? !loadPlanCountry.equals(that.loadPlanCountry) : that.loadPlanCountry != null)
            return false;
        if (loadPlanProvince != null ? !loadPlanProvince.equals(that.loadPlanProvince) : that.loadPlanProvince != null)
            return false;
        if (originShipmentType != null ? !originShipmentType.equals(that.originShipmentType) : that.originShipmentType != null)
            return false;
        return shipmentStatus != null ? shipmentStatus.equals(that.shipmentStatus) : that.shipmentStatus == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (poLineReference1 != null ? poLineReference1.hashCode() : 0);
        result = 31 * result + (poLineReference2 != null ? poLineReference2.hashCode() : 0);
        result = 31 * result + (poLineReference3 != null ? poLineReference3.hashCode() : 0);
        result = 31 * result + (poLineReference4 != null ? poLineReference4.hashCode() : 0);
        result = 31 * result + (poLineReference5 != null ? poLineReference5.hashCode() : 0);
        result = 31 * result + (poLineReference6 != null ? poLineReference6.hashCode() : 0);
        result = 31 * result + (poLineReference7 != null ? poLineReference7.hashCode() : 0);
        result = 31 * result + (poLineReference8 != null ? poLineReference8.hashCode() : 0);
        result = 31 * result + (poLineReference9 != null ? poLineReference9.hashCode() : 0);
        result = 31 * result + (poLineReference10 != null ? poLineReference10.hashCode() : 0);
        result = 31 * result + (poLineReference11 != null ? poLineReference11.hashCode() : 0);
        result = 31 * result + (poLineReference12 != null ? poLineReference12.hashCode() : 0);
        result = 31 * result + (poLineReference13 != null ? poLineReference13.hashCode() : 0);
        result = 31 * result + (poLineReference14 != null ? poLineReference14.hashCode() : 0);
        result = 31 * result + (poLineReference15 != null ? poLineReference15.hashCode() : 0);
        result = 31 * result + (poLineReference16 != null ? poLineReference16.hashCode() : 0);
        result = 31 * result + (poLineReference17 != null ? poLineReference17.hashCode() : 0);
        result = 31 * result + (poLineReference18 != null ? poLineReference18.hashCode() : 0);
        result = 31 * result + (poLineReference19 != null ? poLineReference19.hashCode() : 0);
        result = 31 * result + (poLineReference20 != null ? poLineReference20.hashCode() : 0);
        result = 31 * result + (actualRecieptDate != null ? actualRecieptDate.hashCode() : 0);
        result = 31 * result + (bookedCartons != null ? bookedCartons.hashCode() : 0);
        result = 31 * result + (bookedCbm != null ? bookedCbm.hashCode() : 0);
        result = 31 * result + (bookedQuantity != null ? bookedQuantity.hashCode() : 0);
        result = 31 * result + (bookedWeight != null ? bookedWeight.hashCode() : 0);
        result = 31 * result + (consignee != null ? consignee.hashCode() : 0);
        result = 31 * result + (expectedCargoReceiptDate != null ? expectedCargoReceiptDate.hashCode() : 0);
        result = 31 * result + (expectedCargoReceiptWeek != null ? expectedCargoReceiptWeek.hashCode() : 0);
        result = 31 * result + (expectedDeliveryDate != null ? expectedDeliveryDate.hashCode() : 0);
        result = 31 * result + (latestDeliveryDate != null ? latestDeliveryDate.hashCode() : 0);
        result = 31 * result + (latestCargoReceiptDate != null ? latestCargoReceiptDate.hashCode() : 0);
        result = 31 * result + (orderType != null ? orderType.hashCode() : 0);
        result = 31 * result + (podCountry != null ? podCountry.hashCode() : 0);
        result = 31 * result + (podProvince != null ? podProvince.hashCode() : 0);
        result = 31 * result + (podCity != null ? podCity.hashCode() : 0);
        result = 31 * result + (porCountry != null ? porCountry.hashCode() : 0);
        result = 31 * result + (porProvince != null ? porProvince.hashCode() : 0);
        result = 31 * result + (porCity != null ? porCity.hashCode() : 0);
        result = 31 * result + (plant != null ? plant.hashCode() : 0);
        result = 31 * result + (poNumber != null ? poNumber.hashCode() : 0);
        result = 31 * result + (poLine != null ? poLine.hashCode() : 0);
        result = 31 * result + (productTypeCode != null ? productTypeCode.hashCode() : 0);
        result = 31 * result + (productType != null ? productType.hashCode() : 0);
        result = 31 * result + (pslv != null ? pslv.hashCode() : 0);
        result = 31 * result + (receivedCatons != null ? receivedCatons.hashCode() : 0);
        result = 31 * result + (receivedCbm != null ? receivedCbm.hashCode() : 0);
        result = 31 * result + (receivedQuantity != null ? receivedQuantity.hashCode() : 0);
        result = 31 * result + (receivedWeight != null ? receivedWeight.hashCode() : 0);
        result = 31 * result + (shipto != null ? shipto.hashCode() : 0);
        result = 31 * result + (shipper != null ? shipper.hashCode() : 0);
        result = 31 * result + (skuNumber != null ? skuNumber.hashCode() : 0);
        result = 31 * result + (vendoe != null ? vendoe.hashCode() : 0);
        result = 31 * result + (soReference1 != null ? soReference1.hashCode() : 0);
        result = 31 * result + (soReference2 != null ? soReference2.hashCode() : 0);
        result = 31 * result + (soReference3 != null ? soReference3.hashCode() : 0);
        result = 31 * result + (soReference4 != null ? soReference4.hashCode() : 0);
        result = 31 * result + (soReference5 != null ? soReference5.hashCode() : 0);
        result = 31 * result + (soReference6 != null ? soReference6.hashCode() : 0);
        result = 31 * result + (soReference7 != null ? soReference7.hashCode() : 0);
        result = 31 * result + (soReference8 != null ? soReference8.hashCode() : 0);
        result = 31 * result + (soReference9 != null ? soReference9.hashCode() : 0);
        result = 31 * result + (soReference10 != null ? soReference10.hashCode() : 0);
        result = 31 * result + (soReference11 != null ? soReference11.hashCode() : 0);
        result = 31 * result + (soReference12 != null ? soReference12.hashCode() : 0);
        result = 31 * result + (soReference13 != null ? soReference13.hashCode() : 0);
        result = 31 * result + (soReference14 != null ? soReference14.hashCode() : 0);
        result = 31 * result + (soReference15 != null ? soReference15.hashCode() : 0);
        result = 31 * result + (soReference16 != null ? soReference16.hashCode() : 0);
        result = 31 * result + (soReference17 != null ? soReference17.hashCode() : 0);
        result = 31 * result + (soReference18 != null ? soReference18.hashCode() : 0);
        result = 31 * result + (soReference19 != null ? soReference19.hashCode() : 0);
        result = 31 * result + (soReference20 != null ? soReference20.hashCode() : 0);
        result = 31 * result + (bookingNumber != null ? bookingNumber.hashCode() : 0);
        result = 31 * result + (carrier != null ? carrier.hashCode() : 0);
        result = 31 * result + (destinationShipmentType != null ? destinationShipmentType.hashCode() : 0);
        result = 31 * result + (dischargePortCity != null ? dischargePortCity.hashCode() : 0);
        result = 31 * result + (dischargePortCountry != null ? dischargePortCountry.hashCode() : 0);
        result = 31 * result + (dischargePortProvince != null ? dischargePortProvince.hashCode() : 0);
        result = 31 * result + (eta != null ? eta.hashCode() : 0);
        result = 31 * result + (etd != null ? etd.hashCode() : 0);
        result = 31 * result + (loadPlanCity != null ? loadPlanCity.hashCode() : 0);
        result = 31 * result + (loadPlanCountry != null ? loadPlanCountry.hashCode() : 0);
        result = 31 * result + (loadPlanProvince != null ? loadPlanProvince.hashCode() : 0);
        result = 31 * result + (originShipmentType != null ? originShipmentType.hashCode() : 0);
        result = 31 * result + (shipmentStatus != null ? shipmentStatus.hashCode() : 0);
        return result;
    }
}
