package com.maersk.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Pankaj on 10/9/2017.
 */
@Entity
@Table(name = "allowed_container")
public class AllowedContainer {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "origin")
    private String origin;

    @Column(name = "destination")
    private String destination;

    @Column(name = "allow20Dry")
    private String allow20Dry;

    @Column(name = "allow40Dry")
    private String allow40Dry;

    @Column(name = "allow40High")
    private String allow40High;

    @Column(name = "allow40Reef")
    private String allow40Reef;

    @Column(name = "allow45High")
    private String allow45High;

    public AllowedContainer() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAllow20Dry() {
        return allow20Dry;
    }

    public void setAllow20Dry(String allow20Dry) {
        this.allow20Dry = allow20Dry;
    }

    public String getAllow40Dry() {
        return allow40Dry;
    }

    public void setAllow40Dry(String allow40Dry) {
        this.allow40Dry = allow40Dry;
    }

    public String getAllow40High() {
        return allow40High;
    }

    public void setAllow40High(String allow40High) {
        this.allow40High = allow40High;
    }

    public String getAllow40Reef() {
        return allow40Reef;
    }

    public void setAllow40Reef(String allow40Reef) {
        this.allow40Reef = allow40Reef;
    }

    public String getAllow45High() {
        return allow45High;
    }

    public void setAllow45High(String allow45High) {
        this.allow45High = allow45High;
    }
}
