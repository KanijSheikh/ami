package com.maersk.model;

import com.maersk.enums.AppDataType;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Pankaj on 8/2/2018.
 */
@Entity
@Table(name = "app_data")
public class ApplicationData {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "type")
    private AppDataType type;

    @Column(name = "value")
    private String value;

    public ApplicationData() {

    }

    public ApplicationData(AppDataType type, String value) {
        this.type = type;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AppDataType getType() {
        return type;
    }

    public void setType(AppDataType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
