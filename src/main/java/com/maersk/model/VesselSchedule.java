package com.maersk.model;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "vessel_schedule")
public class VesselSchedule {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "vessel_schedule_id")
    private Integer id;

    @Column(name = "mode_of_transport")
    private String modeOfTransport;

    @Column(name = "loading_port")
    private String loadingPort;

    @Column(name = "discharge_port")
    private String dischargePort;

    @Column(name = "carrier")
    private String carrier;

    @Column(name = "loop")
    private String loop;

    @Column(name = "transit_time")
    private Integer transitTime;

    @Column(name = "etd")
    private String etd;

    @Column(name = "eta")
    private String eta;

    @Column(name = "activation_date")
    private Date activationDate;

    @Column(name = "deactivation_date")
    private Date deactivationDate;

    @Column(name = "cut_off_day")
    private Integer cuttOffDay;

    @Column(name = "twenty_dry_per_week")
    private Integer twentyDryPerWeek;

    @Column(name = "fourty_dry_per_week")
    private Integer fourtyDryPerWeek;

    @Column(name = "fourty_dry_dc_per_week")
    private Integer fourtyDryHcPerWeek;

    @Column(name = "fourty_five_dry_per_week")
    private Integer fourtyFiveDryPerWeek;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getLoop() {
        return loop;
    }

    public void setLoop(String loop) {
        this.loop = loop;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public Integer getCuttOffDay() {
        return cuttOffDay;
    }

    public void setCuttOffDay(Integer cuttOffDay) {
        this.cuttOffDay = cuttOffDay;
    }

    public Integer getTwentyDryPerWeek() {
        return twentyDryPerWeek;
    }

    public void setTwentyDryPerWeek(Integer twentyDryPerWeek) {
        this.twentyDryPerWeek = twentyDryPerWeek;
    }

    public Integer getFourtyDryPerWeek() {
        return fourtyDryPerWeek;
    }

    public void setFourtyDryPerWeek(Integer fourtyDryPerWeek) {
        this.fourtyDryPerWeek = fourtyDryPerWeek;
    }

    public Integer getFourtyDryHcPerWeek() {
        return fourtyDryHcPerWeek;
    }

    public void setFourtyDryHcPerWeek(Integer fourtyDryHcPerWeek) {
        this.fourtyDryHcPerWeek = fourtyDryHcPerWeek;
    }

    public Integer getFourtyFiveDryPerWeek() {
        return fourtyFiveDryPerWeek;
    }

    public void setFourtyFiveDryPerWeek(Integer fourtyFiveDryPerWeek) {
        this.fourtyFiveDryPerWeek = fourtyFiveDryPerWeek;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
