package com.maersk.model;


import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "load_plan")
@DynamicUpdate(true)
public class LoadPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "load_plan_id")
    private Integer id;

    @NotNull
    @Column(name = "load_plan_number", updatable = false)
    private String loadPlanNumber;

    @Column(name = "load_plan_name", updatable = false)
    private String loadPlanName;

    @Column(name = "status")
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @OneToMany(mappedBy = "loadPlan", cascade = {CascadeType.ALL})
    private List<Destination> destination = new ArrayList<>();

    @OneToMany(mappedBy = "loadPlan", cascade = {CascadeType.ALL})
    private List<Origin> origin = new ArrayList<>();

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "mode_of_transport")
    private String modeOfTransport;

    @Column(name = "cut_off_date")
    private Date cutOff;

    @OneToMany(mappedBy = "loadPlan", cascade = {CascadeType.ALL})
    private List<Container> containers = new ArrayList<>();

    public void addContainersToLoadPlan(Container container) {
        containers.add(container);
        container.setLoadPlan(this);
    }

    public Integer getId() {
//        if (id == null) {
//            id = new Integer(0);
//        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoadPlanNumber() {
        return loadPlanNumber;
    }

    public void setLoadPlanNumber(String loadPlanNumber) {
        this.loadPlanNumber = loadPlanNumber;
    }

    public String getLoadPlanName() {
        return loadPlanName;
    }

    public void setLoadPlanName(String loadPlanName) {
        this.loadPlanName = loadPlanName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
//        if (createdDate == null) {
//            createdDate = new Date();
//        }
        return createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }


    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public List<Container> getContainers() {
//        if (containers == null) {
//            containers = new ArrayList<>();
//        }
        return containers;
    }

    public void setContainers(List<Container> containers) {
        this.containers = containers;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public Date getCutOff() {
//        if (cutOff == null) {
//            cutOff = new Date();
//        }
        return cutOff;
    }

    public void setCutOff(Date cutOff) {
        this.cutOff = cutOff;
    }

    public List<Destination> getDestination() {
//        if (destination == null) {
//            destination = new ArrayList<>();
//        }
        return destination;
    }

    public void setDestination(List<Destination> destination) {
        this.destination = destination;
    }

    public List<Origin> getOrigin() {
//        if (origin == null) {
//            origin = new ArrayList<>();
//        }
        return origin;
    }

    public void setOrigin(List<Origin> origin) {
        this.origin = origin;
    }
}
