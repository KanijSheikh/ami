package com.maersk.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by kanij on 10/9/2017.
 */
//@Entity
//@Table(name = "ACTION")
public class Action {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "ACTION_ID")
    private Integer id;

    @NotNull
    @Column(name = "ACTION_NAME")
    private String name;

    @NotNull
    @Column(name = "ACTION_TYPE")
    private String type;

    @NotNull
    @Column(name = "ACTION_VALUE")
    private String value;


    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="RULE_ID")
    private Rule rule;

    public Action() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }
}
