package com.maersk.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by kanij on 14/02/2018.
 */
@Entity
@Table(name = "config_origin")
public class ConfigOrigin {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "config_origin_id")
    private Integer id;

    @Column(name = "origin_name")
    private String originName;

    @Column(name = "place_of_receipt")
    private String placeOfReceipt;

    @Column(name = "cfs_warehouse")
    private String cfsWarehouse;

    @Column(name = "loading_port")
    private String loadingPort;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getPlaceOfReceipt() {
        return placeOfReceipt;
    }

    public void setPlaceOfReceipt(String placeOfReceipt) {
        this.placeOfReceipt = placeOfReceipt;
    }

    public String getCfsWarehouse() {
        return cfsWarehouse;
    }

    public void setCfsWarehouse(String cfsWarehouse) {
        this.cfsWarehouse = cfsWarehouse;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }
}
