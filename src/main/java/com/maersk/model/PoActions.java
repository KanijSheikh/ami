package com.maersk.model;

import javax.persistence.Transient;

public class PoActions {

    @Transient
    private String updateField;

    @Transient
    private String updateValue;

    public String getUpdateField() {
        return updateField;
    }

    public void setUpdateField(String updateField) {
        this.updateField = updateField;
    }

    public String getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(String updateValue) {
        this.updateValue = updateValue;
    }
}
