package com.maersk.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "matching_criteria")
@DynamicUpdate(true)
public class MatchingCriteria implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "criteria_id")
    private Integer id;

    @Column(name = "criteria_name")
    private String criteriaName;

    @Column(name = "criteria_value")
    private String criteriaValue;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "rule_id")
    private Rule rule;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date",updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by",updatable = false)
    private String createdBy;


    @Column(name = "modified_by")
    private String modifiedBy;


    public MatchingCriteria() {

    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Integer getId() {
//        if(id==null)
//        {
//            id=new Integer(0);
//        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCriteriaName() {
        return criteriaName;
    }

    public void setCriteriaName(String criteriaName) {
        this.criteriaName = criteriaName;
    }

    public String getCriteriaValue() {
        return criteriaValue;
    }

    public void setCriteriaValue(String criteriaValue) {
        this.criteriaValue = criteriaValue;
    }

    public Date getCreatedDate() {
//        if(createdDate==null)
//        {
//            createdDate=new Date();
//        }
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


}
