package com.maersk.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "simulate_load_plan")
@DynamicUpdate(true)
public class SimulateLoadPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "po_collection")
    private String poCollection;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "load_plan_id")
    private Integer loadPlanId;

    public Integer getId() {
//        if (id == null) {
//            id = new Integer(0);
//        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedDate() {
//        if (createdDate == null) {
//            createdDate = new Date();
//        }
        return createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }

    public String getPoCollection() {
        return poCollection;
    }

    public void setPoCollection(String poCollection) {
        this.poCollection = poCollection;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public SimulateLoadPlan(String poCollection,Integer loadPlanId) {
        this.poCollection = poCollection;
        this.loadPlanId=loadPlanId;
    }

    public SimulateLoadPlan()
    {

    }

    public Integer getLoadPlanId() {
        return loadPlanId;
    }

    public void setLoadPlanId(Integer loadPlanId) {
        this.loadPlanId = loadPlanId;
    }
}
