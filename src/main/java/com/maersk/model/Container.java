package com.maersk.model;


import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "container")
@DynamicUpdate(true)
public class Container implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "container_id")
    private Integer id;

    @NotNull
    @Column(name = "container_number")
    private String containerNumber;

    @Column(name = "status")
    private String status;

    @Column(name = "carrier")
    private String carrier;

    @Column(name = "container_size_type")
    private String containerSizeType;

    @Column(name = "volume")
    private Double volume;

    @Column(name = "load_port")
    private String loadPort;

    @Column(name = "discharge_port")
    private String dischargePort;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "destination")
    private String destination;

    @Column(name = "eta")
    private Date eta;

    @Column(name = "etd")
    private Date etd;

    @OneToMany(mappedBy = "container", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    private List<PurchaseOrderOp> purchaseOrderOp = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "lp_id")
    private LoadPlan loadPlan;

    public void addPosToContainer(PurchaseOrderOp purchaseOrderOp1) {
        purchaseOrderOp.add(purchaseOrderOp1);
        purchaseOrderOp1.setContainer(this);
    }


    public Integer getId() {
//        if (id == null) {
//            id = new Integer(0);
//        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        if (createdDate == null) {
            createdDate = new Date();
        }
        return createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }


    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getContainerSizeType() {
        return containerSizeType;
    }

    public void setContainerSizeType(String containerSizeType) {
        this.containerSizeType = containerSizeType;
    }

    public Double getVolume() {
//        if (volume == null) {
//            volume = new Double(0.0);
//        }
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getLoadPort() {
        return loadPort;
    }

    public void setLoadPort(String loadPort) {
        this.loadPort = loadPort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getEta() {
//        if (eta == null) {
//            eta = new Date();
//        }
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getEtd() {
//        if (etd == null) {
//            etd = new Date();
//        }
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public LoadPlan getLoadPlan() {
//        if (loadPlan == null) {
//            loadPlan = new LoadPlan();
//        }
        return loadPlan;
    }

    public void setLoadPlan(LoadPlan loadPlan) {
        this.loadPlan = loadPlan;
    }

    public List<PurchaseOrderOp> getPurchaseOrderOp() {
//        if (purchaseOrderOp == null) {
//            purchaseOrderOp = new ArrayList<>();
//        }
        return purchaseOrderOp;
    }

    public void setPurchaseOrderOp(List<PurchaseOrderOp> purchaseOrderOp) {
        this.purchaseOrderOp = purchaseOrderOp;
    }

    public void addPoToContainers(PurchaseOrderOp purchaseOrderOp1) {
        purchaseOrderOp.add(purchaseOrderOp1);
        purchaseOrderOp1.setContainer(this);
    }
}
