package com.maersk.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by kanij on 10/9/2017.
 */
//@Entity
//@Table(name = "RULE_CONDITION")
public class Condition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CONDITION_ID")
    private Integer conditionId;

    @NotNull
    @Column(name = "CONDITION_NAME")
    private String conditionName;

    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="RULE_ID")
    private Rule rule;

    public Condition() {

    }

    public Integer getConditionId() {
        return conditionId;
    }

    public void setConditionId(Integer conditionId) {
        this.conditionId = conditionId;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }
}
