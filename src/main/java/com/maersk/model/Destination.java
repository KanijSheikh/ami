package com.maersk.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "load_plan_destination")
@DynamicUpdate(true)
public class Destination implements Serializable{

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "destination_id", updatable = false)
    private Integer id;

    @NotNull
    @Column(name = "destination_value")
    private String destinationValue;

    @NotNull
    @Column(name = "type")
    private String type;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "lp_id")
    private LoadPlan loadPlan;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    public Integer getId() {
//        if (id == null) {
//            id = new Integer(0);
//        }
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDestinationValue() {
        return destinationValue;
    }

    public void setDestinationValue(String destinationValue) {
        this.destinationValue = destinationValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedDate() {
//        if (createdDate == null) {
//            createdDate = new Date();
//        }
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public LoadPlan getLoadPlan() {
//        if (loadPlan == null) {
//            loadPlan = new LoadPlan();
//        }
        return loadPlan;
    }

    public void setLoadPlan(LoadPlan loadPlan) {
        this.loadPlan = loadPlan;
    }
}
