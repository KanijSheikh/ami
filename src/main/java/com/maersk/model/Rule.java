package com.maersk.model;


import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "ami_rule")
@DynamicUpdate(true)
public class Rule implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "rule_id")
    private Integer id;

    @Column(name = "rule_name")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "tree_order")
    private Integer treeOrder;

    @OneToMany(mappedBy = "rule", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private List<MatchingCriteria> matchingCriteriaList = new ArrayList<>();

    @OneToOne(mappedBy = "rule", cascade = {CascadeType.ALL})
    private CoTree poCoTree;


    public Rule() {
    }

    public CoTree getPoCoTree() {
        return poCoTree;
    }

    public void setPoCoTree(CoTree poCoTree) {
        this.poCoTree = poCoTree;
    }

    public void addMatchingCriteriaToRule(MatchingCriteria matchingCriteria) {
        matchingCriteriaList.add(matchingCriteria);
        matchingCriteria.setRule(this);
    }

    public void addDTreeToRule(CoTree dTree) {
        dTree.setRule(this);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<MatchingCriteria> getMatchingCriteriaList() {
        return matchingCriteriaList;
    }

    public void setMatchingCriteriaList(List<MatchingCriteria> matchingCriteriaList) {
        this.matchingCriteriaList = matchingCriteriaList;
    }

    public Date getCreatedDate() {
//        if(createdDate==null)
//        {
//            createdDate=new Date();
//        }
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }


    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Integer getTreeOrder() {
        return treeOrder;
    }

    public void setTreeOrder(Integer treeOrder) {
        this.treeOrder = treeOrder;
    }
}
