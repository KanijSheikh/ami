package com.maersk.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by kanij on 08/02/2018.
 */
@Entity
@Table(name = "config_destination")
public class ConfigDestination {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "destination_id")
    private Integer id;

    @Column(name = "destination_name")
    private String destinationName;

    @Column(name = "discharge_port")
    private String dischargePort;

    @Column(name = "transit_time")
    private Integer transitTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }
}
