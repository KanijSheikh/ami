package com.maersk.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by Pankaj on 10/9/2017.
 */
@Entity
@Table(name = "warehouse")
public class Warehouse {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "location")
    private String location;

    @Column(name = "location_code")
    private String locationCode;

    @Column(name = "name")
    private String name;

    @Column(name = "working_days")
    private String workingDays;

    @Column(name = "cargo_holding_days")
    private String cargoHoldingDays;

    public Warehouse() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getCargoHoldingDays() {
        return cargoHoldingDays;
    }

    public void setCargoHoldingDays(String cargoHoldingDays) {
        this.cargoHoldingDays = cargoHoldingDays;
    }
}
