package com.maersk.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "co_tree")
@DynamicUpdate(true)
public class CoTree implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "ct_string", length = 8000)
    private String ctString;

    @Column(name = "collection_condition_tree_string", length = 8000)
    private String collectionConditionTreeString;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "rule_id")
    private Rule rule;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", updatable = false)
    private Date createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_date")
    private Date modifiedDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    public CoTree() {

    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }


    public Integer getId() {
//        if (id == null) {
//            id = new Integer(0);
//        }
        return id;
    }

    public String getCtString() {
        return ctString;
    }

    public void setCtString(String ctString) {
        this.ctString = ctString;
    }

    public Rule getRule() {
        if (rule == null) {
            rule = new Rule();
        }
        return rule;
    }

    public Date getCreatedDate() {
//        if (createdDate == null) {
//            createdDate = new Date();
//        }
        return createdDate;
    }

    public Date getModifiedDate() {
        if (modifiedDate == null) {
            modifiedDate = new Date();
        }
        return modifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public String getCollectionConditionTreeString() {
        return collectionConditionTreeString;
    }

    public void setCollectionConditionTreeString(String collectionConditionTreeString) {
        this.collectionConditionTreeString = collectionConditionTreeString;
    }
}
