package com.maersk.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by kanij on 08/02/2018.
 */
@Entity
@Table(name = "container_fill_rate")
public class ContainerFillRate {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "container_fill_id")
    private Integer id;

    @Column(name = "product_type")
    private String productType;

    @Column(name = "container_size")
    private String containerSize;

    @Column(name = "min_cbm")
    private Integer minCbm;

    @Column(name = "max_cbm")
    private Integer maxCbm;

    @Column(name = "min_kgs")
    private Integer minKgs;

    @Column(name = "max_kgs")
    private Integer maxKgs;

    @Column(name = "origin")
    private String origin;

    @Column(name = "destination")
    private String destination;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public Integer getMinCbm() {
        return minCbm;
    }

    public void setMinCbm(Integer minCbm) {
        this.minCbm = minCbm;
    }

    public Integer getMaxCbm() {
        return maxCbm;
    }

    public void setMaxCbm(Integer maxCbm) {
        this.maxCbm = maxCbm;
    }

    public Integer getMinKgs() {
        return minKgs;
    }

    public void setMinKgs(Integer minKgs) {
        this.minKgs = minKgs;
    }

    public Integer getMaxKgs() {
        return maxKgs;
    }

    public void setMaxKgs(Integer maxKgs) {
        this.maxKgs = maxKgs;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
