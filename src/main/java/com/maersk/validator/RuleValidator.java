package com.maersk.validator;

import com.maersk.domain.*;
import com.maersk.exception.BadRequestException;
import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.Rule;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static java.util.Objects.isNull;

@Component
public class RuleValidator {
    public void validate(RuleDto rule) {
        validateRuleDto(rule);
        validateRuleAttributes(rule);

         //not validating dc tree
         //validateOperator(rule.getConditionDto().getCondition());
         //validateDataCatalogue(rule.getConditionDto().getCondition());
    }


    private void validateRuleDto(RuleDto ruleDto) {
        if (ruleDto == null) {
            throw new BadRequestException();
        }
    }

//    private void validateDataCatalogue(String condition) {
//        StringBuilder sb = new StringBuilder();
//        StringTokenizer st = new StringTokenizer(condition);
//        while (st.hasMoreTokens()) {
//            String token = st.nextToken();
//            if (!OPERATOR.contains(token)) {
//                sb.append(token).append(" ");
//            } else {
//                if (!DATACATALOGUE.contains(sb.toString())) {
//                    throw new BadRequestException("Please provide valid variable name");
//                }
//
//                sb = new StringBuilder();
//            }
//
//        }
//    }

//    private void validateOperator(String condition) {
//        String reg = " ";
//        String[] res = condition.split(reg);
//        for (String out : res) {
//            if (!"".equals(out)) {
//                if (!OPERATOR.contains(out.trim())) {
//                    throw new BadRequestException("Please provide valid operator");
//                }
//            }
//        }
//    }

    private void validateRuleAttributes(RuleDto ruleDto) {
        String ruleName = ruleDto.getRuleName();
        CoTreeDto coTreeDto = ruleDto.getPoCoTree();
        List<MatchingCriteriaDto> matchingCriteria=ruleDto.getMatchingCriteriaDto();


        if (isNull(ruleName) || ruleName.trim().isEmpty()) {
            throw new BadRequestException("Please provide Rule Name");
        }
        if (isNull(coTreeDto)) {
            throw new BadRequestException("Please provide decision tree");
        }
        if (isNull(matchingCriteria) || matchingCriteria.isEmpty()) {
            throw new BadRequestException("Please provide matching criteria");
        }
    }


    public void checkForEntityNotFound(Collection<Rule> rules) {
        if (rules.isEmpty()) {
            throw new EntityNotFoundException("The requested rule not found");
        }
    }

    public void validateReorderRequest(Integer ruleId, Integer oldOrder, Integer newOrder){
        if (isNull(ruleId) || ruleId < 1) {
            throw new BadRequestException("Please provide valid Rule Id");
        }
        if (isNull(oldOrder) || oldOrder < 1 ) {
            throw new BadRequestException("Please provide valid Old Order");
        }
        if (isNull(newOrder) || newOrder < 1 ) {
            throw new BadRequestException("Please provide valid New Order");
        }
        if (oldOrder == newOrder) {
            throw new BadRequestException("Old order and new order cannot be same");
        }
    }
}



