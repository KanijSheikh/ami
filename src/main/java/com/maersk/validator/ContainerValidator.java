package com.maersk.validator;

import com.maersk.domain.ContainerDto;
import com.maersk.exception.BadRequestException;
import com.maersk.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static java.util.Objects.isNull;

@Component
public class ContainerValidator {


    public void validate(ContainerDto containerDto) {
        validateContainerDto(containerDto);
        validateContainerAttributes(containerDto);
    }


    private void validateContainerDto(ContainerDto containerDto) {
        if (containerDto == null) {
            throw new BadRequestException("Container is empty");
        }
    }

    private void validateContainerAttributes(ContainerDto containerDto) {
        String containerNumber = containerDto.getContainerNumber();

        if (isNull(containerNumber) || containerNumber.trim().isEmpty()) {
            throw new BadRequestException("Please provide Container Number");
        }
    }


    public void checkForEntityNotFound(Collection<?> containers) {
        if (containers.isEmpty()) {
            throw new EntityNotFoundException("Container records not found");
        }
    }
}



