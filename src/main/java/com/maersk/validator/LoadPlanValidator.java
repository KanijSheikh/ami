package com.maersk.validator;

import com.maersk.exception.BadRequestException;
import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.LoadPlan;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static java.util.Objects.isNull;

@Component
public class LoadPlanValidator {


    public void validate(LoadPlan loadPlan) {
        validateLoadPlanDto(loadPlan);
        validateLpAttributes(loadPlan);
    }


    private void validateLoadPlanDto(LoadPlan loadPlanDto) {
        if (loadPlanDto == null) {
            throw new BadRequestException("Load plan is empty");
        }
    }

    private void validateLpAttributes(LoadPlan loadPlanDto) {
        String loadPlanNumber = loadPlanDto.getLoadPlanNumber();

        if (isNull(loadPlanNumber) || loadPlanNumber.trim().isEmpty()) {
            throw new BadRequestException("Please provide Load Plan Number");
        }
    }


    public void checkForEntityNotFound(Collection<LoadPlan> loadPlans) {
        if (loadPlans.isEmpty()) {
            throw new EntityNotFoundException("Load plans not found");
        }
    }

    public void loadPlanCreate(String error) {
        throw new BadRequestException("Load plan not created :" + error);
    }
}



