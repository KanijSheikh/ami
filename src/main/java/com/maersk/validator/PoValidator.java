package com.maersk.validator;

import com.maersk.domain.PurchaseOrderDto;
import com.maersk.exception.BadRequestException;
import com.maersk.exception.EntityNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collection;

import static java.util.Objects.isNull;

@Component
public class PoValidator {


    public void validate(PurchaseOrderDto purchaseOrderDto) {
        validatePoDto(purchaseOrderDto);
        validatePoAttributes(purchaseOrderDto);
    }


    private void validatePoDto(PurchaseOrderDto purchaseOrderDto) {
        if (purchaseOrderDto == null) {
            throw new BadRequestException("Purchase Order is empty");
        }
    }

    private void validatePoAttributes(PurchaseOrderDto purchaseOrderDto) {
        String poNumber = purchaseOrderDto.getPoNumber();

        if (isNull(poNumber) || poNumber.trim().isEmpty()) {
            throw new BadRequestException("Please provide PO Number");
        }
    }


    public void checkForEntityNotFound(Collection<?> purchaseOrders) {
        if (purchaseOrders.isEmpty()) {
            throw new EntityNotFoundException("Po records not found");
        }
    }

}



