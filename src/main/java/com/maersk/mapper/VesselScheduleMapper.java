package com.maersk.mapper;

import com.maersk.domain.VesselScheduleDto;
import com.maersk.model.VesselSchedule;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class VesselScheduleMapper {

    public Collection<VesselScheduleDto> mapToDto(Collection<VesselSchedule> vesselSchedules) {
        return vesselSchedules.stream().map(vesselSchedule -> createVesselSchedule(vesselSchedule)).collect(Collectors.toList());
    }

    private VesselScheduleDto createVesselSchedule(VesselSchedule vesselSchedule) {
        VesselScheduleDto vesselScheduleDto=new VesselScheduleDto();
        vesselScheduleDto.setId(vesselSchedule.getId());
        vesselScheduleDto.setModeOfTransport(vesselSchedule.getModeOfTransport());
        vesselScheduleDto.setActivationDate(vesselSchedule.getActivationDate());
        vesselScheduleDto.setCarrier(vesselSchedule.getCarrier());
        vesselScheduleDto.setLoop(vesselSchedule.getLoop());
        vesselScheduleDto.setCuttOffDay(vesselSchedule.getCuttOffDay());
        vesselScheduleDto.setTransitTime(vesselSchedule.getTransitTime());
        vesselScheduleDto.setDeactivationDate(vesselSchedule.getDeactivationDate());
        vesselScheduleDto.setDischargePort(vesselSchedule.getDischargePort());
        vesselScheduleDto.setLoadingPort(vesselSchedule.getLoadingPort());
        vesselScheduleDto.setActivationDate(vesselSchedule.getActivationDate());
        vesselScheduleDto.setEta(vesselSchedule.getEta());
        vesselScheduleDto.setEtd(vesselSchedule.getEtd());
        vesselScheduleDto.setTwentyDryPerWeek(vesselSchedule.getTwentyDryPerWeek());
        vesselScheduleDto.setFourtyDryHcPerWeek(vesselSchedule.getFourtyDryHcPerWeek());
        vesselScheduleDto.setFourtyDryPerWeek(vesselSchedule.getFourtyDryPerWeek());
        vesselScheduleDto.setFourtyFiveDryPerWeek(vesselSchedule.getFourtyFiveDryPerWeek());
        vesselScheduleDto.setCreatedDate(vesselSchedule.getCreatedDate());
        vesselScheduleDto.setModifiedDate(vesselSchedule.getModifiedDate());
        vesselScheduleDto.setModifiedBy(vesselSchedule.getModifiedBy());
        vesselScheduleDto.setCreatedBy(vesselSchedule.getCreatedBy());
        return vesselScheduleDto;
    }


    public VesselSchedule mapToDomain(VesselScheduleDto vesselScheduleDto) {
        VesselSchedule vesselSchedule=new VesselSchedule();
        if (nonNull(vesselScheduleDto)) {
            vesselSchedule.setId(vesselScheduleDto.getId());
        } else {
            vesselSchedule.setId(null);
        }
        vesselSchedule.setId(vesselScheduleDto.getId());
        vesselSchedule.setModeOfTransport(vesselScheduleDto.getModeOfTransport());
        vesselSchedule.setActivationDate(vesselScheduleDto.getActivationDate());
        vesselSchedule.setCarrier(vesselScheduleDto.getCarrier());
        vesselSchedule.setCuttOffDay(vesselScheduleDto.getCuttOffDay());
        vesselSchedule.setLoop(vesselScheduleDto.getLoop());
        vesselSchedule.setDeactivationDate(vesselScheduleDto.getDeactivationDate());
        vesselSchedule.setDischargePort(vesselScheduleDto.getDischargePort());
        vesselSchedule.setLoadingPort(vesselScheduleDto.getLoadingPort());
        vesselSchedule.setActivationDate(vesselScheduleDto.getActivationDate());
        vesselSchedule.setEta(vesselScheduleDto.getEta());
        vesselSchedule.setTransitTime(vesselScheduleDto.getTransitTime());
        vesselSchedule.setEtd(vesselScheduleDto.getEtd());
        vesselSchedule.setTwentyDryPerWeek(vesselScheduleDto.getTwentyDryPerWeek());
        vesselSchedule.setFourtyDryHcPerWeek(vesselScheduleDto.getFourtyDryHcPerWeek());
        vesselSchedule.setFourtyDryPerWeek(vesselScheduleDto.getFourtyDryPerWeek());
        vesselSchedule.setFourtyFiveDryPerWeek(vesselScheduleDto.getFourtyFiveDryPerWeek());
        vesselSchedule.setCreatedDate(vesselScheduleDto.getCreatedDate());
        vesselSchedule.setModifiedDate(vesselScheduleDto.getModifiedDate());
        vesselSchedule.setModifiedBy(vesselScheduleDto.getModifiedBy());
        vesselSchedule.setCreatedBy(vesselScheduleDto.getCreatedBy());

        return vesselSchedule;
    }

}
