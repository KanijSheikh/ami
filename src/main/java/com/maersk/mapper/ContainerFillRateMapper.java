package com.maersk.mapper;

import com.maersk.domain.ContainerFillRateDto;
import com.maersk.model.ContainerFillRate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class ContainerFillRateMapper {

    public Collection<ContainerFillRateDto> mapToDto(Collection<ContainerFillRate> containerFillRateList) {
        return containerFillRateList.stream().map(container -> createContainer(container)).collect(Collectors.toList());
    }

    private ContainerFillRateDto createContainer(ContainerFillRate container) {
        ContainerFillRateDto containerFillRateDto = new ContainerFillRateDto();

        containerFillRateDto.setId(container.getId());
        containerFillRateDto.setProductType(container.getProductType());
        containerFillRateDto.setContainerSize(container.getContainerSize());
        containerFillRateDto.setMinCbm(container.getMinCbm());
        containerFillRateDto.setMaxCbm(container.getMaxCbm());
        containerFillRateDto.setMinKgs(container.getMinKgs());
        containerFillRateDto.setMaxKgs(container.getMaxKgs());
        containerFillRateDto.setOrigin(container.getOrigin());
        containerFillRateDto.setDestination(container.getDestination());

        return containerFillRateDto;
    }


    public ContainerFillRate mapToDomain(ContainerFillRateDto containerFillRateDto) {
        ContainerFillRate container = new ContainerFillRate();

        if (nonNull(containerFillRateDto)) {
            container.setId(containerFillRateDto.getId());
        } else {
            container.setId(null);
        }
        container.setProductType(containerFillRateDto.getProductType());
        container.setContainerSize(containerFillRateDto.getContainerSize());
        container.setMinCbm(containerFillRateDto.getMinCbm());
        container.setMaxCbm(containerFillRateDto.getMaxCbm());
        container.setMinKgs(containerFillRateDto.getMinKgs());
        container.setMaxKgs(containerFillRateDto.getMaxKgs());
        container.setOrigin(containerFillRateDto.getOrigin());
        container.setDestination(containerFillRateDto.getDestination());

        return container;
    }

}
