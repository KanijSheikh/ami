package com.maersk.mapper;

import com.maersk.domain.WarehouseDto;
import com.maersk.model.Warehouse;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class WarehouseMapper {

    public Collection<WarehouseDto> mapToDto(Collection<Warehouse> warehouseCollection) {
        return warehouseCollection.stream().map(warehouse  -> createWarehouse(warehouse)).collect(Collectors.toList());
    }

    private WarehouseDto createWarehouse(Warehouse warehouse) {
        WarehouseDto  warehouseDto = new WarehouseDto();

        warehouseDto.setId(warehouse.getId());
        warehouseDto.setLocation(warehouse.getLocation());
        warehouseDto.setLocationCode(warehouse.getLocationCode());
        warehouseDto.setName(warehouse.getName());
        warehouseDto.setCargoHoldingDays(warehouse.getCargoHoldingDays());
        warehouseDto.setWorkingDays(warehouse.getWorkingDays());

        return warehouseDto;
    }


    public Warehouse mapToDomain(WarehouseDto warehouseDto) {
        Warehouse warehouse = new Warehouse();

        if (nonNull(warehouseDto)) {
            warehouse.setId(warehouseDto.getId());
        } else {
            warehouse.setId(null);
        }
        warehouse.setLocation(warehouseDto.getLocation());
        warehouse.setLocationCode(warehouseDto.getLocationCode());
        warehouse.setName(warehouseDto.getName());
        warehouse.setCargoHoldingDays(warehouseDto.getCargoHoldingDays());
        warehouse.setWorkingDays(warehouseDto.getWorkingDays());
        return warehouse;
    }

}
