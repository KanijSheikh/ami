package com.maersk.mapper;


import com.maersk.domain.ContainerDto;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.model.Container;
import com.maersk.model.PurchaseOrderOp;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class ContainerMapper extends CommonMapper {

    public Container mapToDomain(ContainerDto containerDto) {
        Container container = new Container();
        if (nonNull(containerDto)) {
            container.setId(containerDto.getId());
        }
        container.setContainerNumber(containerDto.getContainerNumber());
        container.setCarrier(containerDto.getCarrier());
        container.setContainerSizeType(containerDto.getContainerSizeType());
        container.setDestination(containerDto.getDestination());
        container.setDischargePort(containerDto.getDischargePort());
        container.setLoadPort(containerDto.getLoadPort());
        container.setEta(containerDto.getEta());
        container.setEtd(containerDto.getEtd());
        container.setStatus(containerDto.getStatus());
        if (nonNull(containerDto.getVolume())) {
            container.setVolume(Double.valueOf(format_2Places.format(containerDto.getVolume())));
        }
        container.setPurchaseOrderOp(createPo(containerDto, container));
        container.setModifiedDate(new Date());
        container.setCreatedDate(new Date());
        container.setCreatedBy(userId);
        container.setModifiedBy(userId);
        return container;
    }

    public Collection<ContainerDto> mapToDto(Collection<Container> containers) {
        return containers.stream().map(container -> createContainer(container)).collect(Collectors.toList());
    }

    public Collection<Container> mapFromDto(Collection<ContainerDto> containerDtos) {
        return containerDtos.stream().map(containerDto -> createContainerFromDto(containerDto)).collect(Collectors.toList());
    }

    private List<PurchaseOrderOp> createPo(ContainerDto containerDto, Container container) {

        List<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
        List<PurchaseOrderDto> purchaseOrderDtos = containerDto.getPurchaseOrderOp();
        for (PurchaseOrderDto purchaseOrderDto : purchaseOrderDtos) {
            PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();
            mapPoToPoDto(purchaseOrderOp, purchaseOrderDto);
            container.addPoToContainers(purchaseOrderOp);
            purchaseOrderOps.add(purchaseOrderOp);
        }
        return purchaseOrderOps;
    }

    private ContainerDto createContainer(Container container) {
        ContainerDto containerDto = new ContainerDto();
        containerDto.setId(container.getId());
        containerDto.setContainerNumber(container.getContainerNumber());
        containerDto.setCarrier(container.getCarrier());
        containerDto.setContainerSizeType(container.getContainerSizeType());
        containerDto.setDestination(container.getDestination());
        containerDto.setDischargePort(container.getDischargePort());
        containerDto.setLoadPort(container.getLoadPort());
        containerDto.setEta(container.getEta());
        containerDto.setEtd(container.getEtd());
        containerDto.setStatus(container.getStatus());
        if (nonNull(container.getVolume())) {
            containerDto.setVolume(Double.valueOf(format_2Places.format(container.getVolume())));
        }
        containerDto.setPurchaseOrderOp(getPurchaseOrderOp(container));
        containerDto.setModifiedDate(new Date());
        containerDto.setCreatedDate(new Date());
        containerDto.setCreatedBy(userId);
        containerDto.setModifiedBy(userId);
        return containerDto;
    }


    private Container createContainerFromDto(ContainerDto containerDto) {
        Container container = new Container();
        container.setId(containerDto.getId());
        container.setContainerNumber(containerDto.getContainerNumber());
        container.setContainerSizeType(containerDto.getContainerSizeType());
        container.setDestination(containerDto.getDestination());
        container.setCarrier(containerDto.getCarrier());
        container.setDischargePort(containerDto.getDischargePort());
        container.setLoadPort(containerDto.getLoadPort());
        container.setEta(containerDto.getEta());
        container.setEtd(containerDto.getEtd());
        container.setStatus(containerDto.getStatus());
        container.setVolume(containerDto.getVolume());
        container.setPurchaseOrderOp(getPurchaseOrderOp(containerDto));
        container.setModifiedDate(new Date());
        container.setCreatedDate(new Date());
        container.setCreatedBy(userId);
        container.setModifiedBy(userId);
        return container;
    }

    private List<PurchaseOrderDto> getPurchaseOrderOp(Container container) {
        List<PurchaseOrderOp> purchaseOrderOpList = container.getPurchaseOrderOp();
        List<PurchaseOrderDto> purchaseOrderDtos = new ArrayList<>();
        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOpList) {
            PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
            mapPoDtoToPo(purchaseOrderDto, purchaseOrderOp);
            purchaseOrderDtos.add(purchaseOrderDto);

        }
        return purchaseOrderDtos;
    }


    private List<PurchaseOrderOp> getPurchaseOrderOp(ContainerDto containerDto) {
        List<PurchaseOrderDto> purchaseOrderDtos = containerDto.getPurchaseOrderOp();
        List<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
        for (PurchaseOrderDto purchaseOrderDto : purchaseOrderDtos) {
            PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();
            mapPoToPoDto(purchaseOrderOp, purchaseOrderDto);
            purchaseOrderOps.add(purchaseOrderOp);
        }
        return purchaseOrderOps;
    }
}
