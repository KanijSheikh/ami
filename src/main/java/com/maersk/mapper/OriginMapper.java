package com.maersk.mapper;

import com.maersk.domain.ConfigOriginDto;
import com.maersk.model.ConfigOrigin;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class OriginMapper {

    public Collection<ConfigOriginDto> mapToDto(Collection<ConfigOrigin> originList) {
        return originList.stream().map(origin -> createOrigin(origin)).collect(Collectors.toList());
    }

    private ConfigOriginDto createOrigin(ConfigOrigin origin) {
        ConfigOriginDto originDto = new ConfigOriginDto();

        originDto.setId(origin.getId());
        originDto.setOriginName(origin.getOriginName());
        originDto.setPlaceOfReceipt(origin.getPlaceOfReceipt());
        originDto.setCfsWarehouse(origin.getCfsWarehouse());
        originDto.setLoadingPort(origin.getLoadingPort());

        return originDto;
    }


    public ConfigOrigin mapToDomain(ConfigOriginDto originDto) {
        ConfigOrigin origin = new ConfigOrigin();

        if (nonNull(originDto)) {
            origin.setId(originDto.getId());
        } else {
            origin.setId(null);
        }
        origin.setPlaceOfReceipt(originDto.getPlaceOfReceipt());
        origin.setCfsWarehouse(originDto.getCfsWarehouse());
        origin.setLoadingPort(originDto.getLoadingPort());
        origin.setOriginName(originDto.getOriginName());

        return origin;
    }

}
