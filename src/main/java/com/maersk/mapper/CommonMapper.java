package com.maersk.mapper;

import com.maersk.common.Constants;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.model.PurchaseOrderOp;

import java.util.Date;

import static java.util.Objects.nonNull;

public class CommonMapper implements Constants {

    public PurchaseOrderOp mapPoToPoDto(PurchaseOrderOp purchaseOrderOp, PurchaseOrderDto purchaseOrderDto) {
        if (nonNull(purchaseOrderDto)) {
            purchaseOrderOp.setId(purchaseOrderDto.getId());
        } else {
            purchaseOrderOp.setId(null);
        }
        purchaseOrderOp.setSoNumber(purchaseOrderDto.getSoNumber());
        purchaseOrderOp.setPoLineReference1(purchaseOrderDto.getPoLineReference1());
        purchaseOrderOp.setPoLineReference2(purchaseOrderDto.getPoLineReference2());
        purchaseOrderOp.setPoLineReference3(purchaseOrderDto.getPoLineReference3());
        purchaseOrderOp.setPoLineReference4(purchaseOrderDto.getPoLineReference4());
        purchaseOrderOp.setPoLineReference5(purchaseOrderDto.getPoLineReference5());
        purchaseOrderOp.setPoLineReference6(purchaseOrderDto.getPoLineReference6());
        purchaseOrderOp.setPoLineReference7(purchaseOrderDto.getPoLineReference7());
        purchaseOrderOp.setPoLineReference8(purchaseOrderDto.getPoLineReference8());
        purchaseOrderOp.setPoLineReference9(purchaseOrderDto.getPoLineReference9());
        purchaseOrderOp.setPoLineReference10(purchaseOrderDto.getPoLineReference10());
        purchaseOrderOp.setPoLineReference11(purchaseOrderDto.getPoLineReference11());
        purchaseOrderOp.setPoLineReference12(purchaseOrderDto.getPoLineReference12());
        purchaseOrderOp.setPoLineReference13(purchaseOrderDto.getPoLineReference13());
        purchaseOrderOp.setPoLineReference14(purchaseOrderDto.getPoLineReference14());
        purchaseOrderOp.setPoLineReference15(purchaseOrderDto.getPoLineReference15());
        purchaseOrderOp.setPoLineReference16(purchaseOrderDto.getPoLineReference16());
        purchaseOrderOp.setPoLineReference17(purchaseOrderDto.getPoLineReference17());
        purchaseOrderOp.setPoLineReference18(purchaseOrderDto.getPoLineReference18());
        purchaseOrderOp.setPoLineReference19(purchaseOrderDto.getPoLineReference19());
        purchaseOrderOp.setPoLineReference20(purchaseOrderDto.getPoLineReference20());
        purchaseOrderOp.setActualRecieptDate(purchaseOrderDto.getActualRecieptDate());
        purchaseOrderOp.setBookedCartons(purchaseOrderDto.getBookedCartons());
        if (nonNull(purchaseOrderDto.getBookedCbm())) {
            purchaseOrderOp.setBookedCbm(Double.valueOf(format_2Places.format(purchaseOrderDto.getBookedCbm())));
        }
        purchaseOrderOp.setBookedQuantity(purchaseOrderDto.getBookedQuantity());
        purchaseOrderOp.setBookedWeight(purchaseOrderDto.getBookedWeight());
        purchaseOrderOp.setConsignee(purchaseOrderDto.getConsignee());
        purchaseOrderOp.setExpectedCargoReceiptDate(purchaseOrderDto.getExpectedCargoReceiptDate());
        purchaseOrderOp.setExpectedCargoReceiptWeek(purchaseOrderDto.getExpectedCargoReceiptWeek());
        purchaseOrderOp.setExpectedDeliveryDate(purchaseOrderDto.getExpectedDeliveryDate());
        purchaseOrderOp.setLatestDeliveryDate(purchaseOrderDto.getLatestDeliveryDate());
        purchaseOrderOp.setLatestCargoReceiptDate(purchaseOrderDto.getLatestCargoReceiptDate());
        purchaseOrderOp.setOrderType(purchaseOrderDto.getOrderType());
        purchaseOrderOp.setPodCountry(purchaseOrderDto.getPodCountry());
        purchaseOrderOp.setPodProvince(purchaseOrderDto.getPodProvince());
        purchaseOrderOp.setPodCity(purchaseOrderDto.getPodCity());
        purchaseOrderOp.setPorCountry(purchaseOrderDto.getPorCountry());
        purchaseOrderOp.setPorProvince(purchaseOrderDto.getPorProvince());
        purchaseOrderOp.setPorCity(purchaseOrderDto.getPorCity());
        purchaseOrderOp.setPlant(purchaseOrderDto.getPlant());
        purchaseOrderOp.setPoNumber(purchaseOrderDto.getPoNumber());
        purchaseOrderOp.setPoLine(purchaseOrderDto.getPoLine());
        purchaseOrderOp.setProductTypeCode(purchaseOrderDto.getProductTypeCode());
        purchaseOrderOp.setProductType(purchaseOrderDto.getProductType());
        purchaseOrderOp.setPslv(purchaseOrderDto.getPslv());
        purchaseOrderOp.setReceivedCatons(purchaseOrderDto.getReceivedCatons());
        purchaseOrderOp.setReceivedCbm(purchaseOrderDto.getReceivedCbm());
        purchaseOrderOp.setReceivedQuantity(purchaseOrderDto.getReceivedQuantity());
        purchaseOrderOp.setReceivedWeight(purchaseOrderDto.getReceivedWeight());
        purchaseOrderOp.setShipto(purchaseOrderDto.getShipto());
        purchaseOrderOp.setShipper(purchaseOrderDto.getShipper());
        purchaseOrderOp.setSkuNumber(purchaseOrderDto.getSkuNumber());
        purchaseOrderOp.setVendoe(purchaseOrderDto.getVendoe());
        purchaseOrderOp.setSoReference1(purchaseOrderDto.getSoReference1());
        purchaseOrderOp.setSoReference2(purchaseOrderDto.getSoReference2());
        purchaseOrderOp.setSoReference3(purchaseOrderDto.getSoReference3());
        purchaseOrderOp.setSoReference4(purchaseOrderDto.getSoReference4());
        purchaseOrderOp.setSoReference5(purchaseOrderDto.getSoReference5());
        purchaseOrderOp.setSoReference6(purchaseOrderDto.getSoReference6());
        purchaseOrderOp.setSoReference7(purchaseOrderDto.getSoReference7());
        purchaseOrderOp.setSoReference8(purchaseOrderDto.getSoReference8());
        purchaseOrderOp.setSoReference9(purchaseOrderDto.getSoReference9());
        purchaseOrderOp.setSoReference10(purchaseOrderDto.getSoReference10());
        purchaseOrderOp.setSoReference11(purchaseOrderDto.getSoReference11());
        purchaseOrderOp.setSoReference12(purchaseOrderDto.getSoReference12());
        purchaseOrderOp.setSoReference13(purchaseOrderDto.getSoReference13());
        purchaseOrderOp.setSoReference14(purchaseOrderDto.getSoReference14());
        purchaseOrderOp.setSoReference15(purchaseOrderDto.getSoReference15());
        purchaseOrderOp.setSoReference16(purchaseOrderDto.getSoReference16());
        purchaseOrderOp.setSoReference17(purchaseOrderDto.getSoReference17());
        purchaseOrderOp.setSoReference18(purchaseOrderDto.getSoReference18());
        purchaseOrderOp.setSoReference19(purchaseOrderDto.getSoReference19());
        purchaseOrderOp.setSoReference20(purchaseOrderDto.getSoReference20());
        purchaseOrderOp.setBookingNumber(purchaseOrderDto.getBookingNumber());
        purchaseOrderOp.setCarrier(purchaseOrderDto.getCarrier());
        purchaseOrderOp.setDestinationShipmentType(purchaseOrderDto.getDestinationShipmentType());
        purchaseOrderOp.setDischargePortCity(purchaseOrderDto.getDischargePortCity());
        purchaseOrderOp.setDischargePortCountry(purchaseOrderDto.getDischargePortCountry());
        purchaseOrderOp.setDischargePortProvince(purchaseOrderDto.getDischargePortProvince());
        purchaseOrderOp.setEta(purchaseOrderDto.getEta());
        purchaseOrderOp.setEtd(purchaseOrderDto.getEtd());
        purchaseOrderOp.setLoadPortCity(purchaseOrderDto.getLoadPortCity());
        purchaseOrderOp.setLoadPortCountry(purchaseOrderDto.getLoadPortCountry());
        purchaseOrderOp.setLoadPortProvince(purchaseOrderDto.getLoadPortProvince());
        purchaseOrderOp.setOriginShipmentType(purchaseOrderDto.getOriginShipmentType());
        purchaseOrderOp.setShipmentStatus(purchaseOrderDto.getShipmentStatus());
        purchaseOrderOp.setPoStatus(purchaseOrderDto.getPoStatus());
        purchaseOrderOp.setModifiedDate(new Date());
        purchaseOrderOp.setLoadPlanStatus(purchaseOrderDto.getLoadPlanStatus());
        purchaseOrderOp.setBookDate(purchaseOrderDto.getBookDate());
        purchaseOrderOp.setRegion(purchaseOrderDto.getRegion());
        purchaseOrderOp.setDestination(purchaseOrderDto.getDestination());
        purchaseOrderOp.setCreatedDate(new Date());
        purchaseOrderOp.setCreatedBy(userId);
        purchaseOrderOp.setModifiedBy(userId);
        purchaseOrderOp.setUpdateAction(purchaseOrderDto.getUpdateAction());
        purchaseOrderOp.setModeOfTransport(purchaseOrderDto.getModOfTransport());
        purchaseOrderOp.setOriginServiceType(purchaseOrderDto.getOriginServiceType());
        purchaseOrderOp.setPodRegion(purchaseOrderDto.getPodRegion());
        purchaseOrderOp.setLoadPortRegion(purchaseOrderDto.getLoadPortRegion());
        purchaseOrderOp.setDestinationSelect(purchaseOrderDto.getDestinationSelect());
        purchaseOrderOp.setOriginSelect(purchaseOrderDto.getOriginSelect());
        purchaseOrderOp.setUpdateActionValue(purchaseOrderDto.getUpdateActionValue());
        purchaseOrderOp.setLoadPlanStatus(purchaseOrderDto.getLoadPlanStatus());
        purchaseOrderOp.setUpdateValue(purchaseOrderDto.getUpdateValue());
        purchaseOrderOp.setUpdateField(purchaseOrderDto.getUpdateField());
        purchaseOrderOp.setAssignedTo(purchaseOrderDto.getAssignedTo());
        purchaseOrderOp.setBookedDestinationService(purchaseOrderDto.getBookedDestinationService());

        return purchaseOrderOp;
    }

    public PurchaseOrderDto mapPoDtoToPo(PurchaseOrderDto purchaseOrderDto, PurchaseOrderOp purchaseOrderOp) {
        purchaseOrderDto.setSoNumber(purchaseOrderOp.getSoNumber());
        purchaseOrderDto.setPoLineReference1(purchaseOrderOp.getPoLineReference1());
        purchaseOrderDto.setPoLineReference2(purchaseOrderOp.getPoLineReference2());
        purchaseOrderDto.setPoLineReference3(purchaseOrderOp.getPoLineReference3());
        purchaseOrderDto.setPoLineReference4(purchaseOrderOp.getPoLineReference4());
        purchaseOrderDto.setPoLineReference5(purchaseOrderOp.getPoLineReference5());
        purchaseOrderDto.setPoLineReference6(purchaseOrderOp.getPoLineReference6());
        purchaseOrderDto.setPoLineReference7(purchaseOrderOp.getPoLineReference7());
        purchaseOrderDto.setPoLineReference8(purchaseOrderOp.getPoLineReference8());
        purchaseOrderDto.setPoLineReference9(purchaseOrderOp.getPoLineReference9());
        purchaseOrderDto.setPoLineReference10(purchaseOrderOp.getPoLineReference10());
        purchaseOrderDto.setPoLineReference11(purchaseOrderOp.getPoLineReference11());
        purchaseOrderDto.setPoLineReference12(purchaseOrderOp.getPoLineReference12());
        purchaseOrderDto.setPoLineReference13(purchaseOrderOp.getPoLineReference13());
        purchaseOrderDto.setPoLineReference14(purchaseOrderOp.getPoLineReference14());
        purchaseOrderDto.setPoLineReference15(purchaseOrderOp.getPoLineReference15());
        purchaseOrderDto.setPoLineReference16(purchaseOrderOp.getPoLineReference16());
        purchaseOrderDto.setPoLineReference17(purchaseOrderOp.getPoLineReference17());
        purchaseOrderDto.setPoLineReference18(purchaseOrderOp.getPoLineReference18());
        purchaseOrderDto.setPoLineReference19(purchaseOrderOp.getPoLineReference19());
        purchaseOrderDto.setPoLineReference20(purchaseOrderOp.getPoLineReference20());
        purchaseOrderDto.setActualRecieptDate(purchaseOrderOp.getActualRecieptDate());
        purchaseOrderDto.setBookedCartons(purchaseOrderOp.getBookedCartons());
        if (nonNull(purchaseOrderOp.getBookedCbm())) {
            purchaseOrderDto.setBookedCbm(Double.valueOf(format_2Places.format(purchaseOrderOp.getBookedCbm())));
        }
        purchaseOrderDto.setBookedQuantity(purchaseOrderOp.getBookedQuantity());
        purchaseOrderDto.setBookedWeight(purchaseOrderOp.getBookedWeight());
        purchaseOrderDto.setConsignee(purchaseOrderOp.getConsignee());
        purchaseOrderDto.setExpectedCargoReceiptWeek(purchaseOrderOp.getExpectedCargoReceiptWeek());
        purchaseOrderDto.setExpectedCargoReceiptDate(purchaseOrderOp.getExpectedCargoReceiptDate());
        purchaseOrderDto.setExpectedDeliveryDate(purchaseOrderOp.getExpectedDeliveryDate());
        purchaseOrderDto.setLatestDeliveryDate(purchaseOrderOp.getLatestDeliveryDate());
        purchaseOrderDto.setLatestCargoReceiptDate(purchaseOrderOp.getLatestCargoReceiptDate());
        purchaseOrderDto.setOrderType(purchaseOrderOp.getOrderType());
        purchaseOrderDto.setPodCountry(purchaseOrderOp.getPodCountry());
        purchaseOrderDto.setPodProvince(purchaseOrderOp.getPodProvince());
        purchaseOrderDto.setPodCity(purchaseOrderOp.getPodCity());
        purchaseOrderDto.setPorCountry(purchaseOrderOp.getPorCountry());
        purchaseOrderDto.setPorProvince(purchaseOrderOp.getPorProvince());
        purchaseOrderDto.setPorCity(purchaseOrderOp.getPorCity());
        purchaseOrderDto.setPlant(purchaseOrderOp.getPlant());
        purchaseOrderDto.setPoNumber(purchaseOrderOp.getPoNumber());
        purchaseOrderDto.setPoLine(purchaseOrderOp.getPoLine());
        purchaseOrderDto.setProductTypeCode(purchaseOrderOp.getProductTypeCode());
        purchaseOrderDto.setProductType(purchaseOrderOp.getProductType());
        purchaseOrderDto.setPslv(purchaseOrderOp.getPslv());
        purchaseOrderDto.setReceivedCatons(purchaseOrderOp.getReceivedCatons());
        purchaseOrderDto.setReceivedCbm(purchaseOrderOp.getReceivedCbm());
        purchaseOrderDto.setReceivedQuantity(purchaseOrderOp.getReceivedQuantity());
        purchaseOrderDto.setReceivedWeight(purchaseOrderOp.getReceivedWeight());
        purchaseOrderDto.setShipto(purchaseOrderOp.getShipto());
        purchaseOrderDto.setShipper(purchaseOrderOp.getShipper());
        purchaseOrderDto.setSkuNumber(purchaseOrderOp.getSkuNumber());
        purchaseOrderDto.setVendoe(purchaseOrderOp.getVendoe());
        purchaseOrderDto.setSoReference1(purchaseOrderOp.getSoReference1());
        purchaseOrderDto.setSoReference2(purchaseOrderOp.getSoReference2());
        purchaseOrderDto.setSoReference3(purchaseOrderOp.getSoReference3());
        purchaseOrderDto.setSoReference4(purchaseOrderOp.getSoReference4());
        purchaseOrderDto.setSoReference5(purchaseOrderOp.getSoReference5());
        purchaseOrderDto.setSoReference6(purchaseOrderOp.getSoReference6());
        purchaseOrderDto.setSoReference7(purchaseOrderOp.getSoReference7());
        purchaseOrderDto.setSoReference8(purchaseOrderOp.getSoReference8());
        purchaseOrderDto.setSoReference9(purchaseOrderOp.getSoReference9());
        purchaseOrderDto.setSoReference10(purchaseOrderOp.getSoReference10());
        purchaseOrderDto.setSoReference11(purchaseOrderOp.getSoReference11());
        purchaseOrderDto.setSoReference12(purchaseOrderOp.getSoReference12());
        purchaseOrderDto.setSoReference13(purchaseOrderOp.getSoReference13());
        purchaseOrderDto.setSoReference14(purchaseOrderOp.getSoReference14());
        purchaseOrderDto.setSoReference15(purchaseOrderOp.getSoReference15());
        purchaseOrderDto.setSoReference16(purchaseOrderOp.getSoReference16());
        purchaseOrderDto.setSoReference17(purchaseOrderOp.getSoReference17());
        purchaseOrderDto.setSoReference18(purchaseOrderOp.getSoReference18());
        purchaseOrderDto.setSoReference19(purchaseOrderOp.getSoReference19());
        purchaseOrderDto.setSoReference20(purchaseOrderOp.getSoReference20());
        purchaseOrderDto.setBookingNumber(purchaseOrderOp.getBookingNumber());
        purchaseOrderDto.setCarrier(purchaseOrderOp.getCarrier());
        purchaseOrderDto.setDestinationShipmentType(purchaseOrderOp.getDestinationShipmentType());
        purchaseOrderDto.setDischargePortCity(purchaseOrderOp.getDischargePortCity());
        purchaseOrderDto.setDischargePortCountry(purchaseOrderOp.getDischargePortCountry());
        purchaseOrderDto.setDischargePortProvince(purchaseOrderOp.getDischargePortProvince());
        purchaseOrderDto.setEta(purchaseOrderOp.getEta());
        purchaseOrderDto.setEtd(purchaseOrderOp.getEtd());
        purchaseOrderDto.setLoadPortCity(purchaseOrderOp.getLoadPortCity());
        purchaseOrderDto.setLoadPortCountry(purchaseOrderOp.getLoadPortCountry());
        purchaseOrderDto.setLoadPortProvince(purchaseOrderOp.getLoadPortProvince());
        purchaseOrderDto.setOriginShipmentType(purchaseOrderOp.getOriginShipmentType());
        purchaseOrderDto.setShipmentStatus(purchaseOrderOp.getShipmentStatus());
        purchaseOrderDto.setPoStatus(purchaseOrderOp.getPoStatus());
        purchaseOrderDto.setModifiedDate(new Date());
        purchaseOrderDto.setCreatedDate(new Date());
        purchaseOrderDto.setLoadPlanStatus(purchaseOrderOp.getLoadPlanStatus());
        purchaseOrderDto.setBookDate(purchaseOrderOp.getBookDate());
        purchaseOrderDto.setRegion(purchaseOrderOp.getRegion());
        purchaseOrderDto.setDestination(purchaseOrderOp.getDestination());
        if (nonNull(purchaseOrderOp)) {
            purchaseOrderDto.setId(purchaseOrderOp.getId());
        } else {
            purchaseOrderDto.setId(null);
        }
        purchaseOrderDto.setCreatedBy(userId);
        purchaseOrderDto.setModifiedBy(userId);
        purchaseOrderDto.setUpdateAction(purchaseOrderOp.getUpdateAction());
        purchaseOrderDto.setUpdateAction(purchaseOrderOp.getUpdateAction());
        purchaseOrderDto.setModOfTransport(purchaseOrderOp.getModeOfTransport());
        purchaseOrderDto.setOriginServiceType(purchaseOrderOp.getOriginServiceType());
        purchaseOrderDto.setPodRegion(purchaseOrderOp.getPodRegion());
        purchaseOrderDto.setLoadPortRegion(purchaseOrderOp.getLoadPortRegion());
        purchaseOrderDto.setDestinationSelect(purchaseOrderOp.getDestinationSelect());
        purchaseOrderDto.setOriginSelect(purchaseOrderOp.getOriginSelect());
        purchaseOrderDto.setUpdateActionValue(purchaseOrderOp.getUpdateActionValue());
        purchaseOrderDto.setLoadPlanStatus(purchaseOrderOp.getLoadPlanStatus());
        purchaseOrderDto.setUpdateValue(purchaseOrderOp.getUpdateValue());
        purchaseOrderDto.setUpdateField(purchaseOrderOp.getUpdateField());
        purchaseOrderDto.setAssignedTo(purchaseOrderOp.getAssignedTo());
        purchaseOrderDto.setContainerNumber(purchaseOrderOp.getContainerNumber());
        purchaseOrderDto.setLoadPlanNumber(purchaseOrderOp.getLoadPlanNumber());
        purchaseOrderDto.setBookedDestinationService(purchaseOrderOp.getBookedDestinationService());
        return purchaseOrderDto;
    }

}
