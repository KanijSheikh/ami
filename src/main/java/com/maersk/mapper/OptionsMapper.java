package com.maersk.mapper;

import com.maersk.domain.OptionsDto;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.model.Options;
import com.maersk.model.PurchaseOrderOp;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class OptionsMapper {

    public Options mapToDomain(OptionsDto optionsDto) {
        Options options = new Options();
        if (nonNull(optionsDto)) {
            options.setId(optionsDto.getId());
        } else {
            options.setId(null);
        }
        options.setType1(optionsDto.getType1());
        options.setType2(optionsDto.getType2());
        options.setValue(optionsDto.getValue());
        options.setCreatedBy(optionsDto.getCreatedBy());
        options.setCreatedDate(optionsDto.getCreatedDate());
        options.setModifiedBy(optionsDto.getModifiedBy());
        options.setModifiedDate(optionsDto.getModifiedDate());
        return options;
    }


    public Collection<OptionsDto> mapToDto(Collection<Options> options) {
        return options.stream().map(options1 -> mapFromDto(options1)).collect(Collectors.toList());
    }

    public OptionsDto mapFromDto(Options options) {
        OptionsDto optionsDto = new OptionsDto();
        if (nonNull(options)) {
            optionsDto.setId(options.getId());
        } else {
            optionsDto.setId(null);
        }
        optionsDto.setType1(options.getType1());
        optionsDto.setType2(options.getType2());
        optionsDto.setValue(options.getValue());
        optionsDto.setCreatedBy(options.getCreatedBy());
        optionsDto.setCreatedDate(options.getCreatedDate());
        optionsDto.setModifiedBy(options.getModifiedBy());
        optionsDto.setModifiedDate(options.getModifiedDate());
        return optionsDto;
    }

}
