package com.maersk.mapper;

import com.maersk.domain.LocationDto;
import com.maersk.model.Location;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class LocationMapper {

    public Collection<LocationDto> mapToDto(Collection<Location> locationList) {
        return locationList.stream().map(location -> createLocation(location)).collect(Collectors.toList());
    }

    private LocationDto createLocation(Location location) {
        LocationDto locationDto = new LocationDto();

        locationDto.setId(location.getId());
        locationDto.setLocation(location.getLocation());
        locationDto.setLocationCode(location.getLocationCode());
        locationDto.setCity(location.getCity());
        locationDto.setCityCode(location.getCityCode());
        locationDto.setCountry(location.getCountry());
        locationDto.setCountryCode(location.getCountryCode());

        return locationDto;
    }


    public Location mapToDomain(LocationDto locationDto) {
        Location location = new Location();

        if (nonNull(locationDto)) {
            location.setId(locationDto.getId());
        } else {
            location.setId(null);
        }
        location.setLocation(locationDto.getLocation());
        location.setLocationCode(locationDto.getLocationCode());
        location.setCity(locationDto.getCity());
        location.setCityCode(locationDto.getCityCode());
        location.setCountry(locationDto.getCountry());
        location.setCountryCode(locationDto.getCountryCode());

        return location;
    }

}
