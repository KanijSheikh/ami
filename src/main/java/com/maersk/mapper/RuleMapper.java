package com.maersk.mapper;

import com.maersk.common.Constants;
import com.maersk.domain.MatchingCriteriaDto;
import com.maersk.domain.CoTreeDto;
import com.maersk.domain.RuleDto;
import com.maersk.model.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class RuleMapper implements Constants {

    public Rule mapToDomain(RuleDto ruleDto) {
        Rule rule = new Rule();
        if (nonNull(ruleDto)) {
            rule.setId(ruleDto.getId());
        } else {
            rule.setId(null);
        }
        rule.setName(ruleDto.getRuleName());
        rule.setModifiedDate(new Date());
        rule.setCreatedDate(new Date());
        rule.setCreatedBy(userId);
        rule.setModifiedBy(userId);
        rule.setMatchingCriteriaList(createMatchingCriteria(ruleDto, rule));
        rule.setPoCoTree(createPoCoTree(ruleDto, rule));
        rule.setTreeOrder(ruleDto.getTreeOrder());
        return rule;
    }

    public Collection<RuleDto> mapToDto(Collection<Rule> rules) {
        return rules.stream().map(rule -> createRuleDto(rule)).collect(Collectors.toList());
    }

    private RuleDto createRuleDto(Rule rule) {
        RuleDto ruleDto = new RuleDto(rule.getName());
        if (nonNull(rule)) {
            ruleDto.setId(rule.getId());
        } else {
            ruleDto.setId(null);
        }
        ruleDto.setModifiedDate(new Date());
        ruleDto.setCreatedDate(new Date());
        ruleDto.setCreatedBy(userId);
        ruleDto.setModifiedBy(userId);
        ruleDto.setTreeOrder(rule.getTreeOrder());
        if (nonNull(rule.getMatchingCriteriaList())) {
            ruleDto.setMatchingCriteriaDto(getMatchingCriteriaDtos(rule));
        }
        if (nonNull(rule.getPoCoTree())) {
            ruleDto.setPoCoTree(getCoTreeDto(rule));
        }
        return ruleDto;
    }

    private List<MatchingCriteriaDto> getMatchingCriteriaDtos(Rule rule) {
        List<MatchingCriteria> matchingCriteriaList = rule.getMatchingCriteriaList();
        List<MatchingCriteriaDto> criteriaDtos = new ArrayList<>();
        for (MatchingCriteria matchingCriteria : matchingCriteriaList) {
            MatchingCriteriaDto matchingCriteriaDto = new MatchingCriteriaDto();
            matchingCriteriaDto.setCriteriaName(matchingCriteria.getCriteriaName());
            matchingCriteriaDto.setCriteriaValue(matchingCriteria.getCriteriaValue());
            matchingCriteriaDto.setModifiedDate(new Date());
            matchingCriteriaDto.setCreatedDate(new Date());
            matchingCriteriaDto.setCreatedBy(userId);
            matchingCriteriaDto.setModifiedBy(userId);
            if (nonNull(matchingCriteria)) {
                matchingCriteriaDto.setId(matchingCriteria.getId());
            } else {
                matchingCriteriaDto.setId(null);
            }
            criteriaDtos.add(matchingCriteriaDto);
        }
        return criteriaDtos;
    }

    /*private List<ActionDto> getActionDtos(Rule rule) {
        List<Action> actionsList = rule.getActionsList();
        List<ActionDto> actionDtos = new ArrayList<>();
        for (Action action : actionsList) {
            ActionDto actionDto = new ActionDto();
            actionDto.setActionName(action.getName());
            actionDto.setActionType(action.getType());
            actionDto.setActionValue(action.getValue());
            actionDtos.add(actionDto);
        }
        return actionDtos;
    }*/

    /*private ConditionDto getConditionDto(Rule rule) {
        Condition condition = rule.getCondition();
        ConditionDto conditionDto = new ConditionDto();
        conditionDto.setCondition(condition.getConditionName());
        return conditionDto;
    }*/

    private CoTreeDto getCoTreeDto(Rule rule) {
        CoTree coTree = rule.getPoCoTree();
        CoTreeDto coTreeDto = new CoTreeDto();
        coTreeDto.setCtString(coTree.getCtString());
        coTreeDto.setCollectionConditionTreeString(coTree.getCollectionConditionTreeString());
        if (nonNull(coTree)) {
            coTreeDto.setId(coTree.getId());
        } else {
            coTreeDto.setId(null);
        }
        coTreeDto.setModifiedDate(new Date());
        coTreeDto.setCreatedDate(new Date());
        coTreeDto.setCreatedBy(userId);
        coTreeDto.setModifiedBy(userId);
        return coTreeDto;
    }

    private List<MatchingCriteria> createMatchingCriteria(RuleDto ruleDto, Rule rule) {

        List<MatchingCriteria> listMatchingCriteria = new ArrayList<>();
        List<MatchingCriteriaDto> matchingCriteriaDto = ruleDto.getMatchingCriteriaDto();

        for (MatchingCriteriaDto criteriaDto : matchingCriteriaDto) {
            MatchingCriteria matchingCriteria = new MatchingCriteria();
            matchingCriteria.setCriteriaName(criteriaDto.getCriteriaName());
            matchingCriteria.setCriteriaValue(criteriaDto.getCriteriaValue());
            matchingCriteria.setModifiedDate(new Date());
            matchingCriteria.setCreatedDate(new Date());
            matchingCriteria.setCreatedBy(userId);
            matchingCriteria.setModifiedBy(userId);
            if (nonNull(criteriaDto)) {
                matchingCriteria.setId(criteriaDto.getId());
            } else {
                matchingCriteria.setId(null);
            }
            rule.addMatchingCriteriaToRule(matchingCriteria);
            listMatchingCriteria.add(matchingCriteria);
        }
        return listMatchingCriteria;
    }

    /*private List<Action> createAction(RuleDto ruleDto, Rule rule) {
        List<Action> actionList = new ArrayList<>();
        List<ActionDto> actionDtos = ruleDto.getActionDto();
        for (ActionDto actionDto : actionDtos) {
            Action action = new Action();
            action.setName(actionDto.getActionName());
            action.setType(actionDto.getActionType());
            action.setValue(actionDto.getActionValue());
            rule.addActionToRule(action);
            actionList.add(action);
        }
        return actionList;
    }*/

    /*private Condition createCondition(RuleDto ruleDto, Rule rule) {
        ConditionDto conditionDto = ruleDto.getConditionDto();
        Condition condition = new Condition();
        condition.setConditionName(conditionDto.getCondition());
        rule.addConditionToRule(condition);
        return condition;
    }*/

    private CoTree createPoCoTree(RuleDto ruleDto, Rule rule) {
        CoTreeDto deTreeDto = ruleDto.getPoCoTree();
        CoTree coTree = new CoTree();
        coTree.setCtString(deTreeDto.getCtString());
        coTree.setCollectionConditionTreeString(deTreeDto.getCollectionConditionTreeString());
        coTree.setModifiedDate(new Date());
        coTree.setCreatedDate(new Date());
        if (nonNull(deTreeDto)) {
            coTree.setId(deTreeDto.getId());
        } else {
            coTree.setId(null);
        }
        coTree.setCreatedBy(userId);
        coTree.setModifiedBy(userId);
        rule.addDTreeToRule(coTree);
        return coTree;
    }

}
