package com.maersk.mapper;

import com.maersk.domain.ConfigDestinationDto;
import com.maersk.domain.LocationDto;
import com.maersk.model.ConfigDestination;
import com.maersk.model.Location;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class DestinationMapper {

    public Collection<ConfigDestinationDto> mapToDto(Collection<ConfigDestination> destinationList) {
        return destinationList.stream().map(destination -> createDestination(destination)).collect(Collectors.toList());
    }

    private ConfigDestinationDto createDestination(ConfigDestination destination) {
        ConfigDestinationDto destinationDto = new ConfigDestinationDto();

        destinationDto.setId(destination.getId());
        destinationDto.setDestinationName(destination.getDestinationName());
        destinationDto.setDischargePort(destination.getDischargePort());
        destinationDto.setTransitTime(destination.getTransitTime());

        return destinationDto;
    }


    public ConfigDestination mapToDomain(ConfigDestinationDto destinationDto) {
        ConfigDestination destination = new ConfigDestination();

        if (nonNull(destinationDto)) {
            destination.setId(destinationDto.getId());
        } else {
            destination.setId(null);
        }
        destination.setDestinationName(destinationDto.getDestinationName());
        destination.setDischargePort(destinationDto.getDischargePort());
        destination.setTransitTime(destinationDto.getTransitTime());

        return destination;
    }

}
