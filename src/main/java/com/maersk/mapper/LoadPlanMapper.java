package com.maersk.mapper;


import com.maersk.domain.*;
import com.maersk.model.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class LoadPlanMapper extends CommonMapper {

    public LoadPlan mapToDomain(LoadPlanDto loadPlanDto) {
        LoadPlan loadPlan = new LoadPlan();
        if (nonNull(loadPlanDto)) {
            loadPlan.setId(loadPlanDto.getId());
        }
        loadPlan.setLoadPlanName(loadPlanDto.getLoadPlanName());
        loadPlan.setLoadPlanNumber(loadPlanDto.getLoadPlanNumber());
        loadPlan.setStatus(loadPlanDto.getStatus());
        loadPlan.setModifiedDate(new Date());
        loadPlan.setCreatedDate(new Date());
        loadPlan.setCreatedBy(userId);
        loadPlan.setModifiedBy(userId);
        loadPlan.setDestination(createDestination(loadPlanDto));
        loadPlan.setOrigin(createOrigin(loadPlanDto));
        loadPlan.setCutOff(loadPlanDto.getCutOff());
        loadPlan.setModeOfTransport(loadPlanDto.getModeOfTransport());
        loadPlan.setServiceType(loadPlanDto.getServiceType());
        loadPlan.setContainers(createContainer(loadPlanDto, loadPlan));
        return loadPlan;
    }

    public Collection<LoadPlanDto> mapToDto(Collection<LoadPlan> loadPlans) {
        return loadPlans.stream().map(loadPlan -> createloadPlanDto(loadPlan)).collect(Collectors.toList());
    }

    public LoadPlanDto createloadPlanDto(LoadPlan loadPlan) {
        LoadPlanDto loadPlanDto = new LoadPlanDto();
        loadPlanDto.setLoadPlanName(loadPlan.getLoadPlanName());
        loadPlanDto.setLoadPlanNumber(loadPlan.getLoadPlanNumber());
        loadPlanDto.setId(loadPlan.getId());
        loadPlanDto.setStatus(loadPlan.getStatus());
        loadPlanDto.setModifiedDate(new Date());
        loadPlanDto.setCreatedDate(new Date());
        loadPlanDto.setCreatedBy(userId);
        loadPlanDto.setModifiedBy(userId);
        loadPlanDto.setDestinationDto(getDestinationDto(loadPlan));
        loadPlanDto.setOriginDto(getOriginDto(loadPlan));
        loadPlanDto.setCutOff(loadPlan.getCutOff());
        loadPlanDto.setModeOfTransport(loadPlan.getModeOfTransport());
        loadPlanDto.setServiceType(loadPlan.getServiceType());
        loadPlanDto.setContainerDtos(getContainerDtos(loadPlan));
        return loadPlanDto;
    }

    private List<ContainerDto> getContainerDtos(LoadPlan loadPlan) {
        List<Container> containerList = loadPlan.getContainers();
        List<ContainerDto> containerDtos = new ArrayList<>();
        for (Container container : containerList) {
            ContainerDto containerDto = new ContainerDto();
            containerDto.setContainerNumber(container.getContainerNumber());
            containerDto.setCarrier(container.getCarrier());
            containerDto.setContainerSizeType(container.getContainerSizeType());
            containerDto.setDestination(container.getDestination());
            containerDto.setDischargePort(container.getDischargePort());
            containerDto.setId(container.getId());
            containerDto.setLoadPort(container.getLoadPort());
            containerDto.setEta(container.getEta());
            containerDto.setEtd(container.getEtd());
            containerDto.setStatus(container.getStatus());
            containerDto.setVolume(Double.valueOf(format_2Places.format(container.getVolume())));
            containerDto.setPurchaseOrderOp(getPurchaseOrderOp(container));
            containerDto.setModifiedDate(new Date());
            containerDto.setCreatedDate(new Date());
            containerDto.setCreatedBy(userId);
            containerDto.setModifiedBy(userId);
            containerDtos.add(containerDto);
        }
        return containerDtos;
    }

    private DestinationDto getDestinationDto(LoadPlan loadPlan) {
        List<Destination> destinations = loadPlan.getDestination();
        List<String> valueList = new ArrayList<>();
        for (Destination destination : destinations) {
            valueList.add(destination.getDestinationValue());
        }
        DestinationDto destinationDto = new DestinationDto();
        if (nonNull(destinations) && !destinations.isEmpty()) {
            destinationDto.setType(destinations.get(0).getType());
            destinationDto.setId(destinations.get(0).getId());
        }
        destinationDto.setModifiedDate(new Date());
        destinationDto.setCreatedDate(new Date());
        destinationDto.setCreatedBy(userId);
        destinationDto.setModifiedBy(userId);
        destinationDto.setDestinationValue(valueList);
        return destinationDto;
    }


    private OriginDto getOriginDto(LoadPlan loadPlan) {
        List<Origin> origins = loadPlan.getOrigin();
        List<String> valueList = new ArrayList<>();
        for (Origin origin : origins) {
            valueList.add(origin.getOrignValue());
        }
        OriginDto originDto = new OriginDto();
        if (nonNull(origins) && !origins.isEmpty()) {
            originDto.setType(origins.get(0).getType());
            originDto.setId(origins.get(0).getId());
        }
        originDto.setModifiedDate(new Date());
        originDto.setCreatedDate(new Date());
        originDto.setCreatedBy(userId);
        originDto.setModifiedBy(userId);
        originDto.setOriginValue(valueList);
        return originDto;
    }

    private List<PurchaseOrderDto> getPurchaseOrderOp(Container container) {
        List<PurchaseOrderOp> purchaseOrderOpList = container.getPurchaseOrderOp();
        List<PurchaseOrderDto> purchaseOrderDtos = new ArrayList<>();
        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOpList) {
            PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
            mapPoDtoToPo(purchaseOrderDto, purchaseOrderOp);
            purchaseOrderDtos.add(purchaseOrderDto);
        }
        return purchaseOrderDtos;
    }

    private List<Container> createContainer(LoadPlanDto loadPlanDto, LoadPlan loadPlan) {

        List<Container> containerList = new ArrayList<>();
        List<ContainerDto> containerDtos = loadPlanDto.getContainerDtos();
        for (ContainerDto containerDto : containerDtos) {
            Container container = new Container();
            container.setContainerNumber(containerDto.getContainerNumber());
            container.setCarrier(containerDto.getCarrier());
            container.setContainerSizeType(containerDto.getContainerSizeType());
            container.setDestination(containerDto.getDestination());
            container.setDischargePort(containerDto.getDischargePort());
            container.setLoadPort(containerDto.getLoadPort());
            container.setEta(containerDto.getEta());
            container.setEtd(containerDto.getEtd());
            container.setStatus(containerDto.getStatus());
            container.setVolume(Double.valueOf(format_2Places.format(containerDto.getVolume())));
            container.setPurchaseOrderOp(createPo(containerDto));
            container.setId(containerDto.getId());
            container.setModifiedDate(new Date());
            container.setCreatedDate(new Date());
            container.setCreatedBy(userId);
            container.setModifiedBy(userId);
            loadPlan.addContainersToLoadPlan(container);
            containerList.add(container);
        }
        return containerList;
    }

    private List<Destination> createDestination(LoadPlanDto loadPlanDto) {
        DestinationDto destinationDto = loadPlanDto.getDestinationDto();
        List<Destination> destinationList = new ArrayList<>();
        if (nonNull(destinationDto)) {
            String type = destinationDto.getType();
            for (String value : destinationDto.getDestinationValue()) {
                Destination destination = new Destination();
                destination.setDestinationValue(value);
                destination.setModifiedDate(new Date());
                destination.setCreatedDate(new Date());
                if (nonNull(destination)) {
                    destination.setId(destination.getId());
                } else {
                    destination.setId(null);
                }
                destination.setCreatedBy(userId);
                destination.setType(type);
                destination.setModifiedBy(userId);
                destinationList.add(destination);
            }
        }
        return destinationList;
    }

    private List<Origin> createOrigin(LoadPlanDto loadPlanDto) {
        List<Origin> origins = new ArrayList<>();
        OriginDto originDto = loadPlanDto.getOriginDto();
        if (nonNull(originDto)) {
            String type = originDto.getType();
            for (String value : originDto.getOriginValue()) {
                Origin origin = new Origin();
                origin.setOrignValue(value);
                origin.setModifiedDate(new Date());
                origin.setCreatedDate(new Date());
                if (nonNull(origin)) {
                    origin.setId(origin.getId());
                } else {
                    origin.setId(null);
                }
                origin.setCreatedBy(userId);
                origin.setType(type);
                origin.setModifiedBy(userId);
                origins.add(origin);
            }
        }
        return origins;
    }

    private List<PurchaseOrderOp> createPo(ContainerDto containerDto) {

        List<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
        List<PurchaseOrderDto> purchaseOrderDtos = containerDto.getPurchaseOrderOp();
        for (PurchaseOrderDto purchaseOrderDto : purchaseOrderDtos) {
            PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();
            mapPoToPoDto(purchaseOrderOp, purchaseOrderDto);
            purchaseOrderOps.add(purchaseOrderOp);
        }
        return purchaseOrderOps;
    }
}
