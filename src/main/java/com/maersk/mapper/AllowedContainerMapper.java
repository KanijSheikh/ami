package com.maersk.mapper;

import com.maersk.domain.AllowedContainerDto;
import com.maersk.model.AllowedContainer;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class AllowedContainerMapper {

    public Collection<AllowedContainerDto> mapToDto(Collection<AllowedContainer> allowedContainerCollection) {
        return allowedContainerCollection.stream().map(allowedContainer  -> createAllowedContainer(allowedContainer)).collect(Collectors.toList());
    }

    private AllowedContainerDto createAllowedContainer(AllowedContainer allowedContainer) {
        AllowedContainerDto  allowedContainerDto = new AllowedContainerDto();

        allowedContainerDto.setId(allowedContainer.getId());
        allowedContainerDto.setOrigin(allowedContainer.getOrigin());
        allowedContainerDto.setDestination(allowedContainer.getDestination());
        allowedContainerDto.setAllow20Dry(allowedContainer.getAllow20Dry());
        allowedContainerDto.setAllow40Dry(allowedContainer.getAllow40Dry());
        allowedContainerDto.setAllow40High(allowedContainer.getAllow40High());
        allowedContainerDto.setAllow40Reef(allowedContainer.getAllow40Reef());
        allowedContainerDto.setAllow45High(allowedContainer.getAllow45High());

        return allowedContainerDto;
    }


    public AllowedContainer mapToDomain(AllowedContainerDto allowedContainerDto) {
        AllowedContainer allowedContainer = new AllowedContainer();

        if (nonNull(allowedContainerDto)) {
            allowedContainer.setId(allowedContainerDto.getId());
        } else {
            allowedContainer.setId(null);
        }
        allowedContainer.setOrigin(allowedContainerDto.getOrigin());
        allowedContainer.setDestination(allowedContainerDto.getDestination());
        allowedContainer.setAllow20Dry(allowedContainerDto.getAllow20Dry());
        allowedContainer.setAllow40Dry(allowedContainerDto.getAllow40Dry());
        allowedContainer.setAllow40High(allowedContainerDto.getAllow40High());
        allowedContainer.setAllow40Reef(allowedContainerDto.getAllow40Reef());
        allowedContainer.setAllow45High(allowedContainerDto.getAllow45High());
        return allowedContainer;
    }

}
