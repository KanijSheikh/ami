package com.maersk.mapper;

import com.maersk.domain.CommunicatorDto;
import com.maersk.model.Communicator;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
public class CommunicatorMapper {

    public Communicator mapToDomain(CommunicatorDto communicatorDto){
        Communicator communicator=new Communicator();
        if (nonNull(communicatorDto)) {
            communicator.setId(communicatorDto.getId());
        } else {
            communicator.setId(null);
        }
        communicator.setLoadplan_id(communicatorDto.getLoadplanId());
        communicator.setCreatedBy(communicatorDto.getCreatedBy());
        communicator.setCreatedTime(new Date());
        communicator.setMessage(communicatorDto.getMessage());
        return communicator;
    }

    //mapToDto

    public Collection<CommunicatorDto> mapToDto(Collection<Communicator> CommunicatorLst) {
        return CommunicatorLst.stream().map(communicator -> new CommunicatorDto(communicator)).collect(Collectors.toList());
    }
}
