package com.maersk.mapper;


import com.maersk.domain.ContainerDto;
import com.maersk.domain.LoadPlanDto;
import com.maersk.domain.PurchaseOrderDto;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.PurchaseOrderOp;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PoMapper extends CommonMapper {

    public PurchaseOrderOp mapToDomain(PurchaseOrderDto purchaseOrderDto) {
        PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();
        return mapPoToPoDto(purchaseOrderOp, purchaseOrderDto);
    }

    public Collection<PurchaseOrderDto> mapToDto(Collection<PurchaseOrderOp> purchaseOrderOps) {
        return purchaseOrderOps.stream().map(purchaseOrderOp -> createPoDto(purchaseOrderOp)).collect(Collectors.toList());
    }

    public Collection<PurchaseOrderOp> mapToPo(Collection<PurchaseOrderDto> purchaseOrderDtos) {
        return purchaseOrderDtos.stream().map(purchaseOrderDto -> createDtoPo(purchaseOrderDto)).collect(Collectors.toList());
    }

    private PurchaseOrderDto createPoDto(PurchaseOrderOp purchaseOrderOp) {
        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        return mapPoDtoToPo(purchaseOrderDto, purchaseOrderOp);

    }

    private PurchaseOrderOp createDtoPo(PurchaseOrderDto purchaseOrderDto) {
        PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();
        return mapPoToPoDto(purchaseOrderOp, purchaseOrderDto);

    }
}
