package com.maersk.domain;

public class AllowedContainerDto {

    private Integer id;

    private String origin;

    private String destination;

    private String allow20Dry;

    private String allow40Dry;

    private String allow40High;

    private String allow40Reef;

    private String allow45High;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getAllow20Dry() {
        return allow20Dry;
    }

    public void setAllow20Dry(String allow20Dry) {
        this.allow20Dry = allow20Dry;
    }

    public String getAllow40Dry() {
        return allow40Dry;
    }

    public void setAllow40Dry(String allow40Dry) {
        this.allow40Dry = allow40Dry;
    }

    public String getAllow40High() {
        return allow40High;
    }

    public void setAllow40High(String allow40High) {
        this.allow40High = allow40High;
    }

    public String getAllow40Reef() {
        return allow40Reef;
    }

    public void setAllow40Reef(String allow40Reef) {
        this.allow40Reef = allow40Reef;
    }

    public String getAllow45High() {
        return allow45High;
    }

    public void setAllow45High(String allow45High) {
        this.allow45High = allow45High;
    }
}
