package com.maersk.domain;

/**
 * Created by kanij on 10/17/2017.
 */
public class RuleEvaluationDto {
    private String ruleCondition;

    public String getRuleCondition() {
        return ruleCondition;
    }

    public void setRuleCondition(String ruleCondition) {
        this.ruleCondition = ruleCondition;
    }
}
