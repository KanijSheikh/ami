package com.maersk.domain;

import javax.persistence.Transient;
import java.util.Date;

public class PurchaseOrderDto extends CommonDto {

    private String poLineReference1;
    private String poLineReference2;

    private String poLineReference3;

    private String poLineReference4;

    private String poLineReference5;

    private String poLineReference6;

    private String poLineReference7;

    private String poLineReference8;

    private String poLineReference9;

    private String poLineReference10;

    private String poLineReference11;

    private String poLineReference12;

    private String poLineReference13;

    private String poLineReference14;

    private String poLineReference15;

    private String poLineReference16;

    private String poLineReference17;

    private String poLineReference18;

    private String poLineReference19;

    private String poLineReference20;

    private Date actualRecieptDate;

    private Integer bookedCartons;

    private Double bookedCbm;

    private Integer bookedQuantity;

    private Double bookedWeight;

    private String consignee;

    private Date expectedCargoReceiptDate;

    private Integer expectedCargoReceiptWeek;

    private Date expectedDeliveryDate;

    private Date latestDeliveryDate;

    private Date latestCargoReceiptDate;

    private String orderType;

    private String podCountry;

    private String podProvince;

    private String podCity;

    private String podRegion;

    private String porCountry;

    private String porProvince;

    private String porCity;

    private String plant;

    private String poNumber;

    private String poLine;

    private String productTypeCode;

    private String productType;

    private String pslv;

    private Integer receivedCatons;

    private Double receivedCbm;

    private Integer receivedQuantity;

    private Double receivedWeight;

    private String shipto;

    private String shipper;

    private String skuNumber;

    private String vendoe;

    private String soReference1;

    private String soReference2;

    private String soReference3;

    private String soReference4;

    private String soReference5;

    private String soReference6;

    private String soReference7;

    private String soReference8;

    private String soReference9;

    private String soReference10;

    private String soReference11;

    private String soReference12;

    private String soReference13;

    private String soReference14;

    private String soReference15;

    private String soReference16;

    private String soReference17;

    private String soReference18;

    private String soReference19;

    private String soReference20;

    private String bookingNumber;

    private String carrier;

    private String destinationShipmentType;

    private String dischargePortCity;

    private String dischargePortCountry;

    private String dischargePortProvince;

    private Date eta;

    private Date etd;

    private String loadPortCity;

    private String loadPortCountry;

    private String loadPortRegion;

    private String loadPortProvince;

    private String originShipmentType;

    private String shipmentStatus;

    private String poStatus;

    private String soNumber;

    private String loadPlanStatus;

    private String region;

    private String destination;

    private Date bookDate;

    private String updateAction;

    private String modOfTransport;

    private String originServiceType;

    private String updateField;

    private String updateValue;

    private String originSelect;

    private String destinationSelect;

    private String updateActionValue;

    private String assignedTo;

    private String containerNumber;

    private String loadPlanNumber;

    private String bookedDestinationService;

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getLoadPlanNumber() {
        return loadPlanNumber;
    }

    public void setLoadPlanNumber(String loadPlanNumber) {
        this.loadPlanNumber = loadPlanNumber;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public Date getBookDate() {
//        if (bookDate == null) {
//            bookDate = new Date();
//        }
        return bookDate;
    }

    public void setBookDate(Date bookDate) {
        this.bookDate = bookDate;
    }

    public PurchaseOrderDto(Integer id, String poNumber, String soNumber, Date expectedDeliveryDate, Date bookDate, String loadPlanStatus, String region, String destination, String poStatus, String podCity) {
        this.loadPlanStatus = loadPlanStatus;
        this.id = id;
        this.bookDate = bookDate;
        this.destination = destination;
        this.region = region;
        this.expectedDeliveryDate = expectedDeliveryDate;
        this.soNumber = soNumber;
        this.poNumber = poNumber;
        this.poStatus = poStatus;
        this.podCity = podCity;
    }

    public PurchaseOrderDto() {

    }

    public String getLoadPlanStatus() {
        return loadPlanStatus;
    }

    public void setLoadPlanStatus(String loadPlanStatus) {
        this.loadPlanStatus = loadPlanStatus;
    }

    public String getPoLineReference1() {
        return poLineReference1;
    }

    public String getSoNumber() {
        return soNumber;
    }

    public void setSoNumber(String soNumber) {
        this.soNumber = soNumber;
    }

    public void setPoLineReference1(String poLineReference1) {
        this.poLineReference1 = poLineReference1;
    }

    public String getPoLineReference2() {
        return poLineReference2;
    }

    public void setPoLineReference2(String poLineReference2) {
        this.poLineReference2 = poLineReference2;
    }

    public String getPoLineReference3() {
        return poLineReference3;
    }

    public void setPoLineReference3(String poLineReference3) {
        this.poLineReference3 = poLineReference3;
    }

    public String getPoLineReference4() {
        return poLineReference4;
    }

    public void setPoLineReference4(String poLineReference4) {
        this.poLineReference4 = poLineReference4;
    }

    public String getPoLineReference5() {
        return poLineReference5;
    }

    public void setPoLineReference5(String poLineReference5) {
        this.poLineReference5 = poLineReference5;
    }

    public String getPoLineReference6() {
        return poLineReference6;
    }

    public void setPoLineReference6(String poLineReference6) {
        this.poLineReference6 = poLineReference6;
    }

    public String getPoLineReference7() {
        return poLineReference7;
    }

    public void setPoLineReference7(String poLineReference7) {
        this.poLineReference7 = poLineReference7;
    }

    public String getPoLineReference8() {
        return poLineReference8;
    }

    public void setPoLineReference8(String poLineReference8) {
        this.poLineReference8 = poLineReference8;
    }

    public String getPoLineReference9() {
        return poLineReference9;
    }

    public void setPoLineReference9(String poLineReference9) {
        this.poLineReference9 = poLineReference9;
    }

    public String getPoLineReference10() {
        return poLineReference10;
    }

    public void setPoLineReference10(String poLineReference10) {
        this.poLineReference10 = poLineReference10;
    }

    public String getPoLineReference11() {
        return poLineReference11;
    }

    public void setPoLineReference11(String poLineReference11) {
        this.poLineReference11 = poLineReference11;
    }

    public String getPoLineReference12() {
        return poLineReference12;
    }

    public void setPoLineReference12(String poLineReference12) {
        this.poLineReference12 = poLineReference12;
    }

    public String getPoLineReference13() {
        return poLineReference13;
    }

    public void setPoLineReference13(String poLineReference13) {
        this.poLineReference13 = poLineReference13;
    }

    public String getPoLineReference14() {
        return poLineReference14;
    }

    public void setPoLineReference14(String poLineReference14) {
        this.poLineReference14 = poLineReference14;
    }

    public String getPoLineReference15() {
        return poLineReference15;
    }

    public void setPoLineReference15(String poLineReference15) {
        this.poLineReference15 = poLineReference15;
    }

    public String getPoLineReference16() {
        return poLineReference16;
    }

    public void setPoLineReference16(String poLineReference16) {
        this.poLineReference16 = poLineReference16;
    }

    public String getPoLineReference17() {
        return poLineReference17;
    }

    public void setPoLineReference17(String poLineReference17) {
        this.poLineReference17 = poLineReference17;
    }

    public String getPoLineReference18() {
        return poLineReference18;
    }

    public void setPoLineReference18(String poLineReference18) {
        this.poLineReference18 = poLineReference18;
    }

    public String getPoLineReference19() {
        return poLineReference19;
    }

    public void setPoLineReference19(String poLineReference19) {
        this.poLineReference19 = poLineReference19;
    }

    public String getPoLineReference20() {
        return poLineReference20;
    }

    public void setPoLineReference20(String poLineReference20) {
        this.poLineReference20 = poLineReference20;
    }

    public Integer getExpectedCargoReceiptWeek() {

//        if (expectedCargoReceiptWeek == null) {
//            expectedCargoReceiptWeek = new Integer(0);
//        }

        return expectedCargoReceiptWeek;
    }

    public void setExpectedCargoReceiptWeek(Integer expectedCargoReceiptWeek) {
        this.expectedCargoReceiptWeek = expectedCargoReceiptWeek;
    }

    public Date getActualRecieptDate() {
//        if (actualRecieptDate == null) {
//            actualRecieptDate = new Date();
//        }
        return actualRecieptDate;
    }

    public void setActualRecieptDate(Date actualRecieptDate) {
        this.actualRecieptDate = actualRecieptDate;
    }

    public Integer getBookedCartons() {

//        if (bookedCartons == null) {
//            bookedCartons = new Integer(0);
//        }
        return bookedCartons;
    }

    public void setBookedCartons(Integer bookedCartons) {
        this.bookedCartons = bookedCartons;
    }

    public Double getBookedCbm() {
//        if (bookedCbm == null) {
//            bookedCbm = new Double(0);
//        }
        return bookedCbm;
    }

    public void setBookedCbm(Double bookedCbm) {
        this.bookedCbm = bookedCbm;
    }

    public Integer getBookedQuantity() {
//        if (bookedQuantity == null) {
//            bookedQuantity = new Integer(0);
//        }

        return bookedQuantity;
    }

    public void setBookedQuantity(Integer bookedQuantity) {
        this.bookedQuantity = bookedQuantity;
    }

    public Double getBookedWeight() {
//        if (bookedWeight == null) {
//            bookedWeight = new Double(0);
//        }

        return bookedWeight;
    }

    public void setBookedWeight(Double bookedWeight) {
        this.bookedWeight = bookedWeight;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public Date getExpectedCargoReceiptDate() {
//        if (expectedCargoReceiptDate == null) {
//            expectedCargoReceiptDate = new Date();
//        }
        return expectedCargoReceiptDate;
    }

    public void setExpectedCargoReceiptDate(Date expectedCargoReceiptDate) {
        this.expectedCargoReceiptDate = expectedCargoReceiptDate;
    }


    public Date getExpectedDeliveryDate() {
//        if (expectedDeliveryDate == null) {
//            expectedDeliveryDate = new Date();
//        }
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public Date getLatestDeliveryDate() {
//        if (latestDeliveryDate == null) {
//            latestDeliveryDate = new Date();
//        }
        return latestDeliveryDate;
    }

    public void setLatestDeliveryDate(Date latestDeliveryDate) {
        this.latestDeliveryDate = latestDeliveryDate;
    }

    public Date getLatestCargoReceiptDate() {
//
//        if (latestCargoReceiptDate == null) {
//            latestCargoReceiptDate = new Date();
//        }
        return latestCargoReceiptDate;
    }

    public void setLatestCargoReceiptDate(Date latestCargoReceiptDate) {
        this.latestCargoReceiptDate = latestCargoReceiptDate;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPodCountry() {
        return podCountry;
    }

    public void setPodCountry(String podCountry) {
        this.podCountry = podCountry;
    }

    public String getPodProvince() {
        return podProvince;
    }

    public void setPodProvince(String podProvince) {
        this.podProvince = podProvince;
    }

    public String getPodCity() {
        return podCity;
    }

    public void setPodCity(String podCity) {
        this.podCity = podCity;
    }

    public String getPorCountry() {
        return porCountry;
    }

    public void setPorCountry(String porCountry) {
        this.porCountry = porCountry;
    }

    public String getPorProvince() {
        return porProvince;
    }

    public void setPorProvince(String porProvince) {
        this.porProvince = porProvince;
    }

    public String getPorCity() {
        return porCity;
    }

    public void setPorCity(String porCity) {
        this.porCity = porCity;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getPoLine() {
        return poLine;
    }

    public void setPoLine(String poLine) {
        this.poLine = poLine;
    }

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPslv() {
        return pslv;
    }

    public void setPslv(String pslv) {
        this.pslv = pslv;
    }

    public Integer getReceivedCatons() {
//        if (receivedCatons == null) {
//            receivedCatons = new Integer(0);
//        }
        return receivedCatons;
    }

    public void setReceivedCatons(Integer receivedCatons) {
        this.receivedCatons = receivedCatons;
    }

    public Double getReceivedCbm() {
//
//        if (receivedCbm == null) {
//            receivedCbm = new Double(0);
//        }
        return receivedCbm;
    }

    public void setReceivedCbm(Double receivedCbm) {
        this.receivedCbm = receivedCbm;
    }

    public Integer getReceivedQuantity() {
//        if (receivedQuantity == null) {
//            receivedQuantity = new Integer(0);
//        }
        return receivedQuantity;
    }

    public void setReceivedQuantity(Integer receivedQuantity) {
        this.receivedQuantity = receivedQuantity;
    }

    public Double getReceivedWeight() {
//        if (receivedWeight == null) {
//            receivedWeight = new Double(0);
//        }
        return receivedWeight;
    }

    public void setReceivedWeight(Double receivedWeight) {
        this.receivedWeight = receivedWeight;
    }

    public String getShipto() {
        return shipto;
    }

    public void setShipto(String shipto) {
        this.shipto = shipto;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public String getVendoe() {
        return vendoe;
    }

    public void setVendoe(String vendoe) {
        this.vendoe = vendoe;
    }

    public String getSoReference1() {
        return soReference1;
    }

    public void setSoReference1(String soReference1) {
        this.soReference1 = soReference1;
    }

    public String getSoReference2() {
        return soReference2;
    }

    public void setSoReference2(String soReference2) {
        this.soReference2 = soReference2;
    }

    public String getSoReference3() {
        return soReference3;
    }

    public void setSoReference3(String soReference3) {
        this.soReference3 = soReference3;
    }

    public String getSoReference4() {
        return soReference4;
    }

    public void setSoReference4(String soReference4) {
        this.soReference4 = soReference4;
    }

    public String getSoReference5() {
        return soReference5;
    }

    public void setSoReference5(String soReference5) {
        this.soReference5 = soReference5;
    }

    public String getSoReference6() {
        return soReference6;
    }

    public void setSoReference6(String soReference6) {
        this.soReference6 = soReference6;
    }

    public String getSoReference7() {
        return soReference7;
    }

    public void setSoReference7(String soReference7) {
        this.soReference7 = soReference7;
    }

    public String getSoReference8() {
        return soReference8;
    }

    public void setSoReference8(String soReference8) {
        this.soReference8 = soReference8;
    }

    public String getSoReference9() {
        return soReference9;
    }

    public void setSoReference9(String soReference9) {
        this.soReference9 = soReference9;
    }

    public String getSoReference10() {
        return soReference10;
    }

    public void setSoReference10(String soReference10) {
        this.soReference10 = soReference10;
    }

    public String getSoReference11() {
        return soReference11;
    }

    public void setSoReference11(String soReference11) {
        this.soReference11 = soReference11;
    }

    public String getSoReference12() {
        return soReference12;
    }

    public void setSoReference12(String soReference12) {
        this.soReference12 = soReference12;
    }

    public String getSoReference13() {
        return soReference13;
    }

    public void setSoReference13(String soReference13) {
        this.soReference13 = soReference13;
    }

    public String getSoReference14() {
        return soReference14;
    }

    public void setSoReference14(String soReference14) {
        this.soReference14 = soReference14;
    }

    public String getSoReference15() {
        return soReference15;
    }

    public void setSoReference15(String soReference15) {
        this.soReference15 = soReference15;
    }

    public String getSoReference16() {
        return soReference16;
    }

    public void setSoReference16(String soReference16) {
        this.soReference16 = soReference16;
    }

    public String getSoReference17() {
        return soReference17;
    }

    public void setSoReference17(String soReference17) {
        this.soReference17 = soReference17;
    }

    public String getSoReference18() {
        return soReference18;
    }

    public void setSoReference18(String soReference18) {
        this.soReference18 = soReference18;
    }

    public String getSoReference19() {
        return soReference19;
    }

    public void setSoReference19(String soReference19) {
        this.soReference19 = soReference19;
    }

    public String getSoReference20() {
        return soReference20;
    }

    public void setSoReference20(String soReference20) {
        this.soReference20 = soReference20;
    }

    public String getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getDestinationShipmentType() {
        return destinationShipmentType;
    }

    public void setDestinationShipmentType(String destinationShipmentType) {
        this.destinationShipmentType = destinationShipmentType;
    }

    public String getDischargePortCity() {
        return dischargePortCity;
    }

    public void setDischargePortCity(String dischargePortCity) {
        this.dischargePortCity = dischargePortCity;
    }

    public String getDischargePortCountry() {
        return dischargePortCountry;
    }

    public void setDischargePortCountry(String dischargePortCountry) {
        this.dischargePortCountry = dischargePortCountry;
    }

    public String getDischargePortProvince() {
        return dischargePortProvince;
    }

    public void setDischargePortProvince(String dischargePortProvince) {
        this.dischargePortProvince = dischargePortProvince;
    }

    public Date getEta() {
//        if (eta == null) {
//            eta = new Date();
//        }
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getEtd() {
//        if (etd == null) {
//            etd = new Date();
//        }
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public String getLoadPortProvince() {
        return loadPortProvince;
    }

    public void setLoadPortProvince(String loadPortProvince) {
        this.loadPortProvince = loadPortProvince;
    }

    public String getOriginShipmentType() {
        return originShipmentType;
    }

    public void setOriginShipmentType(String originShipmentType) {
        this.originShipmentType = originShipmentType;
    }

    public String getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public String getPoStatus() {
        return poStatus;
    }

    public void setPoStatus(String poStatus) {
        this.poStatus = poStatus;
    }

    public String getUpdateAction() {
        return updateAction;
    }

    public void setUpdateAction(String updateAction) {
        this.updateAction = updateAction;
    }

    public String getPodRegion() {
        return podRegion;
    }

    public void setPodRegion(String podRegion) {
        this.podRegion = podRegion;
    }

    public String getLoadPortCity() {
        return loadPortCity;
    }

    public void setLoadPortCity(String loadPortCity) {
        this.loadPortCity = loadPortCity;
    }

    public String getLoadPortCountry() {
        return loadPortCountry;
    }

    public void setLoadPortCountry(String loadPortCountry) {
        this.loadPortCountry = loadPortCountry;
    }

    public String getLoadPortRegion() {
        return loadPortRegion;
    }

    public void setLoadPortRegion(String loadPortRegion) {
        this.loadPortRegion = loadPortRegion;
    }

    public String getModOfTransport() {
        return modOfTransport;
    }

    public void setModOfTransport(String modOfTransport) {
        this.modOfTransport = modOfTransport;
    }

    public String getOriginServiceType() {
        return originServiceType;
    }

    public void setOriginServiceType(String originServiceType) {
        this.originServiceType = originServiceType;
    }


    public String getDestinationSelect() {
        return destinationSelect;
    }

    public void setDestinationSelect(String destinationSelect) {
        this.destinationSelect = destinationSelect;
    }

    public String getOriginSelect() {
        return originSelect;
    }

    public void setOriginSelect(String originSelect) {
        this.originSelect = originSelect;
    }

    public String getUpdateActionValue() {
        return updateActionValue;
    }

    public void setUpdateActionValue(String updateActionValue) {
        this.updateActionValue = updateActionValue;
    }

    public String getUpdateField() {
        return updateField;
    }

    public void setUpdateField(String updateField) {
        this.updateField = updateField;
    }

    public String getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(String updateValue) {
        this.updateValue = updateValue;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getBookedDestinationService() {
        return bookedDestinationService;
    }

    public void setBookedDestinationService(String bookedDestinationService) {
        this.bookedDestinationService = bookedDestinationService;
    }
}
