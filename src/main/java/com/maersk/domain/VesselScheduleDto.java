package com.maersk.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class VesselScheduleDto extends  CommonDto {

    private String modeOfTransport;

    private String loadingPort;

    private String dischargePort;

    private String carrier;

    private String loop;

    private Integer transitTime;

    private String etd;

    private String eta;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date activationDate;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deactivationDate;

    private Integer cuttOffDay;

    private Integer twentyDryPerWeek;

    private Integer fourtyDryPerWeek;

    private Integer fourtyDryHcPerWeek;

    private Integer fourtyFiveDryPerWeek;

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getLoop() {
        return loop;
    }

    public void setLoop(String loop) {
        this.loop = loop;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getEtd() {
        return etd;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public Integer getCuttOffDay() {
        return cuttOffDay;
    }

    public void setCuttOffDay(Integer cuttOffDay) {
        this.cuttOffDay = cuttOffDay;
    }

    public Integer getTwentyDryPerWeek() {
        return twentyDryPerWeek;
    }

    public void setTwentyDryPerWeek(Integer twentyDryPerWeek) {
        this.twentyDryPerWeek = twentyDryPerWeek;
    }

    public Integer getFourtyDryPerWeek() {
        return fourtyDryPerWeek;
    }

    public void setFourtyDryPerWeek(Integer fourtyDryPerWeek) {
        this.fourtyDryPerWeek = fourtyDryPerWeek;
    }

    public Integer getFourtyDryHcPerWeek() {
        return fourtyDryHcPerWeek;
    }

    public void setFourtyDryHcPerWeek(Integer fourtyDryHcPerWeek) {
        this.fourtyDryHcPerWeek = fourtyDryHcPerWeek;
    }

    public Integer getFourtyFiveDryPerWeek() {
        return fourtyFiveDryPerWeek;
    }

    public void setFourtyFiveDryPerWeek(Integer fourtyFiveDryPerWeek) {
        this.fourtyFiveDryPerWeek = fourtyFiveDryPerWeek;
    }
}
