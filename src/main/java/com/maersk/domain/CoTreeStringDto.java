package com.maersk.domain;

import java.util.List;


public class CoTreeStringDto extends CommonDto {

    private String conditionName;
    private String ruleName;
    private List<ActionDto> trueActions;
    private List<ActionDto> falseActions;
    private CoTreeStringDto trueChild;
    private CoTreeStringDto falseChild;


    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public CoTreeStringDto getTrueChild() {
        return trueChild;
    }

    public void setTrueChild(CoTreeStringDto trueChild) {
        this.trueChild = trueChild;
    }

    public CoTreeStringDto getFalseChild() {

        return falseChild;
    }

    public void setFalseChild(CoTreeStringDto falseChild) {
        this.falseChild = falseChild;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public List<ActionDto> getFalseActions() {
        return this.falseActions;
    }

    public List<ActionDto> getTrueActions() {
        return trueActions;
    }

    public void setTrueActions(List<ActionDto> trueActions) {
        this.trueActions = trueActions;
    }

    public void setFalseActions(List<ActionDto> falseActions) {
        this.falseActions = falseActions;
    }
}
