package com.maersk.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LoadPlanDto extends CommonDto {

    private String loadPlanNumber;

    private String loadPlanName;

    private String status;

    private List<ContainerDto> containerDtos = new ArrayList<>();

    public String getLoadPlanNumber() {
        return loadPlanNumber;
    }

    public void setLoadPlanNumber(String loadPlanNumber) {
        this.loadPlanNumber = loadPlanNumber;
    }

    public String getLoadPlanName() {
        return loadPlanName;
    }

    public void setLoadPlanName(String loadPlanName) {
        this.loadPlanName = loadPlanName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String serviceType;

    private String modeOfTransport;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date cutOff;
//
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    private Date startDate;
//
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    private Date endDate;
//
//    private String startTime;
//
//    private String repeat;
//
//    private Integer repeatOccurences;
//
//    private List<String> days;
//
//    private String noEndDate;
//
//    private Integer endAfterOccurences;

    private DestinationDto destinationDto;

    private OriginDto originDto;

//    public Date getStartDate() {
//        return startDate;
//    }
//
//    public void setStartDate(Date startDate) {
//        this.startDate = startDate;
//    }
//
//    public Date getEndDate() {
//        return endDate;
//    }
//
//    public void setEndDate(Date endDate) {
//        this.endDate = endDate;
//    }
//
//    public String getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(String startTime) {
//        this.startTime = startTime;
//    }
//
//    public String getRepeat() {
//        return repeat;
//    }
//
//    public void setRepeat(String repeat) {
//        this.repeat = repeat;
//    }
//
//    public Integer getRepeatOccurences() {
//        return repeatOccurences;
//    }
//
//    public void setRepeatOccurences(Integer repeatOccurences) {
//        this.repeatOccurences = repeatOccurences;
//    }
//
//    public List<String> getDays() {
//        return days;
//    }
//
//    public void setDays(List<String> days) {
//        this.days = days;
//    }
//
//    public String getNoEndDate() {
//        return noEndDate;
//    }
//
//    public void setNoEndDate(String noEndDate) {
//        this.noEndDate = noEndDate;
//    }
//
//    public Integer getEndAfterOccurences() {
//        return endAfterOccurences;
//    }
//
//    public void setEndAfterOccurences(Integer endAfterOccurences) {
//        this.endAfterOccurences = endAfterOccurences;
//    }

    public List<ContainerDto> getContainerDtos() {
//        if (containerDtos == null) {
//            containerDtos = new ArrayList<>();
//        }
        return containerDtos;
    }

    public void setContainerDtos(List<ContainerDto> containerDtos) {
        this.containerDtos = containerDtos;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public Date getCutOff() {
//        if (cutOff == null) {
//            cutOff = new Date();
//        }
        return cutOff;
    }

    public void setCutOff(Date cutOff) {
        this.cutOff = cutOff;
    }

    public DestinationDto getDestinationDto() {
        return destinationDto;
    }

    public void setDestinationDto(DestinationDto destinationDto) {
        this.destinationDto = destinationDto;
    }

    public OriginDto getOriginDto() {
        return originDto;
    }

    public void setOriginDto(OriginDto originDto) {
        this.originDto = originDto;
    }
}
