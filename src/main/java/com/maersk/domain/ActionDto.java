package com.maersk.domain;

/**
 * Created by kanij on 10/10/2017.
 */
public class ActionDto extends CommonDto {
    private String actionName;
    private String actionType;
    private String actionValue;
    private Integer actionRange;
    private String actionExpression;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionValue() {
        return actionValue;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    public Integer getActionRange() {
        if (actionRange == null) {
            actionRange = new Integer(0);
        }
        return actionRange;
    }

    public void setActionRange(Integer actionRange) {
        this.actionRange = actionRange;
    }

    public String getActionExpression() {
        if(actionExpression == null){
            actionExpression = new String();
        }
        return actionExpression;
    }

    public void setActionExpression(String actionExpression) { this.actionExpression = actionExpression; }

}
