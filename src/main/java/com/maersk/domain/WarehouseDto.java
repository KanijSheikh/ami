package com.maersk.domain;


public class WarehouseDto {

    private Integer id;
    private String name;
    private String location;
    private String locationCode;
    private String cargoHoldingDays;
    private String workingDays;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(String workingDays) {
        this.workingDays = workingDays;
    }

    public String getCargoHoldingDays() {
        return cargoHoldingDays;
    }

    public void setCargoHoldingDays(String cargoHoldingDays) {
        this.cargoHoldingDays = cargoHoldingDays;
    }
}
