package com.maersk.domain;


import java.util.List;

public class OriginDto extends CommonDto {

    private List<String> originValue;

    private String type;

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getOriginValue() {
        return originValue;
    }

    public void setOriginValue(List<String> originValue) {
        this.originValue = originValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
