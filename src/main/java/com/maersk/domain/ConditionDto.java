package com.maersk.domain;

/**
 * Created by kanij on 10/10/2017.
 */
public class ConditionDto {
    private String condition;

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
