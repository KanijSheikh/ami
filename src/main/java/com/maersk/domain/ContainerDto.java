package com.maersk.domain;

import com.maersk.common.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContainerDto extends CommonDto implements Constants {

    private String containerNumber;

    private String status;

    private String carrier;

    private String containerSizeType;

    private Double volume;

    private String loadPort;

    private String dischargePort;

    private String destination;

    private Date eta;

    private Date etd;

    private List<PurchaseOrderDto> purchaseOrderOp = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getContainerSizeType() {
        return containerSizeType;
    }

    public void setContainerSizeType(String containerSizeType) {
        this.containerSizeType = containerSizeType;
    }

    public Double getVolume() {
//        if (volume == null) {
//            volume = new Double(0.0);
//        }
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getLoadPort() {
        return loadPort;
    }

    public void setLoadPort(String loadPort) {
        this.loadPort = loadPort;
    }

    public String getDischargePort() {
        return dischargePort;
    }

    public void setDischargePort(String dischargePort) {
        this.dischargePort = dischargePort;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getEta() {
//        if (eta == null) {
//            eta = new Date();
//        }
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getEtd() {
//        if (etd == null) {
//            etd = new Date();
//        }
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public List<PurchaseOrderDto> getPurchaseOrderOp() {
        if (purchaseOrderOp == null) {
            purchaseOrderOp = new ArrayList<>();
        }
        return purchaseOrderOp;
    }

    public void setPurchaseOrderOp(List<PurchaseOrderDto> purchaseOrderOp) {
        this.purchaseOrderOp = purchaseOrderOp;
    }
}
