package com.maersk.domain;


import java.util.ArrayList;
import java.util.List;

public class DestinationDto extends CommonDto {

    private List<String> destinationValue;

    private String type;

    public List<String> getDestinationValue() {
        return destinationValue;
    }

    public void setDestinationValue(List<String> destinationValue) {
        this.destinationValue = destinationValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
