package com.maersk.domain;


import java.util.List;

public class RuleDto extends CommonDto {
    private String ruleName;
    private List<MatchingCriteriaDto> matchingCriteriaDto;
    private CoTreeDto poCoTree;
    private Integer treeOrder;


    public RuleDto(String ruleName) {
        this.ruleName = ruleName;
    }

    public RuleDto() {
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public List<MatchingCriteriaDto> getMatchingCriteriaDto() {
        return matchingCriteriaDto;
    }

    public void setMatchingCriteriaDto(List<MatchingCriteriaDto> matchingCriteriaDto) {
        this.matchingCriteriaDto = matchingCriteriaDto;
    }

    public CoTreeDto getPoCoTree() {
        return poCoTree;
    }

    public void setPoCoTree(CoTreeDto poCoTree) {
        this.poCoTree = poCoTree;
    }

    public Integer getTreeOrder() {
        return treeOrder;
    }

    public void setTreeOrder(Integer treeOrder) {
        this.treeOrder = treeOrder;
    }
}
