package com.maersk.domain;

/**
 * Created by kanij on 14/02/2018.
 */

public class ConfigOriginDto {

    private Integer id;
    private String originName;
    private String placeOfReceipt;
    private String cfsWarehouse;
    private String loadingPort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getPlaceOfReceipt() {
        return placeOfReceipt;
    }

    public void setPlaceOfReceipt(String placeOfReceipt) {
        this.placeOfReceipt = placeOfReceipt;
    }

    public String getCfsWarehouse() {
        return cfsWarehouse;
    }

    public void setCfsWarehouse(String cfsWarehouse) {
        this.cfsWarehouse = cfsWarehouse;
    }

    public String getLoadingPort() {
        return loadingPort;
    }

    public void setLoadingPort(String loadingPort) {
        this.loadingPort = loadingPort;
    }
}
