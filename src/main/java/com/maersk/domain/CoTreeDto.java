package com.maersk.domain;

public class CoTreeDto extends CommonDto {

    private String ctString;

    private String collectionConditionTreeString;

    public String getCtString() {
        return ctString;
    }

    public void setCtString(String ctString) {
        this.ctString = ctString;
    }

    public String getCollectionConditionTreeString() {
        return collectionConditionTreeString;
    }

    public void setCollectionConditionTreeString(String collectionConditionTreeString) {
        this.collectionConditionTreeString = collectionConditionTreeString;
    }
}
