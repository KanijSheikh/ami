package com.maersk.domain;


public class ContainerFillRateDto {

    private Integer id;
    private String productType;
    private String containerSize;
    private Integer minCbm;
    private Integer maxCbm;
    private Integer minKgs;
    private Integer maxKgs;
    private String origin;
    private String destination;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public Integer getMinCbm() {
        return minCbm;
    }

    public void setMinCbm(Integer minCbm) {
        this.minCbm = minCbm;
    }

    public Integer getMaxCbm() {
        return maxCbm;
    }

    public void setMaxCbm(Integer maxCbm) {
        this.maxCbm = maxCbm;
    }

    public Integer getMinKgs() {
        return minKgs;
    }

    public void setMinKgs(Integer minKgs) {
        this.minKgs = minKgs;
    }

    public Integer getMaxKgs() {
        return maxKgs;
    }

    public void setMaxKgs(Integer maxKgs) {
        this.maxKgs = maxKgs;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
