package com.maersk.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.maersk.model.Communicator;

import java.util.Date;

public class CommunicatorDto {
    private Integer id;
    private String message;

    @JsonFormat(pattern = "MM/dd/YYYY hh.mma")
    private Date createdTime;
    private String createdBy;
    private String loadplanId;


    public CommunicatorDto() {
    }

    public CommunicatorDto(Communicator communicator) {
        id = communicator.getId();
        message = communicator.getMessage();
        createdTime = communicator.getCreatedTime();
        createdBy = communicator.getCreatedBy();
        loadplanId = communicator.getLoadplan_id();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getLoadplanId() {
        return loadplanId;
    }

    public void setLoadplanId(String loadplanId) {
        this.loadplanId = loadplanId;
    }
}
