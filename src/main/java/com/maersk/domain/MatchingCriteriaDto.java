package com.maersk.domain;

import com.maersk.model.Rule;

import java.util.Date;

public class MatchingCriteriaDto extends CommonDto{
    private String criteriaName;
    private String criteriaValue;

    public String getCriteriaName() {
        return criteriaName;
    }

    public void setCriteriaName(String criteriaName) {
        this.criteriaName = criteriaName;
    }

    public String getCriteriaValue() {
        return criteriaValue;
    }

    public void setCriteriaValue(String criteriaValue) {
        this.criteriaValue = criteriaValue;
    }
}
