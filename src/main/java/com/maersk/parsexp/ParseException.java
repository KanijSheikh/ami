package com.maersk.parsexp;

@SuppressWarnings("serial")
public class ParseException extends RuntimeException {
  public ParseException(String message) {
    super(message);
  }
}
