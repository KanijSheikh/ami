package com.maersk.parsexp;

public enum TokenType {
  LEFT_PAREN,
  RIGHT_PAREN,
  COMMA,
  EQUALITY,
  PLUS,
  MINUS,
  ASTERISK,
  SLASH,
  CARET,
  TILDE,
  BANG,
  QUESTION,
  COLON,
  GRATER,//added this line
  LESS,//added this line
  AND,//added this line
  OR,//added this line
  NAME,
  EOF;
  
  /**
   * If the TokenType represents a punctuator (i.e. a token that can split an
   * identifier like '+', this will get its text.
   */
  public Character punctuator() {
    switch (this) {
    case LEFT_PAREN:  return '(';
    case RIGHT_PAREN: return ')';
    case COMMA:       return ',';
    case EQUALITY:      return '=';
    case PLUS:        return '+';
    case MINUS:       return '-';
    case ASTERISK:    return '*';
    case SLASH:       return '/';
    case CARET:       return '^';
    case TILDE:       return '~';
    case BANG:        return '!';
    case QUESTION:    return '?';
    case COLON:       return ':';
    case GRATER:      return '>';//added this line
    case LESS:        return '<';//added this line
    case AND:         return '&';//added this line
    case OR:          return '|';//added this line
    default:          return null;
    }
  }
}
