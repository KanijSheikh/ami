package com.maersk.parsexp;

public class ValueObject {


    String name;
    String value;
    String type;

    public ValueObject(String name, String value, String type) {
        this.name = name;
        this.value = value;
    }

    public ValueObject(String name, String value) {
        this.name = name;
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
