package com.maersk.parsexp;

import com.maersk.parsexp.expressions.Expression;
//import org.mvel2.*;
import com.maersk.expengine.DynamicVariableCreter;
import com.maersk.expengine.Values;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
Author : Pranav Waila
Date :   10/10/2017
*/
public class Main_new {

//    public static void main(String[] args) {
//        //test("aaa<bbb&bbb>ccc");
//        //String source_expression="ccc>bbb|(bbb+ccc)<53.2&aaa=\"abd\"";
//        //String source_expression="(bbb+ccc)>53.2&aaa=\"abd\"";
//        String source_expression="bbb+ccc>6.1&aaa=\"abd bbj\"&ccc=3.3";
//        //String source_expression = "aaa=\"abj\"";
//        //String source_expression = "aaa=\"abd\"";
//
//        //Checking if expression is valid or not
//        validateExpression(source_expression);
//        //Values valueObj=prepareExecutableStatement();
//
//
//    }



    public static Object validateExpression(String source_expression){
        Object ob=null;
        try {
            DynamicVariableCreter dvc = new DynamicVariableCreter();
            Values valueObj = dvc.generateDimVar(source_expression);
            ob = test(valueObj);
            System.out.println(ob.toString());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Expression is not in correct format");
            ob="Expression is not in correct format";
        }
        return ob;
    }

    public static Object test( Values valueObj) {
        Lexer lexer = new Lexer(valueObj.getSourceStr());
        Parser parser = new BantamParser(lexer);
        Object obj = null;

        //Test variables
        Map mp = new HashMap<String, ValueObject>();
        ValueObject vo1 = new ValueObject("aaa", "abd bbj");
        ValueObject vo2 = new ValueObject("bbb", "3");
        ValueObject vo3 = new ValueObject("ccc", "3.3");
        mp.put("aaa", vo1);
        mp.put("bbb", vo2);
        mp.put("ccc", vo3);

        Iterator it = valueObj.getVariables().entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (pair.getKey().toString().startsWith("Int")) {

                mp.put(pair.getKey(), new ValueObject(pair.getKey().toString(), pair.getValue().toString()));
            } else if (pair.getKey().toString().startsWith("Float")) {
                mp.put(pair.getKey(), new ValueObject(pair.getKey().toString(), pair.getValue().toString()));
            } else if (pair.getKey().toString().startsWith("Str")) {
                mp.put(pair.getKey(), new ValueObject(pair.getKey().toString(), "'" + pair.getValue().toString() + "'"));
            }
        }

        try {
            Expression result = parser.parseExpression();
            StringBuilder builder = new StringBuilder();
            result.print(builder);
            //System.out.println(builder);

            //added from this line to further
            Map vars = new HashMap();
            String str = builder.toString();
            System.out.println(str);
            String[] lst = str.split("[(),=+*-/^~!?:<>&| ]");
            for (String l : lst) {
                if ((!l.equals("")) && (!l.equals(" "))) {
                    vars.put(l, ((ValueObject) mp.get(l)).getValue());
                }
            }
            System.out.println("here is the evaluation:");
            String sst = builder.toString();
            sst = sst.replace("&", "&&");
            sst = sst.replace("|", "||");
            sst = sst.replace("=", "==");
            valueObj.setSourceStr(sst);
            //Evaluation
            //obj=MVEL.eval(valueObj.getSourceStr(),vars);
            //obj=MVEL.eval("aaa==\"abd\"",vars);
            obj = evaluateExpression(valueObj, vars);
            //added till here

        } catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println("   Error: " + ex.getMessage());
        }
        return obj;
    }

    public static Object evaluateExpression(Values valueObj, Map vars) {
        //System.out.println("============================");
        //System.out.println(valueObj.getSourceStr());
        //System.out.println(valueObj.getVariables());

        Iterator it = valueObj.getVariables().entrySet().iterator();
        String str = valueObj.getSourceStr();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String key = pair.getKey().toString();
            if (str.contains("Str")) {
                str = str.replace(key, "'" + pair.getValue().toString()+ "'") ;

            } else {
                str = str.replace(key, pair.getValue().toString());

            }
        }
        System.out.println("This is new String before execution");
        System.out.println(str);
        Object obj = null;
//        MVEL.eval(str, vars);
        return obj;
    }

}
