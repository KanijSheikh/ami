package com.maersk.parsexp;

/**
 * Defines the different precendence levels used by the infix parsers. These
 * determine how a series of infix expressions will be grouped. For example,
 * "a + b * c - d" will be parsed as "(a + (b * c)) - d" because "*" has higher
 * precedence than "+" and "-". Here, bigger numbers mean higher precedence.
 */
public class Precedence {
    // Ordered in increasing precedence.
    public static final int ASSIGNMENT = 1;
    public static final int CONDITIONAL = 2;

    //added this line
    public static final int LOGICAL = 4;
    public static final int RELATIONAL = 3;

    public static final int SUM = 5;
    public static final int PRODUCT = 6;
    public static final int EXPONENT = 7;
    public static final int PREFIX = 8;
    public static final int POSTFIX = 9;
    public static final int CALL = 10;
}
