package com.maersk.parsexp.parselets;

import com.maersk.parsexp.Parser;
import com.maersk.parsexp.Token;
import com.maersk.parsexp.TokenType;
import com.maersk.parsexp.expressions.Expression;

/**
 * Parses parentheses used to group an expression, like "a * (b + c)".
 */
public class GroupParselet implements PrefixParselet {
  public Expression parse(Parser parser, Token token) {
    Expression expression = parser.parseExpression();
    parser.consume(TokenType.RIGHT_PAREN);
    return expression;
  }
}
