package com.maersk.parsexp.parselets;

import com.maersk.parsexp.ParseException;
import com.maersk.parsexp.Parser;
import com.maersk.parsexp.Precedence;
import com.maersk.parsexp.Token;
import com.maersk.parsexp.expressions.AssignExpression;
import com.maersk.parsexp.expressions.Expression;
import com.maersk.parsexp.expressions.NameExpression;

/**
 * Parses assignment expressions like "a = b". The left side of an assignment
 * expression must be a simple name like "a", and expressions are
 * right-associative. (In other words, "a = b = c" is parsed as "a = (b = c)").
 */
public class AssignParselet implements InfixParselet {
  public Expression parse(Parser parser, Expression left, Token token) {
    Expression right = parser.parseExpression(Precedence.ASSIGNMENT - 1);

    //chainging here
    if (!(left instanceof NameExpression)) throw new ParseException(
        "The left-hand side of an assignment must be a name.");

    String name = ((NameExpression)left).getName();
    return new AssignExpression(name, right);
  }

  public int getPrecedence() { return Precedence.ASSIGNMENT; }
}