package com.maersk.parsexp.parselets;

import com.maersk.parsexp.Parser;
import com.maersk.parsexp.Precedence;
import com.maersk.parsexp.Token;
import com.maersk.parsexp.TokenType;
import com.maersk.parsexp.expressions.ConditionalExpression;
import com.maersk.parsexp.expressions.Expression;

/**
 * Parselet for the condition or "ternary" operator, like "a ? b : c".
 */
public class ConditionalParselet implements InfixParselet {
  public Expression parse(Parser parser, Expression left, Token token) {
    Expression thenArm = parser.parseExpression();
    parser.consume(TokenType.COLON);
    Expression elseArm = parser.parseExpression(Precedence.CONDITIONAL - 1);
    
    return new ConditionalExpression(left, thenArm, elseArm);
  }

  public int getPrecedence() {
    return Precedence.CONDITIONAL;
  }
}