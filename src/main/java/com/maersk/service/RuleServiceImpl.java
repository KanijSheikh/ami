package com.maersk.service;

import com.maersk.common.Constants;
import com.maersk.model.MatchingCriteria;
import com.maersk.model.Rule;
import com.maersk.repositories.RuleRepository;
import com.maersk.util.DeepCopy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/3/2017.
 */
@Service
public class RuleServiceImpl implements RuleService, Constants {


    @Autowired
    private RuleRepository ruleRepository;

    @Override
    public Integer add(Rule rule) {
        if (rule.getId() != null && !rule.getId().equals(0)) {
            ruleRepository.remove(rule.getId());
        }else {
            rule.setTreeOrder(getNextTreeOrder());
        }
        rule.setId(null);
        return ruleRepository.add(rule);
    }

    private Integer getNextTreeOrder(){
        Integer maxTreeOrder = ruleRepository.getMaxTreeOrder();
        return maxTreeOrder != null ? maxTreeOrder+1 : 1;
    }

    @Override
    public void copy(Integer ruleId) {
        Rule rule = ruleRepository.getRule(ruleId);
        Rule rule1 = (Rule) DeepCopy.copy(rule);
        String name = "copy_" + rule.getName();
        rule1.setName(name);
        rule1.setId(null);
        rule1.setCreatedBy(userId);
        rule1.setCreatedDate(new Date());
        rule1.setModifiedBy(userId);
        rule1.setModifiedDate(new Date());
        if (nonNull(rule1.getMatchingCriteriaList())) {
            for (MatchingCriteria matchingCriteria : rule1.getMatchingCriteriaList()) {
                matchingCriteria.setId(null);
                matchingCriteria.setModifiedBy(userId);
                matchingCriteria.setModifiedDate(new Date());
            }
        }
        rule1.getPoCoTree().setId(null);
        rule1.getPoCoTree().setModifiedBy(userId);
        rule1.getPoCoTree().setModifiedDate(new Date());
        rule1.setTreeOrder(getNextTreeOrder());
        ruleRepository.copy(rule1);
    }

    @Override
    public MatchingCriteria getRules(Rule rule) {

        return null;
    }

    @Override
    public Collection<Rule> getRules(String criteria, Integer ruleId) {
        if (nonNull(criteria)) {
            return ruleRepository.getRulesBy(criteria);
        } else if (nonNull(ruleId)) {
            return ruleRepository.getRulesBy(ruleId);
        } else {
            return ruleRepository.getAllRules();
        }
    }

    @Override
    public void remove(Integer ruleId) {
        Rule rule = ruleRepository.getRule(ruleId);
        Integer treeOrder = rule.getTreeOrder();
        ruleRepository.decreaseTreeOrderFrom(treeOrder);
        ruleRepository.remove(ruleId);
    }

    @Override
    public Collection<Rule> getAllRules() {
        return ruleRepository.getAllRules();
    }

    @Override
    public Collection<Rule> getOnlyRules() {
        List rules = ruleRepository.getOnlyRules();
        Collection<Rule> rules1 = new ArrayList<>();
        if (nonNull(rules) && !rules.isEmpty()) {
            for (int i = 0; i < rules.size(); i++) {
                Rule rule = new Rule();
                rule.setId((Integer) ((Object[]) rules.get(i))[0]);
                rule.setCreatedBy((String) ((Object[]) rules.get(i))[1]);
                rule.setModifiedBy((String) ((Object[]) rules.get(i))[3]);
                rule.setName((String) ((Object[]) rules.get(i))[5]);
                rule.setCreatedDate((Date) ((Object[]) rules.get(i))[2]);
                rule.setModifiedDate((Date) ((Object[]) rules.get(i))[4]);
                rule.setTreeOrder((Integer) ((Object[]) rules.get(i))[6]);
                rules1.add(rule);
            }
        }
        return rules1;
    }

    @Transactional
    @Override
    public void reOrderRule(Integer ruleId, Integer oldOrder, Integer newOrder) {
        ruleRepository.changeTreeOrder(oldOrder, newOrder);
        ruleRepository.updateTreeOrder(ruleId, newOrder);
    }

}

