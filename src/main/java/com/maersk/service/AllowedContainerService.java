package com.maersk.service;

import com.maersk.domain.AllowedContainerDto;
import com.maersk.domain.WarehouseDto;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by Pankaj on 10/17/2017.
 */
public interface AllowedContainerService {

    void importAllowedContainer(XSSFSheet mySheet, DataFormatter formatter);

    Collection<AllowedContainerDto> getAllowedContainers();

    void remove(List<Integer> containerIds);

    void add(AllowedContainerDto allowedContainerDto);
}
