package com.maersk.service;

import com.maersk.domain.WarehouseDto;
import com.maersk.model.Location;
import com.maersk.model.Warehouse;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/17/2017.
 */
public interface WarehouseService {

    void importWarehouse(XSSFSheet mySheet, DataFormatter formatter);

    Collection<WarehouseDto> getWarehouses();

    void remove(List<Integer> warehouseIds);

    void add(WarehouseDto warehouse);
}
