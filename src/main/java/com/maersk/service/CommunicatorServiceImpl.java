package com.maersk.service;

import com.maersk.model.Communicator;
import com.maersk.repositories.CommunicatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static java.util.Objects.nonNull;

@Service
public class CommunicatorServiceImpl implements CommunicatorService {
    @Autowired
    private CommunicatorRepository communicatorRepository;

    @Override
    public void add(Communicator communicator) {
        communicatorRepository.add(communicator);
    }

    @Override
    public Collection<Communicator> getMessages(String loadplanId) {
        if (nonNull(loadplanId)) {
            return communicatorRepository.getRulesBy(loadplanId);
        } else {
            return communicatorRepository.getAllRules();
        }
    }
}
