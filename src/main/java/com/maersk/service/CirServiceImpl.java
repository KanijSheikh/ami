package com.maersk.service;

import com.maersk.common.Constants;
import com.maersk.model.PurchaseOrderOp;
import com.maersk.repositories.CirRepository;
import com.maersk.repositories.PurchaseOrderRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class CirServiceImpl implements CirService, Constants {

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    CirRepository cirRepository;

    private static final Logger LOG = LoggerFactory.getLogger(CirServiceImpl.class);

    @Override
    public void uploadCirReport(XSSFSheet mySheet, DataFormatter formatter, String fileType) {

        if (fileType.equalsIgnoreCase("CIR-CHINA")) {
            greaterChinaCirUpload(mySheet, formatter);
        } else if (fileType.equalsIgnoreCase("CIR-EUDS")) {
            euDcCirUpload(mySheet, formatter);
        } else if (fileType.equalsIgnoreCase("CIR-APLA")) {
            aplaCirUpload(mySheet, formatter);
        } else if (fileType.equalsIgnoreCase("CIR-EUDRS")) {
            euDrsCirUpload(mySheet, formatter);
        }

    }

    public void euDrsCirUpload(XSSFSheet mySheet, DataFormatter formatter) {
        Collection<PurchaseOrderOp> purchaseOrderOpCollection = purchaseOrderRepository.getAllPoWithStatus("not planned");

        Map<String, Collection<PurchaseOrderOp>> poHashMap = fillPoHashMap(purchaseOrderOpCollection, new HashMap<>());

        Collection<PurchaseOrderOp> purchaseOrderOpCollectionOp = new ArrayList<>();

        int rowNum = mySheet.getPhysicalNumberOfRows() - 1; //excluding headers.

        for (int i = 0; i < rowNum; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {

                int poItem = new Double(row.getCell(6).getNumericCellValue()).intValue();
                String poNumber = formatter.formatCellValue(row.getCell(5)) + "-" + String.format("%05d", poItem);

                LOG.debug(poNumber);

                if (poHashMap.containsKey(poNumber)) {

                    Collection<PurchaseOrderOp> orderOpCollection = poHashMap.get(poNumber);

                    for (PurchaseOrderOp purchaseOrderOp : orderOpCollection) {

                        Date edd = null;
                        Date ldd = null;

                        String globalPo = formatter.formatCellValue(row.getCell(14));
                        String cirMot = formatter.formatCellValue(row.getCell(7));
                        String globalPoFlag = formatter.formatCellValue(row.getCell(8));
                        String customerPo = formatter.formatCellValue(row.getCell(16));
                        String launchInd = formatter.formatCellValue(row.getCell(21));
                        String purchaseGroup = formatter.formatCellValue(row.getCell(22));
                        String aaMaterial = formatter.formatCellValue(row.getCell(27));

                        if (nonNull(row.getCell(11))) {
                            edd = row.getCell(11).getDateCellValue();
                        }
                        if (nonNull(row.getCell(12))) {
                            ldd = row.getCell(12).getDateCellValue();
                        }

                        purchaseOrderOp.setPoLineReference1(globalPo);
                        purchaseOrderOp.setPoLineReference4(cirMot);
                        purchaseOrderOp.setPoLineReference5(globalPoFlag);
                        purchaseOrderOp.setPoLineReference11(customerPo);
                        purchaseOrderOp.setPoLineReference9(launchInd);
                        purchaseOrderOp.setPoLineReference7(purchaseGroup);
                        purchaseOrderOp.setPoLineReference8(aaMaterial);
                        purchaseOrderOp.setExpectedDeliveryDate(edd);
                        purchaseOrderOp.setLatestDeliveryDate(ldd);

                        purchaseOrderOp.setModifiedBy(userId);
                        purchaseOrderOp.setModifiedDate(new Date());

                        purchaseOrderOpCollectionOp.add(purchaseOrderOp);

                    }
                }
            }
        }

        cirRepository.cirUpload(purchaseOrderOpCollectionOp);
    }

    public Map<String, Collection<PurchaseOrderOp>> fillPoHashMap(Collection<PurchaseOrderOp> purchaseOrderOpCollection, Map<String, Collection<PurchaseOrderOp>> poHashMap) {
        if (!purchaseOrderOpCollection.isEmpty()) {
            for (PurchaseOrderOp po : purchaseOrderOpCollection) {
                if (poHashMap.containsKey(po.getPoNumber())) {
                    poHashMap.get(po.getPoNumber()).add(po);
                } else {
                    Collection<PurchaseOrderOp> orderOpCollection = new ArrayList<>();
                    orderOpCollection.add(po);
                    poHashMap.put(po.getPoNumber(), orderOpCollection);
                }
            }
        }

        return poHashMap;
    }

    public void euDcCirUpload(XSSFSheet mySheet, DataFormatter formatter) {
        Collection<PurchaseOrderOp> purchaseOrderOpCollection = purchaseOrderRepository.getAllPoWithStatus("not planned");

        Map<String, Collection<PurchaseOrderOp>> poHashMap = fillPoHashMap(purchaseOrderOpCollection, new HashMap<>());

        Collection<PurchaseOrderOp> purchaseOrderOpCollectionOp = new ArrayList<>();

        int rowNum = mySheet.getPhysicalNumberOfRows() - 1; //excluding headers.

        for (int i = 0; i < rowNum; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(5))) {

                int poItem = new Double(row.getCell(6).getNumericCellValue()).intValue();
                String poNumber = formatter.formatCellValue(row.getCell(5)) + "-" + String.format("%05d", poItem);

                LOG.debug(poNumber);

                if (poHashMap.containsKey(poNumber)) {

                    Collection<PurchaseOrderOp> orderOpCollection = poHashMap.get(poNumber);
                    for (PurchaseOrderOp purchaseOrderOp : orderOpCollection) {

                        Date edd = null;
                        Date ldd = null;

                        String globalPo = formatter.formatCellValue(row.getCell(9));
                        String globalPoFlag = formatter.formatCellValue(row.getCell(8));
                        String cirMot = formatter.formatCellValue(row.getCell(7));
                        Integer quantity = null;
                        Integer currentMonth12 = null;
                        Integer currentMonth = null;
                        Integer currentMonth1 = null;
                        Integer currentMonth2 = null;
                        Integer currentMonthGt2 = null;

                        if (nonNull(row.getCell(14))) {
                            quantity = Integer.parseInt(formatter.formatCellValue(row.getCell(14)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(25))) {
                            currentMonth12 = Integer.parseInt(formatter.formatCellValue(row.getCell(25)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(26))) {
                            currentMonth = Integer.parseInt(formatter.formatCellValue(row.getCell(26)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(27))) {
                            currentMonth1 = Integer.parseInt(formatter.formatCellValue(row.getCell(27)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(28))) {
                            currentMonth2 = Integer.parseInt(formatter.formatCellValue(row.getCell(28)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(29))) {
                            currentMonthGt2 = Integer.parseInt(formatter.formatCellValue(row.getCell(29)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(11))) {
                            edd = row.getCell(11).getDateCellValue();
                        }
                        if (nonNull(row.getCell(12))) {
                            ldd = row.getCell(12).getDateCellValue();
                        }

                        String launchInd = formatter.formatCellValue(row.getCell(18));
                        String purchaseGroup = formatter.formatCellValue(row.getCell(19));
                        String aaMaterialCode = formatter.formatCellValue(row.getCell(30));

                        purchaseOrderOp.setPoLineReference1(globalPo);
                        purchaseOrderOp.setPoLineReference4(cirMot);
                        purchaseOrderOp.setPoLineReference5(globalPoFlag);
                        purchaseOrderOp.setPoLineReference7(purchaseGroup);
                        purchaseOrderOp.setPoLineReference8(aaMaterialCode);
                        purchaseOrderOp.setPoLineReference9(launchInd);
                        purchaseOrderOp.setPoLineReference10(generateSortCodeByQty(quantity, currentMonth, currentMonth1, currentMonth12, currentMonth2, currentMonthGt2));

                        purchaseOrderOp.setExpectedDeliveryDate(edd);
                        purchaseOrderOp.setLatestDeliveryDate(ldd);
                        purchaseOrderOp.setModeOfTransport(cirMot);

                        purchaseOrderOp.setModifiedBy(userId);
                        purchaseOrderOp.setModifiedDate(new Date());

                        purchaseOrderOpCollectionOp.add(purchaseOrderOp);
                    }
                }
            }
        }

        cirRepository.cirUpload(purchaseOrderOpCollectionOp);
    }

    public String generateSortCodeByQty(Integer qty, Integer currMonth, Integer currMonth1, Integer currMonth12, Integer currMonth2, Integer currMonthGt2) {

        String sortCode;

        double firstDigit = (currMonth / qty) * 100;
        double secondDigit = (currMonth1 / qty) * 100;
        double thirdDigit = (currMonth12 / qty) * 100;
        double fourthDigit = (currMonth2 / qty) * 100;
        double fifthDigit = (currMonthGt2 / qty) * 100;

        sortCode = String.valueOf(formatterPercent3.format(firstDigit)) + String.valueOf(formatterPercent3.format(secondDigit)) + String.valueOf(formatterPercent3.format(thirdDigit)) + String.valueOf(formatterPercent3.format(fourthDigit)) + String.valueOf(formatterPercent3.format(fifthDigit));

        return sortCode;
    }


    public void aplaCirUpload(XSSFSheet mySheet, DataFormatter formatter) {
        Collection<PurchaseOrderOp> purchaseOrderOpCollection = purchaseOrderRepository.getAllPoWithStatus("not planned");

        Map<String, Collection<PurchaseOrderOp>> poHashMap = fillPoHashMap(purchaseOrderOpCollection, new HashMap<>());

        Collection<PurchaseOrderOp> purchaseOrderOpCollectionOp = new ArrayList<>();

        int rowNum = mySheet.getPhysicalNumberOfRows() - 1; //excluding headers.

        for (int i = 0; i < rowNum; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {

                Integer poItem = Integer.parseInt(row.getCell(1).getStringCellValue());
                String poNumber = formatter.formatCellValue(row.getCell(0)) + "-" + String.format("%05d", poItem);

                LOG.debug(poNumber);

                if (poHashMap.containsKey(poNumber)) {

                    Collection<PurchaseOrderOp> orderOpCollection = poHashMap.get(poNumber);
                    for (PurchaseOrderOp purchaseOrderOp : orderOpCollection) {

                        Date edd = null;
                        Date ldd = null;

                        String globalPo = formatter.formatCellValue(row.getCell(14));
                        String earlyDelivery = formatter.formatCellValue(row.getCell(23));
                        String materialAvailableDate = formatter.formatCellValue(row.getCell(36));


                        if (nonNull(row.getCell(17))) {
                            edd = row.getCell(17).getDateCellValue();
                        }
                        if (nonNull(row.getCell(16))) {
                            ldd = row.getCell(16).getDateCellValue();
                        }

                        purchaseOrderOp.setPoLineReference1(globalPo);
                        purchaseOrderOp.setPoLineReference2(earlyDelivery);
                        purchaseOrderOp.setPoLineReference3(materialAvailableDate);
                        purchaseOrderOp.setExpectedDeliveryDate(edd);
                        purchaseOrderOp.setLatestDeliveryDate(ldd);

                        purchaseOrderOp.setModifiedBy(userId);
                        purchaseOrderOp.setModifiedDate(new Date());

                        purchaseOrderOpCollectionOp.add(purchaseOrderOp);
                    }
                }
            }
        }

        cirRepository.cirUpload(purchaseOrderOpCollectionOp);
    }


    public void greaterChinaCirUpload(XSSFSheet mySheet, DataFormatter formatter) {

        Collection<PurchaseOrderOp> purchaseOrderOpCollection = purchaseOrderRepository.getAllPoWithStatus("not planned");

        Map<String, Collection<PurchaseOrderOp>> poHashMap = fillPoHashMap(purchaseOrderOpCollection, new HashMap<>());

        Collection<PurchaseOrderOp> purchaseOrderOpCollectionOp = new ArrayList<>();

        int rowNum = mySheet.getPhysicalNumberOfRows() - 1; //excluding headers.

        for (int i = 0; i < rowNum; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(5))) {

                int poItem = new Double(row.getCell(6).getNumericCellValue()).intValue();
                String poNumber = formatter.formatCellValue(row.getCell(5)) + "-" + String.format("%05d", poItem);

                LOG.debug(poNumber);

                if (poHashMap.containsKey(poNumber)) {

                    Collection<PurchaseOrderOp> orderOpCollection = poHashMap.get(poNumber);
                    for (PurchaseOrderOp purchaseOrderOp : orderOpCollection) {

                        Date edd = null;
                        Date ldd = null;

                        String globalPo = formatter.formatCellValue(row.getCell(9));
                        String globalPoFlag = formatter.formatCellValue(row.getCell(8));
                        String cirMot = formatter.formatCellValue(row.getCell(7));

                        if (nonNull(row.getCell(11))) {
                            edd = row.getCell(11).getDateCellValue();
                        }
                        if (nonNull(row.getCell(12))) {
                            ldd = row.getCell(12).getDateCellValue();
                        }

                        // Double currentMonth12 = null;
                        Double currentMonth = null;
                        Double currentMonth1 = null;
                        Double currentMonth2 = null;
                        Double currentMonthGt2 = null;

//                        if (nonNull(row.getCell(25))) {
//                            currentMonth12 = Double.parseDouble(formatter.formatCellValue(row.getCell(25)).replace(",", ""));
//                        }
                        if (nonNull(row.getCell(25))) {
                            currentMonth = Double.parseDouble(formatter.formatCellValue(row.getCell(25)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(26))) {
                            currentMonth1 = Double.parseDouble(formatter.formatCellValue(row.getCell(26)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(27))) {
                            currentMonth2 = Double.parseDouble(formatter.formatCellValue(row.getCell(27)).replace(",", ""));
                        }
                        if (nonNull(row.getCell(28))) {
                            currentMonthGt2 = Double.parseDouble(formatter.formatCellValue(row.getCell(28)).replace(",", ""));
                        }
                        String sortCode = String.valueOf(formatterPercent3.format(currentMonth)) + String.valueOf(formatterPercent3.format(currentMonth1)) + String.valueOf(formatterPercent3.format(currentMonth2)) + String.valueOf(formatterPercent3.format(currentMonthGt2));


                        String launchInd = formatter.formatCellValue(row.getCell(18));
                        String purchaseGroup = formatter.formatCellValue(row.getCell(19));
                        String aaMaterialCode = formatter.formatCellValue(row.getCell(30));

                        purchaseOrderOp.setPoLineReference1(globalPo);
                        purchaseOrderOp.setPoLineReference4(cirMot);
                        purchaseOrderOp.setPoLineReference5(globalPoFlag);
                        purchaseOrderOp.setPoLineReference7(purchaseGroup);
                        purchaseOrderOp.setPoLineReference8(aaMaterialCode);
                        purchaseOrderOp.setPoLineReference9(launchInd);
                        purchaseOrderOp.setPoLineReference10(sortCode);

                        purchaseOrderOp.setExpectedDeliveryDate(edd);
                        purchaseOrderOp.setLatestDeliveryDate(ldd);
                        purchaseOrderOp.setModeOfTransport(cirMot);

                        purchaseOrderOp.setModifiedBy(userId);
                        purchaseOrderOp.setModifiedDate(new Date());

                        purchaseOrderOpCollectionOp.add(purchaseOrderOp);

                    }
                }
            }
        }

        cirRepository.cirUpload(purchaseOrderOpCollectionOp);

    }
}



