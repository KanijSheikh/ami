package com.maersk.service;

import com.maersk.model.VesselSchedule;
import com.maersk.repositories.VesselScheduleRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.parseInt;
import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;


@Service
public class VesselScheduleServiceImpl implements VesselScheduleService {

    @Autowired
    VesselScheduleRepository vesselScheduleRepository;

    @Override
    public void importVesselSchedule(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<VesselSchedule> vesselSchedules = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);
            if (row != null && nonNull(row.getCell(0))) {
                getVesselSchedules(vesselSchedules, row);
            }
        }
        vesselScheduleRepository.add(vesselSchedules);
    }

    private void getVesselSchedules(List<VesselSchedule> vesselSchedules, Row row) {
      VesselSchedule vesselSchedule=new VesselSchedule();

        vesselSchedule.setModeOfTransport(row.getCell(0).getStringCellValue());
        vesselSchedule.setLoadingPort(row.getCell(1).getStringCellValue());
        vesselSchedule.setDischargePort(row.getCell(2).getStringCellValue());
        vesselSchedule.setCarrier(row.getCell(3).getStringCellValue());
        vesselSchedule.setLoop(row.getCell(4).getStringCellValue());
        vesselSchedule.setTransitTime((int)row.getCell(5).getNumericCellValue());
        vesselSchedule.setEtd(row.getCell(6).getStringCellValue());
        vesselSchedule.setEta(row.getCell(7).getStringCellValue());
        vesselSchedule.setActivationDate(row.getCell(8).getDateCellValue());
        vesselSchedule.setDeactivationDate(row.getCell(9).getDateCellValue());
        vesselSchedule.setCuttOffDay((int)row.getCell(10).getNumericCellValue());
        vesselSchedule.setTwentyDryPerWeek((int)row.getCell(11).getNumericCellValue());
        vesselSchedule.setFourtyDryPerWeek((int)row.getCell(12).getNumericCellValue());
        vesselSchedule.setFourtyDryHcPerWeek((int)row.getCell(13).getNumericCellValue());
        vesselSchedule.setFourtyFiveDryPerWeek((int)row.getCell(14).getNumericCellValue());

      vesselSchedules.add(vesselSchedule);
    }

    @Override
    public Collection<VesselSchedule> getVesselSchedule() {
        return vesselScheduleRepository.getVesselSchedule();
    }

    @Override
    public void remove(List<Integer> vesselIds) {
        vesselScheduleRepository.remove(vesselIds);
    }

    @Override
    public void add(VesselSchedule vesselSchedule) {
        if (vesselSchedule.getId() != null && !vesselSchedule.getId().equals(0)) {
            vesselScheduleRepository.remove(Collections.singletonList(vesselSchedule.getId()));
        }
        vesselSchedule.setId(null);
        vesselScheduleRepository.add(singletonList(vesselSchedule));

    }
}



