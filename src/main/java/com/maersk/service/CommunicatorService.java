package com.maersk.service;

import com.maersk.model.Communicator;

import java.util.Collection;

public interface CommunicatorService {
    void add(Communicator communicator);

    Collection<Communicator> getMessages(String loadplanId);
}
