package com.maersk.service;

import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.ContainerFillRate;
import com.maersk.repositories.ContainerFillRateRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 12/02/2018.
 */
@Service
public class ContainerFillRateServiceImpl implements ContainerFillRateService {

    @Autowired
    private ContainerFillRateRepository containerFillRateRepository;

    @Override
    public void importContainerFillRate(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<ContainerFillRate> containers = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getLocation(containers, row);
            }
        }
        containerFillRateRepository.add(containers);
    }


    private void getLocation(List<ContainerFillRate> containerFillRates, Row row) {
        ContainerFillRate container = new ContainerFillRate();

        container.setProductType(row.getCell(0).getStringCellValue());
        container.setContainerSize(row.getCell(1).getStringCellValue());
        container.setMinCbm((int) row.getCell(2).getNumericCellValue());
        container.setMaxCbm((int) row.getCell(3).getNumericCellValue());
        container.setMinKgs((int) row.getCell(4).getNumericCellValue());
        container.setMaxKgs((int) row.getCell(5).getNumericCellValue());
        container.setOrigin( row.getCell(6).getStringCellValue());
        container.setDestination(row.getCell(7).getStringCellValue());

        containerFillRates.add(container);
    }

    @Override
    public Collection<ContainerFillRate> getContainerFillRate() {
        Collection<ContainerFillRate> container = containerFillRateRepository.getContainerFillRate();
        if (container.isEmpty()) {
            throw new EntityNotFoundException("Container fill rate records not found");
        }
        return container;
    }

    @Override
    public void remove(List<Integer> containerIds) {
        containerFillRateRepository.remove(containerIds);
    }


    @Override
    public void add(ContainerFillRate containerFillRate) {

        if (containerFillRate.getId() != null && !containerFillRate.getId().equals(0)) {
            containerFillRateRepository.remove(Collections.singletonList(containerFillRate.getId()));
        }
        containerFillRate.setId(null);
        containerFillRateRepository.add(singletonList(containerFillRate));

    }

}



