package com.maersk.service;

import com.maersk.model.ConfigDestination;
import com.maersk.model.ConfigOrigin;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 14/02/2018.
 */
public interface OriginService {

    void importOrigin(XSSFSheet mySheet, DataFormatter formatter);

    Collection<ConfigOrigin> getOrigin();

    void remove(List<Integer> originIds);

    void add(ConfigOrigin configOrigin);
}
