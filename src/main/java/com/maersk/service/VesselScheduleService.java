package com.maersk.service;

import com.maersk.model.VesselSchedule;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

public interface VesselScheduleService {

    void importVesselSchedule(XSSFSheet mySheet, DataFormatter formatter);

    Collection<VesselSchedule> getVesselSchedule();

    void remove(List<Integer> vesselIds);

    void add(VesselSchedule vesselSchedule);

}
