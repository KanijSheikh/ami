package com.maersk.service;

import com.maersk.model.*;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public interface OptionsService {

    Collection<Options> getAllOptions(String type1,String type2);
}
