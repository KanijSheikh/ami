package com.maersk.service;

import com.maersk.model.Options;
import com.maersk.repositories.OptionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;


@Service
public class OptionsServiceImpl implements OptionsService {

    @Autowired
    OptionsRepository optionsRepository;

    @Override
    public Collection<Options> getAllOptions(String type1,String type2) {
        return optionsRepository.getAllOptions(type1,type2);
    }
}
