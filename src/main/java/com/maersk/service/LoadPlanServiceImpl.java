package com.maersk.service;

import com.maersk.common.Constants;
import com.maersk.domain.ContainerTypeDto;
import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.SimulateLoadPlan;
import com.maersk.repositories.LoadPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

import static com.maersk.common.Constants.userId;

@Service
public class LoadPlanServiceImpl implements LoadPlanService, Constants {

    @Autowired
    LoadPlanRepository loadPlanRepository;

    @Resource
    AppDataService appDataService;


    public LoadPlan getLoadPlan(Integer id) {
        return loadPlanRepository.getLoadPlan(id);
    }

    @Override
    public void saveSimulateData(SimulateLoadPlan simulateLoadPlan) {
        simulateLoadPlan.setCreatedDate(new Date());
        simulateLoadPlan.setModifiedDate(new Date());
        simulateLoadPlan.setModifiedBy(userId);
        simulateLoadPlan.setCreatedBy(userId);
        loadPlanRepository.saveSimulateData(simulateLoadPlan);
    }

    @Override
    public SimulateLoadPlan getSimulateData() {
        return loadPlanRepository.getSimulateData();
    }

    @Override
    public void removeSimulateLoadPlan(Integer id) {
        loadPlanRepository.removeSimulateLoadPlan(id);
    }

    @Override
    public void AddContainer(Integer loadPlanId, Container container) {
        List<Container> containerList = new ArrayList<>();
        HashMap<String, Integer> containerTypeVolumeMap = new HashMap<>();
        try {
            List<ContainerTypeDto> containerTypes = appDataService.getContainerTypes();
            for (ContainerTypeDto containerTypeDto: containerTypes) {
                containerTypeVolumeMap.put(containerTypeDto.getContainerType(), containerTypeDto.getVolume());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Integer timeInSec = (int) System.currentTimeMillis();
        if (timeInSec < 0) {
            timeInSec = timeInSec * (-1);
        }
        container.setStatus("In progress");
        container.setVolume(0.0);
        Integer key = timeInSec + containerTypeVolumeMap.get(container.getContainerSizeType());
        container.setContainerNumber(key.toString() + "-" + containerTypeVolumeMap.get(container.getContainerSizeType()) + "-" + 1);
        containerList.add(container);
        loadPlanRepository.AddContainer(containerList, loadPlanId);
    }

    @Override
    public void deleteContainer(Collection<Container> containers) {
        loadPlanRepository.deleteContainer(containers);
    }
}
