package com.maersk.service;

import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.Location;
import com.maersk.model.PurchaseOrderOp;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/17/2017.
 */
public interface LocationService {

    void importLocations(XSSFSheet mySheet, DataFormatter formatter);

    Collection<Location> getLocation();

    void remove(List<Integer> locationIds);

    void add(Location location);
}
