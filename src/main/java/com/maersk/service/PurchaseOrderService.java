package com.maersk.service;

import com.maersk.domain.PurchaseOrderDto;
import com.maersk.expengine.Response;
import com.maersk.model.*;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 10/17/2017.
 */
public interface PurchaseOrderService {

    Collection<PurchaseOrderOp> getPurchaseOrders(LoadPlan loadPlan);

    //void actionOnPurchaseOrder(Collection<PurchaseOrderOp> purcahseOrders);

    String processPurchaseOrderAndExecuteRules(LoadPlan loadPlan, Boolean confirm, Integer loadPlanId, Collection<PurchaseOrderOp> purchaseOrderOps, Boolean reEvaluate) throws Exception;

    Collection<LoadPlan> getLoadPlansByStatus(String status);

    Collection<Container> getContainersByStatus(String status, Integer id);

    Collection<PurchaseOrderOp> getAllPo(Integer id, String poStatus);

    Collection<PurchaseOrderOp> getAllPoExException(Integer id, String poStatus);

    LoadPlan simulateLoadPlan(Boolean reEvaluate, Collection<PurchaseOrderOp> purchaseOrderOpCollection) throws Exception;

    String importPurchaseOrders(XSSFSheet mySheet, DataFormatter formatter);

    void addPurchaseOrder(Container container);

    void deletePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps);

    void splitPurchaseOrder(PurchaseOrderOp purchaseOrderOp);

    Collection<PurchaseOrderOp> getAllPoForLoadPlan(Integer id);

    void cancelPurchaseOrders(BufferedReader bufferedReader) throws IOException;


}
