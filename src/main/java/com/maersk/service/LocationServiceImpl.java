package com.maersk.service;

import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.Location;
import com.maersk.repositories.LocationRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/17/2017.
 */
@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public void importLocations(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getLocation(locations, row);
            }
        }
        locationRepository.add(locations);
    }


    private void getLocation(List<Location> locations, Row row) {
        Location location = new Location();

        location.setLocation(row.getCell(0).getStringCellValue());
        location.setLocationCode(row.getCell(1).getStringCellValue());
        location.setCity(row.getCell(2).getStringCellValue());
        location.setCityCode(row.getCell(3).getStringCellValue());
        location.setCountry(row.getCell(4).getStringCellValue());
        location.setCountryCode(row.getCell(5).getStringCellValue());

        locations.add(location);
    }

    @Override
    public Collection<Location> getLocation() {
        Collection<Location> location = locationRepository.getLocation();
        if (location.isEmpty()) {
            throw new EntityNotFoundException("Locations not found");
        }
        return location;
    }

    @Override
    public void remove(List<Integer> locationIds) {
        locationRepository.remove(locationIds);
    }


    @Override
    public void add(Location location) {

        if (location.getId() != null && !location.getId().equals(0)) {
            locationRepository.remove(Collections.singletonList(location.getId()));
        }
        location.setId(null);
        locationRepository.add(singletonList(location));

    }

}



