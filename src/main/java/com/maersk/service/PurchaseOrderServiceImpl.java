package com.maersk.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.maersk.common.Constants;
import com.maersk.domain.ActionDto;
import com.maersk.domain.CoTreeStringDto;
import com.maersk.domain.ContainerTypeDto;
import com.maersk.enums.AppDataType;
import com.maersk.expengine.Response;
import com.maersk.expengine.eval.InfixExpEvaluator;
import com.maersk.expengine.eval.Variable;
import com.maersk.expengine.eval.infixEvalExp;
import com.maersk.expengine.util.MathUtils;
import com.maersk.job.JobService;
import com.maersk.model.*;
import com.maersk.optimizer.KnapsackDP;
import com.maersk.repositories.*;
import com.maersk.repositories.AppDataRepository;
import com.maersk.util.DeepCopy;
import com.maersk.util.PurchaseOrderUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/17/2017.
 */
@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService, Constants {

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private LoadPlanRepository loadPlanRepository;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private LoadPlanService loadPlanService;

    @Autowired
    private ContainerFillRateRepository containerFillRateRepository;


    @Autowired
    private OriginRepository originRepository;

    @Autowired
    private AppDataService appDataService;

    @Autowired
    private LocationRepository locationRepository;

    private infixEvalExp iee = new infixEvalExp();

    private ObjectMapper mapper = new ObjectMapper();

    private PurchaseOrderUtils pou = new PurchaseOrderUtils();

    private Integer cVolumes[] = {86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76,
            76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76,
            76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67,
            67, 67, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86, 86,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67,
            67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67, 67,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33,
            33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33, 33};

    private Integer timeInSec = 425;
    private int con_max_fil_rate = 90;

    private Random rand = new Random();

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderServiceImpl.class);


    @Override
    public Collection<PurchaseOrderOp> getPurchaseOrders(LoadPlan loadPlan) {
        String origin = "";
        String destination = "";
        String originType = "";
        String destinationType = "";
        String placeOfReceipts = "";

        if (nonNull(loadPlan)) {
            if (loadPlan.getOrigin().get(0) != null) {
                originType = loadPlan.getOrigin().get(0).getType();
                for (Origin origin1 : loadPlan.getOrigin()) {
                    origin = origin + "'" + origin1.getOrignValue() + "',";
                }
                List<String> placeOfReceipt = originRepository.getPlaceOfReceiptByOrigin(origin.substring(0, origin.lastIndexOf(",")));
                for (String por : placeOfReceipt) {
                    placeOfReceipts = placeOfReceipts + "'" + por + "',";
                }
            }

            if (loadPlan.getDestination().get(0) != null) {
                destinationType = loadPlan.getDestination().get(0).getType();
                for (Destination destination1 : loadPlan.getDestination()) {
                    destination = destination + "'" + destination1.getDestinationValue() + "',";
                }
            }
        }
        destination = destination.substring(0, destination.lastIndexOf(","));
        placeOfReceipts = placeOfReceipts.substring(0, placeOfReceipts.lastIndexOf(","));

        return purchaseOrderRepository.getAllPurchaseOrders(loadPlan, placeOfReceipts, destination, originType, destinationType);
    }

    public Response evalulate(String condition, Map<String, Variable> poMap) {
        Response response = iee.evaluateExpression(condition, poMap);
        return response;
    }

    public void fillContainerVolProfit(HashMap containerVolume, HashMap containerProfit) {
        for (int xy = 0; xy < cVolumes.length; xy++) {
            //multiplying container volume be 100 because po volume is in decimal. eg: we are taking upto 2 decimal points.
            //containerVolume.put(timeInSec + xy, cVolumes[xy] * con_max_fil_rate);
            containerVolume.put(timeInSec + xy, cVolumes[xy]);
            if (cVolumes[xy] == 33)
                containerProfit.put(timeInSec + xy, 100);
            else if (cVolumes[xy] == 67)
                containerProfit.put(timeInSec + xy, 497);
            else if (cVolumes[xy] == 76)
                containerProfit.put(timeInSec + xy, 761);
            else if (cVolumes[xy] == 86)
                containerProfit.put(timeInSec + xy, 1011);
            else
                containerProfit.put(timeInSec + xy, 100);
        }
    }

    public Integer createContainerKey(Integer timeInSec) {
        //create container table like purchaseOrder1.
        //create done list to keep track of container ids
        //or maintain done status column in the table.
        timeInSec = (int) System.currentTimeMillis();
        if (timeInSec < 0) {
            timeInSec = timeInSec * (-1);
        }

        return timeInSec;
    }


    public Boolean checkLoadPlanCriteria(LoadPlan loadPlan, PurchaseOrderOp purchaseOrderOp, Boolean addPo) {
        if (!loadPlan.getModeOfTransport().equalsIgnoreCase(purchaseOrderOp.getModeOfTransport())) {
            if (!loadPlan.getModeOfTransport().equalsIgnoreCase("all")) {
                addPo = false;
            }
        }
        if (!loadPlan.getServiceType().equalsIgnoreCase(purchaseOrderOp.getOriginServiceType()) && addPo.equals(true)) {
            if (!loadPlan.getServiceType().equalsIgnoreCase("all")) {
                addPo = false;
            }
        }
        if ((loadPlan.getCutOff().before(purchaseOrderOp.getExpectedDeliveryDate()) || loadPlan.getCutOff().equals(purchaseOrderOp.getExpectedDeliveryDate())) && addPo.equals(true)) {
            addPo = false;
        }

        if (addPo.equals(true)) {
            Boolean originB = false;
            for (Origin origin : loadPlan.getOrigin()) {
                if (origin.getType().equalsIgnoreCase("city") && !purchaseOrderOp.getLoadPortCity().equalsIgnoreCase(origin.getOrignValue())) {
                    if (originB.equals(false)) {
                        addPo = false;
                    }
                } else {
                    addPo = true;
                }
                if (originB.equals(false) && addPo.equals(true)) {
                    originB = true;
                }
            }
        }
        if (addPo.equals(true)) {
            Boolean destinationB = false;
            for (Destination destination : loadPlan.getDestination()) {
                if (destination.getType().equalsIgnoreCase("city") && !purchaseOrderOp.getPodCity().equalsIgnoreCase(destination.getDestinationValue())) {
                    if (destinationB.equals(false)) {
                        addPo = false;
                    }
                } else if (destination.getType().equalsIgnoreCase("country") && !purchaseOrderOp.getPodCountry().equalsIgnoreCase(destination.getDestinationValue())) {
                    if (destinationB.equals(false)) {
                        addPo = false;
                    }
                } else if (destination.getType().equalsIgnoreCase("region") && !purchaseOrderOp.getPodRegion().equalsIgnoreCase(destination.getDestinationValue())) {
                    if (destinationB.equals(false)) {
                        addPo = false;
                    }
                } else {
                    addPo = true;
                }
                if (destinationB.equals(false) && addPo.equals(true)) {
                    destinationB = true;
                }
            }
        }
        return addPo;
    }

    public void reEvaluateRules(List resList, Collection<PurchaseOrderOp> purchaseOrderOpCollection, LoadPlan loadPlan) {
        for (int i = 0; i < resList.size(); i++) {
            HashSet<PurchaseOrderOp> purchaseOrders1 = (HashSet<PurchaseOrderOp>) resList.get(i);
            for (PurchaseOrderOp purchaseOrders : purchaseOrders1) {
                Boolean addPo = true;
                addPo = checkLoadPlanCriteria(loadPlan, purchaseOrders, addPo);
                if (addPo.equals(true)) {
                    purchaseOrderOpCollection.add(purchaseOrders);
                }
            }
        }
        LOG.info("reEvaluateRules end " + purchaseOrderOpCollection.size());
    }

    @Override
    public String processPurchaseOrderAndExecuteRules(LoadPlan loadPlan, Boolean confirm, Integer loadPlanId, Collection<PurchaseOrderOp> purchaseOrderOps, Boolean reEvaluate) throws Exception {
        String status = "";
        String finalString = "";
        String origin = "";
        String destination = "";
        HashMap containerVolume = new HashMap<Integer, Integer>();
        HashMap containerProfit = new HashMap<Integer, Integer>();
        String containerPoMapping = "";
        String containerId = "";
        List resList = new ArrayList();
        Collection<PurchaseOrderOp> purchaseOrderOpCollection = new ArrayList<>();
        Collection<PurchaseOrderOp> purchaseOrderOpsCopy = new ArrayList<>();

        if (confirm.equals(false) && reEvaluate.equals(false)) {
            purchaseOrderOps.addAll(getPurchaseOrders(loadPlan));
        } else if (confirm.equals(true)) {
            purchaseOrderOps.addAll(getAllPoExException(loadPlanId, "exception"));
        }

        for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOps) {
            purchaseOrderOpsCopy.add((PurchaseOrderOp) DeepCopy.copy(purchaseOrderOp));
        }

        loadPlanRuleExecution(purchaseOrderOps, resList, false);

        reEvaluateRules(resList, purchaseOrderOpCollection, loadPlan);

        if (purchaseOrderOps.size() > purchaseOrderOpCollection.size()) {
            LOG.debug("reEvaluateRules if");
            return processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOpCollection, true);
        } else {
            LOG.debug("reEvaluateRules else");
            for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOpsCopy) {
                for (PurchaseOrderOp purchaseOrderOp1 : purchaseOrderOpCollection) {
                    if (purchaseOrderOp.getId().equals(purchaseOrderOp1.getId())) {
                        if (nonNull(purchaseOrderOp1.getModeOfTransport()) && !purchaseOrderOp1.getModeOfTransport().equalsIgnoreCase(purchaseOrderOp.getModeOfTransport())) {
                            return processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOpCollection, true);
                        }
                        if (nonNull(purchaseOrderOp1.getOriginServiceType()) && !purchaseOrderOp1.getOriginServiceType().equalsIgnoreCase(purchaseOrderOp.getOriginServiceType())) {
                            return processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOpCollection, true);
                        }
                        if (nonNull(purchaseOrderOp1.getShipto()) && !purchaseOrderOp1.getShipto().equalsIgnoreCase(purchaseOrderOp.getShipto())) {
                            return processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOpCollection, true);
                        }
                        if (nonNull(purchaseOrderOp1.getPlant()) && !purchaseOrderOp1.getPlant().equalsIgnoreCase(purchaseOrderOp.getPlant())) {
                            return processPurchaseOrderAndExecuteRules(loadPlan, confirm, loadPlanId, purchaseOrderOpCollection, true);
                        }
                    }
                }
            }
        }


        if (confirm.equals(false)) {
            loadPlan.setLoadPlanNumber(UUID.randomUUID().toString().substring(0, 6).toUpperCase());
            loadPlan.setLoadPlanName("LOAD PLAN - " + UUID.randomUUID().toString().substring(0, 6).toUpperCase());
        }

        if (loadPlan.getOrigin().get(0) != null) {
            origin = loadPlan.getOrigin().get(0).getType();
            for (Origin origin1 : loadPlan.getOrigin()) {
                origin = origin + "," + origin1.getOrignValue();
            }
        }
        if (loadPlan.getDestination().get(0) != null) {
            destination = loadPlan.getDestination().get(0).getType();
            for (Destination destination1 : loadPlan.getDestination()) {
                destination = destination + "," + destination1.getDestinationValue();
            }
        }

        timeInSec = createContainerKey(timeInSec);
        fillContainerVolProfit(containerVolume, containerProfit);

        LOG.info("Optimizer start" + resList.size());

        finalString = loadPlanOptimizer(confirm, resList, containerVolume, containerProfit, containerId, containerPoMapping, finalString);

        if (nonNull(finalString) && !finalString.equals("")) {
            status = loadPlanRepository.saveLoadPlan(loadPlan, finalString, origin, destination, confirm, loadPlanId, resList);
        }

        return status;
    }


    public Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> executeMatchingCriteria(Collection<Rule> rules, Collection<PurchaseOrderOp> purchaseOrderOps) {
        Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> multipleBuckets = new HashMap<>();
        LOG.info("executeMatchingCriteria start");
        for (PurchaseOrderOp purchaseOrder : purchaseOrderOps) {
            Boolean matched = false;
            ArrayList<Rule> rulesList = new ArrayList<>();
            Map<String, Variable> poMap = getPoMap(purchaseOrder);
            for (Rule rule : rules) {
                List<MatchingCriteria> matchingCriteriaList = rule.getMatchingCriteriaList();
                for (MatchingCriteria matchingCriteria : matchingCriteriaList) {
                    Response matchingCriteriaResponse = evalulate(matchingCriteria.getCriteriaValue(), poMap);
                    if (matchingCriteriaResponse.getResult() == "false") {
                        matched = false;
                        break;
                    } else {
                        matched = true;
                    }
                }
                if (matched.equals(true)) {
                    rulesList.add(rule);
                }
            }
            if (multipleBuckets.containsKey(rulesList)) {
                HashSet<PurchaseOrderOp> list = multipleBuckets.get(rulesList);
                list.add(purchaseOrder);
            } else {
                if (!rulesList.isEmpty()) {
                    HashSet<PurchaseOrderOp> list = new HashSet<>();
                    list.add(purchaseOrder);
                    multipleBuckets.put(rulesList, list);
                }
            }
        }
        LOG.info("executeMatchingCriteria end");
        return multipleBuckets;
    }


    public Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> executePoLevelRules
            (Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> multipleBucketsCombined) throws
            ParseException, IOException {

        Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> combinedHashMap = new HashMap<>();

        String ruleId = null;

        for (Map.Entry<ArrayList<Rule>, HashSet<PurchaseOrderOp>> entry : multipleBucketsCombined.entrySet()) {

            ArrayList<Rule> combinedRules = entry.getKey();
            Map<String, ArrayList<HashSet<PurchaseOrderOp>>> multipleBuckets = new HashMap();
            HashSet<PurchaseOrderOp> purchaseOrderOps = entry.getValue();
            Integer ruleCount = 0;

            for (PurchaseOrderOp purchaseOrder : purchaseOrderOps) {

                Map<String, Variable> poMap = getPoMap(purchaseOrder);

                // loop for first tree evaluation
                for (Rule rule : combinedRules) {

                    if (ruleCount.equals(0)) {
                        ruleId = String.valueOf(rand.nextInt(100));
                        ruleCount++;
                    }

                    String coTreeString = rule.getPoCoTree().getCtString();
                    Response response1 = new Response();
                    CoTreeStringDto coTreeStringDto = mapper.readValue(coTreeString, CoTreeStringDto.class);
                    int found = 0;
                    CoTreeStringDto treeStringDto;
                    while (found >= 0) {
                        if (found == 0) {
                            response1 = evalulate(coTreeStringDto.getConditionName(), poMap);
                            multipleBuckets = firstTreeBasedBucketing(coTreeStringDto, purchaseOrder, multipleBuckets, ruleId, response1);
                            found++;
                        } else if (response1.getResult().equals("true") && found > 0) {
                            treeStringDto = coTreeStringDto.getTrueChild();
                            coTreeStringDto = treeStringDto;
                            if (coTreeStringDto == null || coTreeStringDto.getConditionName() == "" || coTreeStringDto.getConditionName() == null) {
                                break;
                            } else {
                                response1 = evalulate(coTreeStringDto.getConditionName(), poMap);
                                multipleBuckets = firstTreeBasedBucketing(coTreeStringDto, purchaseOrder, multipleBuckets, ruleId, response1);
                                found++;
                            }
                        } else if (response1.getResult().equals("false") && found > 0) {
                            treeStringDto = coTreeStringDto.getTrueChild();
                            coTreeStringDto = treeStringDto;
                            if (coTreeStringDto == null || coTreeStringDto.getConditionName() == "" || coTreeStringDto.getConditionName() == null) {
                                break;
                            } else {
                                response1 = evalulate(coTreeStringDto.getConditionName(), poMap);
                                multipleBuckets = firstTreeBasedBucketing(coTreeStringDto, purchaseOrder, multipleBuckets, ruleId, response1);
                                found++;
                            }
                        }
                    }
                }
            }
            for (List bucketList : multipleBuckets.values()) {
                combinedHashMap.put(entry.getKey(), (HashSet<PurchaseOrderOp>) bucketList.get(0));
            }
        }
        LOG.info("executePoLevelRules end" + combinedHashMap.size());
        return combinedHashMap;
    }

    public void executeCollectionLevelRules(Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> combinedPoLevelHashMap, Boolean simulate, List combinedCollectionLevelList) throws IOException, ParseException {
        //flow for second rule evaluation
        //second tree parsing
        for (Map.Entry<ArrayList<Rule>, HashSet<PurchaseOrderOp>> entry : combinedPoLevelHashMap.entrySet()) {

            Map<String, ArrayList<HashSet<PurchaseOrderOp>>> multipleBuckets = new HashMap();
            ArrayList<Rule> combinedRules = entry.getKey();
            Integer ruleCount = 0;
            String ruleId = null;
            ArrayList<HashSet<PurchaseOrderOp>> finalCollection = new ArrayList<>();

            for (Rule rule : combinedRules) {

                if (ruleCount.equals(0)) {
                    ruleId = String.valueOf(rand.nextInt(100));
                    ruleCount++;
                }

                ArrayList<HashSet<PurchaseOrderOp>> tmp_set = new ArrayList<>();
                String coTreeString = rule.getPoCoTree().getCollectionConditionTreeString();
                CoTreeStringDto coTreeStringDto = mapper.readValue(coTreeString, CoTreeStringDto.class);

                if (multipleBuckets.keySet().contains(ruleId)) {
                    finalCollection = dtSecondTreeParser(finalCollection, coTreeStringDto, new ArrayList<>(), simulate);
                    multipleBuckets.put(ruleId, finalCollection);
                } else {
                    tmp_set.add(entry.getValue());
                    finalCollection = dtSecondTreeParser(tmp_set, coTreeStringDto, finalCollection, simulate);
                    multipleBuckets.put(ruleId, finalCollection);
                }
            }

            for (List bucketList : multipleBuckets.values()) {
                combinedCollectionLevelList.addAll(bucketList);
            }
        }

        LOG.info("executeCollectionLevelRules end" + combinedCollectionLevelList.size());
    }

    public Collection<Rule> getAllRules() {
        return ruleRepository.getAllRules();
    }


    public void loadPlanRuleExecution(Collection<PurchaseOrderOp> purchaseOrderOps, List resList, Boolean simulate) throws IOException, ParseException {
        Collection<Rule> allRules = getAllRules();

        Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> multipleBucketsCombined;

        Map<ArrayList<Rule>, HashSet<PurchaseOrderOp>> combinedPoLevelHashMap;

        multipleBucketsCombined = executeMatchingCriteria(allRules, purchaseOrderOps);

        combinedPoLevelHashMap = executePoLevelRules(multipleBucketsCombined);

        executeCollectionLevelRules(combinedPoLevelHashMap, simulate, resList);

    }

    public String getConSize(ContainerFillRate con) {
        if (con.getContainerSize().trim().toLowerCase().equals("20d") || con.getContainerSize().trim().toLowerCase().equals("20dry") || con.getContainerSize().trim().toLowerCase().equals("20 dry")
                || con.getContainerSize().trim().toLowerCase().equals("20ftdry") || con.getContainerSize().trim().toLowerCase().equals("20 ftdry") || con.getContainerSize().trim().toLowerCase().equals("20 ft dry")
                || con.getContainerSize().trim().toLowerCase().equals("20dc") || con.getContainerSize().trim().toLowerCase().equals("20 dc") || con.getContainerSize().trim().toLowerCase().equals("20 ft dc"))
            return "20D";
        else if (con.getContainerSize().trim().toLowerCase().equals("40d") || con.getContainerSize().trim().toLowerCase().equals("40dry") || con.getContainerSize().trim().toLowerCase().equals("40 dry")
                || con.getContainerSize().trim().toLowerCase().equals("40ftdry") || con.getContainerSize().trim().toLowerCase().equals("40 ftdry") || con.getContainerSize().trim().toLowerCase().equals("40 ft dry")
                || con.getContainerSize().trim().toLowerCase().equals("40dc") || con.getContainerSize().trim().toLowerCase().equals("40 dc") || con.getContainerSize().trim().toLowerCase().equals("40 ft dc"))
            return "40D";
        else if (con.getContainerSize().trim().toLowerCase().equals("40h") || con.getContainerSize().trim().toLowerCase().equals("40high") || con.getContainerSize().trim().toLowerCase().equals("40 high")
                || con.getContainerSize().trim().toLowerCase().equals("40ftdry") || con.getContainerSize().trim().toLowerCase().equals("40 fthigh") || con.getContainerSize().trim().toLowerCase().equals("40 ft high")
                || con.getContainerSize().trim().toLowerCase().equals("40hc") || con.getContainerSize().trim().toLowerCase().equals("40 hc") || con.getContainerSize().trim().toLowerCase().equals("40 ft hc"))
            return "40H";
        else if (con.getContainerSize().trim().toLowerCase().equals("45h") || con.getContainerSize().trim().toLowerCase().equals("45high") || con.getContainerSize().trim().toLowerCase().equals("45 high")
                || con.getContainerSize().trim().toLowerCase().equals("45fthigh") || con.getContainerSize().trim().toLowerCase().equals("45 fthigh") || con.getContainerSize().trim().toLowerCase().equals("45 ft high")
                || con.getContainerSize().trim().toLowerCase().equals("45hc") || con.getContainerSize().trim().toLowerCase().equals("45 hc") || con.getContainerSize().trim().toLowerCase().equals("45 ft hc"))
            return "45H";
        return "not found";
    }

    public void getMaxCbmPerContainerType(String PoOriginDes, HashMap<String, Integer> conType) {
        //HashMap <String,Integer> conFilHash = new HashMap<>();
        String conOriginDes;

        Collection<ContainerFillRate> container = containerFillRateRepository.getContainerFillRate();
        for (ContainerFillRate con : container) {
            //System.out.println(con.getMaxCbm());
            conOriginDes = con.getOrigin().trim().toLowerCase().concat(con.getDestination().trim().toLowerCase());
            if (PoOriginDes.equals(conOriginDes)) {
                ////create hash of container type and max cbm
                //conType.put(con.getContainerSize(), con.getMaxCbm());
                String conSize = getConSize(con);
                if (conSize == "not found")
                    System.out.println("please provide a correct container type. " + conSize + " -- not found");
                if (conType.containsKey(conSize)) {
                    if (conType.get(conSize) < con.getMaxCbm()) {
                        conType.put(conSize, con.getMaxCbm());
                    }
                } else {
                    conType.put(conSize, con.getMaxCbm());
                }
            }
        }
    }

    public String getOriginName(String porCity, HashMap<String, String> originTab) {
        if (originTab.containsKey(porCity)) return originTab.get(porCity);
        else return "";
    }

    public void prepareConVol(HashMap<String, Integer> conType, HashMap<Integer, Integer> containerVolume, HashMap<Integer, Integer> tempContainerVolume) {

        for (Integer key : containerVolume.keySet()) {
            if (containerVolume.get(key) == 33) {
                tempContainerVolume.put(key, 33 * (conType.get("20D")));
            }
            if (containerVolume.get(key) == 67) {
                tempContainerVolume.put(key, 67 * (conType.get("40D")));
            }
            if (containerVolume.get(key) == 76) {
                tempContainerVolume.put(key, 76 * (conType.get("40H")));
            }
            if (containerVolume.get(key) == 86) {
                tempContainerVolume.put(key, 86 * (conType.get("45H")));
            }
        }

    }

    public void createOriginHash(HashMap<String, String> originTab) {
        Collection<ConfigOrigin> origin = originRepository.getOrigin();
        for (ConfigOrigin conOrigin : origin) {
            originTab.put(conOrigin.getPlaceOfReceipt().toLowerCase(), conOrigin.getOriginName().toLowerCase());
        }
    }

    public String loadPlanOptimizer(Boolean confirm, List resList, HashMap containerVolume, HashMap containerProfit, String containerId, String containerPoMapping, String finalString) throws IOException {
        HashMap<Integer, List<Integer>> integerListHashMap;
        Multimap<String, PurchaseOrderOp> multiMap = ArrayListMultimap.create();
        HashSet<PurchaseOrderOp> tempPoSet = new HashSet<>();
        HashMap<String, String> originTab = new HashMap<>();
        HashMap<String, Integer> conType = new HashMap<>();
        HashMap<Integer, Integer> tempContainerVolume = new HashMap<>();
        createOriginHash(originTab);

        for (int i = 0; i < resList.size(); i++) {
            HashSet<PurchaseOrderOp> purchaseOrders1 = (HashSet<PurchaseOrderOp>) resList.get(i);
            ////split resList bucket into multiple buckets based on origin and destination combination.
            String originName;
            String PoOriginDes = "";
            //String originDesPt;
            for (PurchaseOrderOp purOr : purchaseOrders1) {
                originName = getOriginName(purOr.getLoadPortCity().trim().toLowerCase(), originTab);
                PoOriginDes = originName.concat(purOr.getPodCity().trim().toLowerCase());
                multiMap.put(PoOriginDes, purOr);
            }

            for (String key : multiMap.keySet()) {
                tempPoSet.addAll(multiMap.get(key));
                //System.out.println("-------------------"+key+"------------");
                for (PurchaseOrderOp po : tempPoSet) {
                    //System.out.println(po.getPodCity()+"----------"+po.getUpdateAction());
                    //originDesPt = getOriginName(po.getLoadPortCity().trim().toLowerCase(), originTab).concat(po.getPodCity().trim().toLowerCase());
                    getMaxCbmPerContainerType(PoOriginDes, conType);

                }
                prepareConVol(conType, containerVolume, tempContainerVolume);

                //System.exit(1);
                if (confirm.equals(true)) {
                    for (PurchaseOrderOp purchaseOrderOp : tempPoSet) {
                        if (nonNull(purchaseOrderOp.getUpdateAction()) && purchaseOrderOp.getUpdateAction().equals("Hold cargo")) {
                            purchaseOrderOp.setConfirmRemove("y");
                        }
                    }
                }
                //integerListHashMap = loadPlanner.Optimizer(purchaseOrders1);

                integerListHashMap = KnapsackDP.Optimizer(purchaseOrders1, tempContainerVolume, containerProfit, con_max_fil_rate);

                for (Integer con_id : integerListHashMap.keySet()) {
                    //containerId = containerId + con_id.toString() + "-" + ( ( (int) containerVolume.get(con_id) * 100 ) / con_max_fil_rate ) / 100 + "-" + i + ",";
                    containerId = containerId + con_id.toString() + "-" + containerVolume.get(con_id) + "-" + i + ",";
                    for (Integer po_id : integerListHashMap.get(con_id)) {
                        containerPoMapping = containerPoMapping + po_id.toString() + ",";
                    }
                    containerPoMapping = containerId + containerPoMapping + "|";
                    finalString = finalString + containerPoMapping;
                    containerId = "";
                    containerPoMapping = "";
                    containerVolume.remove(con_id);
                    containerProfit.remove(con_id);
                }
                tempPoSet = new HashSet<PurchaseOrderOp>();
                tempContainerVolume = new HashMap<Integer, Integer>();
                multiMap = ArrayListMultimap.create();
            }
        }

        LOG.info("Optimizer end " + finalString);

        return finalString;
    }

    public void dtSecondTreeParserStartCollection(CoTreeStringDto coTreeStringDto, ArrayList<HashSet<PurchaseOrderOp>> finalCollection, ArrayList<HashSet<PurchaseOrderOp>> tmp_lst) {
        if (!nonNull(coTreeStringDto.getConditionName())) {
            /* Second Decision Tree is not configured */
            System.out.println("FO: Second Decision Tree is not configured");
            for (HashSet<PurchaseOrderOp> s : tmp_lst) {
                finalCollection.add(s);
            }
            return;
        }
    }

    public void dtSecondTreeParserEndCollection(ArrayList<HashSet<PurchaseOrderOp>> trueCollection, ArrayList<HashSet<PurchaseOrderOp>> falseCollection, CoTreeStringDto coTreeStringDto, ArrayList<HashSet<PurchaseOrderOp>> finalCollection, Boolean simulate) throws ParseException {

        if (nonNull(coTreeStringDto.getTrueChild()) && coTreeStringDto.getTrueChild().getConditionName() == null) {
            for (HashSet<PurchaseOrderOp> s : trueCollection) {
                finalCollection.add(s);
            }
        } else {
            if (nonNull(coTreeStringDto.getTrueChild())) {
                dtSecondTreeParser(trueCollection, coTreeStringDto.getTrueChild(), finalCollection, simulate);
            } else {
                for (HashSet<PurchaseOrderOp> s : trueCollection) {
                    finalCollection.add(s);
                }
            }

        }
    }

    public ArrayList<HashSet<PurchaseOrderOp>> dtSecondTreeParser(ArrayList<HashSet<PurchaseOrderOp>> tmp_lst, CoTreeStringDto coTreeStringDto, ArrayList<HashSet<PurchaseOrderOp>> finalCollection, Boolean simulate) throws ParseException {
        //execute condition
        dtSecondTreeParserStartCollection(coTreeStringDto, finalCollection, tmp_lst);

        ArrayList<String> conditionResults = executeConditions(tmp_lst, coTreeStringDto.getConditionName());
        //true collections         //false collections
        //Execute Actions
        ArrayList<HashSet<PurchaseOrderOp>> trueCollection = new ArrayList<>();
        ArrayList<HashSet<PurchaseOrderOp>> falseCollection = new ArrayList<>();

        HashSet<PurchaseOrderOp> tru = new HashSet<>();

        // ArrayList<HashSet<PurchaseOrderOp>> tempList=new ArrayList<>();

        for (int i = 0; i < conditionResults.size(); i++) {
            HashSet<PurchaseOrderOp> tmp_val = tmp_lst.get(i);
            ArrayList<HashSet<PurchaseOrderOp>> resSet = executeTrueFalseActions(tmp_val, coTreeStringDto, "true".equalsIgnoreCase(conditionResults.get(i)), simulate);
            for (HashSet<PurchaseOrderOp> tt : resSet) {
                //tempList.add(tt);
                trueCollection.add(tt);
            }
        }

        dtSecondTreeParserEndCollection(trueCollection, falseCollection, coTreeStringDto, finalCollection, simulate);

        return finalCollection;
    }

    ArrayList<String> executeConditions(ArrayList<HashSet<PurchaseOrderOp>> purchaseOrders, String condition) {
        ArrayList<String> arrLst = new ArrayList<String>();
        if (nonNull(condition)) {
            for (HashSet<PurchaseOrderOp> tempSet : purchaseOrders) {
                InfixExpEvaluator infixExpEvaluator = new InfixExpEvaluator(tempSet);
                String res = infixExpEvaluator.evaluateExpression(condition).getResult().toString();
                arrLst.add(res);
            }

        }
        return arrLst;
    }

    public ArrayList<HashSet<PurchaseOrderOp>> splitBasedOnValue(ActionDto action, HashSet<PurchaseOrderOp> purchaseOrderOpsHs) {
        //from here
        // to how many container per unit
        int container_per_unit = 1;/*
        if (nonNull(action.getActionValue()) || (!action.getActionValue().toLowerCase().equals(""))) {
            if (!action.getActionValue().toLowerCase().equals("")) {
                container_per_unit = Integer.parseInt(action.getActionValue());
            }
        }*/

        ArrayList<HashSet<PurchaseOrderOp>> tmpResults = new ArrayList<>();

        //from here
        // to how many container per unit
        if (("begins with".equalsIgnoreCase(action.getActionExpression())) || ("ends with".equalsIgnoreCase(action.getActionExpression())) || ("contains".equalsIgnoreCase(action.getActionExpression())) || ("equals".equalsIgnoreCase(action.getActionExpression()))) {
            String pasField = action.getActionExpression();
            String pasVal = action.getActionValue();
            PurchaseOrderUtils pou = new PurchaseOrderUtils();
            Set<String> possibleValues;
            possibleValues = pou.getAllPossibleValues(purchaseOrderOpsHs, action.getActionName());

            HashSet<PurchaseOrderOp> poSetPersist1 = new HashSet<>();
            HashSet<PurchaseOrderOp> poSetPersist2 = new HashSet<>();
            for (String val : possibleValues) {

                for (PurchaseOrderOp po1 : purchaseOrderOpsHs) {
                    if (pou.getValues(po1, action.getActionName()).equals(val)) {
                        if (pasField.equalsIgnoreCase("begins with")) {
                            if (val.substring(0, pasVal.length()).equalsIgnoreCase(pasVal)) {
                                poSetPersist1.add(po1);
                            } else {
                                poSetPersist2.add(po1);
                            }
                        } else if (pasField.equalsIgnoreCase("ends with")) {
                            if (val.substring(val.length() - pasVal.length()).equalsIgnoreCase(pasVal)) {
                                poSetPersist1.add(po1);
                            } else {
                                poSetPersist2.add(po1);
                            }
                        } else if (pasField.equalsIgnoreCase("contains")) {
                            if (val.contains(pasVal)) {
                                poSetPersist1.add(po1);
                            } else {
                                poSetPersist2.add(po1);
                            }
                        } else {
                            if (val.equals(pasVal)) {
                                poSetPersist1.add(po1);
                            } else {
                                poSetPersist2.add(po1);
                            }
                        }
                    }
                }
            }
            if (poSetPersist1.size() > 0) {
                tmpResults.add(poSetPersist1);
            }
            if (poSetPersist2.size() > 0) {
                tmpResults.add(poSetPersist2);
            }
        } else {
            container_per_unit = 1;
            if (nonNull(action.getActionValue()) || (!action.getActionValue().equals(""))) {
                if (!action.getActionValue().equals("")) {
                    container_per_unit = Integer.parseInt(action.getActionValue());
                }
            }
            //till here

            PurchaseOrderUtils pou = new PurchaseOrderUtils();
            Set<String> possibleValues;
            possibleValues = pou.getAllPossibleValues(purchaseOrderOpsHs, action.getActionName());
            if (possibleValues.size() > 1) {
                for (String val : possibleValues) {
                    HashSet<PurchaseOrderOp> poSetPersist1 = new HashSet<>();
                    for (PurchaseOrderOp po1 : purchaseOrderOpsHs) {
                        if (pou.getValues(po1, action.getActionName()).equals(val)) {
                            poSetPersist1.add(po1);
                        }
                    }
                    if (poSetPersist1.size() > 0) {

                        tmpResults.add(poSetPersist1);
                    }
                }
            } else {
                tmpResults.add(purchaseOrderOpsHs);
            }
            //merge unit per container
            tmpResults = container_per_unit_split(tmpResults, container_per_unit);
        }
        //merge unit per container
        return tmpResults;
    }

    public ArrayList<HashSet<PurchaseOrderOp>> splitBasedOnDateRange(ActionDto action, HashSet<PurchaseOrderOp> purchaseOrderOpsHs) {
        // to how many container per unit
        int container_per_unit = 1;

        ArrayList<HashSet<PurchaseOrderOp>> tmpResults = new ArrayList<>();

        if (nonNull(action.getActionValue()) || (!action.getActionValue().toLowerCase().equals(""))) {
            if (!action.getActionValue().toLowerCase().equals("")) {
                container_per_unit = Integer.parseInt(action.getActionValue());
            }
        }

        PurchaseOrderUtils pou = new PurchaseOrderUtils();
        Set<String> possibleValues;
        possibleValues = pou.getAllPossibleValues(purchaseOrderOpsHs, action.getActionName());

        Set<Date> possibleDate = new HashSet<>();

        for (String s : possibleValues) {
            try {
                Date date;
                if (s.contains("-")) {
                    date = dt.parse(s);
                } else {
                    date = dts.parse(s);
                }
                possibleDate.add(date);
            } catch (ParseException e) {
                System.out.println("date parsing in split:" + e.getMessage());
            }
        }

        List sortedList = new ArrayList(possibleDate);
        Collections.sort(sortedList);
        Set<Date> sortedSet = new HashSet(sortedList);
        Set<Date> removedSet = new HashSet<>();

        if (sortedList.size() > 1) {
            int indx = -1;
            Iterator<Date> it = sortedSet.iterator();
            for (Date val : sortedSet) {
                if (!removedSet.contains(val)) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(val);
                    calendar.add(Calendar.DAY_OF_MONTH, action.getActionRange() + 1);
                    Date compareRange = calendar.getTime();
                    indx += 1;
                    HashSet<PurchaseOrderOp> poSetPersist1 = new HashSet<PurchaseOrderOp>();
                    Set<Date> removedSetTemp = new HashSet<>();
                    for (PurchaseOrderOp po1 : purchaseOrderOpsHs) {
                        try {
                            Date compareDate;
                            if (pou.getValues(po1, action.getActionName()).contains("-")) {
                                compareDate = dt.parse(pou.getValues(po1, action.getActionName()));
                            } else {
                                compareDate = dts.parse(pou.getValues(po1, action.getActionName()));
                            }
                            if (!removedSet.contains(compareDate) && compareDate.equals(val)) {
                                poSetPersist1.add(po1);
                                removedSetTemp.add(compareDate);
                            }
                            if (!removedSet.contains(compareDate) && compareDate.after(val) && compareDate.before(compareRange)) {
                                poSetPersist1.add(po1);
                                removedSetTemp.add(compareDate);
                            }
                        } catch (ParseException e) {
                            System.out.println("date parsing in split in for loop:" + e.getMessage());
                        }
                    }
                    if (poSetPersist1.size() > 0) {
                        tmpResults.add(poSetPersist1);
                        removedSet.addAll(removedSetTemp);
                    }

                }
            }
        } else {
            tmpResults.add(purchaseOrderOpsHs);
        }

        //merge unit per container
        return container_per_unit_split(tmpResults, container_per_unit);
    }

    public ArrayList<HashSet<PurchaseOrderOp>> actionsSplitUpdate(Boolean simulate, ActionDto
            action, ArrayList<HashSet<PurchaseOrderOp>> tmpResults) throws ParseException {
        HashSet<PurchaseOrderOp> tru = new HashSet<>();
        ArrayList<HashSet<PurchaseOrderOp>> tmpHashSetArrayList = new ArrayList<>();

        for (HashSet<PurchaseOrderOp> purchaseOrderOpHashSet : tmpResults) {
            if (("update".equalsIgnoreCase(action.getActionType()) || "Assign".equals(action.getActionType())) && simulate.equals(false)) {
                for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOpHashSet) {
                    setUpdatedValues(purchaseOrderOp, action);
                }
                tmpHashSetArrayList.add(purchaseOrderOpHashSet);
            } else if ("update".equalsIgnoreCase(action.getActionType()) && simulate.equals(true)) {
                for (PurchaseOrderOp purchaseOrderOp : purchaseOrderOpHashSet) {
                    if (action.getActionName().equals("status") && action.getActionValue().toLowerCase().equals("release")) {
                        purchaseOrderOp.setShipmentStatus(action.getActionValue());
                        purchaseOrderOp.setPoStatus("optimized");
                    } else if (action.getActionName().equals("status") && action.getActionValue().toLowerCase().equals("on hold")) {
                        purchaseOrderOp.setShipmentStatus(action.getActionValue());
                        purchaseOrderOp.setPoStatus("not planned");
                    }
                }
                tmpHashSetArrayList.add(purchaseOrderOpHashSet);
            } else if ("split".equalsIgnoreCase(action.getActionType()) && ("range".equalsIgnoreCase(action.getActionExpression())) && !action.getActionValue().equals(0) && action.getActionName().toLowerCase().contains("date")) {
                tmpHashSetArrayList.addAll(splitBasedOnDateRange(action, purchaseOrderOpHashSet));
            } else if ("split".equalsIgnoreCase(action.getActionType())) {
                tmpHashSetArrayList.addAll(splitBasedOnValue(action, purchaseOrderOpHashSet));
            } else if ("merge".equalsIgnoreCase(action.getActionType())) {
                for (PurchaseOrderOp po : purchaseOrderOpHashSet) {
                    tru.add(po);
                }
            }

        }

        if (tru.size() > 0) {
            tmpHashSetArrayList.add(tru);
        }

        return tmpHashSetArrayList;
    }

    public ArrayList<HashSet<PurchaseOrderOp>> executeTrueFalseActions
            (HashSet<PurchaseOrderOp> purchaseOrderOpsHs, CoTreeStringDto coTreeStringDto, boolean trueActions, Boolean
                    simulate) throws
            ParseException {
        ArrayList<HashSet<PurchaseOrderOp>> tmpResults = new ArrayList<>();
        tmpResults.add(purchaseOrderOpsHs);
        if (trueActions == true) {
            if (coTreeStringDto.getTrueActions() != null && coTreeStringDto.getTrueActions().size() > 0) {
                for (ActionDto actionDto : coTreeStringDto.getTrueActions()) {
                    tmpResults = actionsSplitUpdate(simulate, actionDto, tmpResults);
                }
            }
        } else {
            if (coTreeStringDto.getFalseActions() != null && coTreeStringDto.getFalseActions().size() > 0) {
                for (ActionDto actionDto : coTreeStringDto.getFalseActions()) {
                    tmpResults = actionsSplitUpdate(simulate, actionDto, tmpResults);
                }
            }
        }
        return tmpResults;
    }

    ArrayList<HashSet<PurchaseOrderOp>> container_per_unit_split
            (ArrayList<HashSet<PurchaseOrderOp>> tmpResults, int container_per_unit) {
        //from here
        //merge the sets based on the number of po per container
        if (container_per_unit > 1) {
            int numberOfMergedSets = MathUtils.ceilDiv(tmpResults.size(), container_per_unit);
            ArrayList<HashSet<PurchaseOrderOp>> newMergedTmpResults = new ArrayList<HashSet<PurchaseOrderOp>>();
            for (int i = 0; i < numberOfMergedSets; i++) {
                HashSet<PurchaseOrderOp> st = new HashSet<PurchaseOrderOp>();
                for (int j = 0; j < container_per_unit; j++) {
                    //System.out.println(i*x+j);
                    if (i * container_per_unit + j < tmpResults.size())
                        st.addAll(tmpResults.get(i * container_per_unit + j));
                }
                newMergedTmpResults.add(st);
            }

            tmpResults = newMergedTmpResults;
        }
        //till here

        return tmpResults;
    }


    public void setUpdatedValues(PurchaseOrderOp purchaseOrderOp, ActionDto actionDto) throws ParseException {
        if (isNull(purchaseOrderOp.getUpdateAction())) {
            if (actionDto.getActionType().equals("Assign")) {
                purchaseOrderOp.setAssignedTo(actionDto.getActionValue());
            }
            if (actionDto.getActionName().equals("modeOfTransport")) {
                purchaseOrderOp.setModeOfTransport(actionDto.getActionValue());
            } else if (actionDto.getActionName().equals("originServiceType")) {
                purchaseOrderOp.setOriginServiceType(actionDto.getActionValue());
            } else if (actionDto.getActionName().equals("plant")) {
                purchaseOrderOp.setPlant(actionDto.getActionValue());
            } else if (actionDto.getActionName().equals("shipto")) {
                purchaseOrderOp.setShipto(actionDto.getActionValue());
            } else if (actionDto.getActionName().equals("shipmentStatus")) {
                purchaseOrderOp.setShipmentStatus(actionDto.getActionValue());
                if (actionDto.getActionValue().equalsIgnoreCase("exception")) {
                    if ((nonNull(purchaseOrderOp.getPoStatus()) && !purchaseOrderOp.getPoStatus().equalsIgnoreCase("optimized")) || !nonNull(purchaseOrderOp.getPoStatus())) {
                        purchaseOrderOp.setPoStatus(actionDto.getActionValue());
                    }
                } else if (actionDto.getActionValue().equalsIgnoreCase("release")) {
                    purchaseOrderOp.setPoStatus("optimized");
                } else if (actionDto.getActionValue().equalsIgnoreCase("on hold")) {
                    purchaseOrderOp.setPoStatus("not planned");
                } else {
                    purchaseOrderOp.setPoStatus(actionDto.getActionValue());
                }
            }
            purchaseOrderOp.setModifiedDate(new Date());
            purchaseOrderOp.setModifiedBy(userId);
        }
    }

    public void firstTreeBasedBucketingLogic(ActionDto actionDto, PurchaseOrderOp purchaseOrder, Map<String, ArrayList<HashSet<PurchaseOrderOp>>> multipleBuckets, String ruleId) throws ParseException {
        setUpdatedValues(purchaseOrder, actionDto);
        //if (PurchaseOrderImpl.release_state.equalsIgnoreCase(actionDto.getActionValue()) || PurchaseOrderImpl.exception_state.equalsIgnoreCase(actionDto.getActionValue())) {
        if (multipleBuckets.keySet().contains(ruleId)) {
            multipleBuckets.get(ruleId).get(0).add(purchaseOrder);
        } else {
            ArrayList<HashSet<PurchaseOrderOp>> poSetPersist = new ArrayList<>();
            HashSet<PurchaseOrderOp> hs = new HashSet<>();
            hs.add(purchaseOrder);
            poSetPersist.add(hs);
            multipleBuckets.put(ruleId, poSetPersist);
        }
        // }
    }

    public Map<String, ArrayList<HashSet<PurchaseOrderOp>>> firstTreeBasedBucketing(CoTreeStringDto coTreeStringDto, PurchaseOrderOp purchaseOrder, Map<String, ArrayList<HashSet<PurchaseOrderOp>>> multipleBuckets, String ruleId, Response response1) throws ParseException {

        if (response1.getResult().equals("true")) {
            if (coTreeStringDto.getTrueActions() != null &&
                    coTreeStringDto.getTrueActions().size() > 0) {
                List<ActionDto> tActions = coTreeStringDto.getTrueActions();
                for (ActionDto tAction : tActions) {
                    if ("update".equals(tAction.getActionType()) || "Assign".equals(tAction.getActionType())) {
                        firstTreeBasedBucketingLogic(tAction, purchaseOrder, multipleBuckets, ruleId);
                    }
                }
            }
        }/*start praveen*/ else if (response1.getResult().equals("false")) {
            if (coTreeStringDto.getFalseActions() != null &&
                    coTreeStringDto.getFalseActions().size() > 0) {
                List<ActionDto> fActions = coTreeStringDto.getFalseActions();
                for (ActionDto fAction : fActions) {
                    if ("update".equals(fAction.getActionType()) || "Assign".equals(fAction.getActionType())) {
                        firstTreeBasedBucketingLogic(fAction, purchaseOrder, multipleBuckets, ruleId);
                    }
                }
            }
        }/*end Praveen*/

        LOG.info("firstTreeBasedBucketing end " + multipleBuckets.size());

        return multipleBuckets;
    }

    @Override
    public Collection<LoadPlan> getLoadPlansByStatus(String status) {
        if (nonNull(status)) {
            return purchaseOrderRepository.getLoadPlansByStatus(status);
        } else {
            return purchaseOrderRepository.getAllLoadPlans();
        }
    }

    public void populateSelectedValue(Collection<PurchaseOrderOp> purchaseOrderOps, Object o) {
        PurchaseOrderOp purchaseOrderOp = (PurchaseOrderOp) ((Object[]) o)[0];
        purchaseOrderOp.setDestinationSelect((String) ((Object[]) o)[2]);
        purchaseOrderOp.setOriginSelect((String) ((Object[]) o)[3]);
        purchaseOrderOps.add(purchaseOrderOp);
    }


    @Override
    public Collection<PurchaseOrderOp> getAllPo(Integer id, String poStatus) {
        if (nonNull(id) && nonNull(poStatus)) {
            List resultList = purchaseOrderRepository.getAllPo(id, poStatus);
            Collection<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
            resultList.stream().forEach((o) -> {
                populateSelectedValue(purchaseOrderOps, o);
            });
            return purchaseOrderOps;
        } else if (isNull(id) && nonNull(poStatus)) {
            return purchaseOrderRepository.getAllPoWithStatus(poStatus);
        } else {
            List resultList = purchaseOrderRepository.getAllPo();
            Collection<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
            resultList.stream().forEach((o) -> {
                PurchaseOrderOp purchaseOrderOp = (PurchaseOrderOp) ((Object[]) o)[0];
                purchaseOrderOp.setLoadPlanStatus((String) ((Object[]) o)[1]);
                purchaseOrderOp.setContainerNumber((String) ((Object[]) o)[4]);
                purchaseOrderOp.setLoadPlanNumber((String) ((Object[]) o)[5]);
                purchaseOrderOps.add(purchaseOrderOp);
            });
            return purchaseOrderOps;
        }
    }

    @Override
    public Collection<PurchaseOrderOp> getAllPoExException(Integer id, String poStatus) {
        Collection<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();
        if (nonNull(id) && nonNull(poStatus)) {
            List resultList = purchaseOrderRepository.getAllPoExException(id, poStatus);
            resultList.stream().forEach((o) -> {
                populateSelectedValue(purchaseOrderOps, o);
            });
            return purchaseOrderOps;
        } else {
            return purchaseOrderOps;
        }
    }

    @Override
    public Collection<Container> getContainersByStatus(String status, Integer id) {
        if (nonNull(status) && nonNull(id)) {
            return purchaseOrderRepository.getContainersByStatus(status, id);
        } else {
            return purchaseOrderRepository.getContainersByLpId(id);
        }
    }


    public Map<String, Variable> getPoMap(PurchaseOrderOp purchaseOrder) {
        Map<String, Variable> poMap = new HashMap<>();
        poMap.put("aaFlag", new Variable("aaFlag", purchaseOrder.getPoLineReference1(), "varchar"));
        poMap.put("demandKey", new Variable("demandKey", purchaseOrder.getPoLineReference2(), "varchar"));
        poMap.put("poLineReference3", new Variable("poLineReference3", purchaseOrder.getPoLineReference3(), "varchar"));
        poMap.put("poLineReference4", new Variable("poLineReference4", purchaseOrder.getPoLineReference4(), "varchar"));
        poMap.put("poLineReference5", new Variable("poLineReference5", purchaseOrder.getPoLineReference5(), "varchar"));
        poMap.put("poLineReference6", new Variable("poLineReference6", purchaseOrder.getPoLineReference6(), "varchar"));
        poMap.put("poLineReference7", new Variable("poLineReference7", purchaseOrder.getPoLineReference7(), "varchar"));
        poMap.put("poLineReference8", new Variable("poLineReference8", purchaseOrder.getPoLineReference8(), "varchar"));
        poMap.put("poLineReference9", new Variable("poLineReference9", purchaseOrder.getPoLineReference9(), "varchar"));
        poMap.put("poLineReference10", new Variable("poLineReference10", purchaseOrder.getPoLineReference10(), "varchar"));
        poMap.put("poLineReference11", new Variable("poLineReference11", purchaseOrder.getPoLineReference11(), "varchar"));
        poMap.put("poLineReference12", new Variable("poLineReference12", purchaseOrder.getPoLineReference12(), "varchar"));
        poMap.put("poLineReference13", new Variable("poLineReference13", purchaseOrder.getPoLineReference13(), "varchar"));
        poMap.put("poLineReference14", new Variable("poLineReference14", purchaseOrder.getPoLineReference14(), "varchar"));
        poMap.put("poLineReference15", new Variable("poLineReference15", purchaseOrder.getPoLineReference15(), "varchar"));
        poMap.put("poLineReference16", new Variable("poLineReference16", purchaseOrder.getPoLineReference16(), "varchar"));
        poMap.put("poLineReference17", new Variable("poLineReference17", purchaseOrder.getPoLineReference17(), "varchar"));
        poMap.put("poLineReference18", new Variable("poLineReference18", purchaseOrder.getPoLineReference18(), "varchar"));
        poMap.put("poLineReference19", new Variable("poLineReference19", purchaseOrder.getPoLineReference19(), "varchar"));
        poMap.put("poLineReference20", new Variable("poLineReference20", purchaseOrder.getPoLineReference20(), "varchar"));
        if (nonNull(purchaseOrder.getActualRecieptDate())) {
            poMap.put("actualRecieptDate", new Variable("actualRecieptDate", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getActualRecieptDate()), "date"));
        }
        if (nonNull(purchaseOrder.getBookedCartons())) {
            poMap.put("bookedCartons", new Variable("bookedCartons", purchaseOrder.getBookedCartons().toString(), "numeric"));
        }
        if (nonNull(purchaseOrder.getBookedCbm())) {
            poMap.put("bookedCbm", new Variable("bookedCbm", purchaseOrder.getBookedCbm().toString(), "float"));
        }
        if (nonNull(purchaseOrder.getBookedQuantity())) {
            poMap.put("bookedQuantity", new Variable("bookedQuantity", purchaseOrder.getBookedQuantity().toString(), "numeric"));
        }
        if (nonNull(purchaseOrder.getBookedWeight())) {
            poMap.put("bookedWeight", new Variable("bookedWeight", purchaseOrder.getBookedWeight().toString(), "float"));
        }
        poMap.put("consignee", new Variable("consignee", purchaseOrder.getConsignee(), "varchar"));
        if (nonNull(purchaseOrder.getExpectedCargoReceiptDate())) {
            poMap.put("expectedCargoReceiptDate", new Variable("expectedCargoReceiptDate", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getExpectedCargoReceiptDate()), "date"));
        }
        if (nonNull(purchaseOrder.getExpectedCargoReceiptWeek())) {
            poMap.put("expectedCargoReceiptWeek", new Variable("expectedCargoReceiptWeek", purchaseOrder.getExpectedCargoReceiptWeek().toString(), "numeric"));
        }
        if (nonNull(purchaseOrder.getExpectedDeliveryDate())) {
            poMap.put("expectedDeliveryDate", new Variable("expectedDeliveryDate", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getExpectedDeliveryDate()), "date"));
        }
        if (nonNull(purchaseOrder.getLatestDeliveryDate())) {
            poMap.put("latestDeliveryDate", new Variable("latestDeliveryDate", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getLatestDeliveryDate()), "date"));
        }
        if (nonNull(purchaseOrder.getLatestCargoReceiptDate())) {
            poMap.put("latestCargoReceiptDate", new Variable("latestCargoReceiptDate", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getLatestCargoReceiptDate()), "date"));
        }
        poMap.put("orderType", new Variable("orderType", purchaseOrder.getOrderType(), "varchar"));
        poMap.put("podCountry", new Variable("podCountry", purchaseOrder.getPodCountry(), "varchar"));
        poMap.put("podProvince", new Variable("podProvince", purchaseOrder.getPodProvince(), "varchar"));
        poMap.put("podCity", new Variable("podCity", purchaseOrder.getPodCity(), "varchar"));
        poMap.put("porCountry", new Variable("porCountry", purchaseOrder.getPorCountry(), "varchar"));
        poMap.put("porProvince", new Variable("porProvince", purchaseOrder.getPorProvince(), "varchar"));
        poMap.put("porCity", new Variable("porCity", purchaseOrder.getPorCity(), "varchar"));
        poMap.put("plant", new Variable("plant", purchaseOrder.getPlant(), "varchar"));
        poMap.put("poNumber", new Variable("poNumber", purchaseOrder.getPoNumber(), "varchar"));
        poMap.put("poLine", new Variable("poLine", purchaseOrder.getPoLine(), "varchar"));
        poMap.put("productTypeCode", new Variable("productTypeCode", purchaseOrder.getProductTypeCode(), "varchar"));
        poMap.put("productType", new Variable("productType", purchaseOrder.getProductType(), "varchar"));
        poMap.put("pslv", new Variable("pslv", purchaseOrder.getPslv(), "varchar"));
        if (nonNull(purchaseOrder.getReceivedCatons())) {
            poMap.put("receivedCatons", new Variable("receivedCatons", purchaseOrder.getReceivedCatons().toString(), "numeric"));
        }
        if (nonNull(purchaseOrder.getReceivedCbm())) {
            poMap.put("receivedCbm", new Variable("receivedCbm", purchaseOrder.getReceivedCbm().toString(), "float"));
        }
        if (nonNull(purchaseOrder.getReceivedQuantity())) {
            poMap.put("receivedQuantity", new Variable("receivedQuantity", purchaseOrder.getReceivedQuantity().toString(), "numeric"));
        }
        if (nonNull(purchaseOrder.getReceivedWeight())) {
            poMap.put("receivedWeight", new Variable("receivedWeight", purchaseOrder.getReceivedWeight().toString(), "float"));
        }
        poMap.put("shipto", new Variable("shipto", purchaseOrder.getShipto(), "varchar"));
        poMap.put("shipper", new Variable("shipper", purchaseOrder.getShipper(), "varchar"));
        poMap.put("skuNumber", new Variable("skuNumber", purchaseOrder.getSkuNumber(), "varchar"));
        poMap.put("vendor", new Variable("vendor", purchaseOrder.getVendoe(), "varchar"));
        poMap.put("soReference1", new Variable("soReference1", purchaseOrder.getSoReference1(), "varchar"));
        poMap.put("soReference2", new Variable("soReference2", purchaseOrder.getSoReference2(), "varchar"));
        poMap.put("soReference3", new Variable("soReference3", purchaseOrder.getSoReference3(), "varchar"));
        poMap.put("soReference4", new Variable("soReference4", purchaseOrder.getSoReference4(), "varchar"));
        poMap.put("soReference5", new Variable("soReference5", purchaseOrder.getSoReference5(), "varchar"));
        poMap.put("soReference6", new Variable("soReference6", purchaseOrder.getSoReference6(), "varchar"));
        poMap.put("soReference7", new Variable("soReference7", purchaseOrder.getSoReference7(), "varchar"));
        poMap.put("soReference8", new Variable("soReference8", purchaseOrder.getSoReference8(), "varchar"));
        poMap.put("soReference9", new Variable("soReference9", purchaseOrder.getSoReference9(), "varchar"));
        poMap.put("soReference10", new Variable("soReference10", purchaseOrder.getSoReference10(), "varchar"));
        poMap.put("soReference11", new Variable("soReference11", purchaseOrder.getSoReference11(), "varchar"));
        poMap.put("soReference12", new Variable("soReference12", purchaseOrder.getSoReference12(), "varchar"));
        poMap.put("soReference13", new Variable("soReference13", purchaseOrder.getSoReference13(), "varchar"));
        poMap.put("soReference14", new Variable("soReference14", purchaseOrder.getSoReference14(), "varchar"));
        poMap.put("soReference15", new Variable("soReference15", purchaseOrder.getSoReference15(), "varchar"));
        poMap.put("soReference16", new Variable("soReference16", purchaseOrder.getSoReference16(), "varchar"));
        poMap.put("soReference17", new Variable("soReference17", purchaseOrder.getSoReference17(), "varchar"));
        poMap.put("soReference18", new Variable("soReference18", purchaseOrder.getSoReference18(), "varchar"));
        poMap.put("soReference19", new Variable("soReference19", purchaseOrder.getSoReference19(), "varchar"));
        poMap.put("soReference20", new Variable("soReference20", purchaseOrder.getSoReference20(), "varchar"));
        poMap.put("bookingNumber", new Variable("bookingNumber", purchaseOrder.getBookingNumber(), "varchar"));
        poMap.put("carrier", new Variable("carrier", purchaseOrder.getCarrier(), "varchar"));
        poMap.put("destinationShipmentType", new Variable("destinationShipmentType", purchaseOrder.getDestinationShipmentType(), "varchar"));
        poMap.put("dischargePortCity", new Variable("dischargePortCity", purchaseOrder.getDischargePortCity(), "varchar"));
        poMap.put("dischargePortCountry", new Variable("dischargePortCountry", purchaseOrder.getDischargePortCountry(), "varchar"));
        poMap.put("dischargePortProvince", new Variable("dischargePortProvince", purchaseOrder.getDischargePortProvince(), "varchar"));
        if (nonNull(purchaseOrder.getEta())) {
            poMap.put("eta", new Variable("eta", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getEta()), "date"));
        }
        if (nonNull(purchaseOrder.getEtd())) {
            poMap.put("etd", new Variable("etd", new SimpleDateFormat("dd-MM-yyyy").format(purchaseOrder.getEtd()), "date"));
        }
        poMap.put("loadPortCity", new Variable("loadPortCity", purchaseOrder.getLoadPortCity(), "varchar"));
        poMap.put("loadPortCountry", new Variable("loadPortCountry", purchaseOrder.getLoadPortCountry(), "varchar"));
        poMap.put("loadPortRegion", new Variable("loadPortRegion", purchaseOrder.getLoadPortRegion(), "varchar"));
        poMap.put("loadPortProvince", new Variable("loadPortProvince", purchaseOrder.getLoadPortProvince(), "varchar"));
        poMap.put("originShipmentType", new Variable("originShipmentType", purchaseOrder.getOriginShipmentType(), "varchar"));
        poMap.put("shipmentStatus", new Variable("shipmentStatus", purchaseOrder.getShipmentStatus(), "varchar"));
        poMap.put("currentDate", new Variable("currentDate", new SimpleDateFormat("dd-MM-yyyy").format(new Date()), "date"));
        poMap.put("modeOfTransport", new Variable("modeOfTransport", purchaseOrder.getModeOfTransport(), "varchar"));
        poMap.put("originServiceType", new Variable("originServiceType", purchaseOrder.getOriginServiceType(), "varchar"));
        return poMap;
    }

    @Override
    public LoadPlan simulateLoadPlan(Boolean reEvaluate, Collection<PurchaseOrderOp> purchaseOrderOpCollection) throws Exception {
        Collection<PurchaseOrderOp> purchaseOrders;
        Integer loadPlanId = null;
        if (reEvaluate.equals(false)) {
            SimulateLoadPlan simulateLoadPlan = loadPlanService.getSimulateData();
            loadPlanId = simulateLoadPlan.getLoadPlanId();
            loadPlanService.removeSimulateLoadPlan(simulateLoadPlan.getId());
            TypeReference<List<PurchaseOrderOp>> mapType = new TypeReference<List<PurchaseOrderOp>>() {
            };
            purchaseOrders = mapper.readValue(simulateLoadPlan.getPoCollection(), mapType);
        } else {
            purchaseOrders = purchaseOrderOpCollection;
        }
        List resList = new ArrayList();
        HashMap<Integer, List<Integer>> integerListHashMap;
        HashMap containerVolume = new HashMap<Integer, Integer>();
        HashMap containerProfit = new HashMap<Integer, Integer>();
        HashMap<Integer, PurchaseOrderOp> poListHashMap = new HashMap<>();

        LoadPlan loadPlan = loadPlanRepository.getLoadPlan(loadPlanId);

        loadPlanRuleExecution(purchaseOrders, resList, true);

        reEvaluateRules(resList, purchaseOrderOpCollection, loadPlan);

        if (purchaseOrders.size() != purchaseOrderOpCollection.size()) {
            return simulateLoadPlan(true, purchaseOrderOpCollection);
        }


        for (int i = 0; i < resList.size(); i++) {
            HashSet<PurchaseOrderOp> purchaseOrders1 = (HashSet<PurchaseOrderOp>) resList.get(i);
            for (PurchaseOrderOp purchaseOrder : purchaseOrders1) {
                if (nonNull(purchaseOrder.getShipmentStatus()) && !purchaseOrder.getShipmentStatus().equals("Hold cargo")) {
                    poListHashMap.put(purchaseOrder.getId(), purchaseOrder);
                } else {
                    poListHashMap.put(purchaseOrder.getId(), purchaseOrder);
                }
            }
        }

        loadPlan.getContainers().clear();

        List<Container> containerList = new ArrayList<>();

        HashMap<String, String> volumeContainerTypeMap = new HashMap<>();
        try {
            List<ContainerTypeDto> containerTypes = appDataService.getContainerTypes();
            for (ContainerTypeDto containerTypeDto : containerTypes) {
                volumeContainerTypeMap.put(containerTypeDto.getVolume().toString(), containerTypeDto.getContainerType());
            }
        } catch (IOException e) {
            LOG.error("simulateLoadPlan" + e.getMessage());
        }

        timeInSec = createContainerKey(timeInSec);
        fillContainerVolProfit(containerVolume, containerProfit);

        for (int i = 0; i < resList.size(); i++) {
            HashSet<PurchaseOrderOp> purchaseOrders1 = (HashSet<PurchaseOrderOp>) resList.get(i);

            LOG.info("simulateLoadPlan optimizer start" + resList.size());

            integerListHashMap = KnapsackDP.Optimizer(purchaseOrders1, containerVolume, containerProfit, con_max_fil_rate);

            for (Integer con_id : integerListHashMap.keySet()) {
                Container container = new Container();
                Double volume = 0.0;
                container.setContainerNumber(con_id.toString() + "-" + (((int) containerVolume.get(con_id) * 100) / con_max_fil_rate) / 100 + "-" + i);
                container.setContainerSizeType(volumeContainerTypeMap.get(String.valueOf(volume)));
                container.setStatus("In progress");

                for (Integer po_id : integerListHashMap.get(con_id)) {
                    container.getPurchaseOrderOp().add(poListHashMap.get(po_id));
                    if (nonNull(poListHashMap.get(po_id))) {
                        volume = volume + poListHashMap.get(po_id).getBookedCbm();
                    }
                }

                container.setVolume(Double.parseDouble(String.valueOf(volume)));

                if (nonNull(container.getPurchaseOrderOp()) && container.getPurchaseOrderOp().size() > 0) {
                    containerList.add(container);
                }
            }
        }
        loadPlan.setContainers(containerList);
        return loadPlan;

    }

    @Override
    public String importPurchaseOrders(XSSFSheet mySheet, DataFormatter formatter) {

        int cnt = mySheet.getPhysicalNumberOfRows() - 1; //excluding headers.
        List<PurchaseOrderOp> purchaseOrderOps = new ArrayList<>();

        Collection<PurchaseOrderOp> purchaseOrderOpCollection = purchaseOrderRepository.getAllPoWithStatus("not planned");
        Map<String, PurchaseOrderOp> poHashMap = new HashMap<>();
        if (!purchaseOrderOpCollection.isEmpty()) {
            for (PurchaseOrderOp po : purchaseOrderOpCollection) {
                poHashMap.put(po.getPoNumber() + po.getSoNumber() + po.getSkuNumber(), po);
            }
        }

        Collection<PurchaseOrderOp> deleteOrderOpCollection = new ArrayList<>();

        Collection<Location> locations = locationRepository.getLocation();

        Boolean stop = false;

        String success = "Success";

        for (int i = 0; i < cnt; i++) {

            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(19))) {

                PurchaseOrderOp purchaseOrderOp = new PurchaseOrderOp();

                String poNumber = formatter.formatCellValue(row.getCell(19));
                String soNumber = formatter.formatCellValue(row.getCell(18));
                String sku = formatter.formatCellValue(row.getCell(20));

                if (!poHashMap.isEmpty() && poHashMap.containsKey(poNumber + soNumber + sku)) {
                    deleteOrderOpCollection.add(poHashMap.get(poNumber + soNumber + sku));
                }

                if (locations.stream().anyMatch(loc -> loc.getLocationCode().equalsIgnoreCase(formatter.formatCellValue(row.getCell(0)))) && locations.stream().anyMatch(loc -> loc.getLocationCode().equalsIgnoreCase(formatter.formatCellValue(row.getCell(5))))) {
                    purchaseOrderOp.setPorCity(pou.toTitleCase(formatter.formatCellValue(row.getCell(1)).toLowerCase()));
                    purchaseOrderOp.setPodCity(pou.toTitleCase(formatter.formatCellValue(row.getCell(6)).toLowerCase()));
                } else {
                    stop = true;
                    success = "Failed";
                    break;
                }

                purchaseOrderOp.setPorCountry(pou.toTitleCase(formatter.formatCellValue(row.getCell(3)).toLowerCase()));
                purchaseOrderOp.setPodCountry(pou.toTitleCase(formatter.formatCellValue(row.getCell(8)).toLowerCase()));
                purchaseOrderOp.setPlant(formatter.formatCellValue(row.getCell(10)));
                purchaseOrderOp.setShipto(formatter.formatCellValue(row.getCell(11)));
                purchaseOrderOp.setVendoe(formatter.formatCellValue(row.getCell(13)));
                if (nonNull(row.getCell(14))) {
                    purchaseOrderOp.setExpectedCargoReceiptDate(row.getCell(14).getDateCellValue());
                }
                if (nonNull(row.getCell(15))) {
                    purchaseOrderOp.setActualRecieptDate(row.getCell(15).getDateCellValue());
                }
                if (nonNull(row.getCell(17))) {
                    purchaseOrderOp.setExpectedDeliveryDate(row.getCell(17).getDateCellValue());
                }

                LOG.info("importPurchaseOrders intermediate step :" + poNumber);

                purchaseOrderOp.setSoNumber(soNumber);
                purchaseOrderOp.setPoNumber(poNumber);
                purchaseOrderOp.setSkuNumber(sku);
                purchaseOrderOp.setProductTypeCode(formatter.formatCellValue(row.getCell(21)).substring(0, 2));
                if (nonNull(row.getCell(22))) {
                    purchaseOrderOp.setBookedCbm(row.getCell(22).getNumericCellValue());
                }
                purchaseOrderOp.setBookedQuantity((Integer.parseInt(formatter.formatCellValue(row.getCell(23)))));
                purchaseOrderOp.setBookedCartons(Integer.parseInt(formatter.formatCellValue(row.getCell(24))));
                if (nonNull(row.getCell(25))) {
                    purchaseOrderOp.setBookedWeight(row.getCell(25).getNumericCellValue());
                }
                purchaseOrderOp.setModeOfTransport(formatter.formatCellValue(row.getCell(26)));
                purchaseOrderOp.setBookedDestinationService(formatter.formatCellValue(row.getCell(27)));
                purchaseOrderOp.setOriginServiceType(formatter.formatCellValue(row.getCell(28)));
                purchaseOrderOp.setPslv(formatter.formatCellValue(row.getCell(29)));


                purchaseOrderOp.setPoStatus("not planned");
                purchaseOrderOp.setCreatedBy(userId);
                purchaseOrderOp.setModifiedBy(userId);
                purchaseOrderOp.setModifiedDate(new Date());
                purchaseOrderOp.setCreatedDate(new Date());

                purchaseOrderOps.add(purchaseOrderOp);
            }
        }
        if (stop.equals(false)) {
            if (!deleteOrderOpCollection.isEmpty()) {
                purchaseOrderRepository.removePurchaseOrder(deleteOrderOpCollection);
            }
            purchaseOrderRepository.persistPurchaseOrders(purchaseOrderOps);
        }

        return success;
    }

    @Override
    public void addPurchaseOrder(Container container) {
        Container container1 = purchaseOrderRepository.findContainerById(container);
        container1.setPurchaseOrderOp(new ArrayList<>());
        Double volume = container1.getVolume();
        if (nonNull(container.getPurchaseOrderOp())) {
            for (PurchaseOrderOp purchaseOrderOp : container.getPurchaseOrderOp()) {
                PurchaseOrderOp purchaseOrderOp1 = purchaseOrderRepository.findPoById(purchaseOrderOp);
                purchaseOrderOp1.setPoStatus("optimized");
                purchaseOrderOp1.setModifiedBy(userId);
                purchaseOrderOp1.setModifiedDate(new Date());
                if (nonNull(purchaseOrderOp1.getBookedCbm())) {
                    volume = volume + purchaseOrderOp1.getBookedCbm();
                }
                container1.addPoToContainers(purchaseOrderOp1);
            }
        }
        container1.setVolume(volume);
        purchaseOrderRepository.addPurchaseOrder(container1);
    }

    @Override
    public void deletePurchaseOrder(Collection<PurchaseOrderOp> purchaseOrderOps) {
        purchaseOrderRepository.deletePurchaseOrder(purchaseOrderOps);
    }

    @Override
    public void splitPurchaseOrder(PurchaseOrderOp purchaseOrderOp) {
        if (nonNull(purchaseOrderOp.getUpdateValue())) {
            PurchaseOrderOp purchaseOrderOp2 = new PurchaseOrderOp();
            Container container = purchaseOrderRepository.findContainerByPoId(purchaseOrderOp);
            container.setPurchaseOrderOp(new ArrayList<>());
            if (nonNull(purchaseOrderOp)) {
                if (purchaseOrderOp.getUpdateField().toUpperCase().equals("CBM")) {
                    Double bookedCbm = purchaseOrderOp.getBookedCbm();
                    Double po1 = Double.parseDouble(purchaseOrderOp.getUpdateValue());
                    Double po2 = bookedCbm - po1;

                    purchaseOrderOp.setModifiedDate(new Date());
                    purchaseOrderOp.setModifiedBy(userId);
                    purchaseOrderOp.setContainer(container);
                    purchaseOrderOp2 = (PurchaseOrderOp) DeepCopy.copy(purchaseOrderOp);
                    purchaseOrderOp.setBookedCbm(Double.parseDouble(formatter.format(po1)));
                    if (nonNull(purchaseOrderOp2)) {
                        purchaseOrderOp2.setId(null);
                        purchaseOrderOp2.setBookedCbm(Double.parseDouble(formatter.format(po2)));
                        purchaseOrderOp2.setContainer(container);
                    }
                }
                if (purchaseOrderOp.getUpdateField().toUpperCase().equals("KG")) {
                    Double bookedWt = purchaseOrderOp.getBookedWeight();
                    Double po1 = Double.parseDouble(purchaseOrderOp.getUpdateValue());
                    Double po2 = bookedWt - po1;

                    purchaseOrderOp.setModifiedDate(new Date());
                    purchaseOrderOp.setModifiedBy(userId);
                    purchaseOrderOp.setContainer(container);
                    purchaseOrderOp2 = (PurchaseOrderOp) DeepCopy.copy(purchaseOrderOp);
                    purchaseOrderOp.setBookedWeight(Double.parseDouble(formatter.format(po1)));
                    if (nonNull(purchaseOrderOp2)) {
                        purchaseOrderOp2.setId(null);
                        purchaseOrderOp2.setBookedWeight(Double.parseDouble(formatter.format(po2)));
                        purchaseOrderOp2.setContainer(container);
                    }
                }
                container.addPoToContainers(purchaseOrderOp);
                container.addPoToContainers(purchaseOrderOp2);
                purchaseOrderRepository.splitPurchaseOrder(container);
            }
        }
    }

    @Override
    public Collection<PurchaseOrderOp> getAllPoForLoadPlan(Integer id) {
        LoadPlan loadPlan = loadPlanRepository.getLoadPlanById(id);
        Collection<PurchaseOrderOp> purchaseOrderOpCollection = getPurchaseOrders(loadPlan);
        return purchaseOrderOpCollection;
    }

    @Override
    public void cancelPurchaseOrders(BufferedReader bufferedReader) throws IOException {
        String line;
        String cvsSplitBy = ",";
        String soNumber = "";

        while ((line = bufferedReader.readLine()) != null) {
            // use comma as separator
            String[] orders = line.split(cvsSplitBy);
            if (orders.length == 2) {
                soNumber = soNumber + "'" + orders[1] + "',";
            }
        }
        purchaseOrderRepository.cancelPurchaseOrders(soNumber.substring(0, soNumber.length() - 1));
    }
}


