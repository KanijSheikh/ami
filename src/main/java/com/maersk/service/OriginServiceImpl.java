package com.maersk.service;

import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.ConfigOrigin;
import com.maersk.repositories.OriginRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 08/02/2018.
 */
@Service
public class OriginServiceImpl implements OriginService {

    @Autowired
    private OriginRepository originRepository;

    @Override
    public void importOrigin(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<ConfigOrigin> origins = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getOrigin(origins, row);
            }
        }
        originRepository.add(origins);
    }


    private void getOrigin(List<ConfigOrigin> origins, Row row) {
        ConfigOrigin origin = new ConfigOrigin();

        origin.setOriginName(row.getCell(0).getStringCellValue());
        origin.setPlaceOfReceipt(row.getCell(1).getStringCellValue());
        origin.setCfsWarehouse(row.getCell(2).getStringCellValue());
        origin.setLoadingPort(row.getCell(3).getStringCellValue());

        origins.add(origin);
    }

    @Override
    public Collection<ConfigOrigin> getOrigin() {
        Collection<ConfigOrigin> origin = originRepository.getOrigin();
        if (origin.isEmpty()) {
            throw new EntityNotFoundException("Origin records not found");
        }
        return origin;
    }

    @Override
    public void remove(List<Integer> originIds) {
        originRepository.remove(originIds);
    }


    @Override
    public void add(ConfigOrigin origin) {

        if (origin.getId() != null && !origin.getId().equals(0)) {
            originRepository.remove(Collections.singletonList(origin.getId()));
        }
        origin.setId(null);
        originRepository.add(singletonList(origin));

    }

}



