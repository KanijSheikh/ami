package com.maersk.service;

import com.maersk.model.Container;
import com.maersk.model.LoadPlan;
import com.maersk.model.SimulateLoadPlan;

import java.util.Collection;


public interface LoadPlanService {

    LoadPlan getLoadPlan(Integer id);

    void saveSimulateData(SimulateLoadPlan simulateLoadPlan);

    SimulateLoadPlan getSimulateData();

    void AddContainer(Integer loadPlanId, Container container);

    void deleteContainer(Collection<Container> containers);

    void removeSimulateLoadPlan(Integer  id);
}
