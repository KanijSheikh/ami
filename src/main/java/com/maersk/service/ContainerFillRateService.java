package com.maersk.service;

import com.maersk.model.ContainerFillRate;
import com.maersk.model.Location;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 12/02/2018.
 */
public interface ContainerFillRateService {

    void importContainerFillRate(XSSFSheet mySheet, DataFormatter formatter);

    Collection<ContainerFillRate> getContainerFillRate();

    void remove(List<Integer> containerIds);

    void add(ContainerFillRate container);
}
