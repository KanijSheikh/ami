package com.maersk.service;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public interface CirService {

    void uploadCirReport(XSSFSheet mySheet, DataFormatter formatter,String fileName);

}
