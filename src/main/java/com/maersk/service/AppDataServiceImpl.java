package com.maersk.service;

import com.maersk.domain.ContainerTypeDto;
import com.maersk.enums.AppDataType;
import com.maersk.model.ApplicationData;
import com.maersk.repositories.AppDataRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppDataServiceImpl implements AppDataService {

    @Resource
    private AppDataRepository appDataRepository;

    ObjectMapper mapper = new ObjectMapper();

    public List<String> getAppDataByType(AppDataType appDataType) {
        List<ApplicationData> applicationData = appDataRepository.getAppDataByType(appDataType);
        List<String> applicationDataDtos = new ArrayList<>();
        for (ApplicationData appData: applicationData) {
            applicationDataDtos.add(appData.getValue());
        }
        return applicationDataDtos;
    }

    @Transactional
    @Override
    public void add(AppDataType appDataType, List<String> values) {
        List<ApplicationData> applicationData = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(values)){
            appDataRepository.deleteAppDataByType(appDataType);
            values.forEach( value -> {
                applicationData.add(new ApplicationData(appDataType, value));
            });
            appDataRepository.add(applicationData);
        }
    }

    @Override
    public List<ContainerTypeDto> getContainerTypes() throws IOException {
        List<ApplicationData> containerTypes = appDataRepository.getAppDataByType(AppDataType.CONTAINER_TYPE);
        List<ContainerTypeDto> containerTypeDtos = new ArrayList<>();
        for (ApplicationData containerType: containerTypes) {
            containerTypeDtos.add(mapper.readValue(containerType.getValue(), ContainerTypeDto.class));
        }
        return containerTypeDtos;
    }

    @Transactional
    @Override
    public void addContainerTypes(List<ContainerTypeDto> types) throws IOException {
        List<ApplicationData> applicationData = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(types)){
            appDataRepository.deleteAppDataByType(AppDataType.CONTAINER_TYPE);
            for (ContainerTypeDto type:  types) {
                applicationData.add(new ApplicationData(AppDataType.CONTAINER_TYPE, mapper.writeValueAsString(type)));
            }
            appDataRepository.add(applicationData);
        }
    }
}
