package com.maersk.service;

import com.maersk.domain.ContainerTypeDto;
import com.maersk.enums.AppDataType;

import java.io.IOException;
import java.util.List;

public interface AppDataService {

     List<String> getAppDataByType(AppDataType appDataType);

     void add(AppDataType appDataType, List<String> values);

    List<ContainerTypeDto> getContainerTypes() throws IOException;

    void addContainerTypes(List<ContainerTypeDto> types) throws IOException;
}
