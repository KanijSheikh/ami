package com.maersk.service;

import com.maersk.model.MatchingCriteria;
import com.maersk.model.Rule;

import java.util.Collection;

public interface RuleService {
    Integer add(Rule rule);

    void copy(Integer ruleId);

    MatchingCriteria getRules(Rule rule);

    Collection<Rule> getRules(String criteria, Integer ruleId);

    void remove(Integer ruleId);

    Collection<Rule> getAllRules();

    Collection<Rule> getOnlyRules();

    void reOrderRule(Integer ruleId, Integer oldOrder, Integer newOrder);
}
