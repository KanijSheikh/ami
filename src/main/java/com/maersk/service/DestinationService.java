package com.maersk.service;

import com.maersk.model.ConfigDestination;
import com.maersk.model.Location;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import java.util.Collection;
import java.util.List;

/**
 * Created by kanij on 08/02/2018.
 */
public interface DestinationService {

    void importDestination(XSSFSheet mySheet, DataFormatter formatter);

    Collection<ConfigDestination> getDestination();

    void remove(List<Integer> destinationIds);

    void add(ConfigDestination configDestination);
}
