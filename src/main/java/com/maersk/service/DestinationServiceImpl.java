package com.maersk.service;

import com.maersk.exception.EntityNotFoundException;
import com.maersk.model.ConfigDestination;
import com.maersk.repositories.DestinationRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 08/02/2018.
 */
@Service
public class DestinationServiceImpl implements DestinationService {

    @Autowired
    private DestinationRepository destinationRepository;

    @Override
    public void importDestination(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<ConfigDestination> destinations = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getDestination(destinations, row);
            }
        }
        destinationRepository.add(destinations);
    }


    private void getDestination(List<ConfigDestination> destinations, Row row) {
        ConfigDestination destination = new ConfigDestination();

        destination.setDestinationName(row.getCell(0).getStringCellValue());
        destination.setDischargePort(row.getCell(1).getStringCellValue());
        destination.setTransitTime((int) row.getCell(2).getNumericCellValue());

        destinations.add(destination);
    }

    @Override
    public Collection<ConfigDestination> getDestination() {
        Collection<ConfigDestination> destination = destinationRepository.getDestination();
        if (destination.isEmpty()) {
            throw new EntityNotFoundException("Destination records not found");
        }
        return destination;
    }

    @Override
    public void remove(List<Integer> destinationIds) {
        destinationRepository.remove(destinationIds);
    }


    @Override
    public void add(ConfigDestination destination) {

        if (destination.getId() != null && !destination.getId().equals(0)) {
            destinationRepository.remove(Collections.singletonList(destination.getId()));
        }
        destination.setId(null);
        destinationRepository.add(singletonList(destination));

    }

}



