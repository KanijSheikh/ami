package com.maersk.service;

import com.maersk.domain.WarehouseDto;
import com.maersk.exception.EntityNotFoundException;
import com.maersk.mapper.WarehouseMapper;
import com.maersk.model.Location;
import com.maersk.model.Warehouse;
import com.maersk.repositories.LocationRepository;
import com.maersk.repositories.WarehouseRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/17/2017.
 */
@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Resource
    private WarehouseMapper warehouseMapper;

    @Override
    public void importWarehouse(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<Warehouse> warehouses = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getWarehouses(warehouses, row);
            }
        }
        warehouseRepository.add(warehouses);
    }


    private void getWarehouses(List<Warehouse> warehouses, Row row) {
        Warehouse warehouse = new Warehouse();

        warehouse.setName(row.getCell(0).getStringCellValue());
        warehouse.setLocation(row.getCell(1).getStringCellValue());
        warehouse.setLocationCode(row.getCell(2).getStringCellValue());
        warehouse.setCargoHoldingDays(row.getCell(3).getStringCellValue());
        warehouse.setWorkingDays(row.getCell(4).getStringCellValue());

        warehouses.add(warehouse);
    }

    @Override
    public Collection<WarehouseDto> getWarehouses() {
        Collection<Warehouse> warehouses = warehouseRepository.getWarehouses();
        if (warehouses.isEmpty()) {
            throw new EntityNotFoundException("Warehouses not found");
        }
        return warehouseMapper.mapToDto(warehouses);
    }

    @Override
    public void remove(List<Integer> warehouseIds) {
        warehouseRepository.remove(warehouseIds);
    }


    @Override
    public void add(WarehouseDto warehouseDto) {

        if(warehouseDto.getId() != null && warehouseDto.getId() != 0){
            update(warehouseDto.getId(), warehouseDto);
        }else{
            Warehouse warehouse = warehouseMapper.mapToDomain(warehouseDto);
            warehouse.setId(null);
            warehouseRepository.add(singletonList(warehouse));
        }
    }

    private void update(Integer id, WarehouseDto warehouseDto) {
        Warehouse warehouse = warehouseRepository.getWareHouse(id);

        warehouse.setLocation(warehouseDto.getLocation());
        warehouse.setLocationCode(warehouseDto.getLocationCode());
        warehouse.setName(warehouseDto.getName());
        warehouse.setCargoHoldingDays(warehouseDto.getCargoHoldingDays());
        warehouse.setWorkingDays(warehouseDto.getWorkingDays());

        warehouseRepository.add(singletonList(warehouse));
    }

}



