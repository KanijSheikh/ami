package com.maersk.service;

import com.maersk.domain.AllowedContainerDto;
import com.maersk.exception.EntityNotFoundException;
import com.maersk.mapper.AllowedContainerMapper;
import com.maersk.model.AllowedContainer;
import com.maersk.repositories.AllowedContainerRepository;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;

/**
 * Created by kanij on 10/17/2017.
 */
@Service
public class AllowedContainerServiceImpl implements AllowedContainerService {

    @Resource
    private AllowedContainerRepository allowedContainerRepository;

    @Resource
    private AllowedContainerMapper allowedContainerMapper;

    @Override
    public void importAllowedContainer(XSSFSheet mySheet, DataFormatter formatter) {
        int cnt = mySheet.getPhysicalNumberOfRows() - 1;
        List<AllowedContainer> allowedContainers = new ArrayList<>();
        for (int i = 0; i < cnt; i++) {
            Row row = mySheet.getRow(i + 1);

            if (row != null && nonNull(row.getCell(0))) {
                getAllowedContainers(allowedContainers, row);
            }
        }
        allowedContainerRepository.add(allowedContainers);
    }


    private void getAllowedContainers(List<AllowedContainer> allowedContainers, Row row) {
        AllowedContainer allowedContainer = new AllowedContainer();

        allowedContainer.setOrigin(row.getCell(0).getStringCellValue());
        allowedContainer.setDestination(row.getCell(1).getStringCellValue());
        allowedContainer.setAllow20Dry(row.getCell(2).getStringCellValue());
        allowedContainer.setAllow40Dry(row.getCell(3).getStringCellValue());
        allowedContainer.setAllow40High(row.getCell(4).getStringCellValue());
        allowedContainer.setAllow40Reef(row.getCell(5).getStringCellValue());
        allowedContainer.setAllow45High(row.getCell(6).getStringCellValue());

        allowedContainers.add(allowedContainer);
    }

    @Override
    public Collection<AllowedContainerDto> getAllowedContainers() {
        Collection<AllowedContainer> allowedContainers = allowedContainerRepository.getAllowedContainers();
        if (allowedContainers.isEmpty()) {
            throw new EntityNotFoundException("Allowed containers not found");
        }
        return allowedContainerMapper.mapToDto(allowedContainers);
    }

    @Override
    public void remove(List<Integer> containerIds) {
        allowedContainerRepository.remove(containerIds);
    }


    @Override
    public void add(AllowedContainerDto allowedContainerDto) {

        if(allowedContainerDto.getId() != null && allowedContainerDto.getId() != 0){
            update(allowedContainerDto.getId(), allowedContainerDto);
        }else{
            AllowedContainer allowedContainer = allowedContainerMapper.mapToDomain(allowedContainerDto);
            allowedContainer.setId(null);
            allowedContainerRepository.add(singletonList(allowedContainer));
        }
    }

    private void update(Integer id, AllowedContainerDto allowedContainerDto) {
        AllowedContainer allowedContainer  = allowedContainerRepository.geAllowedContainer(id);

        allowedContainer.setOrigin(allowedContainerDto.getOrigin());
        allowedContainer.setDestination(allowedContainerDto.getDestination());
        allowedContainer.setAllow20Dry(allowedContainerDto.getAllow20Dry());
        allowedContainer.setAllow40Dry(allowedContainerDto.getAllow40Dry());
        allowedContainer.setAllow40High(allowedContainerDto.getAllow40High());
        allowedContainer.setAllow40Reef(allowedContainerDto.getAllow40Reef());
        allowedContainer.setAllow45High(allowedContainerDto.getAllow40High());

        allowedContainerRepository.add(singletonList(allowedContainer));
    }

}



