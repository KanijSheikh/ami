package com.maersk.util;

import com.maersk.model.PurchaseOrderOp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

public class PurchaseOrderUtils {


    public String getValues(PurchaseOrderOp po, String fieldName) {
        String value = "";

        switch (fieldName.toLowerCase().replace("_", "")) {
            case "actualreceiptdate": {
                value = po.getActualRecieptDate().toString();
            }
            break;
            case "bookdate": {
                value = po.getBookDate().toString();
            }
            break;
            case "bookedcartons": {
                value = po.getBookedCartons().toString();
            }
            break;
            case "bookedcbm": {
                value = po.getBookedCbm().toString();
            }
            break;
            case "bookedquantity": {
                value = po.getBookedQuantity().toString();
            }
            break;
            case "bookedweight": {
                value = po.getBookedWeight().toString();
            }
            break;
            case "carrier": {
                value = po.getConsignee();
            }
            break;
            case "consignee": {
                value = po.getConsignee();
            }
            break;
            case "createdby": {
                value = po.getCreatedBy();
            }
            break;
            case "createddate": {
                value = po.getCreatedDate().toString();
            }
            break;
            case "destination": {
                value = po.getDestination();
            }
            break;
            case "destinationshipmenttype": {
                value = po.getDestinationShipmentType();
            }
            break;
            case "dischargeportcity": {
                value = po.getDischargePortCity();
            }
            break;
            case "dischargeportcountry": {
                value = po.getDischargePortCountry();
            }
            break;
            case "dischargeportprovince": {
                value = po.getDischargePortProvince();
            }
            break;
            case "eta": {
                value = po.getEta().toString();
            }
            break;
            case "edt": {
                value = po.getEtd().toString();
            }
            break;
            case "etd": {
                value = po.getEtd().toString();
            }
            break;
            case "expectedcargoreceiptdate": {
                value = po.getExpectedCargoReceiptDate().toString();
            }
            break;
            case "expectedcargoreceiptweek": {
                value = po.getExpectedCargoReceiptWeek().toString();
            }
            break;
            case "expecteddeliverydate": {
                value = po.getExpectedDeliveryDate().toString();
            }
            break;
            case "latestcargoreceiptdate": {
                value = po.getLatestCargoReceiptDate().toString();
            }
            break;
            case "latestdeliverydate": {
                value = po.getLatestDeliveryDate().toString();
            }
            break;
            case "loadportcity": {
                value = po.getLoadPortCity();
            }
            break;
            case "loadportcountry": {
                value = po.getLoadPortCountry();
            }
            break;
            case "loadportprovince": {
                value = po.getLoadPortProvince();
            }
            break;
            case "loadplanstatus": {
                value = po.getLoadPlanStatus();
            }
            break;
            case "modifiedby": {
                value = po.getModifiedBy();
            }
            break;
            case "modifieddate": {
                value = po.getModifiedDate().toString();
            }
            break;
            case "ordertype": {
                value = po.getOrderType();
            }
            break;
            case "originshipmenttype": {
                value = po.getOriginShipmentType();
            }
            break;
            case "plant": {
                value = po.getPlant();
            }
            break;
            case "poline": {
                value = po.getPoLine();
            }
            break;
            case "producttype": {
                value = po.getProductType();
            }
            break;
            case "producttypecode": {
                value = po.getProductTypeCode();
            }
            break;
            case "shipper": {
                value = po.getShipper();
            }
            break;
            case "skunumber": {
                value = po.getSkuNumber();
            }
            break;
            case "vendor": {
                value = po.getVendoe();
            }
            break;
            case "porcity": {
                value = po.getPorCity();
            }
            break;
            case "porcountry": {
                value = po.getPorCountry();
            }
            break;
            case "porprovince": {
                value = po.getPorProvince();
            }
            break;
            case "podcity": {
                value = po.getPodCity();
            }
            break;
            case "podcountry": {
                value = po.getPodCountry();
            }
            break;
            case "podprovince": {
                value = po.getPodProvince();
            }
            break;
            case "podregion": {
                value = po.getPodRegion();
            }
            break;
            case "polinereference1": {
                value = po.getPoLineReference1();
            }
            break;
            case "ponumber": {
                value = po.getPoNumber();
            }
            break;
            case "modeoftransport": {
                value = po.getModeOfTransport();
            }
            break;
            case "originservicetype": {
                value = po.getOriginServiceType();
            }
            break;
        }


        return value;
    }

    public Set<String> getAllPossibleValues(HashSet<PurchaseOrderOp> tmp_ls, String fieldName) {


        //@Column(name = "Actual_Receipt_Date")
        //private Date actualRecieptDate;
        Set<String> values = new HashSet<String>();

        switch (fieldName.toLowerCase().replace("_", "")) {
            case "actualreceiptdate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getActualRecieptDate().toString());
                }
            }
            break;
            case "bookdate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getBookDate().toString());
                }
            }
            break;
            case "bookedcartons": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getBookedCartons().toString());
                }
            }
            break;
            case "bookedcbm": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getBookedCbm().toString());
                }
            }
            break;
            case "bookedquantity": {
                for (PurchaseOrderOp po : tmp_ls) {

                    values.add(po.getBookedQuantity().toString());
                }
            }
            break;
            case "bookedweight": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getBookedWeight().toString());
                }
            }
            break;
            case "carrier": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getCarrier());
                }
            }
            break;
            case "consignee": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getConsignee());
                }
            }
            case "createdby": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getCreatedBy());
                }
            }
            break;
            case "createddate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getCreatedDate().toString());
                }
            }
            break;
            case "destination": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getDestination());
                }
            }
            break;
            case "destinationshipmenttype": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getDestinationShipmentType());
                }
            }
            break;
            case "dischargeportcity": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getDischargePortCity());
                }
            }
            break;
            case "dischargeportcountry": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getDischargePortCountry());
                }
            }
            break;
            case "dischargeportprovince": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getDischargePortProvince());
                }
            }
            break;
            case "eta": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getEta().toString());
                }
            }
            break;
            case "edt": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getEtd().toString());
                }
            }
            break;
            case "etd": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getEtd().toString());
                }
            }
            break;
            case "expectedcargoreceiptdate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getExpectedCargoReceiptDate().toString());
                }
            }
            break;
            case "expecteddeliverydate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getExpectedDeliveryDate().toString());
                }
            }
            break;
            case "latestcargoreceiptdate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getLatestCargoReceiptDate().toString());
                }
            }
            break;
            case "latestdeliverydate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getLatestDeliveryDate().toString());
                }
            }
            break;
            case "loadplanprovince": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getLoadPortProvince());
                }
            }
            break;
            case "loadplanstatus": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getLoadPlanStatus());
                }
            }
            break;

            case "modifiedby": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getModifiedBy());
                }
            }
            break;
            case "modifieddate": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getModifiedDate().toString());
                }
            }
            break;
            case "ordertype": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getOrderType());
                }
            }
            break;

            case "originshipmenttype": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getOriginShipmentType());
                }
            }
            break;
            case "plant": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPlant());
                }
            }
            break;
            case "poline": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPoLine());
                }
            }
            break;
            case "producttype": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getProductType());
                }
            }
            break;
            case "producttypecode": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getProductTypeCode());
                }
            }
            break;
            case "shipper": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getShipper());
                }
            }
            break;
            case "skunumber": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getSkuNumber());
                }
            }
            break;
            case "vendor": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getVendoe());
                }
            }
            break;
            case "porcity": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPorCity());
                }
            }
            break;
            case "porcountry": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPorCountry());
                }
            }
            break;
            case "porprovince": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPorProvince());
                }
            }
            break;
            case "podcity": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPodCity());
                }
            }
            break;
            case "podcountry": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPodCountry());
                }
            }
            break;
            case "podprovince": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPodProvince());
                }
            }
            break;
            case "podregion": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPodRegion());
                }
            }
            break;
            case "polinereference1": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPoLineReference1());
                }
            }
            break;
            case "ponumber": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getPoNumber());
                }
            }
            break;
            case "modeoftransport": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getModeOfTransport());
                }
            }
            break;
            case "originservicetype": {
                for (PurchaseOrderOp po : tmp_ls) {
                    values.add(po.getOriginServiceType());
                }
            }
            break;
        }
        return values;
    }

    public PurchaseOrderOp setValues(PurchaseOrderOp po, String fieldName, String value) throws ParseException {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy");
        switch (fieldName.toLowerCase().replace("_", "")) {
            case "actualreceiptdate": {
                po.setActualRecieptDate(dt.parse(value));
            }
            break;
            case "bookdate": {
                po.setBookDate(dt.parse(value));
            }
            break;
            case "bookedcartons": {
                po.setBookedCartons(Integer.parseInt(value));
            }
            break;
            case "bookedcbm": {
                po.setBookedCbm(Double.parseDouble(value));
            }
            break;
            case "bookedquantity": {
                po.setBookedQuantity(Integer.parseInt(value));
            }
            break;
            case "bookedweight": {
                po.setBookedWeight(Double.parseDouble(value));
            }
            break;
            case "carrier": {
                po.setConsignee(value);
            }
            break;
            case "consignee": {
                po.setConsignee(value);
            }
            break;
            case "createdby": {
                po.setCreatedBy(value);
            }
            break;
            case "createddate": {
                po.setCreatedDate(dt.parse(value));
            }
            break;
            case "destination": {
                po.setDestination(value);
            }
            break;
            case "destinationshipmenttype": {
                po.setDestinationShipmentType(value);
            }
            break;
            case "dischargeportcity": {
                po.setDischargePortCity(value);
            }
            break;
            case "dischargeportcountry": {
                po.setDischargePortCountry(value);
            }
            break;
            case "dischargeportprovince": {
                po.setDischargePortProvince(value);
            }
            break;
            case "eta": {
                po.setEta(dt.parse(value));
            }
            break;
            case "edt": {
                po.setEtd(dt.parse(value));
            }
            break;
            case "etd": {
                po.setEtd(dt.parse(value));
            }
            break;
            case "expectedcargoreceiptdate": {
                po.setExpectedCargoReceiptDate(dt.parse(value));
            }
            break;
            case "expectedcargoreceiptweek": {
                po.setExpectedCargoReceiptWeek(Integer.parseInt(value));
            }
            break;
            case "expecteddeliverydate": {
                po.setExpectedDeliveryDate(dt.parse(value));
            }
            break;
            case "latestcargoreceiptdate": {
                po.setLatestCargoReceiptDate(dt.parse(value));
            }
            break;
            case "latestdeliverydate": {
                po.setLatestDeliveryDate(dt.parse(value));
            }
            break;
            case "loadplancity": {
                po.setLoadPortCity(value);
            }
            break;
            case "loadplancountry": {
                po.setLoadPortCountry(value);
            }
            break;
            case "loadplanprovince": {
                po.setLoadPortProvince(value);
            }
            break;
            case "loadplanstatus": {
                po.setLoadPlanStatus(value);
            }
            break;
            case "modifiedby": {
                po.setModifiedBy(value);
            }
            break;
            case "modifieddate": {
                po.setModifiedDate(dt.parse(value));
            }
            break;
            case "ordertype": {
                po.setOrderType(value);
            }
            break;
            case "originshipmenttype": {
                po.setOriginShipmentType(value);
            }
            break;
            case "plant": {
                po.setPlant(value);
            }
            break;
            case "poline": {
                po.setPoLine(value);
            }
            break;
            case "producttype": {
                po.setProductType(value);
            }
            break;
            case "producttypecode": {
                po.setProductTypeCode(value);
            }
            break;
            case "shipper": {
                po.setShipper(value);
            }
            break;
            case "skunumber": {
                po.setSkuNumber(value);
            }
            break;
            case "vendor": {
                po.setVendoe(value);
            }
            break;
            case "porcity": {
                po.setPorCity(value);
            }
            break;
            case "porcountry": {
                po.setPorCountry(value);
            }
            break;
            case "porprovince": {
                po.setPorProvince(value);
            }
            break;
            case "podcity": {
                po.setPodCity(value);
            }
            break;
            case "podprovince": {
                po.setPodProvince(value);
            }
            break;
            case "podcountry": {
                po.setPodCountry(value);
            }
            break;
            case "podregion": {
                po.setPodRegion(value);
            }
            break;
            case "polinereference1": {
                po.setPoLineReference1(value);
            }
            break;
            case "ponumber": {
                po.setPoNumber(value);
            }
            break;
            case "modeoftransport": {
                po.setModeOfTransport(value);
            }
            break;
            case "originservicetype": {
                po.setOriginServiceType(value);
            }
            break;
        }
        return po;
    }


    public  String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;
        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }
            titleCase.append(c);
        }
        return titleCase.toString();
    }


}