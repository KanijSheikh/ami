truncate table simulate_load_plan;
DROP TABLE dbo.po_orders_op;
DROP TABLE dbo.load_plan_origin;
DROP TABLE dbo.load_plan_destination;
DROP TABLE dbo.container;
DROP TABLE dbo.load_plan;


INSERT INTO PO_ORDERS_op (Booking_number,Vendor,Expected_cargo_receipt_date,Expected_cargo_receipt_week,pod_city,pod_province,pod_country,Product_type_code,Product_type,Order_type,PO_number,PO_line,SKU_number,Booked_quantity,booked_cartons,booked_weight,booked_cbm,Shipto,po_status,so_number,region,destination,book_date,po_line_reference1,
load_port_city,load_port_country,expected_delivery_date,mode_of_transport,origin_service_type)
SELECT Booking_number,Vendor,Expected_cargo_receipt_date,Expected_cargo_receipt_week,'CPH',pod_province,pod_country,Product_type_code,Product_type,Order_type,PO_number,PO_line,SKU_number,Booked_quantity,booked_cartons,booked_weight,booked_cbm,Shipto,'not planned','SGN2972671','APAC','VNSGN','2017-08-22',po_line_reference1,'CPH',por_country,expected_delivery_date,'Sea','CFS' FROM PO_ORDERS;


{
  "ruleName": "allow_po2",
  "matchingCriteriaDto": [
  	{
      "criteriaName": "shipto",
      "criteriaValue": "0000346282"
      },
      {
      "criteriaName": "podCountry",
      "criteriaValue": "UNITED KINGDOM"
      }

  ],
  "poCoTree": {"ctString":"{\"id\":\"1\",\"conditionName\":\" ( expectedCargoReceiptDate + 5d ) > expectedDeliveryDate \", \"actionDto\": {\"id\":\"1\",\"actionName\":\"status\",\"actionType\":\"update\",\"actionValue\":\"exception\"},\"trueChild\":{},\"falseChild\":{\"id\":\"1\",\"conditionName\":\" ( expectedCargoReceiptDate + 5d ) <= expectedDeliveryDate \", \"actionDto\": {\"id\":\"1\",\"actionName\":\"status\",\"actionType\":\"update\",\"actionValue\":\"release\"}}}","collectionConditionTreeString":"{\"id\":\"1\",\"conditionName\": \" vol > 0 \",\"actionDto\": {\"id\":\"1\",\"actionName\": \"expectedDeliveryDate\",\"actionType\": \"split\",\"actionValue\": \"\"},\"trueChild\":{\"id\":\"1\",\"conditionName\": \" vol < 16 \",\"actionDto\": {\"id\":\"1\",\"actionName\": \"shipto\",\"actionType\": \"update\",\"actionValue\": \"DECON\"},\"trueChild\":{},\"falseChild\":{}}}"}
}


{
  "ruleName": "allow_po2",
  "matchingCriteriaDto": [
  	{
      "criteriaName": "shipto",
      "criteriaValue": "0000346282"
      },
      {
      "criteriaName": "podCountry",
      "criteriaValue": "UNITED KINGDOM"
      }

  ],
  "poCoTree": {"ctString":"{\"id\":\"1\",\"conditionName\":\" ( expectedCargoReceiptDate + 5d ) > expectedDeliveryDate \", \"actionDto\": {\"id\":\"1\",\"actionName\":\"status\",\"actionType\":\"update\",\"actionValue\":\"exception\"},\"trueChild\":{},\"falseChild\":{\"id\":\"1\",\"conditionName\":\" ( expectedCargoReceiptDate + 5d ) <= expectedDeliveryDate \", \"actionDto\": {\"id\":\"1\",\"actionName\":\"status\",\"actionType\":\"update\",\"actionValue\":\"release\"}}}","collectionConditionTreeString":"{\"id\":\"1\",\"conditionName\": \" vol > 0 \",\"actionDto\": {\"id\":\"1\",\"actionName\": \"expectedDeliveryDate\",\"actionType\": \"split\",\"actionValue\": \"\",\"actionRange\": \"3\"},\"trueChild\":{\"id\":\"1\",\"conditionName\": \" vol < 16 \",\"actionDto\": {\"id\":\"1\",\"actionName\": \"shipto\",\"actionType\": \"update\",\"actionValue\": \"DECON\"},\"trueChild\":{},\"falseChild\":{}}}"}
}

