IF OBJECT_ID('ami.dbo.update_load_plan_destination') IS NOT NULL
	BEGIN
		DROP PROCEDURE IF EXISTS save_load_plan
	END
GO

USE ami
GO

CREATE PROCEDURE update_load_plan_destination
    @id int,
		@destination varchar(max),
		@created_by varchar(20),
		@created_date datetime,
		@modified_by varchar(20),
		@modified_date datetime
AS

	-- destination loop start

	delete from ami.dbo.load_plan_destination where lp_id=@id

	Declare @counter_destination int =0;
	Declare @destination_value varchar(20);
	Declare @destination_type varchar(20);

	while len(@destination ) > 0
		begin

			if @counter_destination=0
				begin
					set @destination_type=(select left(@destination , charindex(',', @destination +',')-1))
					set @destination  = stuff(@destination, 1, charindex(',', @destination+','), '')
					set @counter_destination=@counter_destination+1;

				end
			if @counter_destination>0
				begin
					set @destination_value=(select left(@destination , charindex(',', @destination +',')-1))
					set @destination  = stuff(@destination, 1, charindex(',', @destination+','), '')
					set @counter_destination=@counter_destination+1;

					INSERT INTO
						ami.dbo.load_plan_destination(destination_value,type,lp_id,created_by,created_date,modified_by,modified_date) values
						(@destination_value,@destination_type,@id,@created_by,@created_date,@modified_by,@modified_date)

					INSERT INTO
						ami.dbo.test(po_id,container_id,lst_id,de_string) values (null,null,'',@destination_value)
				end
		end

	-- destination loop end
GO