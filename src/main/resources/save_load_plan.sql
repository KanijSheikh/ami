DROP TABLE IF EXISTS dbo.test
CREATE TABLE dbo.test (
  po_id        INT           NULL,
  container_id VARCHAR(20)           NULL,
  lst_id       INT           NULL,
  de_string    VARCHAR(8000) NULL,
  vol decimal NULL
)

IF OBJECT_ID('dbo.save_load_plan') IS NOT NULL
  BEGIN
    DROP PROCEDURE IF EXISTS save_load_plan
  END
GO

USE ami
GO

CREATE PROCEDURE save_load_plan
    @load_plan_name           VARCHAR(30),
    @load_plan_number         VARCHAR(30),
    @container_po_mapping_var VARCHAR(MAX),
    @origin                   VARCHAR(MAX),
    @destination              VARCHAR(MAX),
    @created_by               VARCHAR(20),
    @created_date             DATETIME,
    @modified_by              VARCHAR(20),
    @modified_date            DATETIME,
    @mode_of_transport        VARCHAR(20),
    @service_type             VARCHAR(20),
    @cut_off                  DATETIME,
    @load_plan_id_pr          INT,
    @load_plan_status         VARCHAR(20) OUTPUT
AS

--Declare @load_plan_name varchar(30)='LPNAME'
--Declare @load_plan_number varchar(30)='LP123'
--Declare @container_po_mapping_var varchar(2000)='201,2,3|204,5,6|'
  DECLARE @individual VARCHAR(MAX) = NULL
  DECLARE @po_id INT = NULL
  DECLARE @container_id VARCHAR(MAX) = NULL
  DECLARE @last_insert_id INT = NULL
  DECLARE @last_insert_id_container INT = NULL
  DECLARE @shipto VARCHAR(20) = NULL

  IF @load_plan_id_pr > 0
    BEGIN

    delete from dbo.load_plan_origin where lp_id=@load_plan_id_pr
    delete from dbo.load_plan_destination where lp_id=@load_plan_id_pr
    update dbo.po_orders_op set container_id=NULL where container_id in
    (select distinct container_id from dbo.container where lp_id=@load_plan_id_pr)
    delete from dbo.container where lp_id=@load_plan_id_pr
    delete from dbo.load_plan where load_plan_id=@load_plan_id_pr

  END

  INSERT INTO
    dbo.load_plan
    (created_by, created_date, load_plan_name, load_plan_number, modified_by, modified_date, mode_of_transport, service_type, cut_off_date)
  VALUES
    (@created_by, @created_date, @load_plan_name, @load_plan_number, @modified_by, @modified_date, @mode_of_transport,
     @service_type, @cut_off)

  SET @last_insert_id = (SELECT SCOPE_IDENTITY())

  --SET @load_plan_id = @last_insert_id

--   INSERT INTO
--     dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, @last_insert_id, 'lp')

  WHILE LEN(@container_po_mapping_var) > 0
    BEGIN IF PATINDEX('%|%', @container_po_mapping_var) > 0
      BEGIN
        SET @individual = SUBSTRING(@container_po_mapping_var, 0, PATINDEX('%|%', @container_po_mapping_var))

--         INSERT INTO
--           dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @individual)

        -- po loop

        DECLARE @po_var VARCHAR(MAX) = NULL;
        SET @po_var = @individual
        DECLARE @counter INT = 0;
        DECLARE @container_vol VARCHAR(20) = NULL;
        DECLARE @container_size VARCHAR(20) = NULL;

--         INSERT INTO
--           dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @po_var)

        DECLARE @po_vol float(8) = 0.0;

        WHILE len(@po_var) > 0
          BEGIN

            IF @counter = 0
              BEGIN
                SET @container_id = (SELECT left(@po_var, charindex(',', @po_var + ',') - 1))

                SET @container_vol =(SELECT SUBSTRING(SUBSTRING(@container_id, CHARINDEX('-', @container_id)+1, LEN(@container_id)),1,2))

                IF (@container_vol = '86')
                  SET @container_size = '45FT HC'
                IF (@container_vol = '76')
                  SET @container_size = '40FT HC'
                IF (@container_vol = '67')
                  SET @container_size = '40FT'
                IF (@container_vol = '33')
                  SET @container_size = '22FT'

                INSERT INTO dbo.container
                (carrier,status, container_number, container_size_type, created_by, created_date, destination, discharge_port, eta, etd, load_port, modified_by, modified_date, volume, lp_id)
                VALUES
                  (NULL,'In progress', @container_id, @container_size, @created_by, @created_date, NULL, NULL,
                             NULL, NULL, NULL, @modified_by,
                   @modified_date,0.0, @last_insert_id)

                SET @last_insert_id_container = (SELECT SCOPE_IDENTITY())

--                 INSERT INTO
--                   dbo.test (po_id, container_id, lst_id, de_string)
--                 VALUES (NULL, @container_id, @last_insert_id_container, 'con')

                SET @po_var = stuff(@po_var, 1, charindex(',', @po_var + ','), '')
                SET @counter = @counter + 1;
                SET @po_vol=0.0
              END
            IF @counter > 0
              BEGIN
                SET @po_id = (SELECT left(@po_var, charindex(',', @po_var + ',') - 1))

                SET @shipto = (SELECT shipto from dbo.po_orders_op WHERE po_id = @po_id)

                    UPDATE dbo.po_orders_op
                    SET container_id = @last_insert_id_container, modified_date = @modified_date, modified_by = @modified_by
                    WHERE po_id = @po_id
                    
                    SET @po_vol =@po_vol +(SELECT booked_cbm FROM dbo.po_orders_op WHERE po_id = @po_id)

                INSERT INTO
                  dbo.test (po_id, container_id, lst_id, de_string,vol)
                VALUES (@po_id, @container_id, @last_insert_id_container, 'po',@po_vol)

                SET @po_var = stuff(@po_var, 1, charindex(',', @po_var + ','), '')
                SET @counter = @counter + 1;
              END
              
              update dbo.container SET volume=@po_vol WHERE container_id = @last_insert_id_container;

          END
        -- po loop end
        SET @container_po_mapping_var = SUBSTRING(@container_po_mapping_var, LEN(@individual + '|') + 1,
                                                  LEN(@container_po_mapping_var)) END
    ELSE BEGIN
      SET @individual = @container_po_mapping_var
      SET @container_po_mapping_var = NULL
    END

  END
  -- container loop end

  -- origin loop start

  DECLARE @counter_origin INT = 0;
  DECLARE @origin_value VARCHAR(20);
  DECLARE @origin_type VARCHAR(20);

  WHILE len(@origin) > 0
    BEGIN

      IF @counter_origin = 0
        BEGIN
          SET @origin_type = (SELECT left(@origin, charindex(',', @origin + ',') - 1))
          SET @origin = stuff(@origin, 1, charindex(',', @origin + ','), '')
          SET @counter_origin = @counter_origin + 1;

--           INSERT INTO
--             dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @origin_type)

        END
      IF @counter_origin > 0
        BEGIN
          SET @origin_value = (SELECT left(@origin, charindex(',', @origin + ',') - 1))
          SET @origin = stuff(@origin, 1, charindex(',', @origin + ','), '')
          SET @counter_origin = @counter_origin + 1;

          INSERT INTO
            dbo.load_plan_origin (origin_value, type, lp_id, created_by, created_date, modified_by, modified_date)
          VALUES
            (@origin_value, @origin_type, @last_insert_id, @created_by, @created_date, @modified_by, @modified_date);
--           INSERT INTO
--             dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @origin_value)
        END
    END

  -- origin loop end

  -- destination loop start

  DECLARE @counter_destination INT = 0;
  DECLARE @destination_value VARCHAR(20);
  DECLARE @destination_type VARCHAR(20);

  WHILE len(@destination) > 0
    BEGIN

      IF @counter_destination = 0
        BEGIN
          SET @destination_type = (SELECT left(@destination, charindex(',', @destination + ',') - 1))
          SET @destination = stuff(@destination, 1, charindex(',', @destination + ','), '')
          SET @counter_destination = @counter_destination + 1;

--           INSERT INTO
--             dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @destination_type)
        END
      IF @counter_destination > 0
        BEGIN
          SET @destination_value = (SELECT left(@destination, charindex(',', @destination + ',') - 1))
          SET @destination = stuff(@destination, 1, charindex(',', @destination + ','), '')
          SET @counter_destination = @counter_destination + 1;

          INSERT INTO
            dbo.load_plan_destination (destination_value, type, lp_id, created_by, created_date, modified_by, modified_date)
          VALUES
            (@destination_value, @destination_type, @last_insert_id, @created_by, @created_date, @modified_by,
             @modified_date)

--           INSERT INTO
--             dbo.test (po_id, container_id, lst_id, de_string) VALUES (NULL, NULL, '', @destination_value)
        END
    END

  -- destination loop end


      Declare @load_plan_status_cnt int=null

      set @load_plan_status_cnt=(select count(1) from(select distinct po.po_status from ami.dbo.po_orders_op po join ami.dbo.container c on po.container_id=c.container_id join  ami.dbo.load_plan lp
      on c.lp_id = lp.load_plan_id where lp.load_plan_id=@last_insert_id and po.po_status='exception')a)

      --select @load_plan_status_cnt,@last_insert_id

      if @load_plan_status_cnt=0
      begin
      update ami.dbo.load_plan set status='optimized',modified_date = @modified_date, modified_by = @modified_by where load_plan_id=@last_insert_id
      set @load_plan_status='optimized'
      end

      if @load_plan_status_cnt>0
      begin
      update ami.dbo.load_plan set status='exception',modified_date = @modified_date, modified_by = @modified_by where load_plan_id=@last_insert_id
      set @load_plan_status='exception'
      end


--       UPDATE dbo.load_plan
--       SET status = @load_plan_status, modified_date = @modified_date, modified_by = @modified_by
--       WHERE load_plan_id = @last_insert_id

DELETE FROM container WHERE container_id NOT IN (SELECT DISTINCT container_id FROM po_orders_op WHERE container_id>0)

UPDATE po_orders_op SET shipment_status=NULL,po_status='not planned',assigned_to=NULL WHERE container_id IS NULL and shipment_status IS NOT NULL and po_status IS NOT NULL

GO